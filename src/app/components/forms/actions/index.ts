import manappApi from '../../../../common/apis/manapp';

import {
  FORM_RESET,
  PICTURE_UPLOAD_FORM_SUBMITTED,
  PICTURE_UPLOAD_FORM_SUCCEED,
  PICTURE_UPLOAD_FORM_FAILED,
} from '../actionTypes';

import { UPLOAD } from '../../../../common/constants/apiRoutes';
import { FormActionCreator } from '../types';

export const resetForm: FormActionCreator = payload => dispatch => {
  const { form } = payload;

  dispatch({
    type: FORM_RESET,
    payload: {
      form,
    },
  });
};

export const uploadPicture: FormActionCreator = ({ id, file, form, onUploadProgress, resource }) => dispatch => {
  dispatch({
    type: PICTURE_UPLOAD_FORM_SUBMITTED,
    payload: {
      form,
      resource,
    },
  });

  const fileType = 'picture';
  const url = UPLOAD.replace(':type', fileType)
    .replace(':resource', resource)
    .replace(':id', id);

  const formData = new FormData();
  formData.append('name', fileType);
  formData.append(fileType, file);

  return manappApi.uploader
    .upload({ url, formData, onUploadProgress })
    .then(({ picture, resource }) => {
      const payload = {
        form,
        picture,
        resource,
      };

      dispatch({
        type: PICTURE_UPLOAD_FORM_SUCCEED,
        payload,
      });

      return payload;
    })
    .catch(err => {
      dispatch({
        type: PICTURE_UPLOAD_FORM_FAILED,
        payload: {
          form,
          ...err,
        },
      });

      return err;
    });
};
