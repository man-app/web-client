import styled from 'styled-components';

import { ErrorMessage as DefaultErrorMessage } from '../styles';

import getColor from '../../../../../common/constants/styles/theme';
import { ThemedComponent } from '../../../../types/styled-components';

const PICTURE_SIZE = 200;

export const Preview = styled.div`
  background-color: ${({ theme }: ThemedComponent) => getColor('AVATAR_BACKGROUND', theme.active)};
  border-radius: 4px;
  border: 1px solid ${({ theme }: ThemedComponent) => getColor('PICTURE_WRAPPER_BORDER', theme.active)};
  color: inherit;
  display: block;
  height: ${PICTURE_SIZE}px;
  margin: 0 auto;
  padding: 10px;
  transition: background-color 0.2s, border: 0.2s;
  width: ${PICTURE_SIZE}px;

  input:disabled ~ & {
    border-color: ${({ theme }: ThemedComponent) => getColor('DISABLED_BORDER', theme.active)};
    color: ${({ theme }: ThemedComponent) => getColor('DISABLED_COLOR', theme.active)};
  }
`;
Preview.displayName = 'Preview';

export const NoPicture = styled.span`
  color: inherit;
  display: inline-block;
  margin: 0 auto;
  margin-top: 50%;
  text-align: center;
  transform: translateY(-50%);
  vertical-align: middle;
  width: 100%;
`;
NoPicture.displayName = 'NoPicture';

export const PictureWrapper = styled.div`
  background-color: ${({ theme }: ThemedComponent) => getColor('AVATAR_BACKGROUND', theme.active)};
  border-radius: 50%;
  height: 100%;
  overflow: hidden;
  text-align: center;
  transition: background-color 0.2s;
  width: 100%;
`;
PictureWrapper.displayName = 'PictureWrapper';

export const Picture = styled.img`
  display: inline-block;
  margin: 0 auto;
  margin-top: 50%;
  max-width: 100%;
  transform: translateY(-50%);
  vertical-align: middle;
`;
Picture.displayName = 'Picture';

export const PictureSpacer = styled.div`
  display: block;
  margin-bottom: 10px;
`;
PictureSpacer.displayName = 'PictureSpacer';

export const ErrorMessage = styled(DefaultErrorMessage)`
  margin: 0 auto;
  width: ${PICTURE_SIZE}px;
`;
ErrorMessage.displayName = 'ErrorMessage';
