import React from 'react';
import { useDispatch } from 'react-redux';
import { warn } from '../../../../../common/utils/logger';
import {
  isValidPictureExtension,
  isLargerThanMaxPictureWeight,
  isSmallerThanMinPictureWeight,
  isSmallerThanMinPictureSize,
} from '../../../../utils/error-handler';

import { uploadPicture } from '../../../../components/forms/actions';

import { Buttons } from '../../../touchable';

import { ButtonProps } from '../../../touchable/types';
import { Resource, FieldProps } from '../../types';
import { Wrapper, HiddenInput, Label } from '../styles';
import { NoPicture, PictureWrapper, Picture as StyledPicture, Preview, PictureSpacer, ErrorMessage } from './styles';

interface Options extends FieldProps {
  resource?: Resource;
  onUpload?: (result: string, response: any) => void;
}

interface Props {
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  onClick: (event: React.MouseEvent<HTMLInputElement>) => void;
  onFocus: (event: React.FocusEvent<HTMLInputElement>) => void;
  options: Options;
}

const Picture: React.FC<Props> = ({ onChange, onClick, onFocus, options }: Props) => {
  const [isLoading, setIsFetching] = React.useState(false);
  const [uploadProgress, setUploadProgress] = React.useState(0);
  const [imagePreviewUrl, setImagePreviewUrl] = React.useState('');

  const dispatch = useDispatch();

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    event.preventDefault();
    const { onUpload } = options;

    const reader = new FileReader();
    const file = event.target.files && event.target.files[0];

    if (file) {
      reader.onloadend = () => {
        // Validate file extension
        if (!isValidPictureExtension(file)) {
          // TODO: Show custom form error
          return warn('Trying to upload a file with wrong extension');
        }

        // Validate file weight
        if (isLargerThanMaxPictureWeight(file)) {
          // TODO: Show custom form error
          return warn('Trying to upload a file too big');
        }

        if (isSmallerThanMinPictureWeight(file)) {
          // TODO: Show custom form error
          return warn('Trying to upload a file too small');
        }

        // Validate image resolution
        const uploadedImage = new Image();

        // Start validating image
        const imageValidation = new Promise((resolve, reject) => {
          uploadedImage.onload = () => {
            // Wrong sizes (reject)
            if (isSmallerThanMinPictureSize(uploadedImage)) {
              // TODO: Show custom form error
              return reject(() => {
                warn('Trying to upload a file without enough pixels');
              });
            }

            // Success validation
            resolve();
          };
          uploadedImage.src = reader.result ? reader.result.toString() : '';

          setImagePreviewUrl(reader.result ? reader.result.toString() : '');
        });

        // When image has been validated
        imageValidation.then(
          // Success
          () => {
            // TODO: Clean previous form errors
            const imagePreviewUrl = reader.result ? reader.result.toString() : '';

            // Start uploading image
            setIsFetching(true);

            // Update upload progress
            const onUploadProgress = (event: ProgressEvent) => {
              const { loaded, total } = event;
              const percentage = (loaded / total) * 100;
              const uploadProgress = parseInt(percentage.toString(), 10);

              setUploadProgress(uploadProgress);
            };

            const data = {
              file,
              form: options.form,
              resource: options.resource && options.resource.type,
              id: options.resource && options.resource.id,
              onUploadProgress,
            };

            // Call a service for uploading the picture
            dispatch(uploadPicture(data))
              // @ts-ignore
              .then((response: any) => {
                // Upload finish
                setIsFetching(false);
                setUploadProgress(0);
                setImagePreviewUrl(imagePreviewUrl);

                if (typeof onUpload === 'function') {
                  onUpload(imagePreviewUrl, response.data);
                }

                onChange(event);
              })
              .catch(() => {
                // Upload error
                setIsFetching(false);

                // TODO: Map custom backend error
              });
          },
          // Error
          () => {
            setIsFetching(false);

            // TODO: Map custom error
            return warn('Invalid image');
          }
        );
      };

      reader.readAsDataURL(file);
    }
  };

  const { form, ...cleanOptions } = options;

  const buttons: ButtonProps[] = [
    {
      ...cleanOptions,
      icon: options.icon,
      isDisabled: isLoading || options.isDisabled,
      label: isLoading ? `${uploadProgress}%` : options.label,
      onClick: undefined,
    },
  ] as ButtonProps[];

  return (
    <Wrapper>
      <Label>
        <HiddenInput
          id={options.id}
          name={options.id}
          disabled={options.isDisabled}
          onClick={onClick}
          onChange={handleChange}
          onFocus={onFocus}
          required={options.isRequired}
          type="file"
        />

        <Preview>
          {!imagePreviewUrl && !options.model && <NoPicture>No picture uploaded</NoPicture>}

          {(imagePreviewUrl || options.model) && (
            <PictureWrapper>
              <StyledPicture src={imagePreviewUrl || options.model || ''} />
            </PictureWrapper>
          )}
        </Preview>

        {options.error && <ErrorMessage>{options.error}</ErrorMessage>}

        <PictureSpacer />

        <Buttons options={buttons} align="center" />
      </Label>
    </Wrapper>
  );
};

export default Picture;
