import { SmartFormPicture } from '../types';
import { RootState } from '../../../types';

export const getFormState = ({ form }: RootState) => (formName: string): string =>
  form[formName] ? form[formName].state : 'pristine';

export const getFormErrors = ({ form }: RootState) => (formName: string): any[] =>
  form[formName] ? form[formName].errors : [];

export const getFormPicture = ({ form }: RootState) => (formName: string): SmartFormPicture | undefined =>
  form[formName] ? form[formName].picture : undefined;
