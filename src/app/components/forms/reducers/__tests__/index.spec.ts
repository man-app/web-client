import reducer from '..';

import {
  ACCOUNT_FORM_SUBMITTED,
  ACCOUNT_FORM_SUCCEED,
  ACCOUNT_FORM_FAILED,
} from '../../../../modules/account/actionTypes';

import { LOGOUT_SUCCEED } from '../../../../modules/auth/actionTypes';
import { FORM_RESET } from '../../actionTypes';
import { getState } from '../../models';

describe('Form reducer', () => {
  const initialState = getState();

  describe('Any form submission', () => {
    describe('Form has been submitted', () => {
      it('should return the form with submitted status', () => {
        const state = {
          ...initialState,
          account: {
            ...initialState.account,
            state: 'failed',
            errors: [{ channel: 'form', reason: 'email' }],
          },
        };

        const nextState = reducer(state, { type: ACCOUNT_FORM_SUBMITTED, payload: { form: 'account' } });

        const expectedState = {
          ...initialState,
          account: {
            ...initialState.account,
            state: 'submitted',
          },
        };

        expect(nextState).toStrictEqual(expectedState);
      });
    });

    describe('Form has been succeed on server', () => {
      it('should return the form with succeed status', () => {
        const state = {
          ...initialState,
          account: {
            ...initialState.account,
            state: 'submitted',
            errors: [{ channel: 'form', reason: 'email' }],
          },
        };

        const nextState = reducer(state, { type: ACCOUNT_FORM_SUCCEED, payload: { form: 'account' } });

        const expectedState = { ...initialState, account: { ...initialState.account, state: 'succeed' } };

        expect(nextState).toStrictEqual(expectedState);
      });
    });

    describe('Form has failed on server', () => {
      it('should return the form with failed status', () => {
        const state = {
          ...initialState,
          account: {
            ...initialState.account,
            state: 'submitted',
            errors: [],
          },
        };

        const nextState = reducer(state, {
          type: ACCOUNT_FORM_FAILED,
          payload: { form: 'account', errors: [{ channel: 'form', reason: 'email' }] },
        });

        const expectedState = {
          ...initialState,
          account: {
            ...initialState.account,
            state: 'failed',
            errors: [{ channel: 'form', reason: 'email' }],
          },
        };

        expect(nextState).toStrictEqual(expectedState);
      });
    });

    describe('Form has been reseted', () => {
      it('should return initial state for account form', () => {
        const state = {
          ...initialState,
          account: {
            state: 'failed',
            errors: [{ channel: 'form', reason: 'email' }],
          },
        };

        const nextState = reducer(state, { type: FORM_RESET, payload: { form: 'account' } });

        expect(nextState).toStrictEqual(initialState);
      });
    });
  });

  describe('Logout action', () => {
    it('should return initial state', () => {
      const nextState = reducer(initialState, { type: LOGOUT_SUCCEED });

      expect(nextState).toStrictEqual(initialState);
    });
  });
});
