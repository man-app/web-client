import { FormActionType } from '../types';

export const FORM_RESET: FormActionType = 'FORM_RESET';
export const PICTURE_UPLOAD_FORM_SUBMITTED: FormActionType = 'PICTURE_UPLOAD_FORM_SUBMITTED';
export const PICTURE_UPLOAD_FORM_SUCCEED: FormActionType = 'PICTURE_UPLOAD_FORM_SUCCEED';
export const PICTURE_UPLOAD_FORM_FAILED: FormActionType = 'PICTURE_UPLOAD_FORM_FAILED';
