import React from 'react';
import styled from 'styled-components';

import getColor, { hexToRGB } from '../../../../../common/constants/styles/theme';

import { useThemeContext } from '../../../../contexts/theme';

interface WrapperProps {
  background: {
    r: number;
    g: number;
    b: number;
  };
}

const SkeletonWrapper = styled.div`
  position: relative;
  overflow: hidden;

  &:after {
    @keyframes loading {
      from: {
        left: -100%;
      }
      to {
        left: 100%;
      }
    }

    content: ' ';
    animation: loading 1s infinite linear;
    background-image: ${({ background }: WrapperProps) => `linear-gradient(90deg,
      rgba(${background.r}, ${background.g}, ${background.b}, 0) 0,
      rgba(${background.r}, ${background.g}, ${background.b}, .8) 40%,
      rgba(${background.r}, ${background.g}, ${background.b}, .8) 60%,
      rgba(${background.r}, ${background.g}, ${background.b}, 0) 100%
    )`};
    background-position: 0 0;
    height: 100%;
    left: -100%;
    position: absolute;
    top: 0;
    width: 100%;
  }
`;

interface Props {
  background?: string;
  children: React.ReactNode;
}

const Skeleton: React.FC<Props> = ({ background, children }: Props) => {
  const { active } = useThemeContext();
  const defaultColor = getColor('GLOBAL_BACKGROUND', active);

  return <SkeletonWrapper background={hexToRGB(background || defaultColor)}>{children}</SkeletonWrapper>;
};

export default Skeleton;
