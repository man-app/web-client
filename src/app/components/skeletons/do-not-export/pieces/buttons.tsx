import React from 'react';
import styled from 'styled-components';

import { Buttons as DefaultButtons } from '../../../touchable';

import getColor from '../../../../../common/constants/styles/theme';

import { ThemedComponent } from '../../../../types/styled-components';

const StyledButtons = styled.div`
  a,
  button,
  span {
    &,
    &:active,
    &:disabled,
    &:hover,
    &:visited {
      background: ${({ theme }: ThemedComponent) => getColor('SKELETON_BACKGROUND', theme.active)};
      border-color: ${({ theme }: ThemedComponent) => getColor('SKELETON_BACKGROUND', theme.active)};
      color: ${({ theme }: ThemedComponent) => getColor('SKELETON_BACKGROUND', theme.active)};
      cursor: default;
      min-width: 100px;
    }
  }
`;

interface Props {
  align?: 'left' | 'center' | 'right';
  count?: number;
}

const Buttons: React.FC<Props> = ({ align = 'right', count = 1 }: Props) => {
  const buttons = [];

  for (let x = 1; x <= count; x += 1) {
    buttons.push({
      id: `skeleton-btn-${x}`,
      label: '...',

      type: 'button' as 'button',
    });
  }

  return (
    <StyledButtons>
      <DefaultButtons align={align} options={buttons} />
    </StyledButtons>
  );
};

export default Buttons;
