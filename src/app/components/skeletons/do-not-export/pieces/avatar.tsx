import React from 'react';
import styled from 'styled-components';

import Avatar from '../../../avatar';

import getColor from '../../../../../common/constants/styles/theme';
import { ThemedComponent } from '../../../../types/styled-components';
import { SizeName } from '../../../../../common/types/sizes';

const AvatarWrapper = styled.div`
  display: inline-block;

  & > span {
    background-color: ${({ theme }: ThemedComponent) => getColor('SKELETON_BACKGROUND', theme.active)};

    & .fa {
      color: ${({ theme }: ThemedComponent) => getColor('SKELETON_BACKGROUND', theme.active)};
    }
  }
`;
AvatarWrapper.displayName = 'AvatarWrapper';

interface Props {
  size?: SizeName;
  isExpandable?: boolean;
}

const AvatarSkeleton: React.FC<Props> = (props: Props) => (
  <AvatarWrapper>
    <Avatar {...props} profile={{ fullName: '' }} />
  </AvatarWrapper>
);

export default AvatarSkeleton;
