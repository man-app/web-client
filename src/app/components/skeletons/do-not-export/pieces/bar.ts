import styled from 'styled-components';

import getColor from '../../../../../common/constants/styles/theme';

import { ThemedComponent } from '../../../../types/styled-components';

interface BarWrapperProps {
  align?: 'left' | 'center' | 'right';
}

export const BarWrapper = styled.div`
  margin-bottom: 10px;
  text-align: ${({ align = 'left' }: BarWrapperProps) => align};
  width: 100%;
`;

interface BarProps extends ThemedComponent {
  width?: string;
}
const BarSkeleton = styled.div`
  align-items: center;
  display: flex;
  height: 24px;

  :after {
    background-color: ${({ theme }: BarProps) => getColor('SKELETON_BACKGROUND', theme.active)};
    border-radius: 4px;
    content: ' ';
    height: 8px;
    // margin: 0 auto;
    width: ${({ width = '100%' }: BarProps) => width};
  }
`;
BarSkeleton.displayName = 'BarSkeleton';

export default BarSkeleton;
