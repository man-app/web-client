import React from 'react';
import { createPortal } from 'react-dom';

import { Backdrop, Wrapper, LockBodyScreen } from './styles';

export { Header, Body, Footer } from './styles';

interface Props {
  children: React.ReactChild;
  handleClose: () => void;
  isOpen: boolean;
}

const Modal: React.FC<Props> = ({ children, handleClose, isOpen }: Props) => {
  const portal = document.getElementById('modal-root');

  return isOpen && portal
    ? createPortal(
        <>
          <LockBodyScreen />
          <Backdrop onClick={handleClose} />
          <Wrapper>{children}</Wrapper>
        </>,
        portal
      )
    : null;
};

export default Modal;
