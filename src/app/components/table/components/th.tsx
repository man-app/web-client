import React from 'react';
import { Th as THeading } from '../styles';

interface Props {
  align?: 'left' | 'center' | 'right';
  bold?: boolean;
  children?: any;
  color?: string;
  singleLine?: boolean;
}

const Th = ({ align, children, singleLine, ...props }: Props) => {
  const verticalAlign = !singleLine ? 'top' : align !== 'left' ? 'bottom' : 'top';

  return (
    <THeading align={align} singleLine={singleLine} verticalAlign={verticalAlign} {...props}>
      {children}
    </THeading>
  );
};

export default Th;
