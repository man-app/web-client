import styled from 'styled-components';

import { Card } from '../card';

import getColor from '../../../common/constants/styles/theme';
import { ThemedComponent } from '../../types/styled-components';
import { LANDSCAPE_OR_TABLET, DESKTOP, DESKTOP_L } from '../../../common/constants/styles/media-queries';

export const Wrapper = styled(Card)`
  box-shadow: 0 0 15px -5px ${({ theme }: ThemedComponent) => getColor('PRICING_SHADOW', theme.active)};
  color: ${({ theme }: ThemedComponent) => getColor('PRICING_COLOR', theme.active)};
  display: inline-block;
  font-size: 20px;
  font-weight: 300;
  margin: 15px 5px;
  padding: 10px;
  position: relative;
  text-align: center;
  transition: background-color 0.2s, color 0.2s;
  vertical-align: top;

  ${LANDSCAPE_OR_TABLET} {
    margin: 15px 5px;
    width: calc(50% - 10px);
  }

  ${DESKTOP} {
    width: calc(33% - 10px);
  }

  ${DESKTOP_L} {
    width: calc(25% - 10px);
  }
`;
Wrapper.displayName = 'Wrapper';

export const Title = styled.h4`
  display: block;
  font-size: 28px;
  margin: 20px auto;
  width: 100%;
`;
Title.displayName = 'Title';

export const Icon = styled.span`
  display: block;
  color: ${({ theme }: ThemedComponent) => getColor('PRICING_ICON_COLOR', theme.active)};
  font-size: 48px;
  margin-bottom: 40px;
  transition: color 0.2s;
  width: 100%;
`;
Icon.displayName = 'Icon';

export const Discount = styled.div`
  position: absolute;
  right: 20px;
  top: 80px;
`;
Discount.displayName = 'Discount';

export const DiscountIcon = styled.span`
  color: ${({ theme }: ThemedComponent) => getColor('PRICING_COLOR_DISCOUNT', theme.active)};
  font-size: 80px;
  position: absolute;
  right: 0;
  top: 0;
  transition: color 0.2s;
`;
DiscountIcon.displayName = 'DiscountIcon';

export const DiscountLabel = styled.span`
  color: #fff;
  font-size: 20px;
  font-weight: bold;
  position: absolute;
  right: 13px;
  top: 27px;
  transform: rotate(-25deg);
`;
DiscountLabel.displayName = 'DiscountLabel';

export const Users = styled.span`
  display: block;
  font-size: inherit;
  font-weight: inherit;
  width: 100%;
`;
Users.displayName = 'Users';

export const Price = styled.div``;
Price.displayName = 'Price';

export const SavingsLabel = styled.span`
  display: block;
  font-size: 14px;
  font-weight: inherit;
  width: 100%;
`;
SavingsLabel.displayName = 'SavingsLabel';

interface OriginalPriceProps extends ThemedComponent {
  hasDiscount?: boolean;
}
export const OriginalPrice = styled.span`
  color: ${({ hasDiscount, theme }: OriginalPriceProps) =>
    hasDiscount ? getColor('PRICING_COLOR_ORIGINAL', theme.active) : undefined};
  display: block;
  font-weight: inherit;
  font-size: ${({ hasDiscount }: OriginalPriceProps) => (hasDiscount ? '14px' : 'inherit')};
  text-decoration: ${({ hasDiscount }: OriginalPriceProps) => (hasDiscount ? 'line-through' : undefined)};
  transition: color 0.2s;
  width: 100%;
`;
OriginalPrice.displayName = 'OriginalPrice';

export const FinalPrice = styled.span`
  display: block;
  font-size: inherit;
  font-weight: inherit;
  width: 100%;
`;
FinalPrice.displayName = 'FinalPrice';

export const Savings = styled.div``;
Savings.displayName = 'Savings';

export const SavingsValue = styled.span`
  color: ${({ theme }: ThemedComponent) => getColor('SAVINGS_COLOR', theme.active)};
  display: block;
  font-size: 26px;
  font-weight: inherit;
  transition: color 0.2s;
  width: 100%;
`;
SavingsValue.displayName = 'SavingsValue';

export const ButtonsWrapper = styled.div``;
ButtonsWrapper.displayName = 'ButtonsWrapper';
