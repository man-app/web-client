import React from 'react';
import { currency } from '../../../common/utils/numbers';

import { BUTTON } from '../forms/models';
import Spacer from '../spacer';
import { Buttons } from '../touchable';
import {
  Wrapper,
  Title,
  Icon,
  Discount,
  DiscountIcon,
  DiscountLabel,
  Users,
  Price,
  OriginalPrice,
  FinalPrice,
  Savings,
  SavingsValue,
  SavingsLabel,
  ButtonsWrapper,
} from './styles';

import { ButtonProps } from '../touchable/types';
import { Subscription } from '../../modules/subscriptions/types';
import { payments } from '../../../common/constants/features';

interface Props {
  canPurchase?: boolean;
  onClick?: (id: string) => void;
  subscription: Subscription;
}

const SubscriptionCard = ({ canPurchase = false, onClick, subscription }: Props) => {
  const buttons: ButtonProps[] =
    onClick && subscription.price > 0
      ? [
          {
            buttonType: 'primary',
            icon: 'shopping-cart',
            id: `add-to-cart-${subscription.id}`,
            isAlwaysDisabled: !canPurchase,
            label: 'Upgrade',
            onClick: () => {
              onClick(subscription.id);
            },
            type: BUTTON,
          },
        ]
      : [];

  return (
    <Wrapper>
      <Title>{subscription.name}</Title>

      <Icon>{!!subscription.icon && <i className={`fa fa-${subscription.icon}`} />}</Icon>
      <Spacer />

      <Discount>
        <DiscountIcon>{!!subscription.discount && <i className="fa fa-certificate" />}</DiscountIcon>
        <DiscountLabel>{!!subscription.discount && `${subscription.discount}%`}</DiscountLabel>
      </Discount>

      <Users>
        {subscription.users} user
        {subscription.users !== 1 && <span>s</span>}
      </Users>
      <Spacer />

      <Price>
        <SavingsLabel>Price</SavingsLabel>

        {!!subscription.discount && (
          <OriginalPrice hasDiscount={!!subscription.discount}>{currency(subscription.price)} per month</OriginalPrice>
        )}

        <FinalPrice>
          {currency(
            !!subscription.discount
              ? subscription.price - (subscription.price * subscription.discount) / 100
              : subscription.price
          )}
        </FinalPrice>
      </Price>

      {!!subscription.discount && (
        <>
          <Spacer />
          <Savings>
            <SavingsLabel>You save</SavingsLabel>
            <SavingsValue>{currency((subscription.price * subscription.discount) / 100)} per month</SavingsValue>
          </Savings>
        </>
      )}

      {!!buttons.length && payments && (
        <>
          <Spacer />
          <ButtonsWrapper>
            <Buttons options={buttons} align="center" />
          </ButtonsWrapper>
        </>
      )}
    </Wrapper>
  );
};

export default SubscriptionCard;
