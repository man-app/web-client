import React from 'react';
import { shallow } from 'enzyme';

import SubscriptionCard from '..';

import { Subscription } from '../../../modules/subscriptions/types';

const SUBSCRIPTIONS: Subscription[] = [
  {
    id: '1',
    name: 'Freelance',
    price: 0,
    users: 2,
  },
  {
    id: '2',
    name: 'Start up',
    icon: 'star-o',
    price: 5 * 10,
    users: 10,
    discount: 10,
  },
  {
    id: '3',
    name: 'Bussiness',
    icon: 'briefcase',
    price: 5 * 25,
    users: 25,
    discount: 15,
  },
  {
    id: '4',
    name: 'Large Bussiness',
    icon: 'briefcase',
    price: 5 * 50,
    users: 50,
    discount: 20,
  },
  {
    id: '5',
    name: 'Corporate',
    icon: 'globe',
    price: 5 * 100,
    users: 100,
    discount: 25,
  },
];

const bonifiedPlan = SUBSCRIPTIONS.find(plan => plan.id === '4') as Subscription;
const regularPlan = SUBSCRIPTIONS.find(plan => plan.id === '5') as Subscription;

describe('PricingCard', () => {
  it('Should render a valid card with permissions', () => {
    const component = shallow(<SubscriptionCard canPurchase={true} subscription={bonifiedPlan} onClick={() => {}} />);

    expect(component).toMatchSnapshot();
  });

  it('Should render a valid card with permissions without discount', () => {
    const component = shallow(<SubscriptionCard canPurchase={true} subscription={regularPlan} onClick={() => {}} />);

    expect(component).toMatchSnapshot();
  });

  it('Should render a valid card without permissions', () => {
    const component = shallow(<SubscriptionCard canPurchase={false} subscription={bonifiedPlan} onClick={() => {}} />);

    expect(component).toMatchSnapshot();
  });

  it('Should render a valid card without permissions without discount', () => {
    const component = shallow(<SubscriptionCard canPurchase={false} subscription={regularPlan} onClick={() => {}} />);

    expect(component).toMatchSnapshot();
  });
});
