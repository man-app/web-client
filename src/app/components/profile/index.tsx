import React from 'react';
import { Redirect } from 'react-router-dom';
import { formatDate } from '../../utils/ui';
import { isEmployee } from '../../utils/permissions';

import { BUTTON } from '../forms/models';
import Avatar from '../avatar';
import { Buttons, Link } from '../touchable';
import Spacer from '../spacer';
import {
  Wrapper,
  Title,
  Content,
  LinksWrapper,
  LinkWrapper,
  Info,
  Property,
  ListItemLabel,
  Picture,
  EditButton,
  SpacerWrapper,
} from './styles';

import { CONFIG_ICON, CONFIG_LABEL } from '../../../common/constants/buttons';
import { ACCOUNT, COMPANY_SETTINGS } from '../../../common/constants/appRoutes';
import { UserLogged, UserProfile } from '../../modules/auth/types';
import { ButtonProps, LinkProps } from '../touchable/types';
import { Company } from '../../modules/companies/types';

interface Props {
  profile: UserProfile | Company;
  loggedUser?: UserLogged;
}

const Profile = ({ loggedUser, profile }: Props) => {
  const [redirectTo, setRedirectTo] = React.useState('');

  const getSocialLinks = () => {
    // Social links
    const socialLinks: LinkProps[] = [];

    if (profile.linkedin) {
      socialLinks.push({
        id: 'linkedin-link',
        icon: 'linkedin',
        className: 'is-linkedin',
        to: profile.linkedin,
        isExternal: true,
      });
    }

    if (profile.facebook) {
      socialLinks.push({
        id: 'facebook-link',
        icon: 'facebook',
        className: 'is-facebook',
        to: profile.facebook,
        isExternal: true,
      });
    }

    if (profile.twitter) {
      socialLinks.push({
        id: 'twitter-link',
        icon: 'twitter',
        className: 'is-twitter',
        to: `https://www.twitter.com/${profile.twitter.replace('@', '')}`,
        isExternal: true,
      });
    }

    if (profile.instagram) {
      socialLinks.push({
        id: 'instagram-link',
        icon: 'instagram',
        className: 'is-instagram',
        to: profile.instagram,
        isExternal: true,
      });
    }

    if (profile.youtube) {
      socialLinks.push({
        id: 'youtube-link',
        icon: 'youtube',
        className: 'is-youtube',
        to: profile.youtube,
        isExternal: true,
      });
    }

    return socialLinks;
  };

  const getSettingsButtons = (): ButtonProps[] => {
    if (
      // Valid profile and logged user
      profile &&
      loggedUser &&
      // Is the profile of the logged user
      ((profile.fullName && profile.id === loggedUser.id) ||
        // Is the profile of an employing company
        (profile.name && isEmployee(loggedUser, profile.id)))
    ) {
      return [
        {
          buttonType: 'primary',
          icon: CONFIG_ICON,
          id: 'edit',
          label: CONFIG_LABEL,
          onClick: () => {
            if (profile && profile.slug) {
              setRedirectTo(COMPANY_SETTINGS.replace(':slug', profile.slug).replace(':tab?', ''));
            } else {
              setRedirectTo(ACCOUNT.replace('/:tab?/:id?', ''));
            }
          },
          type: BUTTON,
        },
      ];
    }

    return [];
  };

  const socialLinks = getSocialLinks();
  const settingsButtons = getSettingsButtons();

  if (redirectTo) {
    return <Redirect to={{ pathname: redirectTo }} />;
  }

  return (
    <Wrapper>
      <Title>{profile.name || profile.fullName}</Title>

      <Picture>
        <Avatar
          size="lg"
          isExpandable
          profile={{
            picture: profile.picture || '',
            name: profile.name,
            fullName: profile.fullName,
          }}
        />
      </Picture>

      <Content>
        {settingsButtons && (
          <EditButton>
            <Buttons options={settingsButtons} />
          </EditButton>
        )}

        <LinksWrapper>
          {socialLinks.map((link, index) => (
            <LinkWrapper key={index}>
              <Link options={link} />
            </LinkWrapper>
          ))}
        </LinksWrapper>

        {(profile.website || profile.contactEmail || profile.contactPhone) && (
          <>
            <SpacerWrapper>
              <Spacer />
            </SpacerWrapper>

            <Info>
              {profile.website && (
                <Property>
                  <ListItemLabel>Website:</ListItemLabel>
                  <Link
                    options={{
                      id: 'website-link',
                      to: `${profile.website}`,
                      label: profile.website,
                      isExternal: true,
                    }}
                  />
                </Property>
              )}
              {profile.contactEmail && (
                <Property>
                  <ListItemLabel>Email:</ListItemLabel>
                  <Link
                    options={{
                      id: 'email-link',
                      to: `mailto:${profile.contactEmail}`,
                      label: profile.contactEmail,
                      isExternal: true,
                    }}
                  />
                </Property>
              )}
              {profile.contactPhone && (
                <Property>
                  <ListItemLabel>Phone:</ListItemLabel>
                  <Link
                    options={{
                      id: 'phone-call-link',
                      to: `tel:${profile.contactPhone}`,
                      label: profile.contactPhone,
                      isExternal: true,
                    }}
                  />
                </Property>
              )}
            </Info>
          </>
        )}

        {(profile.address || profile.city || profile.state || profile.country) && (
          <>
            <SpacerWrapper>
              <Spacer />
            </SpacerWrapper>

            <Info>
              {profile.address && (
                <Property>
                  <ListItemLabel>Address:</ListItemLabel>
                  {profile.address}
                </Property>
              )}
              {profile.city && (
                <Property>
                  <ListItemLabel>City:</ListItemLabel>
                  {profile.city}
                </Property>
              )}
              {profile.state && (
                <Property>
                  <ListItemLabel>State:</ListItemLabel>
                  {profile.state}
                </Property>
              )}
              {profile.country && (
                <Property>
                  <ListItemLabel>Country:</ListItemLabel>
                  {profile.country}
                </Property>
              )}
            </Info>
          </>
        )}

        <SpacerWrapper>
          <Spacer />
        </SpacerWrapper>

        <Info>
          <Property>
            <ListItemLabel>Joined:</ListItemLabel>
            {formatDate(profile.created)}
          </Property>
          <Property>
            <ListItemLabel>Last modified:</ListItemLabel>
            {formatDate(profile.lastModified)}
          </Property>
          {profile.users && (
            <Property>
              <ListItemLabel>Users:</ListItemLabel>
              {profile.users}
            </Property>
          )}
        </Info>
      </Content>
    </Wrapper>
  );
};

export default Profile;
