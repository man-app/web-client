import React from 'react';
import { shallow } from 'enzyme';

import Profile from '..';

const loggedUser = {
  id: '2',
  fullName: 'User test',
  email: 'test@user.com',
  jobs: [{ idCompany: '1', permissions: {} }],
  created: 123456789,
  lastModified: 123456789,
};

const company = {
  id: '1',
  name: 'My company',
  slug: 'my-company',
  created: 123456789,
  lastModified: 123456789,
};

const user = {
  ...loggedUser,
  fullName: 'User test',
  email: 'test@user.com',
  joined: new Date().getTime(),
};

const avatar = {
  picture: 'user-picture',
};

const social = {
  instagram: 'my-instagram',
  facebook: 'my-facebook',
  twitter: 'my-twitter',
  linkedin: 'my-linkedin',
  youtube: 'my-youtube',
};

const contact = {
  website: 'www.my-website.com',
  contactEmail: 'my@email.com',
  contactPhone: '111 11 11 11',
};

const personal = {
  city: 'city',
  state: 'state',
  country: 'country,',
};

describe('Profile', () => {
  it('should render picture and user name', () => {
    const component = shallow(<Profile profile={{ ...user, ...avatar }} />);
    expect(component).toMatchSnapshot();
  });

  it('should render picture placeholder, user name and contact info', () => {
    const component = shallow(<Profile profile={{ ...user, ...contact }} />);
    expect(component).toMatchSnapshot();
  });

  it('should render picture, user name and social links', () => {
    const component = shallow(<Profile profile={{ ...user, ...avatar, ...social }} />);
    expect(component).toMatchSnapshot();
  });

  it('should render picture, company name, contact info and personal data', () => {
    const component = shallow(<Profile profile={{ ...company, ...avatar, ...contact, ...personal }} />);
    expect(component).toMatchSnapshot();
  });

  it('should render picture, company name, social links and personal data', () => {
    const component = shallow(<Profile profile={{ ...company, ...avatar, ...social, ...personal }} />);
    expect(component).toMatchSnapshot();
  });

  it('should render settings link on company profile', () => {
    const component = shallow(<Profile loggedUser={loggedUser} profile={{ ...company }} />);
    expect(component).toMatchSnapshot();
  });

  it('should render settings link on user profile', () => {
    const component = shallow(<Profile loggedUser={loggedUser} profile={{ ...user }} />);
    expect(component).toMatchSnapshot();
  });
});
