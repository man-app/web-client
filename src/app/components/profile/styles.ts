import styled from 'styled-components';

import { Card } from '../card';

import { TABLET } from '../../../common/constants/styles/media-queries';

export const Wrapper = styled.div`
  font-size: 14px;
  text-align: center;
`;
Wrapper.displayName = 'Wrapper';

export const Title = styled.h1``;
Title.displayName = 'Title';

export const Picture = styled.div`
  display: inline-block;
  margin: 0 auto;
  margin-top: 10px;
  position: relative;
  text-align: center;
  z-index: 1;
`;
Picture.displayName = 'Picture';

export const Content = styled(Card)`
  margin-top: -55px;
  padding-top: 50px;
  position: relative;
  z-index: 0;

  ${TABLET} {
    margin-top: -105px;
    padding-top: 100px;
  }
`;
Content.displayName = 'Content';

export const EditButton = styled.div`
  min-width: 120px;
  position: absolute;
  right: 5px;
  top: 5px;
`;
EditButton.displayName = 'EditButton';

export const LinksWrapper = styled.ul`
  display: inline-block;
  text-align: center;
  width: 100%;
`;
LinksWrapper.displayName = 'LinksWrapper';

export const LinkWrapper = styled.li`
  display: inline-block;
  margin: 5px;
  font-size: 20px;
`;
LinkWrapper.displayName = 'LinkWrapper';

export const Info = styled.ul`
  display: inline-block;
  margin: 0 auto;
  width: 100%;

  ${TABLET} {
    text-align: center;
  }
`;
Info.displayName = 'Info';

export const Property = styled.li`
  display: inline-block;
  margin-bottom: 10px;
  min-width: 51%;
  text-align: left;
  width: auto;
`;
Property.displayName = 'Property';

export const ListItemLabel = styled.span`
  font-weight: bold;
  margin-right: 10px;
`;
ListItemLabel.displayName = 'ListItemLabel';

export const SpacerWrapper = styled.div`
  margin: 0 auto;
  max-width: 20%;
`;
SpacerWrapper.displayName = 'SpacerWrapper';
