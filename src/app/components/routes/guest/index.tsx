import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { log } from '../../../../common/utils/logger';

import { COMPANIES } from '../../../../common/constants/appRoutes';

interface Props {
  path: string;
  component: any;
  isLoggedUser: boolean;
}

const RouteGuest = ({ component: Component, isLoggedUser, ...options }: Props) => (
  <Route
    {...options}
    render={props => {
      const shouldDisplayThisRoute = props.location.pathname === props.match.url;

      if (shouldDisplayThisRoute && isLoggedUser) {
        log(`Logged user trying to access to a guest page ${props.location.pathname}.`);
        log(`Redirecting to ${COMPANIES} => ${props.location.pathname}`);
        return (
          <Redirect
            to={{
              pathname: COMPANIES,
              state: { from: (props.location.state && props.location.state.from) || props.location.pathname },
            }}
          />
        );
      }

      if (shouldDisplayThisRoute) {
        return <Component {...props} />;
      }

      return null;
    }}
  />
);

export default RouteGuest;
