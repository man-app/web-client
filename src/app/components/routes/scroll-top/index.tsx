import React from 'react';
import { useLocation } from 'react-router-dom';

interface Props {
  children: any;
}

const ScrollTop = ({ children }: Props) => {
  const location = useLocation();

  React.useEffect(() => {
    window.scrollTo(0, 0);
  }, [location]);

  return children;
};

export default ScrollTop;
