import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { log } from '../../../../common/utils/logger';

import { HOME } from '../../../../common/constants/appRoutes';

interface Props {
  path: string;
  component: any;
  isLoggedUser: boolean;
}

const RouteAuth = ({ component: Component, isLoggedUser, ...options }: Props) => (
  <Route
    {...options}
    render={props => {
      const shouldDisplayThisRoute = props.location.pathname === props.match.url;

      if (shouldDisplayThisRoute && !isLoggedUser) {
        log(`Guest trying to access to a user page ${props.location.pathname}.`);
        log(`Redirecting to ${HOME} => ${props.location.pathname}`);
        return (
          <Redirect
            to={{
              pathname: HOME,
              state: { from: (props.location.state && props.location.state.from) || props.location.pathname },
            }}
          />
        );
      }

      if (shouldDisplayThisRoute) {
        return <Component {...props} />;
      }

      return null;
    }}
  />
);

export default RouteAuth;
