import React from 'react';
import { shallow } from 'enzyme';

import RouteAuth from '..';

describe('RouteAuth', () => {
  it('should redirect when no user is logged', () => {
    const component = shallow(
      <RouteAuth component={<div data-auth="component" />} isLoggedUser={false} path="/auth-url" />
    );
    expect(component).toMatchSnapshot();
  });

  it('should render component when user is logged', () => {
    const component = shallow(
      <RouteAuth component={<div data-auth="component" />} isLoggedUser={true} path="/auth-url" />
    );
    expect(component).toMatchSnapshot();
  });
});
