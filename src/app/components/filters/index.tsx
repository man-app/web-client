import React from 'react';

import SmartForm from '../forms';
import { Wrapper, Filter } from './styles';

import { FieldProps } from '../forms/types';

interface Props {
  options: FieldProps[];
  isOpen: boolean;
}

const Filters = ({ isOpen, options }: Props) => (
  <Wrapper isOpen={isOpen}>
    <SmartForm buttons={[]} fields={options} form="filters" handleSubmit={() => {}} isLoading={false}>
      {({ controlledFields, Field }) =>
        controlledFields.map(filter => (
          <Filter key={filter.id}>
            <Field options={filter} />
          </Filter>
        ))
      }
    </SmartForm>
  </Wrapper>
);

export default Filters;
