import React from 'react';

import { List as Wrapper, Item } from './styles';

interface Props {
  children: (props: { Item: typeof Item }) => React.ReactNode;
}

const List: React.FC<Props> = ({ children }: Props) => <Wrapper>{children({ Item })}</Wrapper>;

export default List;
