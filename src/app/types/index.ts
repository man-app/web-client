import { AccountState } from '../modules/account/types';
import { ActivitiesState } from '../modules/activities/types';
import { AuthState } from '../modules/auth/types';
import { CompaniesState } from '../modules/companies/types';
import { ContactState } from '../modules/contact/types';
import { FeedbackState } from '../modules/feedback/types';
import { SmartFormState } from '../components/forms/types';
import { ModulesState } from '../modules/modules/types';
import { NotifierState } from '../modules/notifier/types';
import { PaymentMethodState } from '../modules/payment-methods/types';
import { RolesState } from '../modules/roles/types';
import { SocialState } from '../modules/social/types';
import { SubscriptionsState } from '../modules/subscriptions/types';

export type FilterCollectionActionType = 'FILTERS_APPLIED';

export interface FilterCollectionAction {
  type: FilterCollectionActionType;
  payload: any;
}

export interface RootState {
  account: AccountState;
  activities: ActivitiesState;
  auth: AuthState;
  companies: CompaniesState;
  contact: ContactState;
  feedback: FeedbackState;
  form: SmartFormState;
  modules: ModulesState;
  notifier: NotifierState;
  paymentMethods: PaymentMethodState;
  roles: RolesState;
  social: SocialState;
  subscriptions: SubscriptionsState;
}
