import React from 'react';
import { isProduction } from '../../../../../common/utils/platforms';

import { Link } from '../../../../components/touchable';
import { Wrapper, Content, Text, List, ListHead, ListElement } from './styles';

import { APP_NAME, APP_DEVELOPER } from '../../../../../common/constants/branding';
import { MAX_WIDTH_LG } from '../../../../../common/constants/styles/sizes';
import { LinkProps } from '../../../../components/touchable/types';

interface Props {
  appVersion: string;
  inlineLinks?: LinkProps[];
  legalLinks?: LinkProps[];
  fixedSidebar: boolean;
  usefulLinks?: LinkProps[];
}

const View: React.FC<Props> = ({ appVersion, inlineLinks, legalLinks, fixedSidebar, usefulLinks }: Props) => (
  <Wrapper fixedSidebar={fixedSidebar}>
    <Content maxWidth={MAX_WIDTH_LG}>
      <Text>
        {APP_NAME} {!isProduction() && `v${appVersion}`} &copy; is a registered property of {APP_DEVELOPER} &copy;{' '}
        {new Date().getFullYear()}
        {inlineLinks &&
          inlineLinks.map((link, index) => (
            <span key={index}>
              {' - '}
              <Link options={link} />
            </span>
          ))}
      </Text>

      {usefulLinks && (
        <List>
          <ListHead>Useful links</ListHead>
          {usefulLinks.map((link, index) => (
            <ListElement key={index}>
              <Link options={link} />
            </ListElement>
          ))}
        </List>
      )}

      {legalLinks && (
        <List>
          <ListHead>Legal</ListHead>
          {legalLinks.map((link, index) => (
            <ListElement key={index}>
              <Link options={link} />
            </ListElement>
          ))}
        </List>
      )}
    </Content>
  </Wrapper>
);

export default View;
