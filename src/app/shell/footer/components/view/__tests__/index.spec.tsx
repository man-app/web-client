import React from 'react';
import { shallow } from 'enzyme';

import Footer from '..';

describe('Footer', () => {
  it('should render a valid footer', () => {
    const component = shallow(
      <Footer
        appVersion="1"
        inlineLinks={[{ id: 'inline' }]}
        legalLinks={[{ id: 'legal' }]}
        fixedSidebar={false}
        usefulLinks={[{ id: 'useful' }]}
      />
    );

    expect(component).toMatchSnapshot();
  });
});
