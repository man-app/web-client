import React from 'react';

import View from './components/view';
import { LinkProps } from '../../components/touchable/types';

import {
  ABOUT,
  SUBSCRIPTIONS,
  PRIVACY_POLICY,
  CONTACT,
  COOKIES_POLICY,
  TERMS_AND_CONDITIONS,
} from '../../../common/constants/appRoutes';
import { APP_NAME } from '../../../common/constants/branding';
import { shouldShowCompanyModules } from '../../utils/permissions';
import { useSelector } from 'react-redux';
import { getLoggedUser } from '../../modules/auth/selectors';
import { getSelectedCompany } from '../../modules/companies/selectors';

const packageJson = require('../../../../package.json');
const APP_VERSION = packageJson.version;

const inlineLinks: LinkProps[] = [];

const usefulLinks: LinkProps[] = [
  {
    id: 'about-link',
    to: ABOUT,
    label: `About ${APP_NAME}`,
  },
  {
    id: 'pricing-link',
    to: SUBSCRIPTIONS,
    label: 'Pricing',
  },
  {
    id: 'contact-link',
    to: CONTACT,
    label: 'Support',
  },
];

const legalLinks: LinkProps[] = [
  {
    id: 'terms-link',
    to: TERMS_AND_CONDITIONS,
    label: 'Terms & Conditions',
  },
  {
    id: 'privacy-link',
    to: PRIVACY_POLICY,
    label: 'Privacy policy',
  },
  {
    id: 'cookies-link',
    to: COOKIES_POLICY,
    label: 'Cookies policy',
  },
];

const Footer: React.FC = () => {
  const user = useSelector(getLoggedUser);
  const company = useSelector(getSelectedCompany);

  return (
    <View
      appVersion={APP_VERSION}
      inlineLinks={inlineLinks}
      legalLinks={legalLinks}
      fixedSidebar={shouldShowCompanyModules(user, company)}
      usefulLinks={usefulLinks}
    />
  );
};

export default Footer;
