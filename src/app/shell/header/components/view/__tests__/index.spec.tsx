import React from 'react';
import { shallow } from 'enzyme';

import View from '..';

import { UserLogged } from '../../../../../modules/auth/types';
import { Company } from '../../../../../modules/companies/types';
import { Module } from '../../../../../modules/modules/types';

const module1: Module = {
  id: '1',
  name: 'test-module',
  url: 'module-url',
  listing: false,
};

const company1: Company = {
  id: '1',
  name: 'Company',
  slug: 'company',
  modules: [module1.id],
  created: 123456789,
  lastModified: 123456789,
};

const user: UserLogged = {
  id: '1',
  fullName: 'Test',
  email: 'test@user.com',
  jobs: [{ idCompany: company1.id, permissions: {} }],
  created: 123456789,
  lastModified: 123456789,
};

describe('Header', () => {
  it('should render a header for a guest user', () => {
    const component = shallow(
      <View
        companies={[]}
        handleRedirect={() => {}}
        location={{}}
        logo="custom-logo.png"
        logSmartEvent={() => {}}
        modules={[]}
        openMenu={''}
        toggleMenu={() => {}}
      />
    );

    expect(component).toMatchSnapshot();
  });

  it('should render a header for a non-verified user', () => {
    const component = shallow(
      <View
        companies={[]}
        handleRedirect={() => {}}
        location={{}}
        logo="custom-logo.png"
        logSmartEvent={() => {}}
        modules={[]}
        openMenu={''}
        toggleMenu={() => {}}
        user={{ ...user, jobs: [] }}
      />
    );

    expect(component).toMatchSnapshot();
  });

  it('should render a header for a verified user with jobs', () => {
    const component = shallow(
      <View
        companies={[company1]}
        handleRedirect={() => {}}
        location={{}}
        logo="custom-logo.png"
        logSmartEvent={() => {}}
        modules={[{ ...module1, listing: true }]}
        openMenu={''}
        selectedCompany={company1}
        toggleMenu={() => {}}
        user={{
          ...user,
          isVerified: true,
        }}
      />
    );

    expect(component).toMatchSnapshot();
  });

  it('should render a header for a verified user with jobs without a selected company', () => {
    const component = shallow(
      <View
        companies={[company1]}
        handleRedirect={() => {}}
        location={{}}
        logo="custom-logo.png"
        logSmartEvent={() => {}}
        modules={[{ ...module1, listing: true }]}
        openMenu={''}
        selectedCompany={undefined}
        toggleMenu={() => {}}
        user={{
          ...user,
          isVerified: true,
        }}
      />
    );

    expect(component).toMatchSnapshot();
  });

  it('should render a header for a verified user with jobs without permission', () => {
    const component = shallow(
      <View
        companies={[company1]}
        handleRedirect={() => {}}
        location={{}}
        logo="custom-logo.png"
        logSmartEvent={() => {}}
        modules={[module1]}
        openMenu={''}
        selectedCompany={company1}
        toggleMenu={() => {}}
        user={{
          ...user,
          isVerified: true,
        }}
      />
    );

    expect(component).toMatchSnapshot();
  });

  it('should render a header for a verified user without jobs', () => {
    const component = shallow(
      <View
        companies={[]}
        handleRedirect={() => {}}
        location={{}}
        logo="custom-logo.png"
        logSmartEvent={() => {}}
        modules={[module1]}
        openMenu={''}
        toggleMenu={() => {}}
        user={{ ...user, isVerified: true, jobs: [] }}
      />
    );

    expect(component).toMatchSnapshot();
  });
});
