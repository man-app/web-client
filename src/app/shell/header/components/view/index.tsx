import React from 'react';
import { shouldShowCompanyModules, moduleIds } from '../../../../utils/permissions';

import {
  LOGIN,
  REGISTER,
  USER_PROFILE,
  ACCOUNT,
  FEEDBACK,
  LOGOUT,
  COMPANY_ACTIVITIES,
  COMPANY,
  COMPANY_NEW,
  HOME,
} from '../../../../../common/constants/appRoutes';
import {
  LOGIN_ICON,
  LOGIN_LABEL,
  REGISTER_ICON,
  REGISTER_LABEL,
  ACCOUNT_ICON,
  ACCOUNT_LABEL,
  PROFILE_ICON,
  PROFILE_LABEL,
  FEEDBACK_ICON,
  FEEDBACK_LABEL,
  LOGOUT_ICON,
  LOGOUT_LABEL,
  CREATE_ICON,
  CREATE_LABEL,
  DONATE_LABEL,
  DONATE_ICON,
} from '../../../../../common/constants/buttons';

import Avatar from '../../../../components/avatar';
import { Content } from '../../../../components/touchable';
import Menu from '../menu';
import { Item } from '../menu/components/item';
import Sidebar from '../sidebar';
import { Logo, LogoLink, Header, HeaderBar, HeaderSpacer } from './styles';

import { APP_PATREON } from '../../../../../common/constants/branding';
import { UserLogged } from '../../../../modules/auth/types';
import { Company } from '../../../../modules/companies/types';
import { Module } from '../../../../modules/modules/types';
import { LinkProps } from '../../../../components/touchable/types';
import { modulesMenu, userMenu, companiesMenu } from '../../models';
import { HeaderMenu as HeaderMenuType } from '../../types';
import { HEADER_SIZE } from '../../../../../common/constants/styles/sizes';
import { useThemeContext } from '../../../../contexts/theme';

const registerLink: LinkProps = {
  id: 'register-link',
  to: REGISTER,
  icon: REGISTER_ICON,
  label: REGISTER_LABEL,
  iconLast: true,
};

const loginLink: LinkProps = {
  id: 'login-link',
  to: LOGIN,
  icon: LOGIN_ICON,
  label: LOGIN_LABEL,
  iconLast: true,
};

const accountLink: LinkProps = {
  id: 'account-link',
  to: ACCOUNT.replace('/:tab?/:id?', ''),
  icon: ACCOUNT_ICON,
  label: ACCOUNT_LABEL,
  iconLast: true,
};

const feedbackLink: LinkProps = {
  id: 'feedback-link',
  to: FEEDBACK,
  icon: FEEDBACK_ICON,
  label: FEEDBACK_LABEL,
  iconLast: true,
};

const sponsorLink: LinkProps = {
  id: 'sponsor-header-link',
  to: APP_PATREON,
  label: DONATE_LABEL,
  icon: DONATE_ICON,
  iconLast: true,
  isExternal: true,
};

interface Props {
  companies: Company[];
  handleRedirect: (pathname: string) => void;
  location: any;
  logo?: string;
  logSmartEvent: (options: { label: string; to: string }) => void;
  modules: Module[];
  openMenu: string;
  selectedCompany?: Company;
  toggleMenu: (elem: HeaderMenuType) => void;
  user?: UserLogged;
}

const View: React.FC<Props> = ({
  companies,
  handleRedirect,
  location,
  logo,
  logSmartEvent,
  modules,
  openMenu,
  selectedCompany,
  toggleMenu,
  user,
}: Props) => {
  const theme = useThemeContext();
  const thereAreCompanies = companies.length > 0;

  const modulesList: LinkProps[] = [];
  if (selectedCompany) {
    modulesList.push(
      {
        id: 'dashboard-link',
        label: 'Dashboard',
        to: COMPANY_ACTIVITIES.replace(':slug', selectedCompany.slug),
      },
      {
        id: 'company-profile-link',
        label: 'Company profile',
        to: COMPANY.replace(':slug', selectedCompany.slug),
      }
    );

    // Add modules to sidebar, if user can list them
    modules.forEach(em => {
      if (em.id === moduleIds.companySettings || em.listing) {
        modulesList.push({
          id: `module-${em.id}-link`,
          label: em.name,
          to: em && em.url && em.url.replace(':slug', selectedCompany.slug),
        });
      }
    });
  }

  const userActionsList = [];

  // Logged user
  if (user && user.id) {
    userActionsList.push(
      {
        id: 'user-profile-link',
        to: USER_PROFILE.replace(':id', user.id.toString()),
        icon: PROFILE_ICON,
        label: PROFILE_LABEL,
        iconLast: true,
      },
      accountLink,
      feedbackLink,
      sponsorLink,
      {
        id: 'logout-link',
        to: { pathname: LOGOUT, state: { from: location.pathname } },
        icon: LOGOUT_ICON,
        label: LOGOUT_LABEL,
        iconLast: true,
      }
    );

    // Guest user
  } else {
    userActionsList.push(registerLink, loginLink, sponsorLink);
  }

  return (
    <Header>
      <HeaderBar>
        <>
          {/* Modules menu */}
          {shouldShowCompanyModules(user, selectedCompany) && (
            <Sidebar
              isOpen={openMenu === modulesMenu}
              handleClick={() => {
                toggleMenu(modulesMenu);
              }}
            >
              <ul>
                {modulesList &&
                  modulesList.map(companyModule => (
                    <li key={companyModule.id}>
                      <Item
                        theme={theme}
                        to={companyModule.to}
                        onClick={() => {
                          toggleMenu(modulesMenu);
                        }}
                      >
                        <Content options={companyModule} />
                      </Item>
                    </li>
                  ))}
              </ul>
            </Sidebar>
          )}

          {/* App's logo */}
          {logo && (
            <LogoLink
              to={HOME}
              onClick={() => {
                logSmartEvent({ label: 'Home icon', to: HOME });
              }}
            >
              <Logo src={logo} />
            </LogoLink>
          )}

          {/* User menu */}
          <Menu
            handleClick={() => {
              toggleMenu(userMenu);
            }}
            isOpen={openMenu === userMenu}
            toggler={{
              icon: !user ? LOGIN_ICON : ACCOUNT_ICON,
            }}
          >
            <ul>
              {userActionsList &&
                userActionsList.map(action => (
                  <li key={action.id}>
                    <Item
                      align="flex-end"
                      onClick={() => {
                        toggleMenu(userMenu);
                      }}
                      isExternal={action.isExternal}
                      theme={theme}
                      to={action.to}
                    >
                      <Content options={action} />
                    </Item>
                  </li>
                ))}
            </ul>
          </Menu>

          {/* Companies menu */}
          {user && user.isVerified && (
            <Menu
              handleClick={() => {
                if (!thereAreCompanies) {
                  handleRedirect(COMPANY_NEW);
                } else {
                  toggleMenu(companiesMenu);
                }
              }}
              isOpen={openMenu === companiesMenu}
              right={HEADER_SIZE}
              toggler={{
                icon: !thereAreCompanies ? (
                  CREATE_ICON
                ) : (
                  <Avatar
                    profile={{
                      picture: selectedCompany ? selectedCompany.picture || '' : companies[0].picture || '',
                      name: selectedCompany ? selectedCompany.name : companies[0].name,
                    }}
                    size="xs"
                  />
                ),
                label: !thereAreCompanies
                  ? `${CREATE_LABEL} company`
                  : selectedCompany
                  ? selectedCompany.name
                  : companies[0].name,
              }}
            >
              <ul>
                {thereAreCompanies &&
                  companies &&
                  companies.map(company => (
                    <li key={company.id}>
                      <Item
                        align="flex-end"
                        onClick={() => {
                          toggleMenu(companiesMenu);
                        }}
                        theme={theme}
                        to={COMPANY_ACTIVITIES.replace(':slug', company.slug)}
                      >
                        <Content
                          options={{
                            icon: <Avatar profile={{ picture: company.picture || '', name: company.name }} size="xs" />,
                            iconLast: true,
                            label: company.name,
                          }}
                        />
                      </Item>
                    </li>
                  ))}
                <li>
                  <Item
                    align="flex-end"
                    onClick={() => {
                      toggleMenu(companiesMenu);
                    }}
                    theme={theme}
                    to={COMPANY_NEW}
                  >
                    <Content
                      options={{
                        icon: CREATE_ICON,
                        iconLast: true,
                        label: `${CREATE_LABEL} company`,
                      }}
                    />
                  </Item>
                </li>
              </ul>
            </Menu>
          )}
        </>
      </HeaderBar>
      <HeaderSpacer />
    </Header>
  );
};

export default View;
