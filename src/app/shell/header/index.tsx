import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import useReactRouter from 'use-react-router';
import analyticsApi from '../../../common/apis/reports/analytics';
import { getPermissions } from '../../utils/permissions';

import { getCompanies } from '../../modules/companies/actions';
import { getLoggedUser } from '../../modules/auth/selectors';
import { getSelectedCompany, getEmployingCompanies } from '../../modules/companies/selectors';
import { getModulesInstalled } from '../../modules/modules/selectors';

import View from './components/view';

import { INTERACTIONS } from '../../../common/constants/analytics';
import { noMenu } from './models';
import { HeaderMenu } from './types';

const logo = require('../../../images/logo.png').default;

const Header: React.FC = () => {
  const [openMenu, setOpenMenu] = React.useState(noMenu);
  const { history, location } = useReactRouter();

  const dispatch = useDispatch();
  const user = useSelector(getLoggedUser);
  const companies = useSelector(getEmployingCompanies)(user);
  const selectedCompany = useSelector(getSelectedCompany);
  const modules = useSelector(getModulesInstalled);
  const companyModules = modules.map(installedModule => {
    const permissions = getPermissions(installedModule, user, selectedCompany);
    return {
      ...installedModule,
      listing: permissions.listing && permissions.listing.value,
      creating: permissions.creating && permissions.creating.value,
      editing: permissions.editing && permissions.editing.value,
      approving: permissions.approving && permissions.approving.value,
      disabling: permissions.disabling && permissions.disabling.value,
      deleting: permissions.deleting && permissions.deleting.value,
    };
  });

  React.useEffect(() => {
    if (user) {
      dispatch(getCompanies());
    }
  }, []);

  const handleRedirect = (pathname: string) => {
    history.push({
      pathname,
      state: { from: location.pathname },
    });
  };

  const logSmartEvent = (options: {}) => {
    analyticsApi.logSmartEvent({ ...options, category: INTERACTIONS });
  };

  const toggleMenu = (elem: HeaderMenu) => {
    let action = '';

    // Close any open menu
    if (elem === openMenu) {
      action = 'Close';
      setOpenMenu(noMenu);

      // Open a different menu
    } else {
      action = 'Open';
      setOpenMenu(elem);
    }

    logSmartEvent({
      label: `${action} ${elem} toggler`,
    });
  };

  return (
    <View
      companies={companies}
      handleRedirect={handleRedirect}
      location={location}
      logo={logo}
      logSmartEvent={logSmartEvent}
      modules={companyModules}
      openMenu={openMenu}
      selectedCompany={selectedCompany}
      toggleMenu={toggleMenu}
      user={user}
    />
  );
};

export default Header;
