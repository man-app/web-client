import { isLoadingAccount } from '../modules/account/selectors';
import { isLoadingActivities } from '../modules/activities/selectors';
import { isLoadingAuth } from '../modules/auth/selectors';
import { isLoadingCompanies } from '../modules/companies/selectors';
import { isLoadingContact } from '../modules/contact/selectors';
import { isLoadingFeedback } from '../modules/feedback/selectors';
import { getNotifications } from '../modules/notifier/selectors';
import { isLoadingPaymentMethods } from '../modules/payment-methods/selectors';
import { isLoadingSubscriptionPlans } from '../modules/subscriptions/selectors';
import { isLoadingRoles } from '../modules/roles/selectors';
import { isLoadingSocial } from '../modules/social/selectors';

import { RootState } from '../types';

export const getAppStore = (state: RootState) => state;

export const isLoadingAnything = (state: RootState) =>
  [
    isLoadingAccount(state),
    isLoadingActivities(state),
    isLoadingAuth(state),
    isLoadingCompanies(state),
    isLoadingContact(state),
    isLoadingFeedback(state),
    isLoadingPaymentMethods(state),
    isLoadingSubscriptionPlans(state),
    isLoadingRoles(state),
    isLoadingSocial(state),
  ].filter(m => m).length > 0;

export const isOffline = (rootState: RootState) =>
  getNotifications(rootState).filter(n => n.options.data.reason === 'connection').length > 0;
