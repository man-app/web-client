import setupEnv from '../../../common/utils/setup-test-env';

import {
  mapClientErrors,
  mapServerErrors,
  isValidForm,
  isValidPictureExtension,
  isLargerThanMaxPictureWeight,
  isSmallerThanMinPictureWeight,
  isSmallerThanMinPictureSize,
} from '../error-handler';

import { TEXT, PICTURE, ARRAY } from '../../components/forms/models';

beforeAll(() => {
  setupEnv();
});

describe('Error handler utils', () => {
  // mapClientErrors
  describe('mapClientErrors', () => {
    it('should return no errors', () => {
      const input = { id: 'test-field', model: '', type: TEXT };
      const output = mapClientErrors(input);

      expect(output).toBe('');
    });

    it('should return a minLength error (because field is required)', () => {
      const input = { id: 'test-field', model: '', isRequired: true, minLength: 3, type: TEXT };
      const output = mapClientErrors(input);

      expect(output).toBe(`The ${input.id} should have at least ${input.minLength} characters`);
    });

    it('should return a minLength error', () => {
      const input = { id: 'test-field', model: 'Mi', minLength: 3, type: TEXT };
      const output = mapClientErrors(input);

      expect(output).toBe(`The ${input.id} should have at least ${input.minLength} characters`);
    });

    it('should return a maxLength error', () => {
      const input = { id: 'test-field', model: 'Mike', maxLength: 3, type: TEXT };
      const output = mapClientErrors(input);

      expect(output).toBe(`The ${input.id} cannot have more than ${input.maxLength} characters`);
    });
  });

  // mapServerErrors
  describe('mapServerErrors', () => {
    it('No fields, should return no errors', () => {
      const output = mapServerErrors();

      expect(output).toStrictEqual([]);
    });

    it('No errors, should return no errors', () => {
      const input = { id: 'test-field', model: '', type: TEXT };
      const output = mapServerErrors([input]);

      expect(output).toStrictEqual([input]);
    });

    it('should dismiss no-form errors', () => {
      const input = { id: 'test-field', model: '', type: TEXT };
      const serverError = { channel: 'web', reason: 'global', body: 'Custom error' };
      const output = mapServerErrors([input], [serverError]);

      expect(output).toStrictEqual([input]);
    });

    it('should dismiss errors from other fields', () => {
      const input = { id: 'test-field', model: '', type: TEXT };
      const serverError = { channel: 'form', reason: 'test-field-2', body: 'Custom error' };
      const output = mapServerErrors([input], [serverError]);

      expect(output).toStrictEqual([input]);
    });

    it('should return an error', () => {
      const input = { id: 'test-field', model: '', type: TEXT };
      const serverError = { channel: 'form', reason: 'test-field', body: 'Custom error' };
      const output = mapServerErrors([input], [serverError]);

      expect(output).toStrictEqual([{ ...input, error: 'Custom error' }]);
    });
  });

  // isValidForm
  describe('isValidForm', () => {
    it('No filters, should return true', () => {
      const output = isValidForm();

      expect(output).toBe(true);
    });

    it('Default field with no errors should return true', () => {
      const field = { id: 'test-field', model: '', type: TEXT };
      const output = isValidForm([field]);

      expect(output).toBe(true);
    });

    it('Default field with errors should return false', () => {
      const field = { id: 'test-field', model: '', type: TEXT, error: 'This is required' };
      const output = isValidForm([field]);

      expect(output).toBe(false);
    });

    it('Array field with no errors should return true', () => {
      const field = { id: 'test-field', fields: [{ id: 'test-field-2', model: '', type: TEXT }], type: ARRAY };
      const output = isValidForm([field]);

      expect(output).toBe(true);
    });

    it('Array field with errors should return false', () => {
      const field = {
        id: 'test-field',
        fields: [{ id: 'test-field-2', model: '', type: TEXT, error: 'This is required' }],
        type: ARRAY,
      };
      const output = isValidForm([field]);

      expect(output).toBe(false);
    });
  });

  // isValidPictureExtension
  describe('isValidPictureExtension', () => {
    it('Invalid picture extension, should return false', () => {
      const input = {
        id: 'test-field',
        file: { name: 'test.pdf', lastModified: new Date().getTime(), size: 102400, type: '', slice: () => new Blob() },
        type: PICTURE,
      };
      const output = isValidPictureExtension(input.file);

      expect(output).toBe(false);
    });

    it('Valid picture extension, should return true', () => {
      const input = {
        id: 'test-field',
        file: { name: 'test.jpg', lastModified: new Date().getTime(), size: 102400, type: '', slice: () => new Blob() },
        type: PICTURE,
      };
      const output = isValidPictureExtension(input.file);

      expect(output).toBe(true);
    });
  });

  // isLargerThanMaxPictureWeight
  describe('isLargerThanMaxPictureWeight', () => {
    it('Larger picture weight, should return true', () => {
      const input = {
        id: 'test-field',
        file: {
          name: 'test.jpg',
          lastModified: new Date().getTime(),
          size: 10485761,
          type: '',
          slice: () => new Blob(),
        },
        type: PICTURE,
      };
      const output = isLargerThanMaxPictureWeight(input.file);

      expect(output).toBe(true);
    });

    it('Smaller picture weight, should return false', () => {
      const input = {
        id: 'test-field',
        file: {
          name: 'test.jpg',
          lastModified: new Date().getTime(),
          size: 10485759,
          type: '',
          slice: () => new Blob(),
        },
        type: PICTURE,
      };
      const output = isLargerThanMaxPictureWeight(input.file);

      expect(output).toBe(false);
    });
  });

  // isSmallerThanMinPictureWeight
  describe('isSmallerThanMinPictureWeight', () => {
    it('Smaller picture weight, should return true', () => {
      const input = {
        id: 'test-field',
        file: {
          name: 'test.jpg',
          lastModified: new Date().getTime(),
          size: 10239,
          type: '',
          slice: () => new Blob(),
        },
        type: PICTURE,
      };
      const output = isSmallerThanMinPictureWeight(input.file);

      expect(output).toBe(true);
    });

    it('Larger picture weight, should return false', () => {
      const input = {
        id: 'test-field',
        file: {
          name: 'test.jpg',
          lastModified: new Date().getTime(),
          size: 10241,
          type: '',
          slice: () => new Blob(),
        },
        type: PICTURE,
      };
      const output = isSmallerThanMinPictureWeight(input.file);

      expect(output).toBe(false);
    });
  });

  // isSmallerThanMinPictureSize
  describe('isSmallerThanMinPictureSize', () => {
    it('Smaller picture height, should return true', () => {
      const input = {
        id: 'test-field',
        file: new Image(300, 150),
        type: PICTURE,
      };
      const output = isSmallerThanMinPictureSize(input.file);

      expect(output).toBe(true);
    });

    it('Smaller picture width, should return true', () => {
      const input = {
        id: 'test-field',
        file: new Image(150, 300),
        type: PICTURE,
      };
      const output = isSmallerThanMinPictureSize(input.file);

      expect(output).toBe(true);
    });

    it('Valid picture size, should return true', () => {
      const input = {
        id: 'test-field',
        file: new Image(300, 300),
        type: PICTURE,
      };
      const output = isSmallerThanMinPictureSize(input.file);

      expect(output).toBe(false);
    });
  });
});
