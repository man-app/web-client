import setupEnv from '../../../common/utils/setup-test-env';

import { isEmployee, getPermissions } from '../permissions';

const defaultModule = {
  id: '1',
  name: 'test-module',
  url: 'test-module',
  listing: true,
  creating: true,
  editing: true,
  approving: true,
  disabling: true,
  deleting: true,
};

const defaultPermissions = {
  listing: {
    label: 'Can see',
    value: false,
  },
  creating: {
    label: 'Can create',
    value: false,
  },
  editing: {
    label: 'Can edit',
    value: false,
  },
  approving: {
    label: 'Can approve/reject',
    value: false,
  },
  disabling: {
    label: 'Can enable/disable',
    value: false,
  },
  deleting: {
    label: 'Can delete',
    value: false,
  },
};

beforeEach(() => {
  setupEnv();
});

describe('Permissions utils', () => {
  describe('isEmployee', () => {
    it('User with no jobs, should return false', () => {
      const user = { id: '1', fullName: 'Test', email: 'test@user.com', created: 123456789, lastModified: 123456789 };
      const idCompany = '1';
      const output = isEmployee(user, idCompany);

      expect(output).toBe(false);
    });

    it('User with no jobs in selected company, should return false', () => {
      const user = {
        id: '1',
        fullName: 'Test',
        email: 'test@user.com',
        jobs: [{ idCompany: '2', permissions: {} }],
        created: 123456789,
        lastModified: 123456789,
      };
      const idCompany = '1';
      const output = isEmployee(user, idCompany);

      expect(output).toBe(false);
    });

    it('User with a job in selected company, should return true', () => {
      const user = {
        id: '1',
        fullName: 'Test',
        email: 'test@user.com',
        jobs: [{ idCompany: '1', permissions: {} }],
        created: 123456789,
        lastModified: 123456789,
      };
      const idCompany = '1';
      const output = isEmployee(user, idCompany);

      expect(output).toBe(true);
    });
  });

  describe('getPermissions', () => {
    it('Undefined company, should return all permissions false', () => {
      const user = {
        id: '1',
        fullName: 'Test',
        email: 'test@user.com',
        jobs: [{ idCompany: '2', permissions: {} }],
        created: 123456789,
        lastModified: 123456789,
      };
      const company = undefined;
      const module = defaultModule;

      const output = getPermissions(module, user, company);

      expect(output).toStrictEqual(defaultPermissions);
    });

    it('Undefined user, should return all permissions false', () => {
      const user = undefined;
      const company = { id: '1', slug: 'company', name: 'Company', created: 123456789, lastModified: 123456789 };
      const mod = defaultModule;

      const output = getPermissions(mod, user, company);

      expect(output).toStrictEqual(defaultPermissions);
    });

    it('Valid information, should return all permissions true', () => {
      const user = {
        id: '1',
        fullName: 'Test',
        email: 'test@user.com',
        jobs: [
          {
            idCompany: '1',
            permissions: {
              '1': {
                idModule: '1',
                listing: true,
                creating: true,
                editing: true,
                approving: true,
                disabling: true,
                deleting: true,
              },
            },
          },
        ],
        created: 123456789,
        lastModified: 123456789,
      };
      const company = {
        id: '1',
        slug: 'company',
        name: 'Company',
        modules: ['1'],
        roles: [{ id: 1, modules: [{ idRole: 1 }] }],
        created: 123456789,
        lastModified: 123456789,
      };
      const mod = { ...defaultModule, id: '1', name: 'test-module', url: 'test-module' };

      const output = getPermissions(mod, user, company);

      expect(output).toStrictEqual({
        listing: {
          label: 'Can see',
          value: true,
        },
        creating: {
          label: 'Can create',
          value: true,
        },
        editing: {
          label: 'Can edit',
          value: true,
        },
        approving: {
          label: 'Can approve/reject',
          value: true,
        },
        disabling: {
          label: 'Can enable/disable',
          value: true,
        },
        deleting: {
          label: 'Can delete',
          value: true,
        },
      });
    });
  });
});
