import uuid from 'uuid';
import { getCookie, setCookie } from '../../common/utils/cookies';
import { getPushSubscription } from './push';

import { INSTALLATION_ID } from '../../common/constants/cookies';

/**
 * Get Installation ID
 */
const getInstallationId = () => getCookie(INSTALLATION_ID);

/**
 * If no installation ID available, create it
 */
const setInstallationId = () => {
  if (!getInstallationId()) {
    const value = uuid();
    setCookie(INSTALLATION_ID, value);
  }
};

export interface InstallationData {
  installationId: string;
  userAgent: string;
  token?: string;
  p256dh?: string;
  auth?: string;
}

/**
 * Get installation data:
 * - Installation id
 * - User Agent
 * - Push subscription token
 * - Push subscription keys
 */
export const getInstallationData = () => {
  const subscription = getPushSubscription();

  const data: InstallationData = {
    installationId: getInstallationId(),
    userAgent: navigator.userAgent,
    token: subscription ? subscription.endpoint : undefined,
    p256dh: subscription ? subscription.keys.p256dh : undefined,
    auth: subscription ? subscription.keys.auth : undefined,
  };

  return data;
};

/**
 * Set installation data:
 * - Installation id
 */
export const setInstallationData = () => {
  setInstallationId();
};
