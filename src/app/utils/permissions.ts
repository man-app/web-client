import { UserLogged, Job, ModulePermission } from '../modules/auth/types';
import { Module } from '../modules/modules/types';
import { Company } from '../modules/companies/types';

interface IndexableObject {
  [key: string]: string;
}

export const moduleIds: IndexableObject = {
  companySettings: '1',
  roles: '2',
  shoppingCart: '3',
  employees: '4',
};

export const moduleElementType: IndexableObject = {
  companySettings: 'company',
  roles: 'role',
  shoppingCart: 'shopping-cart',
  employees: 'employee',
};

interface DefaultPermissions {
  listing: boolean;
  creating: boolean;
  editing: boolean;
  approving: boolean;
  disabling: boolean;
  deleting: boolean;
}

export const defaultPermissions: DefaultPermissions = {
  listing: false,
  creating: false,
  editing: false,
  approving: false,
  disabling: false,
  deleting: false,
};

/**
 * Given a user and a company ID, it will answer
 * the question 'Is this user employed by this company?'
 */
export const isEmployee = (loggedUser: UserLogged, idCompany: number | string) => {
  if (!loggedUser || !loggedUser.jobs) {
    return false;
  }

  return !!loggedUser.jobs.find((j: any) => j.idCompany === idCompany);
};

const getJob = (user: UserLogged, idCompany: string): Job | undefined =>
  user && user.jobs && user.jobs.find((j: Job) => j.idCompany === idCompany);

const findModule = (job: Job, moduleId: string): ModulePermission | undefined => {
  let selectedModule;

  Object.keys(job.permissions).forEach(modIndex => {
    const mod = job.permissions[modIndex];
    if (mod && mod.idModule === moduleId) {
      selectedModule = mod;
    }
  });

  return selectedModule;
};

interface Permission {
  label: string;
  value: boolean;
}

export interface Permissions {
  [key: string]: Permission | undefined;

  listing?: Permission;
  creating?: Permission;
  editing?: Permission;
  approving?: Permission;
  disabling?: Permission;
  deleting?: Permission;
}

/**
 * Given a user and a company ID, it will answer
 * get a summary of user permissions
 */
export const getPermissions = (mod: Module, user?: UserLogged, company?: Company): Permissions => {
  const permissions = {
    listing: {
      label: 'Can see',
      value: false,
    },
    creating: {
      label: 'Can create',
      value: false,
    },
    editing: {
      label: 'Can edit',
      value: false,
    },
    approving: {
      label: 'Can approve/reject',
      value: false,
    },
    disabling: {
      label: 'Can enable/disable',
      value: false,
    },
    deleting: {
      label: 'Can delete',
      value: false,
    },
  };

  if (!user || !company || !company.id) {
    return permissions;
  }

  const job = getJob(user, company.id);
  const selectedModule = job && findModule(job, mod.id);

  if (mod.listing) {
    permissions.listing.value = !!(selectedModule && selectedModule.listing);
  }

  if (mod.creating) {
    permissions.creating.value = !!(selectedModule && selectedModule.creating);
  }

  if (mod.editing) {
    permissions.editing.value = !!(selectedModule && selectedModule.editing);
  }

  if (mod.approving) {
    permissions.approving.value = !!(selectedModule && selectedModule.approving);
  }

  if (mod.disabling) {
    permissions.disabling.value = !!(selectedModule && selectedModule.disabling);
  }

  if (mod.deleting) {
    permissions.deleting.value = !!(selectedModule && selectedModule.deleting);
  }

  return permissions;
};

export const shouldShowCompanyModules = (loggedUser?: UserLogged, selectedCompany?: Company) =>
  !!(loggedUser && selectedCompany && isEmployee(loggedUser, selectedCompany.id));
