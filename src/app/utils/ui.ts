import moment from 'moment';
import { moduleIds, moduleElementType } from './permissions';
import { getPushSubscription } from './push';

import { NotificationPreference } from '../modules/auth/types';

export const scrollTo = (elem: HTMLElement, target: HTMLElement, customGap?: number) => {
  if (elem && target) {
    const gap = customGap || 0;
    const element = elem;
    const distance = target.offsetTop - element.scrollTop - gap;
    const pixelsPer10ms = 40;

    // If location changes, will stop scrolling
    const scrollLocation = location && location.href;

    let currentScroll = element.scrollTop;

    const animation = setInterval(() => {
      const isScrollTop = distance < 0;
      const nextScrollPosition = isScrollTop ? currentScroll - pixelsPer10ms : currentScroll + pixelsPer10ms;

      if (
        (distance > 0 && nextScrollPosition >= target.offsetTop - 10 - gap) ||
        (distance < 0 && nextScrollPosition <= target.offsetTop - 10 - gap) ||
        (location && location.href !== scrollLocation)
      ) {
        clearInterval(animation);
      } else {
        currentScroll = nextScrollPosition;
        element.scrollTop = currentScroll;
      }
    }, 10);
  }
};

interface Resource {
  me: string | undefined;
  user: string;
  type: string;
  company: string;
  action: string;
}

export const shouldBeNotified = (resource: Resource, notificationPreferences: NotificationPreference[]): boolean => {
  // Invalid resource
  if (!resource.me || resource.me === resource.user) return false;

  let moduleName;
  switch (resource.type) {
    case moduleElementType.companySettings:
      moduleName = 'companySettings';
      break;
    case moduleElementType.roles:
      moduleName = 'roles';
      break;
    default:
      moduleName = '';
  }

  const moduleId = moduleIds[moduleName];

  // Invalid module
  if (!moduleId) return false;

  const modulePreferences = notificationPreferences.find(
    (np: any) => np.idModule === moduleId && np.idCompany === resource.company
  );

  // Invalid preferences
  if (!modulePreferences) return false;

  let action;
  switch (resource.action) {
    case 'created':
      action = 'creating';
      break;
    case 'modified':
      action = 'editing';
      break;
    case 'enabled':
      action = 'disabling';
      break;
    case 'disabled':
      action = 'disabling';
      break;
    case 'approved':
      action = 'approving';
      break;
    case 'rejected':
      action = 'approving';
      break;
    case 'deleted':
      action = 'deleting';
      break;
    default:
      action = '';
  }

  // Invalid action
  if (!action || action === '') return false;

  // Already being notified by push
  if (getPushSubscription() && modulePreferences[action].push) return false;

  return modulePreferences[action].browser || modulePreferences[action].push;
};

export const formatDate = (date: number, absolute?: boolean, format = 'MMM Do YY') =>
  absolute ? moment(date).format(format) : moment(date).fromNow();
