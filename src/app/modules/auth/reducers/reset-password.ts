import {
  REQUEST_RESET_LINK_FORM_SUBMITTED,
  REQUEST_RESET_LINK_FORM_SUCCEED,
  REQUEST_RESET_LINK_FORM_FAILED,
  RESET_PASSWORD_FORM_SUBMITTED,
  RESET_PASSWORD_FORM_SUCCEED,
  RESET_PASSWORD_FORM_FAILED,
} from '../actionTypes';

import { AuthState, ResetPasswordAction } from '../types';

const reducer = (state: AuthState, action: ResetPasswordAction) => {
  switch (action.type) {
    case REQUEST_RESET_LINK_FORM_SUBMITTED:
    case RESET_PASSWORD_FORM_SUBMITTED:
      return {
        ...state,
        isLoading: true,
      };

    case REQUEST_RESET_LINK_FORM_SUCCEED:
    case REQUEST_RESET_LINK_FORM_FAILED:
    case RESET_PASSWORD_FORM_SUCCEED:
    case RESET_PASSWORD_FORM_FAILED:
      return {
        ...state,
        isLoading: false,
      };

    default:
      return state;
  }
};

export default reducer;
