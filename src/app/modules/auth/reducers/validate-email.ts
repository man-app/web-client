import { AuthState, ValidateEmailAction, RegisterAction } from '../types';

import {
  EMAIL_VALIDATION_SUBMITTED,
  EMAIL_VALIDATION_SUCCEED,
  EMAIL_VALIDATION_FAILED,
  EMAIL_REQUEST_VERIFICATION_SUBMITTED,
  EMAIL_REQUEST_VERIFICATION_FAILED,
} from '../actionTypes';

const reducer = (state: AuthState, action: ValidateEmailAction | RegisterAction) => {
  switch (action.type) {
    case EMAIL_VALIDATION_SUBMITTED:
    case EMAIL_REQUEST_VERIFICATION_SUBMITTED:
      return {
        ...state,
        isLoading: true,
      };

    case EMAIL_VALIDATION_SUCCEED:
    case EMAIL_VALIDATION_FAILED:
    case EMAIL_REQUEST_VERIFICATION_FAILED:
      return {
        ...state,
        isLoading: false,
      };

    default:
      return state;
  }
};

export default reducer;
