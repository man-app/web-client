import setupEnv from '../../../../../common/utils/setup-test-env';

import reducer from '..';

import {
  REQUEST_RESET_LINK_FORM_SUBMITTED,
  REQUEST_RESET_LINK_FORM_SUCCEED,
  REQUEST_RESET_LINK_FORM_FAILED,
  RESET_PASSWORD_FORM_SUBMITTED,
  RESET_PASSWORD_FORM_SUCCEED,
  RESET_PASSWORD_FORM_FAILED,
} from '../../actionTypes';

beforeEach(() => {
  setupEnv();
});

describe('Auth reducer', () => {
  describe('Request change password link', () => {
    it('should mark isLoading as true', () => {
      const initialState = {
        user: undefined,
        isLoading: false,
      };

      const expectedState = {
        user: undefined,
        isLoading: true,
      };

      const nextState = reducer(initialState, { type: REQUEST_RESET_LINK_FORM_SUBMITTED });

      expect(nextState).toStrictEqual(expectedState);
    });

    it('should mark isLoading as false', () => {
      const initialState = {
        user: undefined,
        isLoading: true,
      };

      const expectedState = {
        user: undefined,
        isLoading: false,
      };

      const nextState = reducer(initialState, { type: REQUEST_RESET_LINK_FORM_SUCCEED });

      expect(nextState).toStrictEqual(expectedState);
    });

    it('should mark isLoading as false', () => {
      const initialState = {
        user: undefined,
        isLoading: true,
      };

      const expectedState = {
        user: undefined,
        isLoading: false,
      };

      const nextState = reducer(initialState, { type: REQUEST_RESET_LINK_FORM_FAILED });

      expect(nextState).toStrictEqual(expectedState);
    });
  });

  describe('Reset password', () => {
    it('should mark isLoading as true', () => {
      const initialState = {
        user: undefined,
        isLoading: false,
      };

      const expectedState = {
        user: undefined,
        isLoading: true,
      };

      const nextState = reducer(initialState, { type: RESET_PASSWORD_FORM_SUBMITTED });

      expect(nextState).toStrictEqual(expectedState);
    });

    it('should mark isLoading as false', () => {
      const initialState = {
        user: undefined,
        isLoading: true,
      };

      const expectedState = {
        user: undefined,
        isLoading: false,
      };

      const nextState = reducer(initialState, { type: RESET_PASSWORD_FORM_SUCCEED });

      expect(nextState).toStrictEqual(expectedState);
    });

    it('should mark isLoading as false', () => {
      const initialState = {
        user: undefined,
        isLoading: true,
      };

      const expectedState = {
        user: undefined,
        isLoading: false,
      };

      const nextState = reducer(initialState, { type: RESET_PASSWORD_FORM_FAILED });

      expect(nextState).toStrictEqual(expectedState);
    });
  });
});
