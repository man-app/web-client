import setupEnv from '../../../../../common/utils/setup-test-env';

import reducer from '..';

import { LOGOUT_SUBMITTED, LOGOUT_SUCCEED, LOGOUT_FAILED } from '../../actionTypes';

beforeEach(() => {
  setupEnv();
});

describe('Auth reducer', () => {
  describe('Logout action', () => {
    it('should mark isLoading as true', () => {
      const initialState = {
        user: undefined,
        isLoading: false,
      };

      const expectedState = {
        user: undefined,
        isLoading: true,
      };

      const nextState = reducer(initialState, { type: LOGOUT_SUBMITTED });

      expect(nextState).toStrictEqual(expectedState);
    });

    it('should return initial state', () => {
      const initialState = {
        user: undefined,
        isLoading: false,
      };

      const expectedState = {
        user: undefined,
        isLoading: false,
      };

      const nextState = reducer(initialState, { type: LOGOUT_SUCCEED });

      expect(nextState).toStrictEqual(expectedState);
    });

    it('should mark isLoading as false', () => {
      const initialState = {
        user: undefined,
        isLoading: true,
      };

      const expectedState = {
        user: undefined,
        isLoading: false,
      };

      const nextState = reducer(initialState, { type: LOGOUT_FAILED });

      expect(nextState).toStrictEqual(expectedState);
    });
  });
});
