import setupEnv from '../../../../../common/utils/setup-test-env';

import reducer from '..';

import { ACCOUNT_FORM_SUCCEED } from '../../../account/actionTypes';
import { COMPANY_CREATE_FORM_SUCCEED } from '../../../companies/actionTypes';

beforeEach(() => {
  setupEnv();
});

describe('Auth reducer', () => {
  describe('An action not related to this reducer', () => {
    it('should return current state', () => {
      const initialState = {
        user: {
          id: '1',
          fullName: 'User test',
          email: 'test@user.com',
          created: 123456789,
          lastModified: 123456789,
          isVerified: false,
        },
        isLoading: false,
      };

      const expectedState = {
        ...initialState,
        user: {
          ...initialState.user,
          jobs: [],
        },
      };

      const nextState = reducer(initialState, {
        type: COMPANY_CREATE_FORM_SUCCEED,
        payload: { users: [{ jobs: [] }] },
      });

      expect(nextState).toStrictEqual(expectedState);
    });
  });

  describe('Account form', () => {
    it('should update user', () => {
      const initialState = {
        user: {
          id: '1',
          fullName: 'User test',
          email: 'test@user.com',
          created: 123456789,
          lastModified: 123456789,
        },
        isLoading: false,
      };

      const expectedState = {
        user: {
          id: '1',
          fullName: 'Michael',
          email: 'mike@email.com',
          created: 123456789,
          lastModified: 123456789,
          isVerified: undefined,
        },
        isLoading: false,
      };

      const nextState = reducer(initialState, {
        type: ACCOUNT_FORM_SUCCEED,
        payload: { users: [{ fullName: 'Michael', email: 'mike@email.com' }] },
      });

      expect(nextState).toStrictEqual(expectedState);
    });
  });
});
