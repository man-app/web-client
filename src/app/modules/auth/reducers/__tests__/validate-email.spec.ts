import setupEnv from '../../../../../common/utils/setup-test-env';

import reducer from '..';

import { EMAIL_VALIDATION_SUBMITTED, EMAIL_VALIDATION_SUCCEED, EMAIL_VALIDATION_FAILED } from '../../actionTypes';

beforeEach(() => {
  setupEnv();
});

describe('Auth reducer', () => {
  describe('Validate email', () => {
    it('should mark isLoading as true', () => {
      const initialState = {
        user: undefined,
        isLoading: false,
      };

      const expectedState = {
        user: undefined,
        isLoading: true,
      };

      const nextState = reducer(initialState, { type: EMAIL_VALIDATION_SUBMITTED });

      expect(nextState).toStrictEqual(expectedState);
    });

    it('should mark isLoading as false', () => {
      const initialState = {
        user: undefined,
        isLoading: true,
      };

      const expectedState = {
        user: undefined,
        isLoading: false,
      };

      const nextState = reducer(initialState, { type: EMAIL_VALIDATION_SUCCEED });

      expect(nextState).toStrictEqual(expectedState);
    });

    it('should mark isLoading as false', () => {
      const initialState = {
        user: undefined,
        isLoading: true,
      };

      const expectedState = {
        user: undefined,
        isLoading: false,
      };

      const nextState = reducer(initialState, { type: EMAIL_VALIDATION_FAILED });

      expect(nextState).toStrictEqual(expectedState);
    });
  });
});
