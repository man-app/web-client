import setupEnv from '../../../../../common/utils/setup-test-env';

import reducer from '..';

import {
  EMAIL_UPDATE_REQUEST_SUBMITTED,
  EMAIL_UPDATE_REQUEST_SUCCEED,
  EMAIL_UPDATE_REQUEST_FAILED,
  EMAIL_UPDATE_FORM_SUBMITTED,
  EMAIL_UPDATE_FORM_SUCCEED,
  EMAIL_UPDATE_FORM_FAILED,
} from '../../actionTypes';

beforeEach(() => {
  setupEnv();
});

describe('Auth reducer', () => {
  describe('Request change email link', () => {
    it('should mark isLoading as true', () => {
      const initialState = {
        user: undefined,
        isLoading: false,
      };

      const expectedState = {
        user: undefined,
        isLoading: true,
      };

      const nextState = reducer(initialState, { type: EMAIL_UPDATE_REQUEST_SUBMITTED });

      expect(nextState).toStrictEqual(expectedState);
    });

    it('should mark isLoading as false', () => {
      const initialState = {
        user: undefined,
        isLoading: true,
      };

      const expectedState = {
        user: undefined,
        isLoading: false,
      };

      const nextState = reducer(initialState, { type: EMAIL_UPDATE_REQUEST_SUCCEED });

      expect(nextState).toStrictEqual(expectedState);
    });

    it('should mark isLoading as false', () => {
      const initialState = {
        user: undefined,
        isLoading: true,
      };

      const expectedState = {
        user: undefined,
        isLoading: false,
      };

      const nextState = reducer(initialState, { type: EMAIL_UPDATE_REQUEST_FAILED });

      expect(nextState).toStrictEqual(expectedState);
    });
  });

  describe('Reset email', () => {
    it('should mark isLoading as true', () => {
      const initialState = {
        user: undefined,
        isLoading: false,
      };

      const expectedState = {
        user: undefined,
        isLoading: true,
      };

      const nextState = reducer(initialState, { type: EMAIL_UPDATE_FORM_SUBMITTED });

      expect(nextState).toStrictEqual(expectedState);
    });

    it('should mark isLoading as false', () => {
      const initialState = {
        user: undefined,
        isLoading: true,
      };

      const expectedState = {
        user: undefined,
        isLoading: false,
      };

      const nextState = reducer(initialState, { type: EMAIL_UPDATE_FORM_SUCCEED });

      expect(nextState).toStrictEqual(expectedState);
    });

    it('should mark isLoading as false', () => {
      const initialState = {
        user: undefined,
        isLoading: true,
      };

      const expectedState = {
        user: undefined,
        isLoading: false,
      };

      const nextState = reducer(initialState, { type: EMAIL_UPDATE_FORM_FAILED });

      expect(nextState).toStrictEqual(expectedState);
    });
  });
});
