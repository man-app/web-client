import setupEnv from '../../../../../common/utils/setup-test-env';

import reducer from '..';

import { REGISTER_FORM_SUBMITTED, REGISTER_FORM_SUCCEED, REGISTER_FORM_FAILED } from '../../actionTypes';

beforeEach(() => {
  setupEnv();
});

const user = {
  id: 1,
  name: 'User test',
};

describe('Auth reducer', () => {
  describe('Register', () => {
    it('should mark isLoading as true', () => {
      const initialState = {
        user: undefined,
        isLoading: false,
      };

      const expectedState = {
        user: undefined,
        isLoading: true,
      };

      const nextState = reducer(initialState, { type: REGISTER_FORM_SUBMITTED });

      expect(nextState).toStrictEqual(expectedState);
    });

    it('should update state with recently created user', () => {
      const initialState = {
        user: undefined,
        isLoading: true,
      };

      const expectedState = {
        user,
        isLoading: false,
      };

      const nextState = reducer(initialState, { type: REGISTER_FORM_SUCCEED, payload: { user } });

      expect(nextState).toStrictEqual(expectedState);
    });

    it('should mark isLoading as false', () => {
      const initialState = {
        user: undefined,
        isLoading: true,
      };

      const expectedState = {
        user: undefined,
        isLoading: false,
      };

      const nextState = reducer(initialState, { type: REGISTER_FORM_FAILED });

      expect(nextState).toStrictEqual(expectedState);
    });
  });
});
