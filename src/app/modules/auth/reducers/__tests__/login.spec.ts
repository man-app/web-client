import setupEnv from '../../../../../common/utils/setup-test-env';

import reducer from '..';

import {
  AUTOLOGIN_SUBMITTED,
  AUTOLOGIN_FAILED,
  AUTOLOGIN_SUCCEED,
  LOGIN_FORM_SUBMITTED,
  LOGIN_FORM_FAILED,
  LOGIN_FORM_SUCCEED,
} from '../../actionTypes';

beforeEach(() => {
  setupEnv();
});

describe('Auth reducer', () => {
  describe('Autologin', () => {
    it('should mark reducer as isLoading', () => {
      const initialState = {
        user: undefined,
        isLoading: false,
      };

      const expectedState = {
        user: undefined,
        isLoading: true,
      };

      const nextState = reducer(initialState, { type: AUTOLOGIN_SUBMITTED });

      expect(nextState).toStrictEqual(expectedState);
    });

    it('should store user data', () => {
      const initialState = {
        user: undefined,
        isLoading: false,
      };

      const expectedState = {
        user: { id: '1', fullName: 'Mike', email: 'mike@test.com', created: 123456789, lastModified: 123456789 },
        isLoading: false,
      };

      const nextState = reducer(initialState, {
        type: AUTOLOGIN_SUCCEED,
        payload: {
          users: [
            {
              id: '1',
              fullName: 'Mike',
              email: 'mike@test.com',
              created: 123456789,
              lastModified: 123456789,
            },
          ],
        },
      });

      expect(nextState).toStrictEqual(expectedState);
    });

    it('should mark reducer as not fetching and remove existing user', () => {
      const initialState = {
        user: {
          id: '1',
          fullName: 'User test',
          email: 'test@email.com',
          created: 123456789,
          lastModified: 123456789,
        },
        isLoading: true,
      };

      const expectedState = {
        user: undefined,
        isLoading: false,
      };

      const nextState = reducer(initialState, { type: AUTOLOGIN_FAILED });

      expect(nextState).toStrictEqual(expectedState);
    });
  });

  describe('login', () => {
    it('should mark reducer as isLoading', () => {
      const initialState = {
        user: undefined,
        isLoading: false,
      };

      const expectedState = {
        user: undefined,
        isLoading: true,
      };

      const nextState = reducer(initialState, { type: LOGIN_FORM_SUBMITTED });

      expect(nextState).toStrictEqual(expectedState);
    });

    it('should store user data', () => {
      const initialState = {
        user: undefined,
        isLoading: false,
      };

      const expectedState = {
        user: { name: 'Mike' },
        isLoading: false,
      };

      const nextState = reducer(initialState, {
        type: LOGIN_FORM_SUCCEED,
        payload: {
          users: [
            {
              name: 'Mike',
            },
          ],
        },
      });

      expect(nextState).toStrictEqual(expectedState);
    });

    it('should mark reducer as not fetching', () => {
      const initialState = {
        user: undefined,
        isLoading: true,
      };

      const expectedState = {
        user: undefined,
        isLoading: false,
      };

      const nextState = reducer(initialState, { type: LOGIN_FORM_FAILED });

      expect(nextState).toStrictEqual(expectedState);
    });
  });
});
