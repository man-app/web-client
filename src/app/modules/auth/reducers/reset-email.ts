import { AuthState, ResetEmailAction } from '../types';
import {
  EMAIL_UPDATE_REQUEST_SUBMITTED,
  EMAIL_UPDATE_FORM_SUBMITTED,
  EMAIL_UPDATE_REQUEST_SUCCEED,
  EMAIL_UPDATE_REQUEST_FAILED,
  EMAIL_UPDATE_FORM_SUCCEED,
  EMAIL_UPDATE_FORM_FAILED,
} from '../actionTypes';

const reducer = (state: AuthState, action: ResetEmailAction) => {
  switch (action.type) {
    case EMAIL_UPDATE_REQUEST_SUBMITTED:
    case EMAIL_UPDATE_FORM_SUBMITTED:
      return {
        ...state,
        isLoading: true,
      };

    case EMAIL_UPDATE_REQUEST_SUCCEED:
    case EMAIL_UPDATE_REQUEST_FAILED:
    case EMAIL_UPDATE_FORM_SUCCEED:
    case EMAIL_UPDATE_FORM_FAILED:
      return {
        ...state,
        isLoading: false,
      };

    default:
      return state;
  }
};

export default reducer;
