import { updateUser } from '../utils/user';

import { REGISTER_FORM_SUBMITTED, REGISTER_FORM_SUCCEED, REGISTER_FORM_FAILED } from '../actionTypes';

import { AuthState } from '../types';
import { RegisterAction } from '../types/register';

const reducer = (state: AuthState, action: RegisterAction): AuthState => {
  switch (action.type) {
    case REGISTER_FORM_SUBMITTED:
      return {
        ...state,
        isLoading: true,
      };

    case REGISTER_FORM_SUCCEED:
      const { user: userFromServer } = action.payload as AuthState;
      return {
        ...state,
        isLoading: false,
        user: updateUser(state.user, action.type, userFromServer),
      };

    case REGISTER_FORM_FAILED:
      return {
        ...state,
        isLoading: false,
      };

    default:
      return state;
  }
};

export default reducer;
