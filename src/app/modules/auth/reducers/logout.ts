import { LOGOUT_SUBMITTED, LOGOUT_FAILED, LOGOUT_SUCCEED } from '../actionTypes';
import { getState } from '../models';
import { AuthState, LogoutAction } from '../types';

const reducer = (state: AuthState, action: LogoutAction) => {
  switch (action.type) {
    case LOGOUT_SUBMITTED:
      return {
        ...state,
        isLoading: true,
      };

    case LOGOUT_SUCCEED:
      return {
        ...getState(),
      };

    case LOGOUT_FAILED:
      return {
        ...state,
        isLoading: false,
      };

    default:
      return state;
  }
};

export default reducer;
