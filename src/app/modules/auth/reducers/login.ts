import { updateUser } from '../utils/user';

import {
  AUTOLOGIN_SUBMITTED,
  AUTOLOGIN_SUCCEED,
  AUTOLOGIN_FAILED,
  LOGIN_FORM_SUBMITTED,
  LOGIN_FORM_SUCCEED,
  LOGIN_FORM_FAILED,
} from '../actionTypes';

import { AuthState, LoginAction } from '../types';

const reducer = (state: AuthState, action: LoginAction) => {
  switch (action.type) {
    case AUTOLOGIN_SUBMITTED:
    case LOGIN_FORM_SUBMITTED:
      return {
        ...state,
        isLoading: true,
      };

    case AUTOLOGIN_SUCCEED:
    case LOGIN_FORM_SUCCEED:
      const { users: usersFromServer } = action.payload;
      return {
        ...state,
        isLoading: false,
        user:
          usersFromServer && usersFromServer.length
            ? updateUser(state.user, action.type, usersFromServer[0])
            : state.user,
      };

    case AUTOLOGIN_FAILED:
    case LOGIN_FORM_FAILED:
      return {
        ...state,
        user: undefined,
        isLoading: false,
      };

    default:
      return state;
  }
};

export default reducer;
