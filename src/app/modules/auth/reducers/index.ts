import { updateUser } from '../utils/user';

import registerReducer from './register';
import loginReducer from './login';
import logoutReducer from './logout';
import resetEmailReducer from './reset-email';
import resetPasswordReducer from './reset-password';
import validateEmailReducer from './validate-email';

import { PUBLISH_SOCIAL_PROFILE } from '../../../modules/social/actionTypes';

import { ACCOUNT_FORM_SUCCEED, MODULES_NOTIFICATIONS_FORM_SUCCEED } from '../../account/actionTypes';
import { COMPANY_CREATE_FORM_SUCCEED } from '../../companies/actionTypes';

import {
  REGISTER_FORM_SUBMITTED,
  REGISTER_FORM_SUCCEED,
  REGISTER_FORM_FAILED,
  AUTOLOGIN_SUBMITTED,
  AUTOLOGIN_SUCCEED,
  AUTOLOGIN_FAILED,
  LOGIN_FORM_SUBMITTED,
  LOGIN_FORM_SUCCEED,
  LOGIN_FORM_FAILED,
  LOGOUT_SUBMITTED,
  LOGOUT_FAILED,
  LOGOUT_SUCCEED,
  EMAIL_UPDATE_REQUEST_SUBMITTED,
  EMAIL_UPDATE_REQUEST_SUCCEED,
  EMAIL_UPDATE_REQUEST_FAILED,
  EMAIL_UPDATE_FORM_SUBMITTED,
  EMAIL_UPDATE_FORM_SUCCEED,
  EMAIL_UPDATE_FORM_FAILED,
  REQUEST_RESET_LINK_FORM_SUBMITTED,
  REQUEST_RESET_LINK_FORM_SUCCEED,
  REQUEST_RESET_LINK_FORM_FAILED,
  RESET_PASSWORD_FORM_SUBMITTED,
  RESET_PASSWORD_FORM_SUCCEED,
  RESET_PASSWORD_FORM_FAILED,
  EMAIL_VALIDATION_SUBMITTED,
  EMAIL_VALIDATION_SUCCEED,
  EMAIL_VALIDATION_FAILED,
  EMAIL_REQUEST_VERIFICATION_SUBMITTED,
  EMAIL_REQUEST_VERIFICATION_SUCCEED,
  EMAIL_REQUEST_VERIFICATION_FAILED,
} from '../actionTypes';

import { AccountAction } from '../../account/types';
import { CompaniesAction } from '../../companies/types';
import { GenericSocialAction } from '../../social/types';
import { getState } from '../models';
import {
  RegisterAction,
  LoginAction,
  LogoutAction,
  ResetEmailAction,
  ResetPasswordAction,
  ValidateEmailAction,
} from '../types';

const initialState = getState();

const reducer = (
  state = initialState,
  action:
    | RegisterAction
    | LoginAction
    | LogoutAction
    | ResetEmailAction
    | ResetPasswordAction
    | ValidateEmailAction
    | AccountAction
    | CompaniesAction
    | GenericSocialAction
) => {
  switch (action.type) {
    case REGISTER_FORM_SUBMITTED:
    case REGISTER_FORM_SUCCEED:
    case REGISTER_FORM_FAILED:
      return registerReducer(state, action as RegisterAction);

    case AUTOLOGIN_SUBMITTED:
    case AUTOLOGIN_SUCCEED:
    case AUTOLOGIN_FAILED:
    case LOGIN_FORM_SUBMITTED:
    case LOGIN_FORM_SUCCEED:
    case LOGIN_FORM_FAILED:
      return loginReducer(state, action as LoginAction);

    case LOGOUT_SUBMITTED:
    case LOGOUT_SUCCEED:
    case LOGOUT_FAILED:
      return logoutReducer(state, action as LogoutAction);

    case EMAIL_UPDATE_REQUEST_SUBMITTED:
    case EMAIL_UPDATE_REQUEST_SUCCEED:
    case EMAIL_UPDATE_REQUEST_FAILED:
    case EMAIL_UPDATE_FORM_SUBMITTED:
    case EMAIL_UPDATE_FORM_SUCCEED:
    case EMAIL_UPDATE_FORM_FAILED:
      return resetEmailReducer(state, action as ResetEmailAction);

    case REQUEST_RESET_LINK_FORM_SUBMITTED:
    case REQUEST_RESET_LINK_FORM_SUCCEED:
    case REQUEST_RESET_LINK_FORM_FAILED:
    case RESET_PASSWORD_FORM_SUBMITTED:
    case RESET_PASSWORD_FORM_SUCCEED:
    case RESET_PASSWORD_FORM_FAILED:
      return resetPasswordReducer(state, action as ResetPasswordAction);

    case EMAIL_VALIDATION_SUBMITTED:
    case EMAIL_VALIDATION_SUCCEED:
    case EMAIL_VALIDATION_FAILED:
    case EMAIL_REQUEST_VERIFICATION_SUBMITTED:
    case EMAIL_REQUEST_VERIFICATION_FAILED:
      return validateEmailReducer(state, action as ValidateEmailAction);

    // Need to get user data with new job related to new company
    case COMPANY_CREATE_FORM_SUCCEED:
    // Need to get user data to verify account is validated
    case EMAIL_REQUEST_VERIFICATION_SUCCEED:
    // Need to get new user data to verify it was saved
    case ACCOUNT_FORM_SUCCEED:
    // Need to get user data with new notifications settings
    case MODULES_NOTIFICATIONS_FORM_SUCCEED:
    // Need to get user data with new social profile info
    case PUBLISH_SOCIAL_PROFILE:
      return {
        ...state,
        isLoading: false,
        // @ts-ignore
        user: updateUser(state.user, action.type, action.payload && action.payload.users[0]),
      };

    default:
      return state;
  }
};

export default reducer;
