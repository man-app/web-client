import analyticsApi from '../../../../common/apis/reports/analytics';
import manappApi from '../../../../common/apis/manapp';
import sentryApi from '../../../../common/apis/reports/sentry';

import {
  AUTOLOGIN_SUBMITTED,
  AUTOLOGIN_SUCCEED,
  AUTOLOGIN_FAILED,
  LOGIN_FORM_SUBMITTED,
  LOGIN_FORM_SUCCEED,
  LOGIN_FORM_FAILED,
} from '../actionTypes';
import { LoginActionCreator } from '../types';
import { createUsersFromServer } from '../models';

export const getResidualLogin: LoginActionCreator = (installationId: string) => dispatch => {
  dispatch({
    type: AUTOLOGIN_SUBMITTED,
  });

  return manappApi.auth
    .renewSession(installationId)
    .then(({ users }) => {
      const payload = {
        users: createUsersFromServer(users),
      };

      dispatch({
        type: AUTOLOGIN_SUCCEED,
        payload,
      });

      if (payload.users.length) {
        // @ts-ignore
        analyticsApi.setUser(payload.users[0]);
        // @ts-ignore
        sentryApi.setUser(payload.users[0]);
      }

      return payload;
    })
    .catch(err => {
      dispatch({
        type: AUTOLOGIN_FAILED,
        payload: err,
      });

      return err;
    });
};

export const getLogin: LoginActionCreator = payload => dispatch => {
  const { form, ...params } = payload;
  dispatch({
    type: LOGIN_FORM_SUBMITTED,
    payload: {
      form,
    },
  });

  return manappApi.auth
    .login(params)
    .then(({ users }) => {
      const payload = {
        form,
        users: createUsersFromServer(users),
      };

      dispatch({
        type: LOGIN_FORM_SUCCEED,
        payload,
      });

      if (payload.users.length) {
        // @ts-ignore
        analyticsApi.setUser(payload.users[0]);
        // @ts-ignore
        sentryApi.setUser(payload.users[0]);
      }

      return payload;
    })
    .catch(err => {
      dispatch({
        type: LOGIN_FORM_FAILED,
        payload: {
          form,
          ...err,
        },
      });

      return err;
    });
};
