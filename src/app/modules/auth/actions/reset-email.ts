import manappApi from '../../../../common/apis/manapp';

import {
  EMAIL_UPDATE_REQUEST_SUBMITTED,
  EMAIL_UPDATE_REQUEST_SUCCEED,
  EMAIL_UPDATE_REQUEST_FAILED,
  EMAIL_UPDATE_FORM_SUBMITTED,
  EMAIL_UPDATE_FORM_SUCCEED,
  EMAIL_UPDATE_FORM_FAILED,
} from '../actionTypes';

import { ResetEmailActionCreator } from '../types';

export const requestChangeEmailLink: ResetEmailActionCreator = () => dispatch => {
  dispatch({
    type: EMAIL_UPDATE_REQUEST_SUBMITTED,
  });

  return manappApi.account
    .requestResetEmailLink()
    .then(() => {
      dispatch({
        type: EMAIL_UPDATE_REQUEST_SUCCEED,
      });
    })
    .catch(err => {
      dispatch({
        type: EMAIL_UPDATE_REQUEST_FAILED,
        payload: err,
      });

      return err;
    });
};

export const resetEmail: ResetEmailActionCreator = payload => dispatch => {
  const { form, ...params } = payload;

  dispatch({
    type: EMAIL_UPDATE_FORM_SUBMITTED,
    payload: { form },
  });

  return manappApi.account
    .resetEmail(params)
    .then(({ notifications }) => {
      const payload = {
        form,
        notifications,
      };

      dispatch({
        type: EMAIL_UPDATE_FORM_SUCCEED,
        payload,
      });

      return payload;
    })
    .catch(err => {
      dispatch({
        type: EMAIL_UPDATE_FORM_FAILED,
        payload: {
          form,
          ...err,
        },
      });

      return err;
    });
};
