import manappApi from '../../../../common/apis/manapp';

import {
  REGISTER_FORM_SUBMITTED,
  REGISTER_FORM_SUCCEED,
  REGISTER_FORM_FAILED,
  EMAIL_REQUEST_VERIFICATION_SUBMITTED,
  EMAIL_REQUEST_VERIFICATION_SUCCEED,
  EMAIL_REQUEST_VERIFICATION_FAILED,
} from '../actionTypes';

import { RegisterActionCreator } from '../types';
import { createUsersFromServer } from '../models';

export const register: RegisterActionCreator = params => dispatch => {
  const { form, ...parameters } = params;

  dispatch({
    type: REGISTER_FORM_SUBMITTED,
    payload: {
      form,
    },
  });

  return manappApi.auth
    .register(parameters)
    .then(({ notifications, users }) => {
      const payload = {
        form,
        notifications,
        users: createUsersFromServer(users),
      };

      dispatch({
        type: REGISTER_FORM_SUCCEED,
        payload,
      });

      return payload;
    })
    .catch(err => {
      dispatch({
        type: REGISTER_FORM_FAILED,
        payload: {
          form,
          ...err,
        },
      });

      return err;
    });
};

export const validateEmail: RegisterActionCreator = params => dispatch => {
  dispatch({
    type: EMAIL_REQUEST_VERIFICATION_SUBMITTED,
  });

  return manappApi.auth
    .validateEmail(params)
    .then(({ notifications, users }) => {
      const payload = {
        notifications,
        users: createUsersFromServer(users),
      };

      dispatch({
        type: EMAIL_REQUEST_VERIFICATION_SUCCEED,
        payload,
      });

      return payload;
    })
    .catch(err => {
      dispatch({
        type: EMAIL_REQUEST_VERIFICATION_FAILED,
        payload: {
          ...err,
        },
      });

      return err;
    });
};
