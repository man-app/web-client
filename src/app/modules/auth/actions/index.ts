import manappApi from '../../../../common/apis/manapp';
import { getInstallationData } from '../../../utils/installation';

export { register, validateEmail } from './register';
export { getResidualLogin, getLogin } from './login';
export { logout } from './logout';
export { requestChangeEmailLink, resetEmail } from './reset-email';
export { requestValidationLink } from './validate-email';

export const updateInstallationData = () => manappApi.auth.updateInstallationData(getInstallationData());
