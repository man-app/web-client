import manappApi from '../../../../common/apis/manapp';

import { EMAIL_VALIDATION_SUBMITTED, EMAIL_VALIDATION_SUCCEED, EMAIL_VALIDATION_FAILED } from '../actionTypes';

import { ValidateEmailActionCreator } from '../types';

export const requestValidationLink: ValidateEmailActionCreator = payload => dispatch => {
  dispatch({
    type: EMAIL_VALIDATION_SUBMITTED,
  });

  return manappApi.auth
    .requestValidationLink(payload)
    .then(({ notifications }) => {
      const payload = {
        notifications,
      };

      dispatch({
        type: EMAIL_VALIDATION_SUCCEED,
        payload,
      });

      return payload;
    })
    .catch(err => {
      dispatch({
        type: EMAIL_VALIDATION_FAILED,
        payload: {
          ...err,
        },
      });

      return err;
    });
};
