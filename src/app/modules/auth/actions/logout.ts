import manappApi from '../../../../common/apis/manapp';
import analyticsApi from '../../../../common/apis/reports/analytics';
import sentryApi from '../../../../common/apis/reports/sentry';

import { LOGOUT_SUBMITTED, LOGOUT_SUCCEED, LOGOUT_FAILED } from '../actionTypes';
import { LogoutActionCreator } from '../types';

export const logout: LogoutActionCreator = installationId => dispatch => {
  dispatch({
    type: LOGOUT_SUBMITTED,
  });

  return manappApi.auth
    .logout(installationId)
    .then(() => {
      dispatch({
        type: LOGOUT_SUCCEED,
      });

      analyticsApi.setUser(undefined);
      sentryApi.setUser(undefined);

      return undefined;
    })
    .catch(err => {
      dispatch({
        type: LOGOUT_FAILED,
        payload: err,
      });

      return err;
    });
};
