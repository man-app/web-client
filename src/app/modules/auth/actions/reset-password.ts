import manappApi from '../../../../common/apis/manapp';

import {
  REQUEST_RESET_LINK_FORM_SUBMITTED,
  REQUEST_RESET_LINK_FORM_SUCCEED,
  REQUEST_RESET_LINK_FORM_FAILED,
  RESET_PASSWORD_FORM_SUBMITTED,
  RESET_PASSWORD_FORM_SUCCEED,
  RESET_PASSWORD_FORM_FAILED,
} from '../actionTypes';

import { ResetPasswordActionCreator } from '../types';

export const requestResetPasswordLink: ResetPasswordActionCreator = payload => dispatch => {
  const { form, ...params } = payload;
  dispatch({
    type: REQUEST_RESET_LINK_FORM_SUBMITTED,
    payload: {
      form,
    },
  });

  return manappApi.auth
    .requestResetPasswordLink(params)
    .then(({ notifications }) => {
      const payload = {
        form,
        notifications,
      };

      dispatch({
        type: REQUEST_RESET_LINK_FORM_SUCCEED,
        payload,
      });

      return payload;
    })
    .catch(err => {
      dispatch({
        type: REQUEST_RESET_LINK_FORM_FAILED,
        payload: {
          form,
          ...err,
        },
      });

      return err;
    });
};

export const resetPassword: ResetPasswordActionCreator = payload => dispatch => {
  const { form, ...params } = payload;
  dispatch({
    type: RESET_PASSWORD_FORM_SUBMITTED,
    payload: {
      form,
    },
  });

  return manappApi.auth
    .resetPassword(params)
    .then(() => {
      dispatch({
        type: RESET_PASSWORD_FORM_SUCCEED,
      });

      return undefined;
    })
    .catch(err => {
      dispatch({
        type: RESET_PASSWORD_FORM_FAILED,
        payload: {
          form,
          ...err,
        },
      });

      return err;
    });
};
