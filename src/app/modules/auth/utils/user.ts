import { EMAIL_VALIDATION_SUCCEED } from '../actionTypes';
import { UserLogged, LoginActionType, LogoutActionType, ValidateEmailActionType, RegisterActionType } from '../types';

export const updateUser = (
  existingUser: UserLogged | undefined,
  actionType: RegisterActionType | LoginActionType | LogoutActionType | ValidateEmailActionType,
  newUser: UserLogged | undefined
): UserLogged | undefined => {
  // No previous user logged
  if (!existingUser && newUser) {
    return {
      ...newUser,
    };
  }

  // New user logged in
  if (newUser) {
    return {
      ...existingUser,
      ...newUser,
      isVerified: actionType === EMAIL_VALIDATION_SUCCEED ? true : existingUser && existingUser.isVerified,
    };
  }

  return existingUser;
};

export const updateUserPlatforms = (user: UserLogged, platform: string, isEnabled: boolean) => {
  return {
    ...user,
    [platform]: isEnabled,
  };
};
