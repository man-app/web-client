import { UserLogged, AuthState, Job, NotificationPreference } from '../types';
import { UserFromServer, NotificationPreferencesFromServer } from '../../../../common/apis/manapp';
import { error } from '../../../../common/utils/logger';

export const getState = (user?: UserLogged): AuthState => ({
  isLoading: false,
  user,
});

export const createJobFromServer = (job: any): Job | undefined => {
  if (typeof job.idCompany === 'undefined') {
    error('Auth: createJobFromServer: Invalid model from server. Some of the following properties is missing', {
      idCompany: job.idCompany,
    });

    return undefined;
  }

  return job as Job;
};

// @ts-ignore
export const createJobsFromServer = (jobs: any[]): Job[] => jobs.map(createJobFromServer).filter(job => !!job);

export const createNotificationPreferenceFromServer = ({
  approving,
  creating,
  deleting,
  disabling,
  editing,
  idCompany,
  idModule,
  listing,
}: NotificationPreferencesFromServer): NotificationPreference | undefined => {
  if (typeof idCompany === 'undefined' || typeof idModule === 'undefined') {
    error(
      'Auth: createNotificationPreferenceFromServer: Invalid model from server. Some of the following properties is missing',
      {
        idCompany,
        idModule,
      }
    );

    return undefined;
  }

  return {
    idCompany: String(idCompany),
    idModule: String(idModule),
    listing: {
      browser: !!(listing && listing.browser),
      email: !!(listing && listing.email),
      push: !!(listing && listing.push),
    },
    creating: {
      browser: !!(creating && creating.browser),
      email: !!(creating && creating.email),
      push: !!(creating && creating.push),
    },
    editing: {
      browser: !!(editing && editing.browser),
      email: !!(editing && editing.email),
      push: !!(editing && editing.push),
    },
    approving: {
      browser: !!(approving && approving.browser),
      email: !!(approving && approving.email),
      push: !!(approving && approving.push),
    },
    disabling: {
      browser: !!(disabling && disabling.browser),
      email: !!(disabling && disabling.email),
      push: !!(disabling && disabling.push),
    },
    deleting: {
      browser: !!(deleting && deleting.browser),
      email: !!(deleting && deleting.email),
      push: !!(deleting && deleting.push),
    },
  };
};

export const createNotificationPreferencesFromServer = (notifications: any[]): NotificationPreference[] =>
  // @ts-ignore
  notifications.map(createNotificationPreferenceFromServer).filter(notif => !!notif);

export const createUserFromServer = ({
  id,
  fullName,
  email,
  picture,
  document,
  contactEmail,
  contactPhone,
  address,
  city,
  state,
  country,
  jobs,
  created,
  lastModified,
  linkedin,
  facebook,
  instagram,
  twitter,
  youtube,
  notifications,
  isVerified,
  newsletter,
}: UserFromServer): UserLogged | undefined => {
  if (
    typeof id === 'undefined' ||
    typeof fullName === 'undefined' ||
    typeof email === 'undefined' ||
    typeof created === 'undefined'
  ) {
    error('Auth: Invalid model from server. Some of the following properties is missing', {
      id,
      fullName,
      email,
      created,
    });

    return undefined;
  }

  return {
    id: String(id),

    email: String(email),
    fullName: String(fullName),
    picture: picture ? String(picture) : undefined,

    document: document ? String(document) : undefined,
    contactEmail: contactEmail ? String(contactEmail) : undefined,
    contactPhone: contactPhone ? String(contactPhone) : undefined,
    address: address ? String(address) : undefined,
    city: city ? String(city) : undefined,
    state: state ? String(state) : undefined,
    country: country ? String(country) : undefined,

    // TODO Jobs
    jobs: jobs ? createJobsFromServer(jobs) : [],

    created: Number(created),
    lastModified: Number(lastModified || created),

    facebook: facebook ? true : undefined,
    instagram: instagram ? true : undefined,
    linkedin: linkedin ? true : undefined,
    twitter: twitter ? true : undefined,
    youtube: youtube ? true : undefined,

    notifications: notifications ? createNotificationPreferencesFromServer(notifications) : [],

    isVerified: !!isVerified,
    newsletter: !!newsletter,
  };
};

export const createUsersFromServer = (users: UserFromServer[]): UserLogged[] =>
  // @ts-ignore
  users.map(createUserFromServer).filter(user => !!user);
