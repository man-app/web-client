import { Dispatch } from 'redux';

export type ValidateEmailActionType =
  | 'EMAIL_VALIDATION_SUBMITTED'
  | 'EMAIL_VALIDATION_SUCCEED'
  | 'EMAIL_VALIDATION_FAILED';

export interface ValidateEmailAction {
  type: ValidateEmailActionType;
  payload?: any;
}

export type ValidateEmailActionCreator = (payload?: any) => (dispatch: Dispatch<ValidateEmailAction>) => any;
