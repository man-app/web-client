export { LoginParams as UserLogin, RegisterParams as UserRegister } from '../../../../common/apis/manapp';
export { RegisterActionType, RegisterAction, RegisterActionCreator } from './register';
export { LoginActionType, LoginAction, LoginActionCreator } from './login';
export { LogoutActionType, LogoutAction, LogoutActionCreator } from './logout';
export { ResetEmailActionType, ResetEmailAction, ResetEmailActionCreator } from './reset-email';
export { ResetPasswordActionType, ResetPasswordAction, ResetPasswordActionCreator } from './reset-password';
export { ValidateEmailActionType, ValidateEmailAction, ValidateEmailActionCreator } from './validate-email';

export interface ModulePermission {
  idModule: string;
  listing: boolean;
  creating: boolean;
  editing: boolean;
  approving: boolean;
  disabling: boolean;
  deleting: boolean;
}

export interface Job {
  idCompany: string;
  permissions: { [key: string]: ModulePermission };
}

export interface NotificationBrowserOptions {
  browser: boolean;
  email: boolean;
  push: boolean;
}

export interface NotificationPreference {
  [index: string]: any;

  idCompany: string;
  idModule: string;
  listing: NotificationBrowserOptions;
  creating: NotificationBrowserOptions;
  editing: NotificationBrowserOptions;
  approving: NotificationBrowserOptions;
  disabling: NotificationBrowserOptions;
  deleting: NotificationBrowserOptions;
}

interface User {
  [key: string]: any;
  id: string;

  // Profile
  fullName: string;
  email: string;
  picture?: string;

  // Contact
  document?: string;
  contactEmail?: string;
  contactPhone?: string;
  address?: string;
  city?: string;
  state?: string;
  country?: string;

  // Companies & Permissions
  jobs?: Job[];

  // Misc
  created: number;
  lastModified: number;
}

export interface UserProfile extends User {
  // Social
  linkedin?: string;
  facebook?: string;
  instagram?: string;
  twitter?: string;
  youtube?: string;
}

export interface UserLogged extends User {
  // Social
  linkedin?: boolean;
  facebook?: boolean;
  instagram?: boolean;
  twitter?: boolean;
  youtube?: boolean;

  // Preferences
  notifications?: NotificationPreference[];

  // Misc
  isVerified?: boolean;
  newsletter?: boolean | void;
}

export interface AuthState {
  user?: UserLogged;
  isLoading: boolean;
}
