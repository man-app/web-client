import { Dispatch } from 'redux';

export type LoginActionType =
  | 'AUTOLOGIN_SUBMITTED'
  | 'AUTOLOGIN_SUCCEED'
  | 'AUTOLOGIN_FAILED'
  | 'LOGIN_FORM_SUBMITTED'
  | 'LOGIN_FORM_SUCCEED'
  | 'LOGIN_FORM_FAILED';

export interface LoginAction {
  type: LoginActionType;
  payload?: any;
}

export type LoginActionCreator = (payload?: any) => (dispatch: Dispatch<LoginAction>) => any;
