import { Dispatch } from 'redux';

export type ResetEmailActionType =
  | 'EMAIL_UPDATE_REQUEST_SUBMITTED'
  | 'EMAIL_UPDATE_REQUEST_SUCCEED'
  | 'EMAIL_UPDATE_REQUEST_FAILED'
  | 'EMAIL_UPDATE_FORM_SUBMITTED'
  | 'EMAIL_UPDATE_FORM_SUCCEED'
  | 'EMAIL_UPDATE_FORM_FAILED';

export interface ResetEmailAction {
  type: ResetEmailActionType;
  payload?: any;
}

export type ResetEmailActionCreator = (payload?: any) => (dispatch: Dispatch<ResetEmailAction>) => any;
