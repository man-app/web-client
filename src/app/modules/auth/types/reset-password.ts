import { Dispatch } from 'redux';

export type ResetPasswordActionType =
  | 'REQUEST_RESET_LINK_FORM_SUBMITTED'
  | 'REQUEST_RESET_LINK_FORM_SUCCEED'
  | 'REQUEST_RESET_LINK_FORM_FAILED'
  | 'RESET_PASSWORD_FORM_SUBMITTED'
  | 'RESET_PASSWORD_FORM_SUCCEED'
  | 'RESET_PASSWORD_FORM_FAILED';

export interface ResetPasswordAction {
  type: ResetPasswordActionType;
  payload?: any;
}

export type ResetPasswordActionCreator = (payload?: any) => (dispatch: Dispatch<ResetPasswordAction>) => any;
