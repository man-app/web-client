import { Dispatch } from 'redux';

export type LogoutActionType = 'LOGOUT_SUBMITTED' | 'LOGOUT_SUCCEED' | 'LOGOUT_FAILED';

export interface LogoutAction {
  type: LogoutActionType;
}

export type LogoutActionCreator = (installationId?: string) => (dispatch: Dispatch<LogoutAction>) => any;
