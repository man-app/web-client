import { Dispatch } from 'redux';

export type RegisterActionType =
  | 'REGISTER_FORM_SUBMITTED'
  | 'REGISTER_FORM_SUCCEED'
  | 'REGISTER_FORM_FAILED'
  | 'EMAIL_REQUEST_VERIFICATION_SUBMITTED'
  | 'EMAIL_REQUEST_VERIFICATION_SUCCEED'
  | 'EMAIL_REQUEST_VERIFICATION_FAILED';

export interface RegisterAction {
  type: RegisterActionType;
  payload?: any;
}

export type RegisterActionCreator = (payload?: any) => (dispatch: Dispatch<RegisterAction>) => any;
