import rootReducer from '../../../../reducers';

import * as selectors from '..';

import { MODULES_UPDATED } from '../../../modules/actionTypes';
import { NotificationPreference } from '../../types';

const modules = [
  {
    id: '1',
    name: 'Settings',
    listing: true,
    creating: true,
    editing: true,
    disabling: true,
    approving: false,
    deleting: true,
  },
  {
    id: '2',
    name: 'Roles',
    listing: true,
    creating: true,
    editing: true,
    disabling: true,
    approving: true,
    deleting: true,
  },
];

const company1 = {
  id: '1',
  name: 'Company 1',
  slug: 'company-1',
  modules: ['1', '2'],
  created: 123456789,
  lastModified: 123456789,
};

const company2 = {
  id: '2',
  name: 'Company 2',
  slug: 'company-2',
  modules: ['1', '2'],
  created: 123456789,
  lastModified: 123456789,
};

const loggedUser = {
  id: '1',
  fullName: 'User test',
  email: 'test@user.com',
  jobs: [
    {
      idCompany: '1',
      permissions: {
        '1': {
          idModule: '1',
          listing: true,
          creating: true,
          editing: true,
          disabling: true,
          approving: true,
          deleting: true,
        },
        '2': {
          idModule: '2',
          listing: false,
          creating: false,
          editing: false,
          disabling: false,
          approving: false,
          deleting: false,
        },
      },
    },
  ],
  notifications: [
    {
      id: 1,
      idCompany: '1',
      idModule: '1',
      listing: {
        browser: true,
        email: false,
        push: true,
      },
      creating: {
        browser: true,
        email: false,
        push: true,
      },
      editing: {
        browser: true,
        email: false,
        push: true,
      },
      disabling: {
        browser: true,
        email: false,
        push: true,
      },
      approving: {
        browser: true,
        email: false,
        push: true,
      },
      deleting: {
        browser: true,
        email: false,
        push: true,
      },
    },
  ],
  created: 123456789,
  lastModified: 123456789,
  isVerified: false,
};

describe('Auth selectors', () => {
  const rootState = rootReducer(undefined, { type: MODULES_UPDATED, payload: { modules } });
  const populatedState = {
    ...rootState,
    auth: {
      ...rootState.auth,
      user: loggedUser,
    },
    companies: {
      ...rootState.companies,
      collection: [company1, company2],
    },
  };

  describe('isLoadingAuth selector', () => {
    it('should return false', () => {
      const result = selectors.isLoadingAuth(rootState);
      const expectedResult = false;

      expect(result).toBe(expectedResult);
    });
  });

  describe('isLogged', () => {
    it('should return false', () => {
      const result = selectors.isLogged(rootState);
      const expectedResult = false;

      expect(result).toBe(expectedResult);
    });

    it('should return true', () => {
      const result = selectors.isLogged(populatedState);
      const expectedResult = true;

      expect(result).toBe(expectedResult);
    });
  });

  describe('isVerified', () => {
    it('should return false', () => {
      const result = selectors.isVerified(rootState);
      const expectedResult = false;

      expect(result).toBe(expectedResult);
    });

    it('should return false', () => {
      const result = selectors.isVerified(populatedState);
      const expectedResult = false;

      expect(result).toBe(expectedResult);
    });

    it('should return true', () => {
      const result = selectors.isVerified({
        ...populatedState,
        auth: {
          ...populatedState.auth,
          user: {
            ...populatedState.auth.user,
            isVerified: true,
          },
        },
      });
      const expectedResult = true;

      expect(result).toBe(expectedResult);
    });
  });

  describe('getLoggedUser', () => {
    it('should return user data', () => {
      const result = selectors.getLoggedUser(populatedState);
      const expectedResult = loggedUser;

      expect(result).toStrictEqual(expectedResult);
    });
  });

  describe('getUserNotificationPreferences', () => {
    it('should return an empty array', () => {
      const result = selectors.getUserNotificationPreferences(rootState);
      const expectedResult = [] as NotificationPreference[];

      expect(result).toStrictEqual(expectedResult);
    });

    it('should return user notification preferences', () => {
      const result = selectors.getUserNotificationPreferences(populatedState);
      const expectedResult = loggedUser.notifications;

      expect(result).toStrictEqual(expectedResult);
    });
  });

  describe('getUserProfile', () => {
    it('should return undefined', () => {
      const result = selectors.getUserProfile(rootState);

      expect(result).toBe(undefined);
    });

    it('should return user social profile', () => {
      const result = selectors.getUserProfile(populatedState);
      const expectedResult = {
        ...loggedUser,
        facebook: undefined,
        google: undefined,
        instagram: undefined,
        linkedin: undefined,
        twitter: undefined,
        youtube: undefined,
      };

      expect(result).toStrictEqual(expectedResult);
    });

    it('should return user social profile with social links', () => {
      const result = selectors.getUserProfile({
        ...populatedState,
        auth: {
          ...populatedState.auth,
          user: {
            ...populatedState.auth.user,
            facebook: true,
            google: true,
            instagram: true,
            linkedin: true,
            twitter: true,
            youtube: true,
          },
        },
      });
      const expectedResult = {
        ...loggedUser,
        facebook: '',
        google: '',
        instagram: '',
        linkedin: '',
        twitter: '',
        youtube: '',
      };

      expect(result).toStrictEqual(expectedResult);
    });
  });
});
