import { RootState } from '../../../types';
import { UserProfile } from '../types';
import {
  getLinkedinProfile,
  getFacebookProfile,
  getInstagramProfile,
  getTwitterProfile,
  getGoogleProfile,
} from '../../social/selectors';

export const isLoadingAuth = ({ auth }: RootState) => auth.isLoading;

export const isLogged = ({ auth }: RootState) => !!auth.user;

export const isVerified = ({ auth }: RootState) => !!(auth.user && auth.user.isVerified);

export const getLoggedUser = ({ auth }: RootState) => auth.user;

export const getUserNotificationPreferences = ({ auth }: RootState) =>
  auth.user && auth.user.notifications ? auth.user.notifications : [];

export const getUserProfile = (state: RootState) => {
  const loggedUser = getLoggedUser(state);
  let parsedUser: UserProfile;

  if (loggedUser) {
    const { linkedin, facebook, instagram, twitter, google, youtube, ...user } = loggedUser;

    const linkedinProfile = getLinkedinProfile(state);
    const facebookProfile = getFacebookProfile(state);
    const instagramProfile = getInstagramProfile(state);
    const twitterProfile = getTwitterProfile(state);
    const googleProfile = getGoogleProfile(state);

    user.linkedin = linkedin ? linkedinProfile : undefined;
    user.facebook = facebook ? facebookProfile : undefined;
    user.instagram = instagram ? instagramProfile : undefined;
    user.twitter = twitter ? twitterProfile : undefined;
    user.google = google ? googleProfile : undefined;
    user.youtube = youtube ? googleProfile : undefined;
    parsedUser = user;

    return parsedUser;
  }

  return undefined;
};
