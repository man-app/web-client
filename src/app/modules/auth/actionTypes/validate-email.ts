import { ValidateEmailActionType } from '../types';

export const EMAIL_VALIDATION_SUBMITTED: ValidateEmailActionType = 'EMAIL_VALIDATION_SUBMITTED';
export const EMAIL_VALIDATION_SUCCEED: ValidateEmailActionType = 'EMAIL_VALIDATION_SUCCEED';
export const EMAIL_VALIDATION_FAILED: ValidateEmailActionType = 'EMAIL_VALIDATION_FAILED';
