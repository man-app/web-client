import { ResetPasswordActionType } from '../types/reset-password';

export const REQUEST_RESET_LINK_FORM_SUBMITTED: ResetPasswordActionType = 'REQUEST_RESET_LINK_FORM_SUBMITTED';
export const REQUEST_RESET_LINK_FORM_SUCCEED: ResetPasswordActionType = 'REQUEST_RESET_LINK_FORM_SUCCEED';
export const REQUEST_RESET_LINK_FORM_FAILED: ResetPasswordActionType = 'REQUEST_RESET_LINK_FORM_FAILED';
export const RESET_PASSWORD_FORM_SUBMITTED: ResetPasswordActionType = 'RESET_PASSWORD_FORM_SUBMITTED';
export const RESET_PASSWORD_FORM_SUCCEED: ResetPasswordActionType = 'RESET_PASSWORD_FORM_SUCCEED';
export const RESET_PASSWORD_FORM_FAILED: ResetPasswordActionType = 'RESET_PASSWORD_FORM_FAILED';
