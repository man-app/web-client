import { LogoutActionType } from '../types/logout';

export const LOGOUT_SUBMITTED: LogoutActionType = 'LOGOUT_SUBMITTED';
export const LOGOUT_SUCCEED: LogoutActionType = 'LOGOUT_SUCCEED';
export const LOGOUT_FAILED: LogoutActionType = 'LOGOUT_FAILED';
