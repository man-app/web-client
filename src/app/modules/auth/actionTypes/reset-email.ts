import { ResetEmailActionType } from '../types';

export const EMAIL_UPDATE_REQUEST_SUBMITTED: ResetEmailActionType = 'EMAIL_UPDATE_REQUEST_SUBMITTED';
export const EMAIL_UPDATE_REQUEST_SUCCEED: ResetEmailActionType = 'EMAIL_UPDATE_REQUEST_SUCCEED';
export const EMAIL_UPDATE_REQUEST_FAILED: ResetEmailActionType = 'EMAIL_UPDATE_REQUEST_FAILED';

export const EMAIL_UPDATE_FORM_SUBMITTED: ResetEmailActionType = 'EMAIL_UPDATE_FORM_SUBMITTED';
export const EMAIL_UPDATE_FORM_SUCCEED: ResetEmailActionType = 'EMAIL_UPDATE_FORM_SUCCEED';
export const EMAIL_UPDATE_FORM_FAILED: ResetEmailActionType = 'EMAIL_UPDATE_FORM_FAILED';
