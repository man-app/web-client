import { LoginActionType } from '../types';

export const AUTOLOGIN_SUBMITTED: LoginActionType = 'AUTOLOGIN_SUBMITTED';
export const AUTOLOGIN_SUCCEED: LoginActionType = 'AUTOLOGIN_SUCCEED';
export const AUTOLOGIN_FAILED: LoginActionType = 'AUTOLOGIN_FAILED';
export const LOGIN_FORM_SUBMITTED: LoginActionType = 'LOGIN_FORM_SUBMITTED';
export const LOGIN_FORM_SUCCEED: LoginActionType = 'LOGIN_FORM_SUCCEED';
export const LOGIN_FORM_FAILED: LoginActionType = 'LOGIN_FORM_FAILED';
