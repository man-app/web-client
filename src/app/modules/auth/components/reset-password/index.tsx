import React from 'react';
import useReactRouter from 'use-react-router';

import View from './components/view';

const ResetPassword: React.FC = () => {
  const {
    match: { params },
  } = useReactRouter();

  return <View email={params.email} code={params.code} />;
};

export default ResetPassword;
