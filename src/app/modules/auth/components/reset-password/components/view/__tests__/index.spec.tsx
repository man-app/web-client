import React from 'react';
import { shallow } from 'enzyme';

import View from '..';

describe('ResetPassword', () => {
  it('Should render a valid screen for changing password', () => {
    const component = shallow(<View code="123" email="test@email.com" />);
    expect(component).toMatchSnapshot();
  });

  it('Should render a valid screen for requesting change password link', () => {
    const component = shallow(<View code="" email="" />);
    expect(component).toMatchSnapshot();
  });
});
