import React from 'react';

import { Page } from '../../../../../../components/page';
import { Card } from '../../../../../../components/card';
import ResetPasswordForm from '../form';

import { MAX_WIDTH_SM } from '../../../../../../../common/constants/styles/sizes';

interface Props {
  email: string;
  code: string;
}

const View: React.FC<Props> = ({ code, email }: Props) => (
  <Page isCentered maxWidth={MAX_WIDTH_SM}>
    <Card>
      <div>
        {(!email || !code) && (
          <>
            <p>We are going to send you an email with a link to change your password.</p>
            <br />
            <p>Please, type the email of the account you want to modify.</p>
          </>
        )}

        {email && code && <p>Setup your new password</p>}

        <br />
        <ResetPasswordForm email={email} code={code} />
      </div>
    </Card>
  </Page>
);

export default View;
