import React from 'react';
import { shallow } from 'enzyme';

import ResetPasswordForm from '..';

import { FieldProps } from '../../../../../../../../../components/forms/types';
import { ButtonProps } from '../../../../../../../../../components/touchable/types';

describe('ResetPasswordForm', () => {
  it('Should render a valid ResetPasswordForm view', () => {
    const buttons = [] as ButtonProps[];
    const fields = [] as FieldProps[];
    const form = 'login';
    const handleSubmit = () => {};
    const isLoading = false;

    const component = shallow(
      <ResetPasswordForm
        buttons={buttons}
        fields={fields}
        form={form}
        handleSubmit={handleSubmit}
        isLoading={isLoading}
      />
    );

    expect(component).toMatchSnapshot();
  });
});
