import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import useReactRouter from 'use-react-router';
import { mapServerErrors, isValidForm } from '../../../../../../utils/error-handler';

import { getFormErrors } from '../../../../../../components/forms/selectors';
import { requestResetPasswordLink, resetPassword } from '../../../../actions/reset-password';
import { isLoadingAuth, getLoggedUser } from '../../../../selectors';

import View from './components/view';

import { HOME, LOGOUT } from '../../../../../../../common/constants/appRoutes';
import { SEND_ICON, SEND_LABEL, GOBACK_ICON, GOBACK_LABEL } from '../../../../../../../common/constants/buttons';
import { EMAIL, PASSWORD, SUBMIT, BUTTON } from '../../../../../../components/forms/models';
import { FieldProps } from '../../../../../../components/forms/types';
import { ButtonProps } from '../../../../../../components/touchable/types';

const form = 'resetPassword';

interface Props {
  email: string;
  code: string;
}

const ResetPasswordForm: React.FC<Props> = ({ code, email }: Props) => {
  const dispatch = useDispatch();
  const { history } = useReactRouter();

  const user = useSelector(getLoggedUser);
  const errors = useSelector(getFormErrors)(form);
  const isLoading = useSelector(isLoadingAuth);

  const [requestLinkFields, setRequestLinkFields] = React.useState([
    {
      id: 'email',
      isRequired: true,
      label: 'Email',
      model: user && user.email,
      type: EMAIL,
    },
  ] as FieldProps[]);

  const [resetPasswordFields, setResetPasswordFields] = React.useState([
    {
      id: 'password',
      isRequired: true,
      label: 'Password',
      type: PASSWORD,
    },
    {
      id: 'confirmPassword',
      isRequired: true,
      label: 'Confirm password',
      type: PASSWORD,
    },
  ] as FieldProps[]);

  const shouldRequestLink = !email || !code;

  React.useEffect(() => {
    if (shouldRequestLink) {
      setRequestLinkFields(mapServerErrors(requestLinkFields, errors));
    } else {
      setResetPasswordFields(mapServerErrors(resetPasswordFields, errors));
    }
  }, [errors]);

  const handleGoBack = () => {
    history.goBack();
  };

  let formFields: FieldProps[] = [];
  if (shouldRequestLink) {
    formFields = requestLinkFields;
  } else {
    formFields = resetPasswordFields;
  }

  const handleRequestResetPasswordLink = (fields: FieldProps[]) => {
    if (isValidForm(fields)) {
      const email = fields.find(f => f.id === 'email');

      const data = {
        form,
        email: email ? email.model : '',
      };

      // @ts-ignore
      dispatch(requestResetPasswordLink(data)).then(({ errors }: { errors: any[] }) => {
        if (!errors || !errors.length) {
          history.push({
            pathname: HOME,
            state: {
              from: location.pathname,
            },
          });
        }
      });
    }
  };

  const handleResetPassword = (fields: FieldProps[]) => {
    const password = fields.find(f => f.id === 'password');
    const confirmPassword = fields.find(f => f.id === 'confirmPassword');

    if (!password || !confirmPassword || password.model !== confirmPassword.model) {
      setResetPasswordFields(
        fields.map(field => {
          if (field.id === 'confirmPassword') {
            return {
              ...field,
              error: "Password confirmation doesn't match",
            };
          }

          return field;
        })
      );
    } else if (isValidForm(fields)) {
      const data = {
        form,
        email,
        code,
        password: password.model,
      };

      // @ts-ignore
      dispatch(resetPassword(data)).then(({ errors }: { errors: any[] }) => {
        if (!errors || !errors.length) {
          history.push({
            pathname: LOGOUT,
            state: {
              from: location.pathname,
            },
          });
        }
      });
    }
  };

  const buttons: ButtonProps[] = [
    {
      buttonType: 'primary',
      icon: SEND_ICON,
      id: 'send',
      label: SEND_LABEL,
      type: SUBMIT,
    },
    {
      icon: GOBACK_ICON,
      id: 'goBack',
      isAlwaysEnabled: true,
      label: GOBACK_LABEL,
      onClick: handleGoBack,
      type: BUTTON,
    },
  ];

  return (
    <View
      buttons={buttons}
      fields={formFields}
      form={form}
      handleSubmit={shouldRequestLink ? handleRequestResetPasswordLink : handleResetPassword}
      isLoading={isLoading}
    />
  );
};

export default ResetPasswordForm;
