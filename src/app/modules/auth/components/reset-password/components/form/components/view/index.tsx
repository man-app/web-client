import React from 'react';

import SmartForm from '../../../../../../../../components/forms';

import { FieldProps } from '../../../../../../../../components/forms/types';
import { ButtonProps } from '../../../../../../../../components/touchable/types';

interface Props {
  buttons: ButtonProps[];
  fields: FieldProps[];
  form: string;
  handleSubmit: (fields: FieldProps[]) => void;
  isLoading: boolean;
}

const View: React.FC<Props> = ({ buttons, fields, form, handleSubmit, isLoading }: Props) => (
  <SmartForm buttons={buttons} fields={fields} form={form} handleSubmit={handleSubmit} isLoading={isLoading}>
    {({ Buttons, controlledButtons, controlledFields, Field }) => (
      <>
        {controlledFields.map(field => (
          <Field key={field.id} options={field} />
        ))}

        <Buttons options={controlledButtons} />
      </>
    )}
  </SmartForm>
);

export default View;
