import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { getInstallationData } from '../../../../utils/installation';
import { unsubscribeFromPush, getPushSubscriptionsSupport } from '../../../../utils/push';
import { sendMessageToServiceWorker } from '../../../../utils/service-worker';
import { clearStore } from '../../../../../common/utils/idb';
import { log } from '../../../../../common/utils/logger';

import { logout } from '../../actions';
import { isLogged } from '../../selectors';

import { HOME } from '../../../../../common/constants/appRoutes';
import { CLEAR_CACHE } from '../../../../../common/constants/sw-message-types';

interface Props {
  location: any;
}

const Logout: React.FC<Props> = ({ location }: Props) => {
  const dispatch = useDispatch();
  const isLoggedUser = useSelector(isLogged);

  React.useEffect(() => {
    const installationData = getInstallationData();

    // @ts-ignore
    dispatch(logout(installationData.installationId)).then(() => {
      log('Clearing persisted store');
      clearStore();

      log('Clearing cached files');
      sendMessageToServiceWorker({ type: CLEAR_CACHE });

      if (getPushSubscriptionsSupport()) {
        log('Subscription active, unsubscribing');
        unsubscribeFromPush().then(() => {
          log('Unsubscribed');
        });
      } else {
        log('No subscription');
      }
    });
  }, []);

  if (isLoggedUser) {
    return null;
  }

  return <Redirect to={{ pathname: HOME, state: { from: location.state && location.state.from } }} />;
};

export default Logout;
