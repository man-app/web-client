import React from 'react';
import { shallow } from 'enzyme';

import View from '..';

describe('Register', () => {
  it('Should render a valid screen', () => {
    const component = shallow(<View />);
    expect(component).toMatchSnapshot();
  });
});
