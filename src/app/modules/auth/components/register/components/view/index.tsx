import React from 'react';

import { LOGIN, RESET_PASSWORD } from '../../../../../../../common/constants/appRoutes';
import { LOGIN_LABEL } from '../../../../../../../common/constants/buttons';

import { Card } from '../../../../../../components/card';
import { Page } from '../../../../../../components/page';
import { Link } from '../../../../../../components/touchable';
import Spacer from '../../../../../../components/spacer';
import RegisterForm from '../form';
import { Secondary } from './styles';

import { MAX_WIDTH_SM } from '../../../../../../../common/constants/styles/sizes';
import { LinkProps } from '../../../../../../components/touchable/types';

const loginLink: LinkProps = {
  id: 'login-link',
  to: LOGIN,
  label: LOGIN_LABEL,
};

const resetPasswordLink: LinkProps = {
  id: 'reset-password-link',
  to: RESET_PASSWORD.replace(':email?', '').replace('/:code?', ''),
  label: 'Reset password',
};

const View: React.FC = () => (
  <Page isCentered maxWidth={MAX_WIDTH_SM}>
    <Card>
      <RegisterForm />

      <Spacer />

      <Secondary>
        <p>
          <Link options={loginLink} /> | <Link options={resetPasswordLink} />
        </p>
      </Secondary>
    </Card>
  </Page>
);

export default View;
