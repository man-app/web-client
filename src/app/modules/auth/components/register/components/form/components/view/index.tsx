import React from 'react';

import { Card } from '../../../../../../../../components/card';
import SmartForm from '../../../../../../../../components/forms';
import { Link } from '../../../../../../../../components/touchable';

import { PRIVACY_POLICY, TERMS_AND_CONDITIONS } from '../../../../../../../../../common/constants/appRoutes';
import { FieldProps } from '../../../../../../../../components/forms/types';
import { ButtonProps, LinkProps } from '../../../../../../../../components/touchable/types';

const privacyPolicyLink: LinkProps = {
  id: 'privacy-link',
  label: 'Privacy Policy',
  to: PRIVACY_POLICY,
};

const termsAndConditionsLink: LinkProps = {
  id: 'terms-link',
  label: 'Terms and Conditions',
  to: TERMS_AND_CONDITIONS,
};

interface Props {
  buttons: ButtonProps[];
  fields: FieldProps[];
  form: string;
  handleSubmit: () => void;
  isLoading: boolean;
}

const View: React.FC<Props> = ({ buttons, fields, form, handleSubmit, isLoading }: Props) => (
  <SmartForm buttons={buttons} fields={fields} form={form} handleSubmit={handleSubmit} isLoading={isLoading}>
    {({ Buttons, controlledButtons, controlledFields, Field }) => (
      <>
        {controlledFields.map(field => (
          <Field key={field.id} options={field} />
        ))}

        <Card>
          <p>
            By registering, you are accepting our <Link options={termsAndConditionsLink} /> and our{' '}
            <Link options={privacyPolicyLink} />.
          </p>
        </Card>

        <Buttons options={controlledButtons} />
      </>
    )}
  </SmartForm>
);

export default View;
