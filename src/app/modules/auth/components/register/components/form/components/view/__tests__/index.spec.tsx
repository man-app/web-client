import React from 'react';
import { shallow } from 'enzyme';

import RegisterForm from '..';

import { FieldProps } from '../../../../../../../../../components/forms/types';
import { ButtonProps } from '../../../../../../../../../components/touchable/types';

describe('RegisterForm', () => {
  it('Should render a valid RegisterForm view', () => {
    const buttons = [] as ButtonProps[];
    const fields = [] as FieldProps[];
    const form = 'login';
    const handleSubmit = () => {};
    const isLoading = false;

    const component = shallow(
      <RegisterForm buttons={buttons} fields={fields} form={form} handleSubmit={handleSubmit} isLoading={isLoading} />
    );

    expect(component).toMatchSnapshot();
  });
});
