import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Redirect } from 'react-router-dom';
import useReactRouter from 'use-react-router';
import { mapServerErrors, isValidForm } from '../../../../../../utils/error-handler';
import { getInstallationData } from '../../../../../../utils/installation';
import { requestNotificationsPermission } from '../../../../../../utils/notifications';
import { subscribeToPush } from '../../../../../../utils/push';

import { getFormErrors } from '../../../../../../components/forms/selectors';
import { register } from '../../../../actions';
import { isLoadingAuth } from '../../../../selectors';

import View from './components/view';

import { CREATE_ICON, CREATE_LABEL, GOBACK_ICON, GOBACK_LABEL } from '../../../../../../../common/constants/buttons';
import { HOME } from '../../../../../../../common/constants/appRoutes';
import { BUTTON, SUBMIT, TEXT, EMAIL, PASSWORD } from '../../../../../../components/forms/models';
import { FieldProps } from '../../../../../../components/forms/types';
import { ButtonProps } from '../../../../../../components/touchable/types';

const form = 'register';

interface Props {
  hideBackBtn?: boolean;
}

const RegisterForm: React.FC<Props> = ({ hideBackBtn }: Props) => {
  const [redirectTo, setRedirectTo] = React.useState('' as any);

  if (redirectTo) {
    return <Redirect to={redirectTo} />;
  }

  const [fields, setFields] = React.useState([
    {
      type: TEXT,
      id: 'fullName',
      label: 'Full name',
      isRequired: true,
    },
    {
      type: EMAIL,
      id: 'email',
      label: 'Email',
      isRequired: true,
    },
    {
      type: PASSWORD,
      id: 'password',
      label: 'Password',
      isRequired: true,
    },
    {
      type: PASSWORD,
      id: 'confirmPassword',
      label: 'Confirm password',
      isRequired: true,
    },
  ] as FieldProps[]);

  const dispatch = useDispatch();
  const { history, location } = useReactRouter();

  const errors = useSelector(getFormErrors)(form);
  const isLoading = useSelector(isLoadingAuth);

  React.useEffect(() => {
    setFields(mapServerErrors(fields, errors));
  }, [errors]);

  const handleGoBack = () => {
    history.goBack();
  };

  const handleSubmit = () => {
    const password = fields.find(f => f.id === 'password');
    const confirmPassword = fields.find(f => f.id === 'confirmPassword');

    if (!password || !confirmPassword || password.model !== confirmPassword.model) {
      setFields(
        fields.map(field => {
          if (field.id === 'confirmPassword') {
            return {
              ...field,
              error: "Password confirmation doesn't match",
            };
          }

          return field;
        })
      );
    } else if (isValidForm(fields)) {
      const fullName = fields.find(f => f.id === 'fullName');
      const email = fields.find(f => f.id === 'email');
      const installationData = getInstallationData();

      const data = {
        form,
        email: email ? email.model : '',
        fullName: fullName ? fullName.model : '',
        password: password.model,
        ...installationData,
      };

      // @ts-ignore
      dispatch(register(data)).then(({ errors }: { errors: any[] }) => {
        if (!errors || !errors.length) {
          requestNotificationsPermission(subscribeToPush);

          setRedirectTo({
            pathname: (location.state && location.state.from) || HOME,
            state: { from: location.pathname },
          });
        }
      });
    }
  };

  const buttons: ButtonProps[] = [
    {
      buttonType: 'primary',
      icon: CREATE_ICON,
      id: 'login-btn',
      label: CREATE_LABEL,
      type: SUBMIT,
    },
  ];

  if (!hideBackBtn) {
    buttons.push({
      icon: GOBACK_ICON,
      id: 'goBack',
      isAlwaysEnabled: true,
      label: GOBACK_LABEL,
      onClick: handleGoBack,
      type: BUTTON,
    });
  }

  return <View buttons={buttons} fields={fields} form={form} handleSubmit={handleSubmit} isLoading={isLoading} />;
};

export default RegisterForm;
