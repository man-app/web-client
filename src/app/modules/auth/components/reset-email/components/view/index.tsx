import React from 'react';

import { Card } from '../../../../../../components/card';
import { Page } from '../../../../../../components/page';
import ResetEmailForm from '../form';

import { MAX_WIDTH_SM } from '../../../../../../../common/constants/styles/sizes';

const ResetEmailView: React.FC = () => (
  <Page isCentered maxWidth={MAX_WIDTH_SM}>
    <Card>
      <p>Setup your new email</p>
      <br />

      <ResetEmailForm />
    </Card>
  </Page>
);

export default ResetEmailView;
