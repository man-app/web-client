import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import useReactRouter from 'use-react-router';
import { mapServerErrors, isValidForm } from '../../../../../../utils/error-handler';

import { getFormErrors } from '../../../../../../components/forms/selectors';
import { resetEmail } from '../../../../actions';
import { isLoadingAuth } from '../../../../selectors';

import View from './components/view';

import { LOGOUT } from '../../../../../../../common/constants/appRoutes';
import { SEND_ICON, SEND_LABEL, GOBACK_ICON, GOBACK_LABEL } from '../../../../../../../common/constants/buttons';
import { SUBMIT, EMAIL } from '../../../../../../components/forms/models';
import { FieldProps } from '../../../../../../components/forms/types';
import { ButtonProps } from '../../../../../../components/touchable/types';

const form = 'resetEmail';

const ResetEmailForm: React.FC = () => {
  const [fields, setFields] = React.useState([
    {
      type: EMAIL,
      id: 'newEmail',
      label: 'New email',
      isRequired: true,
    },
  ] as FieldProps[]);

  const dispatch = useDispatch();
  const {
    history,
    match: { params },
  } = useReactRouter();

  const errors = useSelector(getFormErrors)(form);
  const isLoading = useSelector(isLoadingAuth);

  React.useEffect(() => {
    setFields(mapServerErrors(fields, errors));
  }, [errors]);

  const handleGoBack = () => {
    history.goBack();
  };

  const handleSubmit = (fields: FieldProps[]) => {
    if (isValidForm(fields)) {
      const { email, code } = params;
      const newEmail = fields.find(f => f.id === 'newEmail');

      const data = {
        form,
        code,
        email,
        newEmail: newEmail ? newEmail.model : '',
      };

      // @ts-ignore
      dispatch(resetEmail(data)).then(({ errors }: { errors: any[] }) => {
        if (!errors || !errors.length) {
          history.push({
            pathname: LOGOUT,
            state: {
              from: location.pathname,
            },
          });
        }
      });
    }
  };

  const buttons: ButtonProps[] = [
    {
      buttonType: 'primary',
      icon: SEND_ICON,
      id: 'send',
      label: SEND_LABEL,
      type: SUBMIT,
    },
    {
      icon: GOBACK_ICON,
      id: 'goBack',
      isAlwaysEnabled: true,
      label: GOBACK_LABEL,
      onClick: handleGoBack,
      type: 'button',
    },
  ];

  return <View buttons={buttons} fields={fields} form={form} handleSubmit={handleSubmit} isLoading={isLoading} />;
};

export default ResetEmailForm;
