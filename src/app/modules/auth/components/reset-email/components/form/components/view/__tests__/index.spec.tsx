import React from 'react';
import { shallow } from 'enzyme';

import ResetEmailForm from '..';

import { FieldProps } from '../../../../../../../../../components/forms/types';
import { ButtonProps } from '../../../../../../../../../components/touchable/types';

describe('ResetEmailForm', () => {
  it('Should render a valid ResetEmailForm view', () => {
    const buttons = [] as ButtonProps[];
    const fields = [] as FieldProps[];
    const form = 'login';
    const handleSubmit = () => {};
    const isLoading = false;

    const component = shallow(
      <ResetEmailForm buttons={buttons} fields={fields} form={form} handleSubmit={handleSubmit} isLoading={isLoading} />
    );

    expect(component).toMatchSnapshot();
  });
});
