import React from 'react';

import ResetEmailView from './components/view';

const ResetEmail: React.FC = () => <ResetEmailView />;

export default ResetEmail;
