import React from 'react';
import { useDispatch } from 'react-redux';
import useReactRouter from 'use-react-router';
import { getInstallationData } from '../../../../utils/installation';

import { validateEmail } from '../../actions';
import { HOME } from '../../../../../common/constants/appRoutes';

const ValidateEmail: React.FC = () => {
  const dispatch = useDispatch();
  const {
    history,
    match: { params },
  } = useReactRouter();

  React.useEffect(() => {
    const installationData = getInstallationData();

    // @ts-ignore
    dispatch(validateEmail({ ...params, ...installationData })).then(() => {
      history.push({
        pathname: HOME,
        state: {},
      });
    });
  }, []);

  return null;
};

export default ValidateEmail;
