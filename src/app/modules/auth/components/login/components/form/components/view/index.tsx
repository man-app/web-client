import React from 'react';

import SmartForm from '../../../../../../../../components/forms';

import { FieldProps } from '../../../../../../../../components/forms/types';
import { ButtonProps } from '../../../../../../../../components/touchable/types';

interface Props {
  buttons: ButtonProps[];
  fields: FieldProps[];
  form: string;
  handleSubmit: (fields: FieldProps[]) => void;
  isLoading: boolean;
  socialButtons: ButtonProps[];
}

const View: React.FC<Props> = ({ buttons, fields, form, handleSubmit, isLoading, socialButtons }: Props) => (
  <SmartForm buttons={buttons} form={form} handleSubmit={handleSubmit} fields={fields} isLoading={isLoading}>
    {({ Buttons, controlledButtons, controlledFields, Field, isEditEnabled }) => (
      <>
        {controlledFields.map(field => (
          <Field key={field.id} options={field} />
        ))}

        <Buttons options={controlledButtons} />

        <Buttons
          options={socialButtons.map(button => ({
            ...button,
            isDisabled: !isEditEnabled || isLoading,
          }))}
        />
      </>
    )}
  </SmartForm>
);
export default View;
