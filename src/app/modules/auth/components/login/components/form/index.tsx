import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import useReactRouter from 'use-react-router';
import { mapServerErrors, isValidForm } from '../../../../../../utils/error-handler';
import { getInstallationData } from '../../../../../../utils/installation';
import { requestNotificationsPermission } from '../../../../../../utils/notifications';
import { subscribeToPush } from '../../../../../../utils/push';
// import { info } from '../../../../../../../common/utils/logger';

import { getLogin } from '../../../../actions';
// import {
//   facebookLogin,
//   facebookGetResidualLogin,
//   instagramGetResidualLogin,
//   linkedinGetResidualLogin,
//   twitterLogin,
// } from '../../../../../social/actions';
import { getFormErrors } from '../../../../../../components/forms/selectors';
import { isLoadingAuth } from '../../../../selectors';

import View from './components/view';

import {
  SUPPORTED_PLATFORMS,
  FACEBOOK,
  GOOGLE,
  TWITTER,
  LINKEDIN,
  INSTAGRAM,
} from '../../../../../../../common/constants/social-networks';
import { LOGIN_ICON, LOGIN_LABEL, GOBACK_ICON, GOBACK_LABEL } from '../../../../../../../common/constants/buttons';
import { BUTTON, SUBMIT, EMAIL, PASSWORD } from '../../../../../../components/forms/models';
import { FieldProps } from '../../../../../../components/forms/types';
import { ButtonProps } from '../../../../../../components/touchable/types';
// import { UserLogged } from '../../../../types';

const form = 'login';

interface Props {
  hideBackBtn?: boolean;
}

const LoginForm: React.FC<Props> = ({ hideBackBtn }: Props) => {
  const [fields, setFields] = React.useState([
    {
      type: EMAIL,
      id: 'email',
      isAlwaysEnabled: true,
      label: 'Email',
      isRequired: true,
    },
    {
      type: PASSWORD,
      id: 'password',
      isAlwaysEnabled: true,
      label: 'Password',
      isRequired: true,
    },
  ] as FieldProps[]);

  const dispatch = useDispatch();
  const { history } = useReactRouter();

  const errors = useSelector(getFormErrors)(form);
  const isLoading = useSelector(isLoadingAuth);

  React.useEffect(() => {
    if (errors && errors.length) {
      setFields(mapServerErrors(fields, errors));
    }
  }, [errors]);

  const handleGoBack = () => {
    history.goBack();
  };

  const handleSubmit = (fields: FieldProps[]) => {
    if (isValidForm(fields)) {
      const email = fields.find(f => f.id === 'email');
      const password = fields.find(f => f.id === 'password');
      const installationData = getInstallationData();

      const data = {
        form,
        email: email ? email.model : '',
        password: password ? password.model : '',
        ...installationData,
      };

      // @ts-ignore
      dispatch(getLogin(data)).then(({ errors }) => {
        if (!errors || !errors.length) {
          requestNotificationsPermission(subscribeToPush);
          // dispatch(linkedinGetResidualLogin());
          // dispatch(facebookGetResidualLogin());
          // dispatch(instagramGetResidualLogin());
        }
      });
    }
  };

  const initFacebookFlow = () => {
    // @ts-ignore
    // dispatch(facebookLogin()).then((user: UserLogged | void) => {
    //   if (user) {
    //     const installationData = getInstallationData();
    //     const data = {
    //       form,
    //       ...user,
    //       ...installationData,
    //       platform: FACEBOOK,
    //     };
    //     // @ts-ignore
    //     dispatch(getLogin(data)).then(({ errors }: { errors: any[] }) => {
    //       if (!errors || !errors.length) {
    //         requestNotificationsPermission(subscribeToPush);
    //       }
    //     });
    //   }
    // });
  };

  const initTwitterFlow = () => {
    // @ts-ignore
    // dispatch(twitterLogin()).then((user: any | void) => {
    //   if (user) {
    //     info('This user should be registered/logged', user);
    //   }
    // });
  };

  const buttons: ButtonProps[] = [
    {
      buttonType: 'primary',
      icon: LOGIN_ICON,
      id: 'login-btn',
      isAlwaysEnabled: true,
      label: LOGIN_LABEL,
      type: SUBMIT,
    },
  ];

  if (!hideBackBtn) {
    buttons.push({
      icon: GOBACK_ICON,
      id: 'goBack',
      isAlwaysEnabled: true,
      label: GOBACK_LABEL,
      onClick: handleGoBack,
      type: BUTTON,
    });
  }

  const socialButtons: ButtonProps[] = [
    {
      buttonType: 'linkedin',
      icon: 'linkedin',
      id: 'linkedin-login',
      isAlwaysDisabled: !SUPPORTED_PLATFORMS.find(p => p === LINKEDIN),
      type: BUTTON,
    },
    {
      buttonType: 'facebook',
      icon: 'facebook',
      id: 'facebook-login',
      isAlwaysDisabled: !SUPPORTED_PLATFORMS.find(p => p === FACEBOOK),
      onClick: initFacebookFlow,
      type: BUTTON,
    },
    {
      buttonType: 'google',
      icon: 'google',
      id: 'google-login',
      isAlwaysDisabled: !SUPPORTED_PLATFORMS.find(p => p === GOOGLE),
      type: BUTTON,
    },
    {
      buttonType: 'twitter',
      icon: 'twitter',
      id: 'twitter-login',
      isAlwaysDisabled: !SUPPORTED_PLATFORMS.find(p => p === TWITTER),
      onClick: initTwitterFlow,
      type: BUTTON,
    },
    {
      buttonType: 'instagram',
      icon: 'instagram',
      id: 'instagram-login',
      isAlwaysDisabled: !SUPPORTED_PLATFORMS.find(p => p === INSTAGRAM),
      type: BUTTON,
    },
  ];

  return (
    <View
      buttons={buttons}
      fields={fields}
      form={form}
      handleSubmit={handleSubmit}
      isLoading={isLoading}
      socialButtons={socialButtons}
    />
  );
};

export default LoginForm;
