import React from 'react';
import { shallow } from 'enzyme';

import LoginForm from '..';

import { FieldProps } from '../../../../../../../../../components/forms/types';
import { ButtonProps } from '../../../../../../../../../components/touchable/types';

describe('LoginForm', () => {
  it('Should render a valid LoginForm view', () => {
    const buttons = [] as ButtonProps[];
    const fields = [] as FieldProps[];
    const form = 'login';
    const handleSubmit = () => {};
    const isLoading = false;
    const socialButtons = [] as ButtonProps[];

    const component = shallow(
      <LoginForm
        buttons={buttons}
        fields={fields}
        form={form}
        handleSubmit={handleSubmit}
        isLoading={isLoading}
        socialButtons={socialButtons}
      />
    );

    expect(component).toMatchSnapshot();
  });
});
