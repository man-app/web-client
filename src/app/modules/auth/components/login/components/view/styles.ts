import styled from 'styled-components';

import getColor from '../../../../../../../common/constants/styles/theme';
import { ThemedComponent } from '../../../../../../types/styled-components';

export const Secondary = styled.div`
  color: ${({ theme }: ThemedComponent) => getColor('GLOBAL_COLOR_3', theme.active)};
  font-style: italic;
  margin-bottom: 10px;
  text-align: center;
  transition: color 0.2s;
`;
Secondary.displayName = 'Secondary';
