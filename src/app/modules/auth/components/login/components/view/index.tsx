import React from 'react';

import { Card } from '../../../../../../components/card';
import { Page } from '../../../../../../components/page';
import Spacer from '../../../../../../components/spacer';
import { Link } from '../../../../../../components/touchable';
import LoginForm from '../form';
import { Secondary } from './styles';

import { REGISTER, RESET_PASSWORD } from '../../../../../../../common/constants/appRoutes';
import { REGISTER_LABEL } from '../../../../../../../common/constants/buttons';
import { MAX_WIDTH_SM } from '../../../../../../../common/constants/styles/sizes';
import { LinkProps } from '../../../../../../components/touchable/types';

const registerLink: LinkProps = {
  id: 'register-link',
  to: REGISTER,
  label: REGISTER_LABEL,
};

const resetPasswordLink: LinkProps = {
  id: 'reset-password-link',
  to: RESET_PASSWORD,
  label: 'Reset password',
};

const View: React.FC = () => (
  <Page isCentered maxWidth={MAX_WIDTH_SM}>
    <Card>
      <LoginForm />

      <Spacer />

      <Secondary>
        <p>
          <Link options={registerLink} /> | <Link options={resetPasswordLink} />
        </p>
      </Secondary>
    </Card>
  </Page>
);

export default View;
