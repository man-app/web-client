import manappApi from '../../../../common/apis/manapp';

import {
  SUBSCRIPTIONS_REQUESTED,
  SUBSCRIPTIONS_UPDATED,
  SUBSCRIPTIONS_NOT_UPDATED,
  SELECT_SUBSCRIPTION,
  ACTIVATE_SUBSCRIPTION_SUBMITTED,
  ACTIVATE_SUBSCRIPTION_SUCCEED,
  ACTIVATE_SUBSCRIPTION_FAILED,
} from '../actionTypes';

import { createSubscriptionPlansFromServer } from '../models';
import { SubscriptionsAction, SubscriptionsActionCreator } from '../types';

export const getSubscriptionPlans: SubscriptionsActionCreator = () => dispatch => {
  dispatch({
    type: SUBSCRIPTIONS_REQUESTED,
  });

  return manappApi.subscriptions
    .getSubscriptionPlans()
    .then(({ notifications, subscriptionPlans }) => {
      const payload = {
        notifications,
        subscriptionPlans: createSubscriptionPlansFromServer(subscriptionPlans),
      };

      dispatch({
        type: SUBSCRIPTIONS_UPDATED,
        payload,
      });

      return payload;
    })
    .catch(err => {
      const payload = {
        err,
      };

      dispatch({
        type: SUBSCRIPTIONS_NOT_UPDATED,
        payload,
      });

      return payload;
    });
};

export const selectSubscription = (id: string): SubscriptionsAction => ({
  type: SELECT_SUBSCRIPTION,
  payload: {
    id,
  },
});

export const activateSubscription: SubscriptionsActionCreator = ({
  idCompany,
  idPaymentMethod,
  idSubscriptionPlan,
  form,
}) => dispatch => {
  dispatch({
    type: ACTIVATE_SUBSCRIPTION_SUBMITTED,
  });

  return manappApi.subscriptions
    .activateSubscription({ idCompany, idPaymentMethod, idSubscriptionPlan })
    .then(({ notifications, payments, subscriptions }) => {
      const payload = {
        form,
        notifications,
        payments,
        subscriptions,
      };

      dispatch({
        type: ACTIVATE_SUBSCRIPTION_SUCCEED,
        payload,
      });

      return payload;
    })
    .catch(err => {
      dispatch({
        type: ACTIVATE_SUBSCRIPTION_FAILED,
        payload: {
          form,
          ...err,
        },
      });

      return {
        form,
        ...err,
      };
    });
};
