import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import useReactRouter from 'use-react-router';
import { moduleIds, getPermissions } from '../../../../utils/permissions';

import { getLoggedUser } from '../../../auth/selectors';
import { getSelectedCompany } from '../../../companies/selectors';
import { getModulesEnabled } from '../../../modules/selectors';
import { createNotification } from '../../../notifier/actions';
import { getSubscriptionPlans as getPlansAction, selectSubscription } from '../../actions';
import { getSubscriptionPlans, isLoadingSubscriptionPlans } from '../../selectors';

import View from './view';

import { createNotificationParams } from '../../../notifier/models';
import { RawNotificationParams } from '../../../notifier/types';
import { ACTIVATE_SUBSCRIPTION } from '../../../../../common/constants/appRoutes';

const Plans = () => {
  const dispatch = useDispatch();
  const { history, location } = useReactRouter();
  const isLoading = useSelector(isLoadingSubscriptionPlans);
  const plans = useSelector(getSubscriptionPlans);
  const user = useSelector(getLoggedUser);
  const company = useSelector(getSelectedCompany);
  const modules = useSelector(getModulesEnabled)((company && company.modules) || []);
  const thisModule = modules.find(mod => mod.id === moduleIds.companySettings);
  const permissions = (thisModule && getPermissions(thisModule, user, company)) || {};
  const canCreateSettings = !!(permissions.create && permissions.create.value);
  const canEditSettings = !!(permissions.editing && permissions.editing.value);

  React.useEffect(() => {
    dispatch(getPlansAction());
  }, []);

  const handleSelectPlan = async (id: string) => {
    if (!canCreateSettings && !canEditSettings) {
      const notification: RawNotificationParams = {
        title: 'Need permissions',
        body: "You don't have permissions to complete this action",
        tag: 'purchase-plan-permission',
        type: 'error',
      };

      dispatch(createNotification(createNotificationParams(notification)));

      return;
    }

    dispatch(selectSubscription(id));
    history.push({ pathname: ACTIVATE_SUBSCRIPTION, state: { from: location.pathname } });
  };

  return (
    <View
      canActivateSubscription={canCreateSettings || canEditSettings}
      company={company}
      handleSelectPlan={handleSelectPlan}
      isLoading={isLoading}
      subscriptions={plans}
    />
  );
};

export default Plans;
