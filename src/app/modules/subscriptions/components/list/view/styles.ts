import styled from 'styled-components';

import { Page } from '../../../../../components/page';

import getColor from '../../../../../../common/constants/styles/theme';
import { ThemedComponent } from '../../../../../types/styled-components';

export const Wrapper = styled(Page)`
  color: ${({ theme }: ThemedComponent) => getColor('PRICING_COLOR', theme.active)};
  font-size: 14px;
  transition: color 0.2s;

  & p,
  & h3 {
    margin-bottom: 10px;
  }

  & p span,
  & h3 span {
    color: ${({ theme }: ThemedComponent) => getColor('PRICING_COLOR_HIGHLIGHT', theme.active)};
  }
`;
Wrapper.displayName = 'Wrapper';

export const Subscriptions = styled.div`
  text-align: center;
  width: 100%;

  & > div > span:last-child {
    margin-bottom: 20px;
  }
`;
Subscriptions.displayName = 'Subscriptions';
