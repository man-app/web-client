import React from 'react';
// import styled from 'styled-components';

import { Skeleton, BarWrapper, Bar } from '../../../../../components/skeletons';
import { Wrapper, Title, Icon, Users, Price } from '../../../../../components/subscription-card/styles';

import { Avatar as AvatarSkeleton } from '../../../../../components/skeletons';
import Spacer from '../../../../../components/spacer';
import { useThemeContext } from '../../../../../contexts/theme';
import getColor from '../../../../../../common/constants/styles/theme';

interface Props {
  count: string[];
}

const SubscriptionPlansSkeleton: React.FC<Props> = ({ count }: Props) => {
  const { active } = useThemeContext();
  return (
    <Skeleton background={getColor('CARD_BACKGROUND', active)}>
      {count.map((subscription, index) => (
        <Wrapper key={index}>
          <Title>
            <BarWrapper align="center">
              <Bar width="50%" />
            </BarWrapper>
          </Title>

          <Icon>
            <AvatarSkeleton size="md" />
          </Icon>
          <Spacer />

          <Users>
            <BarWrapper align="center">
              <Bar width="40%" />
            </BarWrapper>
          </Users>
          <Spacer />

          <Price>
            <BarWrapper align="center">
              <Bar width="35%" />
              <Bar width="50%" />
            </BarWrapper>
          </Price>
        </Wrapper>
      ))}
    </Skeleton>
  );
};

export default SubscriptionPlansSkeleton;
