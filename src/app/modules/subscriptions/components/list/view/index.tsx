import React from 'react';

import { Card } from '../../../../../components/card';
import SubscriptionCard from '../../../../../components/subscription-card';
import CompanyAdvice from '../../company-advice';
import PermissionMessage from '../PermissionMessage';
import { Wrapper, Subscriptions } from './styles';

import { APP_NAME } from '../../../../../../common/constants/branding';
import { MAX_WIDTH_LG } from '../../../../../../common/constants/styles/sizes';
import { Company } from '../../../../companies/types';
import { Subscription } from '../../../types';
import SubscriptionPlansSkeleton from './skeleton';

interface Props {
  canActivateSubscription: boolean;
  company?: Company;
  handleSelectPlan: (id: string) => void;
  isLoading?: boolean;
  subscriptions: Subscription[];
}

const View = ({ canActivateSubscription, company, handleSelectPlan, isLoading = false, subscriptions }: Props) => (
  <Wrapper maxWidth={MAX_WIDTH_LG}>
    <Card>
      <h3>
        {APP_NAME} is <span>free to use</span>.
      </h3>

      <p>
        When you create your account, you are ready to start using {APP_NAME}, and you will be able to create/manage as
        many companies as you want.
      </p>
      <Subscriptions>
        {isLoading && !subscriptions.length && <SubscriptionPlansSkeleton count={['']} />}

        {(!isLoading || !!subscriptions.length) &&
          subscriptions
            .filter(subscription => subscription.price === 0)
            .map((subscription, index) => (
              <SubscriptionCard
                key={index}
                canPurchase={canActivateSubscription}
                onClick={handleSelectPlan}
                subscription={subscription}
              />
            ))}
      </Subscriptions>
    </Card>

    <Card>
      <p>When your company starts growing, you can select the subscription plan that fits best your company:</p>

      {canActivateSubscription ? (
        <CompanyAdvice company={company} />
      ) : (
        <PermissionMessage canActivateSubscription={canActivateSubscription} company={company} />
      )}
      <Subscriptions>
        {isLoading && !subscriptions.length && <SubscriptionPlansSkeleton count={['', '', '', '']} />}

        {(!isLoading || !!subscriptions.length) &&
          subscriptions
            .filter(subscription => subscription.price !== 0)
            .map((subscription, index) => (
              <SubscriptionCard
                key={index}
                canPurchase={canActivateSubscription}
                onClick={handleSelectPlan}
                subscription={subscription}
              />
            ))}
      </Subscriptions>
    </Card>
  </Wrapper>
);

export default View;
