import React from 'react';
import { shallow } from 'enzyme';

import View from '..';

import { Subscription } from '../../../../types';

const SUBSCRIPTIONS: Subscription[] = [
  {
    id: '1',
    name: 'Freelance',
    price: 0,
    users: 2,
  },
  {
    id: '2',
    name: 'Start up',
    icon: 'star-o',
    price: 5 * 10,
    users: 10,
    discount: 10,
  },
  {
    id: '3',
    name: 'Bussiness',
    icon: 'briefcase',
    price: 5 * 25,
    users: 25,
    discount: 15,
  },
  {
    id: '4',
    name: 'Large Bussiness',
    icon: 'briefcase',
    price: 5 * 50,
    users: 50,
    discount: 20,
  },
  {
    id: '5',
    name: 'Corporate',
    icon: 'globe',
    price: 5 * 100,
    users: 100,
    discount: 25,
  },
];

describe('Pricing', () => {
  it('Should render a valid screen with permissions', () => {
    const component = shallow(
      <View canActivateSubscription={true} handleSelectPlan={() => {}} subscriptions={SUBSCRIPTIONS} />
    );

    expect(component).toMatchSnapshot();
  });
  it('Should render a valid screen without permissions', () => {
    const component = shallow(
      <View canActivateSubscription={false} handleSelectPlan={() => {}} subscriptions={SUBSCRIPTIONS} />
    );

    expect(component).toMatchSnapshot();
  });
});
