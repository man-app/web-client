import React from 'react';
import { shallow } from 'enzyme';

import PermissionMessage from '..';

describe('PermissionMessage', () => {
  it('should render a valid component with permissions', () => {
    const company = {
      id: '1',
      name: 'Company test',
      slug: 'company-test',
      created: 123456789,
      lastModified: 123456789,
    };
    const component = shallow(<PermissionMessage company={company} canActivateSubscription />);

    expect(component).toMatchSnapshot();
  });

  it('should render a valid component without permissions', () => {
    const company = {
      id: '1',
      name: 'Company test',
      slug: 'company-test',
      created: 123456789,
      lastModified: 123456789,
    };
    const component = shallow(<PermissionMessage company={company} />);

    expect(component).toMatchSnapshot();
  });
});
