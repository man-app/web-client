import React from 'react';

import Avatar from '../../../../../components/avatar';
import Message from '../../../../../components/message';
import { MessageText } from '../../../../../components/message/styles';
import { Content } from '../../../../../components/touchable';

import { XS } from '../../../../../../common/constants/sizes';
import { Company } from '../../../../companies/types';

interface Props {
  canActivateSubscription?: boolean;
  company?: Company;
}

const PermissionMessage: React.FC<Props> = ({ canActivateSubscription: canPurchase, company }: Props) => {
  if (!canPurchase && company) {
    return (
      <Message type="warning">
        <>
          <MessageText>You cannot purchase a plan{company && ' for:'}</MessageText>
          {company && (
            <MessageText>
              <Content
                options={{
                  label: company.name,
                  icon: (
                    <Avatar
                      size={XS}
                      profile={{
                        name: company.name,
                        picture: company.picture || '',
                      }}
                    />
                  ),
                }}
              />
            </MessageText>
          )}
        </>
      </Message>
    );
  }

  return null;
};

export default PermissionMessage;
