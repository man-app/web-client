import React from 'react';

import Avatar from '../../../../components/avatar';
import Message from '../../../../components/message';
import { MessageText } from '../../../../components/message/styles';
import { Content } from '../../../../components/touchable';

import { XS } from '../../../../../common/constants/sizes';
import { Company } from '../../../companies/types';

interface Props {
  company?: Company;
}

const View: React.FC<Props> = ({ company }: Props) => {
  if (company) {
    return (
      <Message type="warning">
        <>
          <MessageText>All purchases you make will be applied to:</MessageText>
          <MessageText>
            <Content
              options={{
                label: company.name,
                icon: (
                  <Avatar
                    size={XS}
                    profile={{
                      name: company.name,
                      picture: company.picture || '',
                    }}
                  />
                ),
              }}
            />
          </MessageText>
        </>
      </Message>
    );
  }

  return null;
};

export default View;
