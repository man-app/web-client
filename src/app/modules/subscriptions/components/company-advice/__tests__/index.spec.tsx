import React from 'react';
import { shallow } from 'enzyme';

import CompanyAdvice from '..';

describe('CompanyAdvice', () => {
  it('should render a valid component', () => {
    const company = {
      id: '1',
      name: 'Company test',
      slug: 'company-test',
      created: 123456789,
      lastModified: 123456789,
    };

    const component = shallow(<CompanyAdvice company={company} />);

    expect(component).toMatchSnapshot();
  });
});
