import React from 'react';
import { useSelector } from 'react-redux';

import { getSelectedCompany } from '../../../companies/selectors';

import View from './components/view';
import { Redirect } from 'react-router';

import { getSelectedSubscriptionPlan } from '../../selectors';

import { SUBSCRIPTIONS } from '../../../../../common/constants/appRoutes';
import { DROPDOWN } from '../../../../components/forms/models';
import { CREDIT_CARD, PAYPAL } from '../../models';
import { PaymentMethod } from '../../types';

const form = 'activate-subscription';

interface FieldOnChangeCallback {
  target: {
    value: PaymentMethod;
  };
}

const PaymentMethodSelector: React.FC = () => {
  const [selectedPaymentMethod, setSelectedPaymentMethod] = React.useState(CREDIT_CARD);

  const company = useSelector(getSelectedCompany);
  const selectedSubscription = useSelector(getSelectedSubscriptionPlan);

  if (!selectedSubscription) {
    return <Redirect to={{ pathname: SUBSCRIPTIONS }} />;
  }

  const fields = [
    {
      id: 'payment-method',
      onChange: ({ target: { value } }: FieldOnChangeCallback) => {
        setSelectedPaymentMethod(value);
      },
      options: [
        {
          id: 'payment-method-credit-card',
          label: 'Credit card',
          type: 'option',
          value: CREDIT_CARD,
        },
        {
          id: 'payment-method-paypal',
          isDisabled: true,
          label: 'Paypal (soon)',
          type: 'option',
          value: PAYPAL,
        },
      ],
      type: DROPDOWN,
    },
  ];

  return (
    <View
      company={company}
      fields={fields}
      form={form}
      handleSubmit={() => {}}
      subscription={selectedSubscription}
      selectedPaymentMethod={selectedPaymentMethod}
    />
  );
};

export default PaymentMethodSelector;
