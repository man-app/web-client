import React from 'react';
import { shallow } from 'enzyme';

import PaymentSelector from '../../view';

describe('PaymentSelector', () => {
  const company = {
    id: '1',
    name: 'Company test',
    slug: 'company-test',
    created: 123456789,
    lastModified: 123456789,
  };

  const subscription = {
    id: '2',
    name: 'Start up',
    icon: 'star-o',
    price: 5 * 10,
    users: 10,
    discount: 10,
  };

  const handleSubmit = () => {};

  it('should render a vaild payment selector screen with credit card selected', () => {
    const component = shallow(
      <PaymentSelector
        company={company}
        fields={[]}
        form="payment-selector"
        handleSubmit={handleSubmit}
        selectedPaymentMethod={'credit-card'}
        subscription={subscription}
      />
    );
    expect(component).toMatchSnapshot();
  });

  it('should render a vaild payment selector screen with paypal selected', () => {
    const component = shallow(
      <PaymentSelector
        company={company}
        fields={[]}
        form="payment-selector"
        handleSubmit={handleSubmit}
        selectedPaymentMethod={'paypal'}
        subscription={subscription}
      />
    );
    expect(component).toMatchSnapshot();
  });
});
