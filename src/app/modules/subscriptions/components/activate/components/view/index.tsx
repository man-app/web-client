import React from 'react';

import { Page, PageH2 } from '../../../../../../components/page';

import { Card } from '../../../../../../components/card';
import SmartForm from '../../../../../../components/forms';
import SubscriptionCard from '../../../../../../components/subscription-card';
import CompanyAdvice from '../../../company-advice';
import CreditCard from '../credit-card';

import { MAX_WIDTH } from '../../../../../../../common/constants/styles/sizes';
import { FieldProps } from '../../../../../../components/forms/types';
import { Company } from '../../../../../companies/types';
import { CREDIT_CARD, PAYPAL } from '../../../../models';
import { PaymentMethod, Subscription } from '../../../../types';

interface Props {
  company?: Company;
  fields: FieldProps[];
  form: string;
  handleSubmit: (fields: FieldProps[]) => void;
  selectedPaymentMethod: PaymentMethod;
  subscription: Subscription;
}

const View: React.FC<Props> = ({ company, fields, form, handleSubmit, selectedPaymentMethod, subscription }: Props) => (
  <Page maxWidth={MAX_WIDTH}>
    {company && <CompanyAdvice company={company} />}

    <Card width="50%">
      <PageH2>Your subscription</PageH2>

      <SubscriptionCard subscription={subscription} />
    </Card>

    <Card width="50%">
      <PageH2 style={{ textAlign: 'left' }}>Payment method</PageH2>

      <SmartForm buttons={[]} fields={fields} form={form} handleSubmit={handleSubmit} isLoading={false}>
        {({ controlledFields, Field }) => (
          <>
            {controlledFields.map(field => (
              <Field key={field.id} options={field} />
            ))}
          </>
        )}
      </SmartForm>

      {selectedPaymentMethod === CREDIT_CARD && <CreditCard />}

      {selectedPaymentMethod === PAYPAL && <p>Paypal</p>}
    </Card>
  </Page>
);

export default View;
