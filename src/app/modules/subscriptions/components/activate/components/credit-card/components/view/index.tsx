import React from 'react';

import SmartForm from '../../../../../../../../components/forms';
import Spinner from '../../../../../../../../components/spinner';
import { Link } from '../../../../../../../../components/touchable';
import PaymentCard from '../../../../../../../payment-methods/components/payment-card';

import { PAYMENT_METHOD } from '../../../../../../../../../common/constants/appRoutes';
import { ADD_LABEL } from '../../../../../../../../../common/constants/buttons';
import { FieldProps } from '../../../../../../../../components/forms/types';
import { ButtonProps } from '../../../../../../../../components/touchable/types';
import { PaymentMethod } from '../../../../../../../payment-methods/types';

const AddPaymentMethodLink: React.FC = () => (
  <p style={{ padding: 10 }}>
    Not in the list?{' '}
    <Link
      options={{
        id: 'add-payment-method',
        label: `${ADD_LABEL} it`,
        to: PAYMENT_METHOD.replace(':id', 'new'),
      }}
    />
    .
  </p>
);

interface Props {
  buttons: ButtonProps[];
  fields: FieldProps[];
  form: string;
  handleSubmit: (fields: FieldProps[]) => void;
  isLoading: boolean;
  paymentMethod?: PaymentMethod;
}

const View: React.FC<Props> = ({ buttons, fields, form, handleSubmit, isLoading, paymentMethod }: Props) => {
  if (!paymentMethod || !fields.length) {
    return (
      <>
        {isLoading && <Spinner />}
        <AddPaymentMethodLink />
      </>
    );
  }

  return (
    <>
      <PaymentCard fullSize obfuscateNumber paymentMethod={paymentMethod} />

      <SmartForm buttons={buttons} fields={fields} form={form} handleSubmit={handleSubmit} isLoading={isLoading}>
        {({ Buttons, controlledButtons, controlledFields, Field }) => (
          <>
            {controlledFields.map(field => (
              <Field key={field.id} options={field} />
            ))}

            <AddPaymentMethodLink />

            <Buttons options={controlledButtons} />
          </>
        )}
      </SmartForm>
    </>
  );
};

export default View;
