import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import useReactRouter from 'use-react-router';
import { obfuscateCardNumber } from '../../../../../../../common/utils/numbers';
import { isValidForm } from '../../../../../../utils/error-handler';

import { getPaymentMethods as getPaymentMethodsAction } from '../../../../../payment-methods/actions';
import { getPaymentMethods } from '../../../../../payment-methods/selectors';
import { getSelectedCompanyId } from '../../../../../companies/selectors';
import { activateSubscription } from '../../../../actions';
import { getSelectedSubscriptionPlan } from '../../../../selectors';

import View from './components/view';

import { HOME } from '../../../../../../../common/constants/appRoutes';
import { GOBACK_LABEL, GOBACK_ICON } from '../../../../../../../common/constants/buttons';
import { SUBMIT, BUTTON, DROPDOWN } from '../../../../../../components/forms/models';
import { FieldProps } from '../../../../../../components/forms/types';
import { ButtonProps } from '../../../../../../components/touchable/types';
import { PaymentMethod } from '../../../../../payment-methods/types';

const form = 'credit-card';

const CreditCard: React.FC = () => {
  const [fields, setFields] = React.useState([] as FieldProps[]);
  const [selectedPaymentMethod, setSelectedPaymentMethod] = React.useState(undefined as PaymentMethod | undefined);

  const { history } = useReactRouter();
  const dispatch = useDispatch();
  const company = useSelector(getSelectedCompanyId);
  const paymentMethods = useSelector(getPaymentMethods);
  const subscriptionPlan = useSelector(getSelectedSubscriptionPlan);

  const handleChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
    const selectedOption = event.target.value;
    const selectedPaymentMethod = paymentMethods.find(pm => pm.id === selectedOption);
    if (selectedPaymentMethod) {
      setSelectedPaymentMethod(selectedPaymentMethod);
    }
  };

  const getInitialFields = (): FieldProps[] => [
    {
      id: 'payment-method',
      label: 'Selected payment method',
      model: selectedPaymentMethod && selectedPaymentMethod.id.toString(),
      onChange: handleChange,
      options: paymentMethods.map(({ id, description, cardNumber }) => ({
        id: id.toString(),
        label: description || obfuscateCardNumber(cardNumber),
        value: id.toString(),
        type: 'option',
      })),
      type: DROPDOWN,
    },
  ];

  React.useEffect(() => {
    dispatch(getPaymentMethodsAction());
  }, []);

  React.useEffect(() => {
    const defaultPaymentMethod = paymentMethods.find(pm => pm.isDefault);
    const firstPaymentMethod = paymentMethods[0];

    if (defaultPaymentMethod) {
      setSelectedPaymentMethod(defaultPaymentMethod);
    } else if (firstPaymentMethod) {
      setSelectedPaymentMethod(firstPaymentMethod);
    }
  }, [paymentMethods]);

  React.useEffect(() => {
    if (selectedPaymentMethod) {
      setFields(getInitialFields());
    }
  }, [selectedPaymentMethod]);

  const handleSubmit = (controlledFields: FieldProps[]) => {
    if (isValidForm(controlledFields) && typeof company !== 'undefined') {
      const paymentMethod = controlledFields.find(f => f.id === 'payment-method');

      const params = {
        form,
        idCompany: company,
        idPaymentMethod: paymentMethod && paymentMethod.model,
        idSubscriptionPlan: subscriptionPlan && subscriptionPlan.id,
      };

      // @ts-ignore
      dispatch(activateSubscription(params)).then(res => {
        if (!res.errors || !res.errors.length) {
          history.push({ pathname: HOME });
        }
      });
    }
  };

  const isLoading = false;

  const buttons: ButtonProps[] = [
    {
      buttonType: 'primary',
      icon: 'credit-card',
      id: 'submit',
      label: 'Activate',
      type: SUBMIT,
    },
    {
      icon: GOBACK_ICON,
      id: 'go-back',
      label: GOBACK_LABEL,
      onClick: () => {
        history.goBack();
      },
      type: BUTTON,
    },
  ];

  return (
    <View
      buttons={buttons}
      fields={fields}
      form={form}
      handleSubmit={handleSubmit}
      isLoading={isLoading}
      paymentMethod={selectedPaymentMethod}
    />
  );
};

export default CreditCard;
