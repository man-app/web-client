import React from 'react';
import { shallow } from 'enzyme';

import CreditCard from '../../view';
import { DROPDOWN } from '../../../../../../../../../components/forms/models';

describe('CreditCard', () => {
  const handleSubmit = () => {};

  it('should render a loading credit-card-form', () => {
    const component = shallow(
      <CreditCard buttons={[]} fields={[]} form="credit-card" handleSubmit={handleSubmit} isLoading={true} />
    );
    expect(component).toMatchSnapshot();
  });

  it('should render a vaild credit-card-form', () => {
    const component = shallow(
      <CreditCard
        buttons={[]}
        fields={[{ id: 'test-field', type: DROPDOWN }]}
        form="credit-card"
        handleSubmit={handleSubmit}
        isLoading={false}
        paymentMethod={{
          cardNumber: '1234567891234567',
          ccv: '123',
          id: '1',
          isDefault: false,
          owner: 'Test card owner',
          validUntil: '01/19',
          created: 123456789,
          lastModified: 123456789,
        }}
      />
    );
    expect(component).toMatchSnapshot();
  });

  it('should render an empty credit-card-form', () => {
    const component = shallow(
      <CreditCard buttons={[]} fields={[]} form="credit-card" handleSubmit={handleSubmit} isLoading={false} />
    );
    expect(component).toMatchSnapshot();
  });
});
