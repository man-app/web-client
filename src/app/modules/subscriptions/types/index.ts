import { Dispatch } from 'redux';

export type SubscriptionsActionType =
  | 'SUBSCRIPTIONS_REQUESTED'
  | 'SUBSCRIPTIONS_UPDATED'
  | 'SUBSCRIPTIONS_NOT_UPDATED'
  | 'SELECT_PLAN'
  | 'ACTIVATE_SUBSCRIPTION_SUBMITTED'
  | 'ACTIVATE_SUBSCRIPTION_SUCCEED'
  | 'ACTIVATE_SUBSCRIPTION_FAILED';

export interface CardInfo {
  number: number;
  owner: string;
  validity: string;
  ccv: number;
}

export interface SubscriptionsAction {
  type: SubscriptionsActionType;
  payload?: any;
}

export type SubscriptionsActionCreator = (payload?: any) => (dispatch: Dispatch<SubscriptionsAction>) => any;

export interface Subscription {
  id: string;
  name: string;
  icon?: string;
  price: number;
  users: number;
  discount?: number;
}

export interface SubscriptionsState {
  plans: {
    collection: Subscription[];
    selected: string;
    isLoading: boolean;
  };
}

export type PaymentMethod = 'credit-card' | 'paypal';
