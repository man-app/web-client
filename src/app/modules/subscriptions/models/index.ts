import { SubscriptionPlanFromServer } from '../../../../common/apis/manapp';
import { error } from '../../../../common/utils/logger';

import { PaymentMethod, Subscription } from '../types';
import { getState as getPlansState } from './plans';

export const CREDIT_CARD: PaymentMethod = 'credit-card';
export const PAYPAL: PaymentMethod = 'paypal';

export const getState = () => ({
  plans: getPlansState(),
});

export const createSubscriptionPlanFromServer = ({
  discount,
  icon,
  id,
  name,
  price,
  users,
}: SubscriptionPlanFromServer): Subscription | undefined => {
  if (
    typeof id === 'undefined' ||
    typeof name === 'undefined' ||
    typeof price === 'undefined' ||
    typeof users === 'undefined'
  ) {
    error('Subscription plans: Invalid model from server. Some of the following properties is missing', {
      id,
      name,
      price,
      users,
    });

    return undefined;
  }

  return {
    id: String(id),
    name: String(name),
    icon: icon ? String(icon) : undefined,
    price: Number(price),
    users: Number(users),
    discount: discount ? Number(discount) : undefined,
  };
};

export const createSubscriptionPlansFromServer = (subscriptions: SubscriptionPlanFromServer[]): Subscription[] =>
  // @ts-ignore
  subscriptions.map(createSubscriptionPlanFromServer).filter(subscription => !!subscription);
