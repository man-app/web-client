import { SubscriptionsState } from '../types';

export const getState = (): SubscriptionsState['plans'] => ({
  collection: [],
  selected: '',
  isLoading: false,
});
