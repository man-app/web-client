import rootReducer from '../../../../reducers';

import {
  getSubscriptionPlans,
  getFreeSubscriptionPlan,
  getPaidSubscriptionPlans,
  getSelectedSubscriptionPlan,
  getSelectedSubscriptionPlanId,
  isLoadingSubscriptionPlans,
} from '../../selectors';
import { Subscription } from '../../types';

const SUBSCRIPTIONS: Subscription[] = [
  {
    id: '1',
    name: 'Freelance',
    price: 0,
    users: 2,
  },
  {
    id: '2',
    name: 'Start up',
    icon: 'star-o',
    price: 5 * 10,
    users: 10,
    discount: 10,
  },
  {
    id: '3',
    name: 'Bussiness',
    icon: 'briefcase',
    price: 5 * 25,
    users: 25,
    discount: 15,
  },
  {
    id: '4',
    name: 'Large Bussiness',
    icon: 'briefcase',
    price: 5 * 50,
    users: 50,
    discount: 20,
  },
  {
    id: '5',
    name: 'Corporate',
    icon: 'globe',
    price: 5 * 100,
    users: 100,
    discount: 25,
  },
];

describe('SubscriptionPlans selectors', () => {
  const rootState = rootReducer(undefined, { type: 'INIT' });
  const populatedState = {
    ...rootState,
    subscriptions: {
      ...rootState.subscriptions,
      plans: {
        ...rootState.subscriptions.plans,
        collection: SUBSCRIPTIONS,
        selected: SUBSCRIPTIONS[1].id,
      },
    },
  };

  describe('getSubscriptionPlans', () => {
    it('should return all plans', () => {
      const output = getSubscriptionPlans(populatedState);

      expect(output).toStrictEqual(SUBSCRIPTIONS);
    });
  });

  describe('getFreeSubscriptionPlan', () => {
    it('should return a single plan', () => {
      const output = getFreeSubscriptionPlan(populatedState);
      const expectedOutput = SUBSCRIPTIONS[0];

      expect(output).toStrictEqual(expectedOutput);
    });
  });

  describe('getPaidSubscriptionPlans', () => {
    it('should return paid plans', () => {
      const output = getPaidSubscriptionPlans(populatedState);
      const expectedOutput = SUBSCRIPTIONS.slice(1, 5);

      expect(output).toStrictEqual(expectedOutput);
    });
  });

  describe('getSelectedSubscriptionPlan', () => {
    it('should return paid plans', () => {
      const output = getSelectedSubscriptionPlan(populatedState);
      const expectedOutput = SUBSCRIPTIONS[1];

      expect(output).toStrictEqual(expectedOutput);
    });
  });

  describe('getSelectedSubscriptionPlanId', () => {
    it('should return paid plans', () => {
      const output = getSelectedSubscriptionPlanId(populatedState);
      const expectedOutput = SUBSCRIPTIONS[1].id;

      expect(output).toStrictEqual(expectedOutput);
    });
  });

  describe('isLoadingSubscriptionPlans', () => {
    it('should return false', () => {
      const output = isLoadingSubscriptionPlans(populatedState);
      expect(output).toBeFalsy();
    });

    it('should return true', () => {
      const output = isLoadingSubscriptionPlans({
        ...populatedState,
        subscriptions: {
          ...populatedState.subscriptions,
          plans: { ...populatedState.subscriptions.plans, isLoading: true },
        },
      });
      expect(output).toBeTruthy();
    });
  });
});
