import { RootState } from '../../../types';

export const getSubscriptionPlans = ({ subscriptions }: RootState) => subscriptions.plans.collection;

export const getFreeSubscriptionPlan = ({ subscriptions }: RootState) =>
  subscriptions.plans.collection.find(subscription => subscription.price === 0 && subscription.users > 1);

export const getPaidSubscriptionPlans = ({ subscriptions }: RootState) =>
  subscriptions.plans.collection.filter(subscription => subscription.price !== 0 && subscription.users > 1);

export const getSelectedSubscriptionPlan = ({ subscriptions }: RootState) =>
  subscriptions.plans.collection.find(subscription => subscription.id === subscriptions.plans.selected);

export const getSelectedSubscriptionPlanId = ({ subscriptions }: RootState) => subscriptions.plans.selected;

export const isLoadingSubscriptionPlans = ({ subscriptions }: RootState) => subscriptions.plans.isLoading;
