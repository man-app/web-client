import { SubscriptionsActionType } from '../types';

export const SUBSCRIPTIONS_REQUESTED: SubscriptionsActionType = 'SUBSCRIPTIONS_REQUESTED';
export const SUBSCRIPTIONS_UPDATED: SubscriptionsActionType = 'SUBSCRIPTIONS_UPDATED';
export const SUBSCRIPTIONS_NOT_UPDATED: SubscriptionsActionType = 'SUBSCRIPTIONS_NOT_UPDATED';

export const SELECT_SUBSCRIPTION: SubscriptionsActionType = 'SELECT_PLAN';

export const ACTIVATE_SUBSCRIPTION_SUBMITTED: SubscriptionsActionType = 'ACTIVATE_SUBSCRIPTION_SUBMITTED';
export const ACTIVATE_SUBSCRIPTION_SUCCEED: SubscriptionsActionType = 'ACTIVATE_SUBSCRIPTION_SUCCEED';
export const ACTIVATE_SUBSCRIPTION_FAILED: SubscriptionsActionType = 'ACTIVATE_SUBSCRIPTION_FAILED';
