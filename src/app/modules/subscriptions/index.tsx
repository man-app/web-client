import React from 'react';
import { useSelector } from 'react-redux';
import { Switch, Route } from 'react-router';

import { isLogged } from '../auth/selectors';

import RouteAuth from '../../components/routes/auth';
import List from './components/list';
import Activate from './components/activate';

import { SUBSCRIPTIONS, ACTIVATE_SUBSCRIPTION } from '../../../common/constants/appRoutes';

const ShoppingCart: React.FC = () => {
  const isLoggedUser = useSelector(isLogged);

  return (
    <Switch>
      <RouteAuth path={ACTIVATE_SUBSCRIPTION} component={Activate} isLoggedUser={isLoggedUser} />
      <Route path={SUBSCRIPTIONS} component={List} />
    </Switch>
  );
};

export default ShoppingCart;
