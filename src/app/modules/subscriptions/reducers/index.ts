import { combineReducers, Reducer } from 'redux';

import plans from './plans';

import { SubscriptionsState } from '../types';

const reducer: Reducer<SubscriptionsState> = combineReducers({
  plans,
});

export default reducer;
