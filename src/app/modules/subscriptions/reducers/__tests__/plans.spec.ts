import setupEnv from '../../../../../common/utils/setup-test-env';

import reducer from '../plans';

import {
  SUBSCRIPTIONS_REQUESTED,
  SUBSCRIPTIONS_UPDATED,
  SUBSCRIPTIONS_NOT_UPDATED,
  SELECT_SUBSCRIPTION,
} from '../../actionTypes';

import { getState } from '../../models/plans';
import { Subscription } from '../../types';

const freeSubscription: Subscription = {
  id: '1',
  name: 'Freelance',
  price: 0,
  users: 2,
};

const paidSubscription: Subscription = {
  id: '2',
  name: 'Start up',
  icon: 'star-o',
  price: 5 * 10,
  users: 10,
  discount: 10,
};

beforeEach(() => {
  setupEnv();
});

describe('SubscriptionPlans reducer', () => {
  const defaultState = getState();

  describe('Fetch subscriptionPlans', () => {
    it('should mark reducer as isLoading', () => {
      const expectedState = {
        ...defaultState,
        isLoading: true,
      };

      const nextState = reducer(defaultState, { type: SUBSCRIPTIONS_REQUESTED });

      expect(nextState).toStrictEqual(expectedState);
    });

    it('should store subscriptionPlans', () => {
      const initialState = {
        ...defaultState,
        isLoading: true,
      };

      const expectedState = {
        ...initialState,
        collection: [freeSubscription],
        isLoading: false,
      };

      const nextState = reducer(initialState, {
        type: SUBSCRIPTIONS_UPDATED,
        payload: { subscriptionPlans: [freeSubscription] },
      });

      expect(nextState).toStrictEqual(expectedState);
    });

    it('should update subscriptionPlans', () => {
      const initialState = {
        ...defaultState,
        collection: [freeSubscription],
        isLoading: true,
      };

      const expectedState = {
        ...defaultState,
        collection: [freeSubscription, paidSubscription],
      };

      const nextState = reducer(initialState, {
        type: SUBSCRIPTIONS_UPDATED,
        payload: { subscriptionPlans: [paidSubscription] },
      });

      expect(nextState).toStrictEqual(expectedState);
    });

    it('should mark reducer as not fetching', () => {
      const initialState = {
        ...defaultState,
        isLoading: true,
      };

      const nextState = reducer(initialState, { type: SUBSCRIPTIONS_NOT_UPDATED });

      expect(nextState).toStrictEqual(defaultState);
    });
  });

  describe('Select subscription', () => {
    it('should select a subscription', () => {
      const expectedState = {
        ...defaultState,
        selected: paidSubscription.id,
      };

      const nextState = reducer(defaultState, {
        type: SELECT_SUBSCRIPTION,
        payload: { id: paidSubscription.id },
      });

      expect(nextState).toStrictEqual(expectedState);
    });
  });
});
