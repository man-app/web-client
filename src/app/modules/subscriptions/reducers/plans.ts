import { getState } from '../models/plans';
import { SubscriptionsAction, Subscription } from '../types';
import {
  SUBSCRIPTIONS_REQUESTED,
  SUBSCRIPTIONS_UPDATED,
  SUBSCRIPTIONS_NOT_UPDATED,
  SELECT_SUBSCRIPTION,
} from '../actionTypes';
import { updateCollection } from '../../../utils/collections';

const initialState = getState();

const reducer = (state = initialState, action: SubscriptionsAction) => {
  switch (action.type) {
    case SUBSCRIPTIONS_REQUESTED:
      return {
        ...state,
        isLoading: true,
      };

    case SUBSCRIPTIONS_UPDATED:
      const subscriptionPlansFromServer = action.payload.subscriptionPlans as Subscription[];

      return {
        ...state,
        collection: updateCollection(state.collection, subscriptionPlansFromServer),
        isLoading: false,
      };

    case SUBSCRIPTIONS_NOT_UPDATED:
      return {
        ...state,
        isLoading: false,
      };

    case SELECT_SUBSCRIPTION:
      return {
        ...state,
        selected: action.payload.id,
      };

    default:
      return state;
  }
};

export default reducer;
