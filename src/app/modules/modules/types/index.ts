import { Dispatch } from 'redux';

export interface Module {
  [key: string]: string | boolean | undefined;

  id: string;
  name?: string;
  url?: string;
  listing?: boolean;
  creating?: boolean;
  editing?: boolean;
  disabling?: boolean;
  approving?: boolean;
  deleting?: boolean;
  isDeactivatable?: boolean;
}

export interface ModulesState {
  collection: Module[];
  isLoading: boolean;
}

export type ModulesActionType = 'MODULES_REQUESTED' | 'MODULES_UPDATED' | 'MODULES_NOT_UPDATED';

export interface ModulesAction {
  type: ModulesActionType;
  payload?: any;
}

export type ModulesActionCreator = (payload?: any) => (dispatch: Dispatch<ModulesAction>) => any;
