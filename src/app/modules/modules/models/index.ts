import { ModuleFromServer } from '../../../../common/apis/manapp';

import { ModulesState, Module } from '../types';
import { error } from '../../../../common/utils/logger';

export const getState = (): ModulesState => ({
  collection: [],
  isLoading: false,
});

export const createModuleFromServer = ({
  actions: { approving, creating, deleting, disabling, editing, listing },
  description,
  id,
  isDeactivatable,
  name,
  url,
}: ModuleFromServer): Module | undefined => {
  if (!id || !name) {
    error('Modules: Invalid model from server. Some of the following properties is missing', { id, name, url });
    return undefined;
  }

  return {
    id: String(id),
    name: String(name),
    description: description ? String(description) : '',
    url: url ? String(url) : undefined,
    listing: !!listing,
    creating: !!creating,
    editing: !!editing,
    approving: !!approving,
    disabling: !!disabling,
    deleting: !!deleting,
    isDeactivatable: !!isDeactivatable,
  };
};

export const createModulesFromServer = (modules: ModuleFromServer[]): Module[] =>
  // @ts-ignore
  modules.map(createModuleFromServer).filter(mod => !!mod);
