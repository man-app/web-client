import { ModulesActionType } from '../types';

export const MODULES_REQUESTED: ModulesActionType = 'MODULES_REQUESTED';
export const MODULES_UPDATED: ModulesActionType = 'MODULES_UPDATED';
export const MODULES_NOT_UPDATED: ModulesActionType = 'MODULES_NOT_UPDATED';
