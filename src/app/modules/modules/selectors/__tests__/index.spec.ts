import rootReducer from '../../../../reducers';
import * as selectors from '..';

import { Company } from '../../../companies/types';
import { Module } from '../../types';

const company: Company = {
  id: '1',
  name: 'Company',
  slug: 'company',
  modules: ['1'],
  created: 123456789,
  lastModified: 123456789,
};

const module1: Module = {
  id: '1',
  name: 'Test 1',
  url: 'test-module',
};

const module2: Module = {
  id: '2',
  name: 'Test 2',
  url: 'test-module',
};

const initialState = rootReducer(undefined, { type: 'INIT' });
const populatedState = {
  ...initialState,
  companies: {
    ...initialState.companies,
    collection: [company],
  },
  modules: {
    ...initialState.modules,
    collection: [module1, module2],
  },
};

describe('Modules selectors', () => {
  describe('getInstalledModules', () => {
    it('should return 2 modules', () => {
      const expectedOutput = [module1, module2];

      const modules = selectors.getModulesInstalled(populatedState);

      expect(modules).toStrictEqual(expectedOutput);
    });
  });

  describe('getEnabledModules', () => {
    it('should return first module', () => {
      const expectedOutput = [module1];

      const modules = selectors.getModulesEnabled(populatedState)(['1']);

      expect(modules).toStrictEqual(expectedOutput);
    });

    it('should return second module', () => {
      const expectedOutput = [module2];

      const modules = selectors.getModulesEnabled(populatedState)(['2']);

      expect(modules).toStrictEqual(expectedOutput);
    });

    it('should return an empty array', () => {
      const expectedOutput: Module[] = [];

      const modules = selectors.getModulesEnabled(populatedState)(['3']);

      expect(modules).toStrictEqual(expectedOutput);
    });
  });
});
