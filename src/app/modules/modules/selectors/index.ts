import { RootState } from '../../../types';
import { Module } from '../types';

export const getModulesInstalled = ({ modules }: RootState) => modules.collection;

export const getModulesEnabled = ({ modules }: RootState) => (moduleIds: string[]): Module[] => {
  const enabledModules: Module[] = [];

  moduleIds.forEach(m => {
    const existingModule = modules.collection.find(mod => mod.id === m);

    if (existingModule) {
      enabledModules.push(existingModule);
    }
  });

  return enabledModules;
};
