import { MODULES_REQUESTED, MODULES_UPDATED, MODULES_NOT_UPDATED } from '../actionTypes';

import { getState } from '../models';
import { Module, ModulesAction } from '../types';

const initialState = getState();

const reducer = (state = initialState, action: ModulesAction) => {
  switch (action.type) {
    case MODULES_REQUESTED:
      return {
        ...state,
        isLoading: true,
      };

    case MODULES_UPDATED:
      const modulesFromServer = action.payload.modules as Module[];
      return {
        ...state,
        collection: modulesFromServer,
        isLoading: false,
      };

    case MODULES_NOT_UPDATED:
      return {
        ...state,
        isLoading: false,
      };

    default:
      return state;
  }
};

export default reducer;
