import reducer from '..';
import { getState } from '../../models';
import { MODULES_REQUESTED, MODULES_UPDATED, MODULES_NOT_UPDATED } from '../../actionTypes';

const module = {
  id: 1,
};

const initialState = getState();

describe('Modules reducer', () => {
  it('should mark reducer as fetching', () => {
    const expectedState = {
      ...initialState,
      isLoading: true,
    };

    const nextState = reducer(initialState, { type: MODULES_REQUESTED });

    expect(nextState).toStrictEqual(expectedState);
  });

  it('should populate collection', () => {
    const expectedState = {
      ...initialState,
      collection: [module],
    };

    const nextState = reducer(initialState, { type: MODULES_UPDATED, payload: { modules: [module] } });

    expect(nextState).toStrictEqual(expectedState);
  });

  it('should mark reducer as not fetching', () => {
    const nextState = reducer(
      {
        ...initialState,
        isLoading: true,
      },
      { type: MODULES_NOT_UPDATED }
    );

    expect(nextState).toStrictEqual(initialState);
  });
});
