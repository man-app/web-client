import manappApi from '../../../../common/apis/manapp';

import { MODULES_REQUESTED, MODULES_UPDATED, MODULES_NOT_UPDATED } from '../actionTypes';

import { ModulesActionCreator } from '../types';
import { createModulesFromServer } from '../models';

export const getModulesInstalled: ModulesActionCreator = () => dispatch => {
  dispatch({
    type: MODULES_REQUESTED,
  });

  return manappApi.modules
    .getInstalledModules()
    .then(({ modules }) => {
      const payload = {
        modules: createModulesFromServer(modules),
      };

      dispatch({
        type: MODULES_UPDATED,
        payload,
      });

      return payload;
    })
    .catch(err => {
      dispatch({
        type: MODULES_NOT_UPDATED,
        payload: err,
      });

      return err;
    });
};
