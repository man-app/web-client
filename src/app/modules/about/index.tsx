import React from 'react';

import View from './components/view';

const packageJson = require('../../../../package.json');
const VERSION = packageJson.version;

const About: React.FC = () => <View appVersion={VERSION} />;

export default About;
