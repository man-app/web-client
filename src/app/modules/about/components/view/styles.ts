import styled from 'styled-components';

import { Page } from '../../../../components/page';
import { Card as DefaultCard } from '../../../../components/card';

import { LANDSCAPE, TABLET } from '../../../../../common/constants/styles/media-queries';
import getColor from '../../../../../common/constants/styles/theme';
import { ThemedComponent } from '../../../../types/styled-components';

export const Wrapper = styled(Page)`
  ${LANDSCAPE} {
    width: 75%;
  }
`;
Wrapper.displayName = 'Wrapper';

export const Card = styled(DefaultCard)`
  text-align: center;
`;
Card.displayName = 'Card';

export const PictureWrapper = styled.div`
  border-radius: 50%;
  display: inline-block;
  margin: 20px auto;
  overflow: hidden;
  vertical-align: top;
  width: 100px;

  ${TABLET} {
    margin-right: 20px;
    width: 28%;
  }
`;
PictureWrapper.displayName = 'PictureWrapper';

export const Picture = styled.img`
  width: 100%;
`;
Picture.displayName = 'Picture';

export const Info = styled.div`
  color: ${({ theme }: ThemedComponent) => getColor('GLOBAL_COLOR_2', theme.active)};
  display: inline-block;
  font-size: 14px;
  text-align: left;
  transition: color 0.2s;
  vertical-align: top;
  width: 100%;

  ${TABLET} {
    width: calc(70% - 20px);
  }

  & > * {
    line-height: 1.2em;
  }

  & > p {
    margin-bottom: 10px;
  }

  & > ul {
    margin-bottom: 10px;
  }

  & > ul > li {
    font-size: 14px;
    margin-left: 20px;
    line-height: 1.3em;
    list-style: disc;
  }
`;
Info.displayName = 'Info';

export const Title = styled.h3`
  font-size: 26px;
  font-weight: 300;
  margin-bottom: 0;
`;
Title.displayName = 'Title';

export const SubTitle = styled.h4`
  color: ${({ theme }: ThemedComponent) => getColor('GLOBAL_COLOR_3', theme.active)};
  font-size: 12px;
  font-weight: 300;
  margin-bottom: 20px;
  transition: color 0.2s;
`;
SubTitle.displayName = 'SubTitle';
