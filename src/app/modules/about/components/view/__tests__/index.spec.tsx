import React from 'react';
import { shallow } from 'enzyme';

import View from '..';

describe('About', () => {
  it('should render a vaild about screen', () => {
    const component = shallow(<View appVersion={'1.2.3'} />);
    expect(component).toMatchSnapshot();
  });
});
