import React from 'react';

import { Link } from '../../../../components/touchable';
import Spacer from '../../../../components/spacer';
import { Wrapper, Card, PictureWrapper, Picture, Info, Title, SubTitle } from './styles';

import { SUBSCRIPTIONS } from '../../../../../common/constants/appRoutes';
import {
  APP_CHANGELOG,
  APP_DEVELOPER_WEBSITE,
  APP_DEVELOPER,
  APP_NAME,
} from '../../../../../common/constants/branding';
import { MAX_WIDTH } from '../../../../../common/constants/styles/sizes';
import { LinkProps } from '../../../../components/touchable/types';
import List from '../../../../components/list';

const developerLink: LinkProps = {
  id: 'developer-website-link',
  to: APP_DEVELOPER_WEBSITE,
  label: APP_DEVELOPER,
  isExternal: true,
};

const pricingLink: LinkProps = {
  id: 'pricing-link',
  to: SUBSCRIPTIONS,
  label: 'pricing',
};

interface Props {
  appVersion: string;
}

const View: React.FC<Props> = ({ appVersion }: Props) => (
  <Wrapper maxWidth={MAX_WIDTH}>
    <Card>
      <PictureWrapper>
        <Picture src="src/images/logo.png" alt={APP_NAME} />
      </PictureWrapper>

      <Info>
        <Title>{APP_NAME}</Title>
        <SubTitle>
          v{appVersion} -{' '}
          <Link
            options={{
              id: 'changelog-link',
              to: APP_CHANGELOG.replace(':version', appVersion.replace(/\./g, '')),
              label: 'Changelog',
              isExternal: true,
            }}
          />{' '}
          - Developed by <Link options={developerLink} />
        </SubTitle>

        <p>
          {APP_NAME} is a software that allows you to create companies (or group, or communities), so you can manage
          some stuff into it.
        </p>
        <p>Some of the things you will be able to manage are:</p>
        <List>
          {({ Item }) => (
            <>
              <Item>Employees (soon)</Item>
              <Item>Clients (soon)</Item>
              <Item>Tickets (public or internal) (soon)</Item>
              <Item>Products (stock) you sell (soon)</Item>
              <Item>Services you provide (soon)</Item>
              <Item>Budgets (soon)</Item>
              <Item>Calendars (soon)</Item>
              <Item>And more to come! (soon)</Item>
            </>
          )}
        </List>

        <p>You will be able to enable/disable any of those modules, so your interface won {"'"}t be overloaded.</p>
        <Spacer />

        <p>
          As a company manager, you will be able to add other users (employees), so they can help you in your tasks.
        </p>
        <p>
          Checkout our <Link options={pricingLink} /> to know more about it.
        </p>
      </Info>
    </Card>
  </Wrapper>
);

export default View;
