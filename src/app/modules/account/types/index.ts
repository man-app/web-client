import { Dispatch } from 'redux';

export type AccountActionType =
  | 'ACCOUNT_REQUESTED'
  | 'ACCOUNT_UPDATED'
  | 'ACCOUNT_NOT_UPDATED'
  | 'ACCOUNT_FORM_SUBMITTED'
  | 'ACCOUNT_FORM_SUCCEED'
  | 'ACCOUNT_FORM_FAILED'
  | 'EMAIL_VALIDATION_SUBMITTED'
  | 'EMAIL_VALIDATION_SUCCEED'
  | 'EMAIL_VALIDATION_FAILED'
  | 'MODULES_NOTIFICATIONS_FORM_SUBMITTED'
  | 'MODULES_NOTIFICATIONS_FORM_SUCCEED'
  | 'MODULES_NOTIFICATIONS_FORM_FAILED'
  | 'EMAIL_UPDATE_REQUEST_SUBMITTED'
  | 'EMAIL_UPDATE_REQUEST_SUCCEED'
  | 'EMAIL_UPDATE_REQUEST_FAILED'
  | 'EMAIL_UPDATE_FORM_SUBMITTED'
  | 'EMAIL_UPDATE_FORM_SUCCEED'
  | 'EMAIL_UPDATE_FORM_FAILED'
  | 'SESSIONS_REQUESTED'
  | 'SESSIONS_UPDATED'
  | 'SESSIONS_NOT_UPDATED'
  | 'CLOSE_SESSION_REQUESTED'
  | 'CLOSE_SESSION_UPDATED'
  | 'CLOSE_SESSION_NOT_UPDATED'
  | 'CLOSE_SESSIONS_REQUESTED'
  | 'CLOSE_SESSIONS_UPDATED'
  | 'CLOSE_SESSIONS_NOT_UPDATED';

export interface AccountAction {
  type: AccountActionType;
  payload?: any;
}

export type AccountActionCreator = (payload?: any) => (dispatch: Dispatch<AccountAction>) => any;

export interface Session {
  id: string;
  installationId: string;
  userAgent?: string;
  created: number;
  lastModified: number;
  currentSession: boolean;
}

export interface AccountState {
  sessions: Session[];
  isLoading: boolean;
}
