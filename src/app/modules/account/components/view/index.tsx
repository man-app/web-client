import React from 'react';

import Message from '../../../../components/message';
import { MessageText, MessageButtonsWrapper } from '../../../../components/message/styles';
import AccountForm from '../form';
import { Wrapper } from './styles';

import { APP_NAME } from '../../../../../common/constants/branding';
import { UserLogged } from '../../../auth/types';
import { BUTTON } from '../../../../components/forms/models';
import { ButtonProps } from '../../../../components/touchable/types';
import { Buttons } from '../../../../components/touchable';

interface Props {
  user?: UserLogged;
  resendVerificationLink: () => void;
}

const View = ({ resendVerificationLink, user }: Props) => {
  if (!user) return null;

  const buttons: ButtonProps[] = [
    {
      buttonType: 'primary',
      id: 'resend-verification-link',
      label: 'Resend verification link',
      onClick: resendVerificationLink,
      type: BUTTON,
    },
  ];

  return (
    <Wrapper>
      {!user.isVerified && (
        <Message type="warning">
          <>
            <MessageText>Verify your account to start using {APP_NAME}!</MessageText>
            <MessageButtonsWrapper>
              <Buttons options={buttons} />
            </MessageButtonsWrapper>
          </>
        </Message>
      )}

      <AccountForm />
    </Wrapper>
  );
};

export default View;
