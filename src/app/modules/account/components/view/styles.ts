import styled from 'styled-components';

import { Page } from '../../../../components/page';

import { MAX_WIDTH } from '../../../../../common/constants/styles/sizes';

export const Wrapper = styled(Page)`
  max-width: ${MAX_WIDTH}px;
  text-align: center;
`;
Wrapper.displayName = 'Wrapper';
