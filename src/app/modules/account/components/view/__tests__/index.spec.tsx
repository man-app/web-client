import React from 'react';
import { shallow } from 'enzyme';

import View from '..';

import { UserLogged } from '../../../../auth/types';

describe('AccountView', () => {
  const user: UserLogged = {
    id: '1',
    fullName: 'User test',
    email: 'test@user.com',
    created: 123456789,
    lastModified: 123456789,
    isVerified: true,
  };

  it('should render a vaild account screen', () => {
    const component = shallow(<View resendVerificationLink={() => {}} user={user} />);
    expect(component).toMatchSnapshot();
  });

  it('should render a vaild account screen with a validation message', () => {
    const component = shallow(<View resendVerificationLink={() => {}} user={{ ...user, isVerified: false }} />);
    expect(component).toMatchSnapshot();
  });

  it('should render nothing', () => {
    const component = shallow(<View resendVerificationLink={() => {}} user={undefined} />);
    expect(component).toMatchSnapshot();
  });
});
