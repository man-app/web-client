import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Redirect } from 'react-router-dom';
import useReactRouter from 'use-react-router';
import { isValidForm, mapServerErrors } from '../../../../utils/error-handler';

import { getFormErrors, getFormPicture } from '../../../../components/forms/selectors';
import { requestChangeEmailLink } from '../../../auth/actions';
import { getLoggedUser } from '../../../auth/selectors';
import { userUpdate } from '../../actions';
import { isLoadingAccount } from '../../selectors';
import { updateActiveTab, updateAccountFormFields, updateFormState } from './actions';
import reducer from './reducers';

import { updateCollection } from '../../../../components/forms';
import View from './components/view';

import { RESET_PASSWORD, PAYMENT_METHODS, ACCOUNT, SESSIONS } from '../../../../../common/constants/appRoutes';
import { SAVE_ICON, SAVE_LABEL, GOBACK_ICON, GOBACK_LABEL } from '../../../../../common/constants/buttons';
import { UserLogged } from '../../../auth/types';
import { BUTTON, SUBMIT } from '../../../../components/forms/models';
import { FieldProps } from '../../../../components/forms/types';
import { ButtonProps } from '../../../../components/touchable/types';
import { getState } from './models';
import { AccountTab, AccountTabIds } from './types';
import { payments } from '../../../../../common/constants/features';

const form = 'account';

const AccountForm = () => {
  const dispatch = useDispatch();
  const { history, location } = useReactRouter();
  const user = useSelector(getLoggedUser);
  const isLoading = useSelector(isLoadingAccount);
  const errors = useSelector(getFormErrors)(form);
  const formPicture = useSelector(getFormPicture)(form);

  const [redirectTo, setRedirectTo] = React.useState();
  const [{ accountFields, activeTab, optionsFields, profileFields }, stateDispatch] = React.useReducer(
    reducer,
    getState({
      form,
      /* eslint-disable @typescript-eslint/no-use-before-define */
      // @ts-ignore
      handleChangePassword,
      // @ts-ignore
      handleRequestChangeEmailLink,
      location,
      // @ts-ignore
      user,
      /* eslint-enable @typescript-eslint/no-use-before-define */
    })
  );

  const handleRequestChangeEmailLink = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();
    dispatch(requestChangeEmailLink());
  };

  const handleChangePassword = () => {
    setRedirectTo({ pathname: RESET_PASSWORD.replace(':email?', '').replace('/:code?', ''), from: location.pathname });
  };

  React.useEffect(() => {
    stateDispatch(
      updateAccountFormFields({
        accountFields: mapServerErrors(accountFields, errors),
        optionsFields: mapServerErrors(optionsFields, errors),
        profileFields: mapServerErrors(profileFields, errors),
      })
    );
  }, [errors]);

  React.useEffect(() => {
    const initialState = getState({
      form,
      handleChangePassword,
      handleRequestChangeEmailLink,
      location,
      user,
    });

    stateDispatch(updateAccountFormFields(initialState));
  }, [user]);

  if (redirectTo) {
    return <Redirect to={redirectTo} />;
  }

  const toggleTab = (tab: AccountTabIds) => {
    if (activeTab !== tab) {
      stateDispatch(updateActiveTab(tab));
    }
  };

  const goBack = () => {
    history.goBack();
  };

  const onSubmit = (fields: FieldProps[]) => {
    if (!user) {
      return;
    }

    const { id: userId } = user;
    const controlledFields = [
      ...updateCollection(accountFields, fields),
      ...updateCollection(profileFields, fields),
      ...updateCollection(optionsFields, fields),
    ];

    if (isValidForm(controlledFields)) {
      const controlledUser: UserLogged = {
        id: userId,
        fullName: user.fullName,
        email: user.email,
        created: user.created,
        lastModified: user.lastModified,
      };

      // Prepare all properties
      controlledFields.forEach(field => {
        controlledUser[field.id] = field.model;
      });

      if (formPicture) {
        controlledUser.picture = formPicture.url;
      }

      // Prepare checkboxes
      const newsletter = controlledFields.find(field => field.id === 'newsletter');
      controlledUser.newsletter = newsletter && newsletter.isChecked;

      // Delete unused properties
      delete controlledUser.password;

      dispatch(userUpdate({ form, ...controlledUser }));
    } else {
      stateDispatch(updateFormState(true));
    }
  };

  const tabs: AccountTab[] = [
    { id: 'account', label: 'Account', onClick: toggleTab, to: ACCOUNT.replace('/:tab?/:id?', '') },
    { id: 'profile', label: 'Profile', onClick: toggleTab, to: ACCOUNT.replace('/:tab?/:id?', '') },
    { id: 'social', label: 'Social', onClick: toggleTab, to: ACCOUNT.replace('/:tab?/:id?', '') },
    { id: 'sessions', label: 'Sessions', onClick: toggleTab, to: SESSIONS },
    { id: 'options', label: 'Options', onClick: toggleTab, to: ACCOUNT.replace('/:tab?/:id?', '') },
  ];

  if (payments) {
    tabs.push({ id: 'payment-methods', label: 'Payment methods', onClick: toggleTab, to: PAYMENT_METHODS });
  }

  const goBackButton: ButtonProps = {
    icon: GOBACK_ICON,
    id: 'goBack',
    isAlwaysEnabled: true,
    label: GOBACK_LABEL,
    onClick: goBack,
    type: BUTTON,
  };

  const buttons: ButtonProps[] = [
    {
      buttonType: 'primary',
      icon: SAVE_ICON,
      id: 'save',
      label: `${SAVE_LABEL} account`,
      type: SUBMIT,
    },
    goBackButton,
  ];

  return (
    <View
      accountFields={accountFields}
      activeTab={activeTab}
      buttons={buttons}
      form={form}
      goBackButton={goBackButton}
      isLoading={isLoading}
      onSubmit={onSubmit}
      optionsFields={optionsFields}
      profileFields={profileFields}
      tabs={tabs}
    />
  );
};

export default AccountForm;
