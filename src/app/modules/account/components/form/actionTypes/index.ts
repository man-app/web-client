import { UpdateFieldsActionType, UpdateActiveTabActionType, UpdateFormStateActionType } from '../types';

export const UPDATE_FIELDS: UpdateFieldsActionType = 'UPDATE_FIELDS';
export const UPDATE_ACTIVE_TAB: UpdateActiveTabActionType = 'UPDATE_ACTIVE_TAB';
export const UPDATE_FORM_STATE: UpdateFormStateActionType = 'UPDATE_FORM_STATE';
