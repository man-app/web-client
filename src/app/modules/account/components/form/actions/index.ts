import { UPDATE_FIELDS, UPDATE_ACTIVE_TAB, UPDATE_FORM_STATE } from '../actionTypes';

import { FieldProps } from '../../../../../components/forms/types';
import { ButtonProps } from '../../../../../components/touchable/types';
import { AccountTabIds, UpdateFieldsAction, UpdateActiveTabAction, UpdateFormStateAction } from '../types';

export const updateAccountFormFields: (payload: {
  accountFields: FieldProps[];
  profileFields: FieldProps[];
  optionsFields: (FieldProps | ButtonProps)[];
}) => UpdateFieldsAction = payload => ({
  type: UPDATE_FIELDS,
  payload,
});

export const updateActiveTab: (payload: AccountTabIds) => UpdateActiveTabAction = payload => ({
  type: UPDATE_ACTIVE_TAB,
  payload,
});

export const updateFormState: (payload: boolean) => UpdateFormStateAction = payload => ({
  type: UPDATE_FORM_STATE,
  payload,
});
