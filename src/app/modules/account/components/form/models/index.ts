import { FormState } from '../types';
import { PICTURE, EMAIL, BUTTON, TEXT, SWITCH } from '../../../../../components/forms/models';
import { UserLogged } from '../../../../auth/types';
import { APP_NAME } from '../../../../../../common/constants/branding';
import { RouteComponentProps } from 'react-router';
import { PAYMENT_METHODS } from '../../../../../../common/constants/appRoutes';

interface StateDependencies {
  form?: string;
  handleChangePassword?: () => void;
  handleRequestChangeEmailLink?: (event: React.MouseEvent<HTMLButtonElement>) => void;
  location?: RouteComponentProps['location'];
  user?: UserLogged;
}

export const getState = ({
  form = '',
  handleChangePassword = () => {},
  handleRequestChangeEmailLink = () => {},
  location,
  user,
}: StateDependencies): FormState => ({
  isSubmitted: false,
  activeTab: location && new RegExp(PAYMENT_METHODS).test(location.pathname) ? 'payment-methods' : 'account',
  accountFields: [
    {
      type: PICTURE,
      id: 'picture',
      label: 'Upload avatar',
      icon: 'upload',
      model: user && user.picture,
      resource: {
        type: 'user',
        id: user && user.id,
      },
      form,
    },
    {
      type: EMAIL,
      id: 'email',
      label: 'Email',
      model: user && user.email,
      isRequired: true,
      isAlwaysDisabled: true,
    },
    {
      type: BUTTON,
      id: 'change-email',
      label: 'Change email',
      isAlwaysDisabled: false,
      onClick: handleRequestChangeEmailLink,
    },
    {
      type: TEXT,
      id: 'fullName',
      label: 'Full name',
      model: user && user.fullName,
      isRequired: true,
    },
    {
      id: 'password',
      label: 'Change password',
      onClick: handleChangePassword,
      type: BUTTON,
    },
  ],
  profileFields: [
    {
      type: TEXT,
      id: 'document',
      label: 'Document',
      model: user && user.document,
    },
    {
      type: TEXT,
      id: 'contactEmail',
      label: 'Contact email',
      model: user && user.contactEmail,
    },
    {
      type: TEXT,
      id: 'contactPhone',
      label: 'Contact phone',
      model: user && user.contactPhone,
    },
    {
      type: TEXT,
      id: 'address',
      label: 'Address',
      model: user && user.address,
    },
    {
      type: TEXT,
      id: 'city',
      label: 'City',
      model: user && user.city,
    },
    {
      type: TEXT,
      id: 'state',
      label: 'State',
      maxWidth: '50%',
      model: user && user.state,
    },
    {
      type: TEXT,
      id: 'country',
      label: 'Country',
      maxWidth: '50%',
      model: user && user.country,
    },
  ],
  optionsFields: [
    {
      type: SWITCH,
      id: 'newsletter',
      label: `Subscribe me to ${APP_NAME}\'s newsletter, so I can get updates about new features, events, and so on.`,
      isChecked: !!(user && user.newsletter),
    },
    {
      buttonType: 'danger',
      icon: 'chain-broken',
      id: 'cancelAccount',
      isAlwaysDisabled: true,
      label: 'Cancel account',
      type: BUTTON,
    },
  ],
});
