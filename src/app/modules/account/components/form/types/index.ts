import { TabProps } from '../../../../../components/tabs/types';
import { ButtonProps } from '../../../../../components/touchable/types';
import { FieldProps } from '../../../../../components/forms/types';

export type UpdateFieldsActionType = 'UPDATE_FIELDS';
export type UpdateActiveTabActionType = 'UPDATE_ACTIVE_TAB';
export type UpdateFormStateActionType = 'UPDATE_FORM_STATE';

export type AccountFormActionCreator = (payload?: any) => AccountFormAction;

export type AccountTabIds = 'account' | 'profile' | 'social' | 'sessions' | 'options' | 'payment-methods';

export interface UpdateFieldsAction {
  type: 'UPDATE_FIELDS';
  payload: {
    accountFields: FieldProps[];
    profileFields: FieldProps[];
    optionsFields: (FieldProps | ButtonProps)[];
  };
}

export interface UpdateActiveTabAction {
  type: 'UPDATE_ACTIVE_TAB';
  payload: AccountTabIds;
}

export interface UpdateFormStateAction {
  type: 'UPDATE_FORM_STATE';
  payload: boolean;
}

export type AccountFormAction = UpdateFieldsAction | UpdateActiveTabAction | UpdateFormStateAction;

export interface FormState {
  isSubmitted: boolean;
  activeTab: AccountTabIds;
  accountFields: FieldProps[];
  profileFields: FieldProps[];
  optionsFields: (FieldProps | ButtonProps)[];
}

// @ts-ignore
export interface AccountTab extends TabProps {
  id: AccountTabIds;
  onClick?: (id: AccountTabIds) => void;
}
