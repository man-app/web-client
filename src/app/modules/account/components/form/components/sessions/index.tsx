import React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { getSessions, closeSession, closeAllSessions } from '../../../../actions';
import { getSessions as getSessionsSelector, isLoadingAccount } from '../../../../selectors';

import SessionCard from '../session-card';
import SessionsSkeleton from './skeleton';
import { CardsWrapper } from './styled';

import { Buttons } from '../../../../../../components/touchable';
import { BUTTON } from '../../../../../../components/forms/models';

const Sessions = () => {
  const dispatch = useDispatch();
  const sessions = useSelector(getSessionsSelector);
  const isLoading = useSelector(isLoadingAccount);
  const currentSession = sessions.find(session => session.currentSession);
  const otherSessions = sessions.filter(session => !session.currentSession);

  React.useEffect(() => {
    dispatch(getSessions());
  }, []);

  const handleCloseSession = (id: string) => {
    dispatch(closeSession(id));
  };

  const handleCloseAll = () => {
    dispatch(closeAllSessions());
  };

  if (isLoading && !sessions.length) {
    return <SessionsSkeleton count={['', '']} />;
  }

  return (
    <>
      <CardsWrapper>
        {currentSession && (
          <SessionCard key={currentSession.id} handleClose={handleCloseSession} session={currentSession} />
        )}
      </CardsWrapper>

      <CardsWrapper>
        {otherSessions.map(session => (
          <SessionCard key={session.id} handleClose={handleCloseSession} session={session} />
        ))}
      </CardsWrapper>

      <Buttons
        options={[
          {
            buttonType: 'primary',
            icon: 'close',
            id: 'close-all',
            label: 'Close all',
            onClick: handleCloseAll,
            type: BUTTON,
          },
        ]}
      />
    </>
  );
};

export default Sessions;
