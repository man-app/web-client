import React from 'react';
// import styled from 'styled-components';

import { Skeleton, BarWrapper, Bar, Buttons } from '../../../../../../components/skeletons';
import { Card } from '../../../../../../components/card';

interface Props {
  count: string[];
}

const SessionsSkeleton: React.FC<Props> = ({ count }: Props) => (
  <Skeleton>
    {count.map((session, index) => (
      <Card key={index}>
        <BarWrapper>
          <Bar width="40%" />
          <Bar width="30%" />
          <Bar width="35%" />
          <Bar width="27%" />
        </BarWrapper>

        <Buttons count={1} />
      </Card>
    ))}
  </Skeleton>
);

export default SessionsSkeleton;
