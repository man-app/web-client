import React from 'react';
import { shallow } from 'enzyme';

import FormView from '..';

import { TEXT, SUBMIT, BUTTON, CHECKBOX } from '../../../../../../../components/forms/models';
import { AccountTab } from '../../../types';

describe('FormView', () => {
  const accountFields = [{ id: 'account-field', type: TEXT }];
  const buttons = [
    { id: 'submit', type: SUBMIT },
    { id: 'cancel', type: BUTTON },
  ];
  const form = 'account';
  const goBackButton = { id: 'go-back', type: BUTTON };
  const isLoading = false;
  const onSubmit = () => {};
  const optionsFields = [{ id: 'options-field', type: CHECKBOX }];
  const profileFields = [{ id: 'profile-field', type: BUTTON }];
  const tabs = [{ id: 'account' }, { id: 'social' }] as AccountTab[];

  it('should render a valid form view with account tab selected', () => {
    const activeTab = 'account';

    const component = shallow(
      <FormView
        accountFields={accountFields}
        activeTab={activeTab}
        buttons={buttons}
        form={form}
        goBackButton={goBackButton}
        isLoading={isLoading}
        onSubmit={onSubmit}
        optionsFields={optionsFields}
        profileFields={profileFields}
        tabs={tabs}
      />
    );
    expect(component).toMatchSnapshot();
  });

  it('should render a valid form view with payment methods tab selected', () => {
    const activeTab = 'payment-methods';

    const component = shallow(
      <FormView
        accountFields={accountFields}
        activeTab={activeTab}
        buttons={buttons}
        form={form}
        goBackButton={goBackButton}
        isLoading={isLoading}
        onSubmit={onSubmit}
        optionsFields={optionsFields}
        profileFields={profileFields}
        tabs={tabs}
      />
    );
    expect(component).toMatchSnapshot();
  });

  it('should render a valid form view with social tab selected', () => {
    const activeTab = 'social';

    const component = shallow(
      <FormView
        accountFields={accountFields}
        activeTab={activeTab}
        buttons={buttons}
        form={form}
        goBackButton={goBackButton}
        isLoading={isLoading}
        onSubmit={onSubmit}
        optionsFields={optionsFields}
        profileFields={profileFields}
        tabs={tabs}
      />
    );
    expect(component).toMatchSnapshot();
  });
});
