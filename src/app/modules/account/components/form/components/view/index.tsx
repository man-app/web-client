import React from 'react';

import { Buttons } from '../../../../../../components/touchable';
import Tabs from '../../../../../../components/tabs';
import SmartForm, { updateCollection } from '../../../../../../components/forms';
import SocialConnector from '../../../../../social';
import PaymentMethods from '../../../../../payment-methods';
import Sessions from '../sessions';

import { FACEBOOK, GOOGLE, INSTAGRAM, LINKEDIN, TWITTER } from '../../../../../../../common/constants/social-networks';
import { FieldProps } from '../../../../../../components/forms/types';
import { TabProps } from '../../../../../../components/tabs/types';
import { ButtonProps } from '../../../../../../components/touchable/types';
import { Section, Description, SectionTitle } from './styles';
import { AccountTab } from '../../types';

interface Props {
  accountFields: FieldProps[];
  activeTab: string;
  buttons: ButtonProps[];
  form: string;
  goBackButton: ButtonProps;
  isLoading: boolean;
  onSubmit: (values: FieldProps[]) => void;
  optionsFields: FieldProps[];
  profileFields: FieldProps[];
  tabs: AccountTab[];
}

const View = ({
  accountFields,
  activeTab,
  buttons,
  form,
  goBackButton,
  isLoading,
  onSubmit,
  optionsFields,
  profileFields,
  tabs,
}: Props) => (
  <>
    <Tabs activeTab={activeTab} options={tabs as TabProps[]} />

    {activeTab !== 'social' && activeTab !== 'sessions' && activeTab !== 'payment-methods' && (
      <SmartForm
        buttons={buttons}
        fields={[...accountFields, ...profileFields, ...optionsFields]}
        form={form}
        handleSubmit={onSubmit}
        isLoading={isLoading}
      >
        {({ Buttons, controlledButtons, controlledFields, Field }) => (
          <>
            <Section isActive={activeTab === 'account'}>
              {updateCollection(accountFields, controlledFields).map(field => (
                <Field key={field.id} options={field} />
              ))}
            </Section>

            <Section isActive={activeTab === 'profile'}>
              <Description>
                This information is <b>private</b>.
              </Description>
              <Description>
                This information only will be used to populate automatically the Join form, when joining to a company.
              </Description>

              {updateCollection(profileFields, controlledFields).map(field => (
                <Field key={field.id} options={field} />
              ))}
            </Section>

            <Section isActive={activeTab === 'options'}>
              {updateCollection(optionsFields, controlledFields).map(field => (
                <Field key={field.id} options={field} />
              ))}
            </Section>

            <Buttons options={controlledButtons} />
          </>
        )}
      </SmartForm>
    )}

    {activeTab === 'social' && (
      <>
        <Section isActive>
          <Description>
            This information is <b>public</b>.
          </Description>
          <Description>Any company you have joined will be able to see this data.</Description>
        </Section>

        <Section isActive>
          <SectionTitle>LinkedIn account</SectionTitle>

          <SocialConnector platform={LINKEDIN} />
        </Section>

        <Section isActive>
          <SectionTitle>Facebook account</SectionTitle>

          <SocialConnector platform={FACEBOOK} />
        </Section>

        <Section isActive>
          <SectionTitle>Twitter account</SectionTitle>

          <SocialConnector platform={TWITTER} />
        </Section>

        <Section isActive>
          <SectionTitle>Google account</SectionTitle>

          <SocialConnector platform={GOOGLE} />
        </Section>

        <Section isActive>
          <SectionTitle>Instagram account</SectionTitle>

          <SocialConnector platform={INSTAGRAM} />
        </Section>

        <Buttons options={[goBackButton]} />
      </>
    )}

    {activeTab === 'sessions' && <Sessions />}

    {activeTab === 'payment-methods' && <PaymentMethods />}
  </>
);

export default View;
