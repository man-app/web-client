import styled from 'styled-components';

import { Card as DefaultCard } from '../../../../../../components/card';
import {
  DESKTOP_L,
  MOBILE_LANDSCAPE_OR_TABLET,
  TABLET,
} from '../../../../../../../common/constants/styles/media-queries';
import getColor from '../../../../../../../common/constants/styles/theme';
import { ThemedComponent } from '../../../../../../types/styled-components';

interface SectionProps {
  isActive: boolean;
}
export const Section = styled(DefaultCard)`
  display: ${({ isActive }: SectionProps) => (isActive ? 'inline-block' : 'none')};
  margin: 5px 0px;
  text-align: left;
  vertical-align: top;
  width: calc(100% - 10px);

  ${MOBILE_LANDSCAPE_OR_TABLET} {
    width: 75%;
  }

  ${DESKTOP_L} {
    margin: 10px 0px;
    width: 100%;
  }
`;
Section.displayName = 'Section';

export const SectionTitle = styled.h3`
  display: none;

  ${TABLET} {
    display: block;
  }
`;
SectionTitle.displayName = 'SectionTitle';

export const Description = styled.p`
  color: ${({ theme }: ThemedComponent) => getColor('GLOBAL_COLOR_3', theme.active)};
  font-size: 12px;
  line-height: 1em;
  font-style: italic;
  margin-bottom: 10px;
  transition: color 0.2s;

  &:last-child {
    margin-bottom: 0;
  }
`;
Description.displayName = 'Description';
