import styled from 'styled-components';

import { Card as DefaultCard } from '../../../../../../components/card';

import { TABLET, LANDSCAPE, DESKTOP } from '../../../../../../../common/constants/styles/media-queries';
import getColor from '../../../../../../../common/constants/styles/theme';
import { ThemedComponent } from '../../../../../../types/styled-components';

export const Card = styled(DefaultCard)`
  ${LANDSCAPE} {
    max-width: 75%;
  }

  ${TABLET} {
    max-width: 75%;
  }

  ${DESKTOP} {
    max-width: calc(50% - 24px);
  }
`;

export const CurrentSession = styled.span`
  color: ${({ theme }: ThemedComponent) => getColor('SUCCESS_COLOR', theme.active)};
  font-weight: 700;
`;

export const StyledButtons = styled.div`
  & > div,
  & > div > a,
  & > div > button,
  & > div > span {
    margin: 0;
  }
`;
StyledButtons.displayName = 'StyledButtons';
