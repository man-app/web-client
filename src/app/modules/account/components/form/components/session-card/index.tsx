import React from 'react';
// @ts-ignore
import UAParser from 'ua-parser-js';
import { getInstallationData } from '../../../../../../utils/installation';
import { formatDate } from '../../../../../../utils/ui';

import { Buttons } from '../../../../../../components/touchable';
import { Card, CurrentSession, StyledButtons } from './styled';

import { BUTTON } from '../../../../../../components/forms/models';
import { Session } from '../../../../types';

interface Props {
  handleClose: (id: string) => void;
  session: Session;
}

const SessionCard = ({ handleClose, session }: Props) => {
  const { installationId } = getInstallationData();
  const thisDevice = installationId === session.installationId;

  const userAgent = new UAParser(session.userAgent || navigator.userAgent);
  const device = userAgent.getDevice();
  const os = userAgent.getOS();
  const browser = userAgent.getBrowser();

  const buttons = session.currentSession
    ? []
    : [
        {
          buttonType: 'link' as 'link',
          icon: 'close',
          id: 'close',
          label: 'Close',
          onClick: () => {
            handleClose(session.id);
          },
          type: BUTTON,
        },
      ];

  let deviceToShow = 'This device';
  if (device.vendor) {
    deviceToShow = `${device.vendor} ${device.model || ''}`;
  } else if (!thisDevice) {
    deviceToShow = 'PC';
  }

  return (
    <Card>
      <ul>
        <li>
          <b>Device</b>: {deviceToShow}
        </li>

        {os.name && (
          <li>
            <b>OS</b>: {`${os.name} (${os.version})`}
          </li>
        )}

        {browser.name && (
          <li>
            <b>Browser</b>: {`${browser.name} ${browser.major}`}
          </li>
        )}

        <li>
          {session.currentSession ? (
            <CurrentSession>Current session</CurrentSession>
          ) : (
            <>
              <b>Last access</b>: {formatDate(session.lastModified)}
            </>
          )}
        </li>
      </ul>

      <StyledButtons>
        <Buttons options={buttons} />
      </StyledButtons>
    </Card>
  );
};

export default SessionCard;
