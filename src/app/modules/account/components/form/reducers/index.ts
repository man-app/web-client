import { UPDATE_FIELDS, UPDATE_ACTIVE_TAB, UPDATE_FORM_STATE } from '../actionTypes';

import { FormState, AccountFormAction } from '../types';

const reducer = (state: FormState, action: AccountFormAction) => {
  switch (action.type) {
    case UPDATE_FIELDS:
      return {
        ...state,
        accountFields: action.payload.accountFields,
        optionsFields: action.payload.optionsFields,
        profileFields: action.payload.profileFields,
      };

    case UPDATE_ACTIVE_TAB:
      return {
        ...state,
        activeTab: action.payload,
      };

    case UPDATE_FORM_STATE:
      return {
        ...state,
        isSubmitted: action.payload,
      };

    default:
      return state;
  }
};

export default reducer;
