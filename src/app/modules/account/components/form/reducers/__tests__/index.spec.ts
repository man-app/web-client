import setupEnv from '../../../../../../../common/utils/setup-test-env';

import { updateActiveTab, updateFormState, updateAccountFormFields } from '../../actions';
import reducer from '..';

import { getState } from '../../models';

beforeEach(() => {
  setupEnv();
});

describe('AccountForm component reducer', () => {
  describe('updateFormState', () => {
    it('should set isSubmitted to true', () => {
      const initialState = getState({});

      const expectedState = {
        ...initialState,
        isSubmitted: true,
      };

      const nextState = reducer(initialState, updateFormState(true));
      expect(nextState).toStrictEqual(expectedState);
    });
  });

  describe('updateActiveTab', () => {
    it('should set activeTab to "account"', () => {
      const initialState = getState({});

      const expectedState = {
        ...initialState,
        activeTab: 'account',
      };

      const nextState = reducer(initialState, updateActiveTab('account'));
      expect(nextState).toStrictEqual(expectedState);
    });
  });

  describe('updateFields', () => {
    it('should set an error on "test" field', () => {
      const initialState = getState({});

      const expectedState = {
        ...initialState,
        accountFields: initialState.accountFields.map((f, idx) =>
          idx === 0 ? { ...f, error: 'This field has an error' } : f
        ),
      };

      const nextState = reducer(
        initialState,
        updateAccountFormFields({
          accountFields: initialState.accountFields.map((f, idx) =>
            idx === 0 ? { ...f, error: 'This field has an error' } : f
          ),
          optionsFields: initialState.optionsFields,
          profileFields: initialState.profileFields,
        })
      );
      expect(nextState).toStrictEqual(expectedState);
    });
  });
});
