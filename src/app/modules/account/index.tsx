import React from 'react';
import { useSelector, useDispatch } from 'react-redux';

import { requestValidationLink } from '../auth/actions';
import { getLoggedUser } from '../auth/selectors';

import View from './components/view';

const Account = () => {
  const user = useSelector(getLoggedUser);
  const dispatch = useDispatch();

  const resendVerificationLink = () => {
    if (!user) return;

    const data = {
      email: user.email,
    };

    dispatch(requestValidationLink(data));
  };

  return <View user={user} resendVerificationLink={resendVerificationLink} />;
};

export default Account;
