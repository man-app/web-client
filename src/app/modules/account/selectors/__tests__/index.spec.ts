import rootReducer from '../../../../reducers';
import * as selectors from '..';

describe('Activities selectors', () => {
  const rootState = rootReducer(undefined, { type: 'INIT' });

  describe('isSilentUpdate', () => {
    it('should return false', () => {
      const result = selectors.isLoadingAccount(rootState);
      const expectedResult = false;

      expect(result).toBe(expectedResult);
    });
  });
});
