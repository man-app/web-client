import { RootState } from '../../../types';
import { sortBy } from '../../../utils/arrays';

export const getSessions = ({ account }: RootState) =>
  account.sessions.map(s => s).sort(sortBy('lastModified', 'desc'));

export const isLoadingAccount = ({ account }: RootState) => account.isLoading;
