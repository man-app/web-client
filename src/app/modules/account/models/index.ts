import { AccountState, Session } from '../types';
import { SessionFromServer } from '../../../../common/apis/manapp';

export const getState = (isLoading = false): AccountState => ({
  sessions: [],
  isLoading,
});

const createSessionFromServer = ({
  created,
  currentSession,
  id,
  installationId,
  lastModified,
  userAgent,
}: SessionFromServer): Session | undefined => {
  if (typeof created === 'undefined' || typeof id === 'undefined' || typeof installationId === 'undefined') {
    return undefined;
  }

  return {
    created: Number(created),
    currentSession: !!currentSession,
    id: String(id),
    installationId: String(installationId),
    lastModified: Number(lastModified || created),
    userAgent: userAgent ? String(userAgent) : undefined,
  };
};

export const createSessionsFromServer = (sessions: SessionFromServer[]): Session[] =>
  // @ts-ignore
  sessions.map(createSessionFromServer).filter(session => !!session);
