import manappApi from '../../../../common/apis/manapp';

import {
  ACCOUNT_FORM_SUBMITTED,
  ACCOUNT_FORM_SUCCEED,
  ACCOUNT_FORM_FAILED,
  MODULES_NOTIFICATIONS_FORM_SUBMITTED,
  MODULES_NOTIFICATIONS_FORM_SUCCEED,
  MODULES_NOTIFICATIONS_FORM_FAILED,
  SESSIONS_REQUESTED,
  SESSIONS_UPDATED,
  SESSIONS_NOT_UPDATED,
  CLOSE_SESSION_REQUESTED,
  CLOSE_SESSION_UPDATED,
  CLOSE_SESSION_NOT_UPDATED,
  CLOSE_SESSIONS_REQUESTED,
  CLOSE_SESSIONS_UPDATED,
  CLOSE_SESSIONS_NOT_UPDATED,
} from '../actionTypes';

import { AccountActionCreator } from '../types';
import { createUserFromServer, createUsersFromServer } from '../../auth/models';
import { createSessionsFromServer } from '../models';

export const userUpdate: AccountActionCreator = payload => dispatch => {
  const { form, ...params } = payload;
  dispatch({
    type: ACCOUNT_FORM_SUBMITTED,
    payload: { form },
  });

  return manappApi.account
    .update(params)
    .then(({ notifications, users }) => {
      const payload = {
        form,
        notifications,
        users: createUsersFromServer(users),
      };

      dispatch({
        type: ACCOUNT_FORM_SUCCEED,
        payload,
      });

      return payload;
    })
    .catch(err => {
      dispatch({
        type: ACCOUNT_FORM_FAILED,
        payload: {
          form,
          ...err,
        },
      });

      return err;
    });
};

export const userModuleNotificationsUpdate: AccountActionCreator = payload => dispatch => {
  const { form, idCompany, params } = payload;
  dispatch({
    type: MODULES_NOTIFICATIONS_FORM_SUBMITTED,
    payload: { form },
  });

  return manappApi.account
    .updateNotifications(idCompany, params)
    .then(({ notifications, user }) => {
      const payload = {
        form,
        notifications,
        user: createUserFromServer(user),
      };

      dispatch({
        type: MODULES_NOTIFICATIONS_FORM_SUCCEED,
        payload,
      });

      return payload;
    })
    .catch(err => {
      dispatch({
        type: MODULES_NOTIFICATIONS_FORM_FAILED,
        payload: {
          form,
          ...err,
        },
      });

      return err;
    });
};

export const getSessions: AccountActionCreator = () => dispatch => {
  dispatch({
    type: SESSIONS_REQUESTED,
  });

  return manappApi.account
    .getSessions()
    .then(({ notifications, sessions }) => {
      const payload = {
        notifications,
        sessions: createSessionsFromServer(sessions),
      };

      dispatch({
        type: SESSIONS_UPDATED,
        payload,
      });

      return payload;
    })
    .catch(err => {
      dispatch({
        type: SESSIONS_NOT_UPDATED,
        payload: {
          err,
        },
      });

      return err;
    });
};

export const closeSession: AccountActionCreator = (idSession: string) => dispatch => {
  dispatch({
    type: CLOSE_SESSION_REQUESTED,
  });

  return manappApi.account
    .closeSession(idSession)
    .then(() => {
      const payload = {
        id: idSession,
      };

      dispatch({
        type: CLOSE_SESSION_UPDATED,
        payload,
      });

      return payload;
    })
    .catch(err => {
      dispatch({
        type: CLOSE_SESSION_NOT_UPDATED,
        payload: {
          err,
        },
      });

      return err;
    });
};

export const closeAllSessions: AccountActionCreator = () => dispatch => {
  dispatch({
    type: CLOSE_SESSIONS_REQUESTED,
  });

  return manappApi.account
    .closeAllSessions()
    .then(() => {
      dispatch({
        type: CLOSE_SESSIONS_UPDATED,
      });
    })
    .catch(err => {
      dispatch({
        type: CLOSE_SESSIONS_NOT_UPDATED,
        payload: {
          err,
        },
      });

      return err;
    });
};
