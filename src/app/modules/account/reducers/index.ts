import {
  ACCOUNT_FORM_SUBMITTED,
  ACCOUNT_FORM_SUCCEED,
  ACCOUNT_FORM_FAILED,
  SESSIONS_REQUESTED,
  SESSIONS_UPDATED,
  SESSIONS_NOT_UPDATED,
  CLOSE_SESSION_REQUESTED,
  CLOSE_SESSION_UPDATED,
  CLOSE_SESSION_NOT_UPDATED,
  CLOSE_SESSIONS_REQUESTED,
  CLOSE_SESSIONS_UPDATED,
  CLOSE_SESSIONS_NOT_UPDATED,
} from '../actionTypes';
import { LOGOUT_SUCCEED } from '../../auth/actionTypes';

import { LogoutAction } from '../../auth/types';
import { getState } from '../models';
import { AccountState, AccountAction, Session } from '../types';

const initialState = getState();

const reducer = (state: AccountState = initialState, action: AccountAction | LogoutAction): AccountState => {
  switch (action.type) {
    case ACCOUNT_FORM_SUBMITTED:
    case SESSIONS_REQUESTED:
    case CLOSE_SESSION_REQUESTED:
    case CLOSE_SESSIONS_REQUESTED:
      return {
        ...state,
        isLoading: true,
      };

    case ACCOUNT_FORM_SUCCEED:
    case ACCOUNT_FORM_FAILED:
    case SESSIONS_NOT_UPDATED:
    case CLOSE_SESSION_NOT_UPDATED:
    case CLOSE_SESSIONS_NOT_UPDATED:
      return {
        ...state,
        isLoading: false,
      };

    case SESSIONS_UPDATED:
      const sessionsFromServer = action.payload.sessions as Session[];

      return {
        ...state,
        sessions: sessionsFromServer,
        isLoading: false,
      };

    case CLOSE_SESSION_UPDATED:
      const sessionFromserver = action.payload as Session;

      return {
        ...state,
        sessions: state.sessions.filter(session => session.id !== sessionFromserver.id),
        isLoading: false,
      };

    case CLOSE_SESSIONS_UPDATED:
      return {
        ...state,
        sessions: state.sessions.filter(session => session.currentSession),
        isLoading: false,
      };

    case LOGOUT_SUCCEED:
      return {
        ...initialState,
      };

    default:
      return state;
  }
};

export default reducer;
