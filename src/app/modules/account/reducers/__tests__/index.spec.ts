import setupEnv from '../../../../../common/utils/setup-test-env';

import reducer from '..';

import { LOGOUT_SUCCEED } from '../../../auth/actionTypes';
import { ACCOUNT_FORM_SUBMITTED, ACCOUNT_FORM_SUCCEED, ACCOUNT_FORM_FAILED } from '../../actionTypes';

beforeEach(() => {
  setupEnv();
});

describe('Account reducer', () => {
  describe('Account form', () => {
    it('should submit the account form', () => {
      const initialState = {
        sessions: [],
        isLoading: false,
      };

      const expectedState = {
        sessions: [],
        isLoading: true,
      };

      const nextState = reducer(initialState, { type: ACCOUNT_FORM_SUBMITTED });
      expect(nextState).toStrictEqual(expectedState);
    });

    it('should set the form as successfully submitted', () => {
      const initialState = {
        sessions: [],
        isLoading: true,
      };

      const expectedState = {
        sessions: [],
        isLoading: false,
      };

      const nextState = reducer(initialState, { type: ACCOUNT_FORM_SUCCEED });
      expect(nextState).toStrictEqual(expectedState);
    });

    it('should set the form as failed submitted', () => {
      const initialState = {
        sessions: [],
        isLoading: true,
      };

      const expectedState = {
        sessions: [],
        isLoading: false,
      };

      const nextState = reducer(initialState, { type: ACCOUNT_FORM_FAILED });
      expect(nextState).toStrictEqual(expectedState);
    });
  });

  describe('Logout action', () => {
    it('should return initial state', () => {
      const initialState = {
        sessions: [],
        isLoading: false,
      };

      const expectedState = {
        sessions: [],
        isLoading: false,
      };

      const nextState = reducer(initialState, { type: LOGOUT_SUCCEED });

      expect(nextState).toStrictEqual(expectedState);
    });
  });
});
