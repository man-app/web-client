import reducer from '..';

import { LOGOUT_SUCCEED } from '../../../auth/actionTypes';
import { ROLES_REQUESTED, ROLES_UPDATED, ROLES_NOT_UPDATED, ROLE_SELECTED } from '../../actionTypes';

import { Role, RolesState } from '../../types';
import { getState } from '../../models';

const role1: Role = {
  id: '1',
  idCompany: '1',
  name: 'Admin',
  description: 'Description test',
  isActive: true,
  isDeactivatable: false,
  modules: [
    {
      id: '1',
      approving: true,
      creating: true,
      deleting: true,
      disabling: true,
      editing: true,
      listing: true,
    },
  ],
  created: 123456789,
  lastModified: 123456789,
};

const role2: Role = {
  id: '2',
  idCompany: '1',
  name: 'Demo',
  description: 'Description test',
  isActive: true,
  isDeactivatable: true,
  modules: [
    {
      id: '1',
      listing: true,
    },
  ],
  created: 123456789,
  lastModified: 123456789,
};

const role3: Role = {
  id: '3',
  idCompany: '2',
  name: 'Demo',
  description: 'Description test',
  isActive: true,
  isDeactivatable: true,
  modules: [
    {
      id: '1',
      listing: true,
    },
  ],
  created: 123456789,
  lastModified: 123456789,
};

const initialState = getState();
const populatedState = {
  ...initialState,
  collection: [role1, role2, role3],
};

describe('Roles reducer', () => {
  describe('ROLES actions', () => {
    it('should mark isLoading as true', () => {
      const expectedState = {
        ...initialState,
        isLoading: true,
      };

      const nextState = reducer(initialState, { type: ROLES_REQUESTED });

      expect(nextState).toStrictEqual(expectedState);
    });

    it('should mark isLoading as false, and update roles collection', () => {
      const nextState = reducer(
        { ...initialState, isLoading: true },
        { type: ROLES_UPDATED, payload: { roles: populatedState.collection } }
      );

      expect(nextState).toStrictEqual(populatedState);
    });

    it('should mark isLoading as false, and not updating roles collection', () => {
      const nextState = reducer(
        { ...initialState, isLoading: true },
        { type: ROLES_NOT_UPDATED, payload: { roles: populatedState.collection } }
      );

      expect(nextState).toStrictEqual(initialState);
    });

    it('should mark a role as selected', () => {
      const expectedState: RolesState = {
        ...populatedState,
        selected: '1',
      };

      const nextState = reducer(populatedState, { type: ROLE_SELECTED, payload: { id: '1' } });

      expect(nextState).toStrictEqual(expectedState);
    });
  });

  describe('LOGOUT_ACTION', () => {
    it('should return initial state', () => {
      const nextState = reducer(populatedState, { type: LOGOUT_SUCCEED });

      expect(nextState).toStrictEqual(initialState);
    });
  });
});
