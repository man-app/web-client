import { updateCollection } from '../../../utils/collections';

import {
  ROLES_REQUESTED,
  ROLES_UPDATED,
  ROLES_NOT_UPDATED,
  ROLE_FORM_SUBMITTED,
  ROLE_FORM_SUCCEED,
  ROLE_FORM_FAILED,
  ROLE_SELECTED,
} from '../actionTypes';
import { LOGOUT_SUCCEED } from '../../auth/actionTypes';

import { LogoutAction } from '../../auth/types';
import { getState } from '../models';
import { RolesAction, RolesState, Role } from '../types';

const initialState = getState();

const reducer = (state = initialState, action: RolesAction | LogoutAction): RolesState => {
  switch (action.type) {
    case ROLES_REQUESTED:
    case ROLE_FORM_SUBMITTED:
      return {
        ...state,
        errors: [],
        isLoading: true,
      };

    case ROLES_UPDATED:
    case ROLE_FORM_SUCCEED:
      const rolesFromServer = action.payload.roles as Role[];
      return {
        ...state,
        collection: updateCollection(state.collection, rolesFromServer),
        errors: [] as any[],
        isLoading: false,
      };

    case ROLES_NOT_UPDATED:
    case ROLE_FORM_FAILED:
      return {
        ...state,
        errors: (action.payload.errors as any[]) || ([] as any[]),
        isLoading: false,
      };

    case ROLE_SELECTED:
      return {
        ...state,
        selected: String(action.payload.id),
      };

    case LOGOUT_SUCCEED:
      return {
        ...initialState,
      };

    default:
      return state;
  }
};

export default reducer;
