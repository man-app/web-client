import rootReducer from '../../../../reducers';
import * as selectors from '..';

import { Company } from '../../../companies/types';
import { Role } from '../../types';

const company: Company = {
  id: '1',
  name: 'Company',
  slug: 'company',
  created: 123456789,
  lastModified: 123456789,
};

const role1: Role = {
  id: '1',
  idCompany: '1',
  name: 'Admin',
  description: 'Description test',
  isActive: true,
  isDeactivatable: false,
  modules: [
    {
      id: '1',
      approving: true,
      creating: true,
      deleting: true,
      disabling: true,
      editing: true,
      listing: true,
    },
  ],
  created: 123456789,
  lastModified: 123456789,
};

const role2: Role = {
  id: '2',
  idCompany: '1',
  name: 'Demo',
  description: 'Description test',
  isActive: true,
  isDeactivatable: true,
  modules: [
    {
      id: '1',
      listing: true,
    },
  ],
  created: 123456789,
  lastModified: 123456789,
};

const role3: Role = {
  id: '3',
  idCompany: '2',
  name: 'Demo',
  description: 'Description test',
  isActive: true,
  isDeactivatable: true,
  modules: [
    {
      id: '1',
      listing: true,
    },
  ],
  created: 123456789,
  lastModified: 123456789,
};

const initialState = rootReducer(undefined, { type: 'INIT' });
const populatedState = {
  ...initialState,
  companies: {
    ...initialState.companies,
    collection: [company],
  },
  roles: {
    ...initialState.roles,
    collection: [role1, role2, role3],
    selected: '2',
  },
};

describe('Roles selectors', () => {
  describe('isLoadingRoles', () => {
    it('Should return false', () => {
      const expectedOutput = false;
      const output = selectors.isLoadingRoles(populatedState);

      expect(output).toStrictEqual(expectedOutput);
    });
  });

  describe('getRoles', () => {
    it('Should return one role', () => {
      const expectedOutput = [role3];
      const output = selectors.getRoles(populatedState)('2');

      expect(output).toStrictEqual(expectedOutput);
    });

    it('Should return two roles', () => {
      const expectedOutput = [role1, role2];
      const output = selectors.getRoles(populatedState)('1');

      expect(output).toStrictEqual(expectedOutput);
    });
  });

  describe('getRole', () => {
    it('Should return one role', () => {
      const expectedOutput = role3;
      const output = selectors.getRole(populatedState)('3');

      expect(output).toStrictEqual(expectedOutput);
    });
  });

  describe('getSelectedRole', () => {
    it('Should return one role', () => {
      const expectedOutput = role2;
      const output = selectors.getSelectedRole(populatedState);

      expect(output).toStrictEqual(expectedOutput);
    });
  });
});
