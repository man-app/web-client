import { RootState } from '../../../types';

export const isLoadingRoles = ({ roles }: RootState) => roles.isLoading;

export const getRoles = ({ roles }: RootState) => (companyId: string) =>
  roles.collection.filter(r => r.idCompany === companyId);

export const getRole = ({ roles }: RootState) => (idRole: string) => roles.collection.find(r => r.id === idRole);

export const getSelectedRole = ({ roles }: RootState) => roles.collection.find(r => r.id === roles.selected);
