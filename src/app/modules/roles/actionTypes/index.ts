import { RolesActionType } from '../types';

export const ROLES_REQUESTED: RolesActionType = 'ROLES_REQUESTED';
export const ROLES_UPDATED: RolesActionType = 'ROLES_UPDATED';
export const ROLES_NOT_UPDATED: RolesActionType = 'ROLES_NOT_UPDATED';

export const ROLE_FORM_SUBMITTED: RolesActionType = 'ROLE_FORM_SUBMITTED';
export const ROLE_FORM_SUCCEED: RolesActionType = 'ROLE_FORM_SUCCEED';
export const ROLE_FORM_FAILED: RolesActionType = 'ROLE_FORM_FAILED';

export const ROLE_SELECTED: RolesActionType = 'ROLE_SELECTED';
