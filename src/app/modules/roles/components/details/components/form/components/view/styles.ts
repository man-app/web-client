import styled from 'styled-components';

import { TABLET } from '../../../../../../../../../common/constants/styles/media-queries';

export const RoleModule = styled.div`
  margin-top: 20px;
`;

export const Title = styled.h2``;

export const Description = styled.p``;

export const FieldWrapper = styled.span`
  & > span {
    ${TABLET} {
      display: inline-block !important;
      margin: 5px;
      min-width: 150px;
      width: auto;
    }
  }
`;
