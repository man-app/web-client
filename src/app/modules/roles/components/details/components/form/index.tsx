import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Redirect } from 'react-router-dom';
import useReactRouter from 'use-react-router';
import { CreateRole } from '../../../../../../../common/apis/manapp';
import { mapServerErrors, isValidForm } from '../../../../../../utils/error-handler';
import { getPermissions, moduleIds } from '../../../../../../utils/permissions';

import { SAVE_LABEL, SAVE_ICON, GOBACK_LABEL, GOBACK_ICON } from '../../../../../../../common/constants/buttons';
import { COMPANY_ROLES } from '../../../../../../../common/constants/appRoutes';

import { createRole, updateRole } from '../../../../actions';
import { getFormErrors } from '../../../../../../components/forms/selectors';
import { getLoggedUser } from '../../../../../auth/selectors';
import { getSelectedCompany } from '../../../../../companies/selectors';
import { getModulesEnabled } from '../../../../../modules/selectors';
import { getSelectedRole, isLoadingRoles } from '../../../../../roles/selectors';

import View from './components/view';

import { BUTTON, SUBMIT, ARRAY, TEXT, SWITCH, TEXTAREA } from '../../../../../../components/forms/models';
import { FieldProps } from '../../../../../../components/forms/types';
import { ButtonProps } from '../../../../../../components/touchable/types';
import { Company } from '../../../../../companies/types';
import { Module } from '../../../../../modules/types';
import { ModulePermissionsField, RoleModule } from '../../../../types';

const form = 'role';

const RoleForm: React.FC = () => {
  const [redirectTo, setRedirectTo] = React.useState('' as any);

  const {
    history,
    match: { params },
  } = useReactRouter();
  const dispatch = useDispatch();

  const user = useSelector(getLoggedUser);
  const isNewRole = !params.id;
  const isLoading = useSelector(isLoadingRoles);
  const errors = useSelector(getFormErrors)(form);
  const company = useSelector(getSelectedCompany) as Company;
  const enabledModules = company.modules ? useSelector(getModulesEnabled)(company.modules) : [];
  const thisModule = enabledModules.find(m => m.id === moduleIds.roles);
  const permissions = thisModule ? getPermissions(thisModule, user, company) : {};
  const selectedRole = useSelector(getSelectedRole);

  const transformRoleToFields = (): ModulePermissionsField[] => {
    const moduleFields: ModulePermissionsField[] = [];

    enabledModules
      .map(mod => {
        const getDefaultActionValue = (action: string): boolean | undefined => {
          const selectedModule =
            selectedRole && selectedRole.modules && selectedRole.modules.find(m => m.id === mod.id);

          if (!selectedModule) {
            return false;
          }

          return selectedModule[action] as boolean | undefined;
        };

        const getRoleModuleId = () => {
          const roleModule = selectedRole && selectedRole.modules.find(m => m.id === mod.id);
          if (roleModule) {
            return roleModule.id;
          }

          return undefined;
        };

        const m: Module = {
          id: mod.id,
          name: mod.name,
          url: mod.url,
          description: mod.description,
          idRole: getRoleModuleId(),
        };

        if (mod.listing) m.listing = getDefaultActionValue('listing');
        if (mod.creating) m.creating = getDefaultActionValue('creating');
        if (mod.editing) m.editing = getDefaultActionValue('editing');
        if (mod.approving) m.approving = getDefaultActionValue('approving');
        if (mod.disabling) m.disabling = getDefaultActionValue('disabling');
        if (mod.deleting) m.deleting = getDefaultActionValue('deleting');

        return m;
      })
      .forEach(mod => {
        const referenceModule = enabledModules.find(m => m.id === mod.id);

        const module: ModulePermissionsField = {
          description: mod.description as string,
          id: `array-${mod.id}`,
          idModule: mod.id as string,
          idRole: (mod.idRole as unknown) as string,
          fields: [] as FieldProps[],
          name: mod.name || '',
          type: 'array',
        };

        Object.keys(mod).forEach(key => {
          if (
            key !== 'id' &&
            key !== 'name' &&
            key !== 'description' &&
            key !== 'url' &&
            referenceModule &&
            referenceModule[key]
          ) {
            const field = {
              className: 'is-inline',
              id: `${mod.id}-${key}`,
              isChecked: !isNewRole && !!mod[key],
              // @ts-ignore
              label: (permissions[key] && permissions[key].label) || '',
              updateId: company && company.slug,
              type: SWITCH,
            };

            module.fields.push(field);
          }
        });

        moduleFields.push(module);
      });

    return moduleFields;
  };

  const transformFieldsToRole = (newFields: FieldProps[]) => {
    const roleName: FieldProps | void = newFields.find(f => f.id === 'name');
    const roleDescription: FieldProps | void = newFields.find(f => f.id === 'description');

    const role: CreateRole = {
      id: isNewRole ? undefined : (selectedRole && selectedRole.id) || '0',
      idCompany: company.id,
      name: (roleName && roleName.model) || '',
      description: (roleDescription && roleDescription.model) || '',
      modules: [] as RoleModule[],
    };

    // Create a role for each section/module
    newFields
      .filter(f => f.type === ARRAY)
      // @ts-ignore
      .forEach((field: ModulePermissionsField) => {
        const listing = field.fields.find(f => f.id === `${field.idModule}-listing`);
        const creating = field.fields.find(f => f.id === `${field.idModule}-creating`);
        const editing = field.fields.find(f => f.id === `${field.idModule}-editing`);
        const approving = field.fields.find(f => f.id === `${field.idModule}-approving`);
        const disabling = field.fields.find(f => f.id === `${field.idModule}-disabling`);
        const deleting = field.fields.find(f => f.id === `${field.idModule}-deleting`);

        const roleModule = {
          id: field.idModule,
          listing: listing && listing.isChecked,
          creating: creating && creating.isChecked,
          editing: editing && editing.isChecked,
          approving: approving && approving.isChecked,
          disabling: disabling && disabling.isChecked,
          deleting: deleting && deleting.isChecked,
        };

        role.modules.push(roleModule);
      });

    return role;
  };

  const generateFields = (): FieldProps[] => [
    {
      type: TEXT,
      id: 'name',
      updateId: company && company.slug,
      label: 'Name',
      model: !isNewRole ? (selectedRole && selectedRole.name) || '' : undefined,
      isRequired: true,
    },
    {
      type: TEXTAREA,
      id: 'description',
      updateId: company && company.slug,
      label: 'Description',
      model: !isNewRole ? (selectedRole && selectedRole.description) || '' : undefined,
      isRequired: true,
    },
    ...transformRoleToFields(),
  ];

  const [fields, setFields] = React.useState([] as FieldProps[]);

  React.useEffect(() => {
    setFields(mapServerErrors(fields, errors));
  }, [errors]);

  React.useEffect(() => {
    if (isNewRole || (selectedRole && selectedRole.id === String(params.id))) {
      setFields(generateFields());
    } else {
      setFields([]);
    }
  }, [selectedRole]);

  const goBack = () => {
    history.goBack();
  };

  const handleSubmit = (fields: FieldProps[]) => {
    // Valid form
    if (isValidForm(fields)) {
      const role = transformFieldsToRole(fields);

      const actionToDispatch = role.id ? updateRole : createRole;

      dispatch(
        actionToDispatch({
          form,
          idCompany: company.id,
          roles: [
            {
              ...role,
              modules: role.modules.map(({ id, ...mod }) => ({
                ...mod,
                idModule: id,
              })),
            },
          ],
        })
        // @ts-ignore
      ).then(({ errors }: { errors: any[] }) => {
        if (!errors || !errors.length) {
          setRedirectTo({
            pathname: COMPANY_ROLES.replace(':slug', company.slug),
            state: { from: window.location.pathname },
          });
        }
      });
    }
  };

  // When no company data is present, stop execution
  // TODO: Show a loading spinner?
  if (fields.length === 0) {
    return null;
  }

  const buttons: ButtonProps[] = [
    {
      buttonType: 'primary',
      icon: SAVE_ICON,
      id: 'save',
      label: `${SAVE_LABEL} role`,
      type: SUBMIT,
    },
    {
      icon: GOBACK_ICON,
      id: 'goback',
      isAlwaysEnabled: true,
      label: GOBACK_LABEL,
      onClick: goBack,
      type: BUTTON,
    },
  ];

  if (redirectTo) {
    return <Redirect to={redirectTo} />;
  }

  return (
    <View
      buttons={buttons}
      defaultEnabled={isNewRole}
      fields={fields}
      form={form}
      handleSubmit={handleSubmit}
      isLoading={isLoading}
      permissions={isNewRole ? permissions.creating : permissions.editing}
    />
  );
};

export default RoleForm;
