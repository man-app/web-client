import React from 'react';
import { shallow } from 'enzyme';

import View from '..';

describe('Role Form', () => {
  it('Should render a valid screen', () => {
    const component = shallow(
      <View buttons={[]} fields={[]} form={'test'} handleSubmit={() => {}} isLoading={false} />
    );

    expect(component).toMatchSnapshot();
  });
});
