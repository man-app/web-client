import React from 'react';

import SmartForm from '../../../../../../../../components/forms';
import { ButtonProps } from '../../../../../../../../components/touchable/types';
import { FieldProps } from '../../../../../../../../components/forms/types';
import { Permissions } from '../../../../../../../../utils/permissions';
import { RoleModule, Title, Description, FieldWrapper } from './styles';

interface Props {
  buttons: ButtonProps[];
  defaultEnabled?: boolean;
  fields: FieldProps[];
  form: string;
  isLoading: boolean;
  handleSubmit: (values: FieldProps[]) => void;
  permissions?: Permissions['listing'];
}

const View: React.FC<Props> = ({
  buttons,
  defaultEnabled,
  fields,
  form,
  handleSubmit,
  isLoading,
  permissions,
}: Props) => (
  <SmartForm
    buttons={buttons}
    fields={fields}
    form={form}
    handleSubmit={handleSubmit}
    isLoading={isLoading}
    defaultEnabled={defaultEnabled}
    permissions={permissions}
  >
    {({ Buttons, controlledButtons, controlledFields, Field, isEditEnabled }) => (
      <>
        {controlledFields.map((field, index) =>
          field.type !== 'array' ? (
            <Field key={field.id} options={field} />
          ) : (
            <RoleModule key={`option-${index}`}>
              <Title>{field.name}</Title>
              <Description>
                {
                  // @ts-ignore
                  field.description
                }
              </Description>

              {field.fields &&
                field.fields.map(fieldData => {
                  if (fieldData.type) {
                    return (
                      <FieldWrapper key={fieldData.id}>
                        <Field options={{ ...fieldData, isDisabled: !isEditEnabled || isLoading }} />
                      </FieldWrapper>
                    );
                  }
                })}
            </RoleModule>
          )
        )}

        <Buttons options={controlledButtons} />
      </>
    )}
  </SmartForm>
);

export default View;
