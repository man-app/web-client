import React from 'react';
import { shallow } from 'enzyme';

import View from '..';

describe('Role', () => {
  it('Should render a valid screen', () => {
    const component = shallow(<View />);

    expect(component).toMatchSnapshot();
  });
});
