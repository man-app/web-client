import React from 'react';

import { Card } from '../../../../../../components/card';
import { Page } from '../../../../../../components/page';
import RoleForm from '../form';

import { MAX_WIDTH_MD } from '../../../../../../../common/constants/styles/sizes';

const View: React.FC = () => (
  <Page maxWidth={MAX_WIDTH_MD}>
    <Card>
      <RoleForm />
    </Card>
  </Page>
);

export default View;
