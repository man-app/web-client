import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Redirect } from 'react-router';
import useReactRouter from 'use-react-router';
import { getPermissions, moduleIds } from '../../../../utils/permissions';

import { getCompany } from '../../../companies/actions';
import { getLoggedUser } from '../../../auth/selectors';
import { getSelectedCompany } from '../../../companies/selectors';
import { getModulesEnabled } from '../../../modules/selectors';
import { getCompanyRoles, selectRole } from '../../actions';
import { getSelectedRole } from '../../selectors';

import View from './components/view';

import { HOME } from '../../../../../common/constants/appRoutes';

const Role: React.FC = () => {
  const [redirectTo, setRedirectTo] = React.useState('' as any);

  const {
    location: { pathname },
    match: { params },
  } = useReactRouter();

  const user = useSelector(getLoggedUser);
  const dispatch = useDispatch();
  const company = useSelector(getSelectedCompany);
  const role = useSelector(getSelectedRole);
  const isNewRole = !params.id;

  React.useEffect(() => {
    // New role
    if (isNewRole) {
      dispatch(selectRole(-1));

      // Existing role, but company data has not been fetched
    } else if (!company) {
      dispatch(getCompany(params.slug));
    }
  }, []);

  React.useEffect(() => {
    if (company && params.slug === company.slug) {
      dispatch(getCompanyRoles(company.id));
      dispatch(selectRole(params.id));
    }
  }, [company]);

  const enabledModules = company && company.modules ? useSelector(getModulesEnabled)(company.modules) : [];
  const thisModule = enabledModules.find(m => m.id === moduleIds.roles);
  const permissions = thisModule ? getPermissions(thisModule, user, company) : {};

  if (redirectTo) {
    return <Redirect to={redirectTo} />;
  }

  // When no company data is present, stop execution
  // TODO: Show a loading spinner?
  if (!isNewRole && (!company || company.slug !== params.slug || !role)) {
    return null;
  }

  if (!permissions || !permissions.listing || !permissions.listing.value) {
    setRedirectTo({
      pathname: HOME,
      state: { from: pathname },
    });
  }

  return <View />;
};

export default Role;
