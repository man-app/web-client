import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Redirect } from 'react-router';
import useReactRouter from 'use-react-router';
import { moduleIds, getPermissions } from '../../../../utils/permissions';

import { isOffline as isOfflineSelector } from '../../../../selectors';
import { getCompany } from '../../../companies/actions';
import { updateRole, getCompanyRoles } from '../../actions';
import { getLoggedUser } from '../../../auth/selectors';
import { getSelectedCompany } from '../../../companies/selectors';
import { getModulesEnabled } from '../../../modules/selectors';
import { getRoles, isLoadingRoles } from '../../selectors';

import View from './components/view';

import { HOME } from '../../../../../common/constants/appRoutes';
import { Role } from '../../types';

const Roles: React.FC = () => {
  const [redirectTo, setRedirectTo] = React.useState('' as any);

  if (redirectTo) {
    return <Redirect to={redirectTo} />;
  }

  const user = useSelector(getLoggedUser);
  const company = useSelector(getSelectedCompany);
  const isLoading = useSelector(isLoadingRoles);
  const isOffline = useSelector(isOfflineSelector);

  // When no company data is present, stop execution
  // TODO: Show a loading spinner?
  if (!company) {
    return null;
  }

  const roles = useSelector(getRoles)(company.id);
  const enabledModules = company.modules ? useSelector(getModulesEnabled)(company.modules) : [];
  const thisModule = enabledModules.find(m => m.id === moduleIds.roles);
  const permissions = thisModule ? getPermissions(thisModule, user, company) : {};

  const dispatch = useDispatch();
  const {
    location: { pathname },
    match: { params },
  } = useReactRouter();

  // On component mount, fetch company data, if it's not present
  React.useEffect(() => {
    if (!company) {
      dispatch(getCompany(params.slug));
    }
  }, []);

  // When company data is preset, fetch company roles
  React.useEffect(() => {
    if (params.slug === company.slug) {
      dispatch(getCompanyRoles(company.id));
    }
  }, [company]);

  const toggleRoleStatus = (role: Role) => {
    if (role.isDeactivatable && permissions.disabling && permissions.disabling.value) {
      const payload = {
        ...role,
        modules: role.modules.map(({ id, ...mod }) => ({ ...mod, idModule: id })),
        isActive: !role.isActive,
      };

      dispatch(updateRole({ form: '', idCompany: company.id, roles: [payload] }));
    }
  };

  if (!permissions || !permissions.listing || !permissions.listing.value) {
    setRedirectTo({
      pathname: HOME,
      state: { from: pathname },
    });
  }

  return (
    <View
      company={company}
      isLoading={isLoading}
      isOffline={isOffline}
      permissions={permissions}
      roles={roles}
      toggleRoleStatus={toggleRoleStatus}
    />
  );
};

export default Roles;
