import React from 'react';
// import styled from 'styled-components';

import { Skeleton, Bar, Buttons, BarWrapper } from '../../../../../../components/skeletons';
import { RolePreview, Name, Description, ButtonsWrapper } from './styles';

const RolesSkeleton: React.FC = () => (
  <Skeleton>
    {['', '', ''].map((role, index) => (
      <RolePreview key={`role-skeleton-${index}`}>
        <Name>
          <BarWrapper>
            <Bar width="40%" />
          </BarWrapper>
        </Name>

        <Description>
          <BarWrapper>
            <Bar />
            <Bar width="50%" />
          </BarWrapper>
        </Description>

        <ButtonsWrapper>
          <Buttons count={1} />
        </ButtonsWrapper>
      </RolePreview>
    ))}
  </Skeleton>
);

export default RolesSkeleton;
