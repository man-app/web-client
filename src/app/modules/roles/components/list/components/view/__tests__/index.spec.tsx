import React from 'react';
import { shallow } from 'enzyme';

import View from '..';

import { Company } from '../../../../../../companies/types';
import { Role } from '../../../../../types';
import { Permissions } from '../../../../../../../utils/permissions';

const company: Company = {
  id: '1',
  name: 'Test',
  slug: 'test',
  created: 123456789,
  lastModified: 123456789,
};

const role1: Role = {
  id: '1',
  idCompany: '1',
  name: 'Admin',
  description: 'Description test',
  isActive: true,
  isDeactivatable: false,
  modules: [
    {
      id: '1',
    },
  ],
  created: 123456789,
  lastModified: 123456789,
};

const role2: Role = {
  id: '2',
  idCompany: '1',
  name: 'Demo',
  description: 'Description test',
  isActive: true,
  isDeactivatable: true,
  modules: [
    {
      id: '2',
    },
  ],
  created: 123456789,
  lastModified: 123456789,
};

const permissions: Permissions = {};

describe('Role', () => {
  it('Should render a valid screen', () => {
    const component = shallow(
      <View isLoading company={company} permissions={permissions} roles={[role1, role2]} toggleRoleStatus={() => {}} />
    );

    expect(component).toMatchSnapshot();
  });

  it('Should render a valid screen with creating permissions', () => {
    const component = shallow(
      <View
        isLoading
        company={company}
        permissions={{ ...permissions, creating: { label: 'Create', value: true } }}
        roles={[role1, role2]}
        toggleRoleStatus={() => {}}
      />
    );

    expect(component).toMatchSnapshot();
  });

  it('Should render a valid screen with disabling permissions', () => {
    const component = shallow(
      <View
        isLoading
        company={company}
        permissions={{
          ...permissions,
          creating: { label: 'Create', value: true },
          disabling: { label: 'Disable', value: true },
        }}
        roles={[role1, role2]}
        toggleRoleStatus={() => {}}
      />
    );

    expect(component).toMatchSnapshot();
  });

  it('Should render a valid screen with disabling permissions and an inactive role', () => {
    const component = shallow(
      <View
        company={company}
        isLoading
        permissions={{
          ...permissions,
          creating: { label: 'Create', value: true },
          disabling: { label: 'Disable', value: true },
        }}
        roles={[{ ...role1, isActive: false }, role2]}
        toggleRoleStatus={() => {}}
      />
    );

    expect(component).toMatchSnapshot();
  });
});
