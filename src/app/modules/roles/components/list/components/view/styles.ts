import styled from 'styled-components';
import { Card } from '../../../../../../components/card';
import { TABLET, DESKTOP } from '../../../../../../../common/constants/styles/media-queries';

export const RolePreview = styled(Card)`
  position: relative;
  vertical-align: top;

  ${TABLET} {
    max-width: calc(50% - 20px);
  }

  ${DESKTOP} {
    max-width: calc(33% - 20px);
  }
`;
RolePreview.displayName = 'RolePreview';

export const Name = styled.p`
  font-size: 20px;
  margin-bottom: 10px;
`;
Name.displayName = 'Name';

export const Description = styled.div`
  font-size: 18px;
  height: 48px; // 2 lines height
  overflow: hidden;
`;
Description.displayName = 'Description';

export const ButtonsWrapper = styled.div`
  & > div {
    margin: 0px;
    margin-top: 10px;
    width: 100%;
  }
`;
ButtonsWrapper.displayName = 'ButtonsWrapper';
