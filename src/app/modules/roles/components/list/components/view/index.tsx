import React from 'react';
import { Permissions } from '../../../../../../utils/permissions';

import { Page } from '../../../../../../components/page';
import { Buttons } from '../../../../../../components/touchable';
import { RolePreview, Name, Description, ButtonsWrapper } from './styles';

import { COMPANY_ROLE, COMPANY_ROLE_NEW } from '../../../../../../../common/constants/appRoutes';
import {
  DETAILS_LABEL,
  DETAILS_ICON,
  ENABLE_LABEL,
  ENABLE_ICON,
  DISABLE_LABEL,
  DISABLE_ICON,
  CREATE_LABEL,
  CREATE_ICON,
} from '../../../../../../../common/constants/buttons';

import { BUTTON } from '../../../../../../components/forms/models';
import { ButtonProps } from '../../../../../../components/touchable/types';
import { Company } from '../../../../../companies/types';
import { Role } from '../../../../types';
import { MAX_WIDTH_LG } from '../../../../../../../common/constants/styles/sizes';
import RolesSkeleton from './skeleton';

interface Props {
  company: Company;
  isLoading: boolean;
  isOffline?: boolean;
  permissions: Permissions;
  roles: Role[];
  toggleRoleStatus: Function;
}

const View: React.FC<Props> = ({ company, isLoading, isOffline, permissions, roles, toggleRoleStatus }: Props) => {
  const renderRoleWithButtons = (r: Role, c: Company, p: Permissions) => {
    const buttons: ButtonProps[] = [];

    buttons.push({
      buttonType: 'primary',
      icon: DETAILS_ICON,
      id: `edit-role-${r.id}`,
      isTransparent: true,
      label: DETAILS_LABEL,
      to: COMPANY_ROLE.replace(':slug', c.slug).replace(':id', r.id.toString()),
      type: BUTTON,
    });

    if (r.isDeactivatable && p.disabling && p.disabling.value) {
      if (!r.isActive) {
        buttons.push({
          buttonType: 'success',
          icon: ENABLE_ICON,
          id: `enable-role-${r.id}`,
          isDisabled: isOffline,
          isTransparent: true,
          label: ENABLE_LABEL,
          onClick: () => {
            toggleRoleStatus(r);
          },
          type: BUTTON,
        });
      } else {
        buttons.push({
          buttonType: 'danger',
          icon: DISABLE_ICON,
          id: `disable-role-${r.id}`,
          isDisabled: isOffline,
          isTransparent: true,
          label: DISABLE_LABEL,
          onClick: () => {
            toggleRoleStatus(r);
          },
          type: BUTTON,
        });
      }
    }

    return (
      <RolePreview key={r.id}>
        <Name>{r.name}</Name>
        <Description>{r.description}</Description>

        <ButtonsWrapper>
          <Buttons options={buttons.map(button => ({ ...button, isDisabled: button.isDisabled || isLoading }))} />
        </ButtonsWrapper>
      </RolePreview>
    );
  };

  const genericButtons: ButtonProps[] = [];
  if (permissions.creating && permissions.creating.value) {
    genericButtons.push({
      buttonType: 'primary',
      icon: CREATE_ICON,
      id: 'create',
      isDisabled: isOffline,
      label: CREATE_LABEL,
      to: COMPANY_ROLE_NEW.replace(':slug', company.slug),
      type: BUTTON,
    });
  }

  return (
    <Page maxWidth={MAX_WIDTH_LG}>
      {isLoading && !roles.length && <RolesSkeleton />}

      {(!isLoading || !!roles.length) && roles.map(role => renderRoleWithButtons(role, company, permissions))}

      <Buttons options={genericButtons.map(button => ({ ...button, isDisabled: isLoading }))} />
    </Page>
  );
};

export default View;
