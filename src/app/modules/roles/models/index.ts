import { RoleFromServer, RoleModuleFromServer } from '../../../../common/apis/manapp';
import { error } from '../../../../common/utils/logger';

import { RolesState, Role, RoleModule } from '../types';

export const getState = (): RolesState => ({
  collection: [],
  errors: [],
  isLoading: false,
  selected: undefined,
});

export const createRoleModuleFromServer = ({
  idModule,
  approving,
  creating,
  deleting,
  disabling,
  editing,
  listing,
}: RoleModuleFromServer): RoleModule | undefined => {
  if (!idModule) {
    error('Role module: InvalidModule model from server. Some of the following properties is missing', { idModule });
    return undefined;
  }

  return {
    id: String(idModule),
    listing: !!listing,
    creating: !!creating,
    editing: !!editing,
    approving: !!approving,
    disabling: !!disabling,
    deleting: !!deleting,
  };
};

export const createRoleModulesFromServer = (modules: RoleModuleFromServer[]): RoleModule[] =>
  // @ts-ignore
  modules.map(createRoleModuleFromServer).filter(mod => !!mod);

export const createRoleFromServer = ({
  created,
  description,
  id,
  idCompany,
  isActive,
  isDeactivatable,
  lastModified,
  modules,
  name,
}: RoleFromServer): Role | undefined => {
  if (
    typeof id === 'undefined' ||
    typeof idCompany === 'undefined' ||
    typeof name === 'undefined' ||
    typeof modules === 'undefined'
  ) {
    error('Roles: Invalid model from server. Some of the following properties is missing', {
      id,
      idCompany,
      name,
      modules,
    });
    return undefined;
  }

  let newModules = [] as RoleModule[];
  if (!Array.isArray(modules)) {
    error("Roles: Invalid model from server. Modules donesn't have a valid model");
  } else {
    newModules = createRoleModulesFromServer(modules);
  }

  return {
    id: String(id),
    idCompany: String(idCompany),
    name: String(name),
    modules: newModules,
    description: description ? String(description) : undefined,
    created: Number(created),
    lastModified: Number(lastModified || created),
    isActive: !!isActive,
    isDeactivatable: !!isDeactivatable,
  };
};

export const createRolesFromServer = (roles: RoleFromServer[]): Role[] =>
  // @ts-ignore
  roles.map(createRoleFromServer).filter(role => !!role);
