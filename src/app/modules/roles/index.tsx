import React from 'react';
import { Switch } from 'react-router';

import role from './components/details';
import roles from './components/list';

import { COMPANY_ROLE, COMPANY_ROLES, COMPANY_ROLE_NEW } from '../../../common/constants/appRoutes';
import RouteAuth from '../../components/routes/auth';

interface Props {
  isLoggedUser: boolean;
}

const Roles: React.FC<Props> = ({ isLoggedUser }: Props) => (
  <Switch>
    <RouteAuth path={COMPANY_ROLE_NEW} component={role} isLoggedUser={isLoggedUser} />
    <RouteAuth path={COMPANY_ROLE} component={role} isLoggedUser={isLoggedUser} />
    <RouteAuth path={COMPANY_ROLES} component={roles} isLoggedUser={isLoggedUser} />
  </Switch>
);

export default Roles;
