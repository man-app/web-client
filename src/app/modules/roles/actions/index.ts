import manappApi from '../../../../common/apis/manapp';

import {
  ROLES_REQUESTED,
  ROLES_UPDATED,
  ROLES_NOT_UPDATED,
  ROLE_FORM_SUBMITTED,
  ROLE_FORM_SUCCEED,
  ROLE_FORM_FAILED,
  ROLE_SELECTED,
} from '../actionTypes';

import { RolesActionCreator } from '../types';
import { createRolesFromServer } from '../models';

export const getCompanyRoles: RolesActionCreator = idCompany => dispatch => {
  dispatch({
    type: ROLES_REQUESTED,
  });

  return manappApi.roles
    .getRoles(idCompany)
    .then(({ roles }) => {
      const payload = {
        roles: createRolesFromServer(roles),
      };

      dispatch({
        type: ROLES_UPDATED,
        payload,
      });

      return payload;
    })
    .catch(err => {
      dispatch({
        type: ROLES_NOT_UPDATED,
        payload: err,
      });

      return err;
    });
};

export const createRole: RolesActionCreator = payload => dispatch => {
  const { form, idCompany, roles } = payload;

  dispatch({
    type: ROLE_FORM_SUBMITTED,
    payload: {
      form,
    },
  });

  return manappApi.roles
    .createRole(idCompany, { roles })
    .then(({ notifications, roles }) => {
      const payload = {
        form,
        notifications,
        roles: createRolesFromServer(roles),
      };

      dispatch({
        type: ROLE_FORM_SUCCEED,
        payload,
      });

      return payload;
    })
    .catch(err => {
      dispatch({
        type: ROLE_FORM_FAILED,
        payload: {
          form,
          ...err,
        },
      });

      return err;
    });
};

export const updateRole: RolesActionCreator = payload => dispatch => {
  const { form, idCompany, roles } = payload;

  dispatch({
    type: ROLE_FORM_SUBMITTED,
    payload: {
      form,
    },
  });

  return manappApi.roles
    .updateRole(idCompany, { roles })
    .then(({ notifications, roles }) => {
      const payload = {
        form,
        notifications,
        roles: createRolesFromServer(roles),
      };

      dispatch({
        type: ROLE_FORM_SUCCEED,
        payload,
      });

      return payload;
    })
    .catch(err => {
      dispatch({
        type: ROLE_FORM_FAILED,
        payload: {
          form,
          ...err,
        },
      });

      return err;
    });
};

export const selectRole: RolesActionCreator = payload => dispatch => {
  dispatch({
    type: ROLE_SELECTED,
    payload: {
      id: payload,
    },
  });

  return Promise.resolve();
};
