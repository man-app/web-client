import { Dispatch } from 'redux';

import { FieldProps } from '../../../components/forms/types';

export interface RoleModule {
  [key: string]: string | boolean | undefined;

  id: string;
  listing?: boolean;
  creating?: boolean;
  editing?: boolean;
  disabling?: boolean;
  approving?: boolean;
  deleting?: boolean;
}

export interface Role {
  id: string;
  idCompany: string;
  name: string;
  description?: string;
  modules: RoleModule[];
  created: number;
  lastModified: number;
  isActive: boolean;
  isDeactivatable: boolean;
}

export interface ModulePermissionsField {
  type: 'array';
  id: string;
  idModule: string;
  name: string;
  description: string;
  idRole: string;
  fields: FieldProps[];
}

export interface RolesState {
  collection: Role[];
  errors: any[];
  isLoading: boolean;
  selected?: string;
}

export type RolesActionType =
  | 'ROLES_REQUESTED'
  | 'ROLES_UPDATED'
  | 'ROLES_NOT_UPDATED'
  | 'ROLE_FORM_SUBMITTED'
  | 'ROLE_FORM_SUCCEED'
  | 'ROLE_FORM_FAILED'
  | 'ROLE_STATUS_SUBMITTED'
  | 'ROLE_STATUS_SUCCEED'
  | 'ROLE_STATUS_FAILED'
  | 'ROLE_SELECTED';

export interface RolesAction {
  type: RolesActionType;
  payload?: any;
}

export type RolesActionCreator = (payload?: any) => (dispatch: Dispatch<RolesAction>) => any;
