import React from 'react';
import { Switch, Route } from 'react-router';

import CreateCompany from './components/create';
import Company from './components/details';
import CompaniesList from './components/list';
import CompanySettings from './components/settings';
import RouteAuth from '../../components/routes/auth';

import { COMPANIES, COMPANY, COMPANY_SETTINGS, COMPANY_NEW } from '../../../common/constants/appRoutes';

interface Props {
  isLoggedUser: boolean;
}

const Companies: React.FC<Props> = ({ isLoggedUser }: Props) => (
  <Switch>
    <RouteAuth path={COMPANY_SETTINGS} component={CompanySettings} isLoggedUser={isLoggedUser} />
    <RouteAuth path={COMPANY_NEW} component={CreateCompany} isLoggedUser={isLoggedUser} />
    <Route path={COMPANY} component={Company} />
    <RouteAuth path={COMPANIES} component={CompaniesList} isLoggedUser={isLoggedUser} />
  </Switch>
);

export default Companies;
