import { updateCollection } from '../../../utils/collections';

import {
  COMPANIES_REQUESTED,
  COMPANIES_UPDATED,
  COMPANIES_NOT_UPDATED,
  COMPANY_REQUESTED,
  COMPANY_UPDATED,
  COMPANY_NOT_UPDATED,
  COMPANY_CREATE_FORM_SUBMITTED,
  COMPANY_CREATE_FORM_SUCCEED,
  COMPANY_CREATE_FORM_FAILED,
  COMPANY_UPDATE_FORM_SUBMITTED,
  COMPANY_UPDATE_FORM_SUCCEED,
  COMPANY_UPDATE_FORM_FAILED,
  COMPANY_SELECTED,
} from '../actionTypes';
import { LOGOUT_SUCCEED } from '../../auth/actionTypes';

import { LogoutAction } from '../../auth/types';
import { getState } from '../models';
import { CompaniesAction, CompaniesState, Company } from '../types';

const initialState = getState();

const reducer = (state = initialState, action: CompaniesAction | LogoutAction): CompaniesState => {
  switch (action.type) {
    case COMPANIES_REQUESTED:
    case COMPANY_REQUESTED:
    case COMPANY_CREATE_FORM_SUBMITTED:
    case COMPANY_UPDATE_FORM_SUBMITTED:
      return {
        ...state,
        isLoading: true,
      };

    case COMPANIES_UPDATED:
    case COMPANY_UPDATED:
    case COMPANY_CREATE_FORM_SUCCEED:
    case COMPANY_UPDATE_FORM_SUCCEED:
      const companiesFromServer = action.payload.companies as Company[];
      return {
        ...state,
        collection: updateCollection(state.collection, companiesFromServer),
        isLoading: false,
      };

    case COMPANIES_NOT_UPDATED:
    case COMPANY_NOT_UPDATED:
    case COMPANY_CREATE_FORM_FAILED:
    case COMPANY_UPDATE_FORM_FAILED:
      return {
        ...state,
        isLoading: false,
      };

    case COMPANY_SELECTED:
      return {
        ...state,
        selected: typeof action.payload === 'string' ? action.payload : state.selected,
      };

    case LOGOUT_SUCCEED:
      return {
        ...initialState,
      };

    default:
      return state;
  }
};

export default reducer;
