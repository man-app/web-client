import setupEnv from '../../../../../common/utils/setup-test-env';

import reducer from '..';

import { COMPANIES_REQUESTED, COMPANIES_UPDATED, COMPANIES_NOT_UPDATED, COMPANY_SELECTED } from '../../actionTypes';

import { LOGOUT_SUCCEED } from '../../../auth/actionTypes';

beforeEach(() => {
  setupEnv();
});

describe('Companies reducer', () => {
  const defaultState = {
    collection: [],
    isLoading: false,
    selected: '-1',
  };

  describe('Fetch companies', () => {
    it('should mark reducer as isLoading', () => {
      const expectedState = {
        ...defaultState,
        isLoading: true,
      };

      const nextState = reducer(defaultState, { type: COMPANIES_REQUESTED });

      expect(nextState).toStrictEqual(expectedState);
    });

    it('should store companies data', () => {
      const initialState = {
        ...defaultState,
        isLoading: true,
      };

      const expectedState = {
        ...initialState,
        collection: [{ id: '1', name: 'Example', created: 123456789, lastModified: 123456789 }],
        isLoading: false,
      };

      const nextState = reducer(initialState, {
        type: COMPANIES_UPDATED,
        payload: { companies: [{ id: '1', name: 'Example', created: 123456789, lastModified: 123456789 }] },
      });

      expect(nextState).toStrictEqual(expectedState);
    });

    it('should update company data', () => {
      const initialState = {
        ...defaultState,
        collection: [{ id: '1', name: 'Example', slug: 'example', created: 123456789, lastModified: 123456789 }],
        isLoading: true,
      };

      const expectedState = {
        ...defaultState,
        collection: [
          { id: '1', name: 'Example', slug: 'example', created: 123456789, lastModified: 123456789 },
          { id: '2', name: 'Example 2', created: 123456789, lastModified: 123456789 },
        ],
        isLoading: false,
      };

      const nextState = reducer(initialState, {
        type: COMPANIES_UPDATED,
        payload: { companies: [{ id: '2', name: 'Example 2', created: 123456789, lastModified: 123456789 }] },
      });

      expect(nextState).toStrictEqual(expectedState);
    });

    it('should mark reducer as not fetching', () => {
      const initialState = {
        ...defaultState,
        isLoading: true,
      };

      const nextState = reducer(initialState, { type: COMPANIES_NOT_UPDATED });

      expect(nextState).toStrictEqual(defaultState);
    });
  });

  describe('Select a company', () => {
    it('should return the selected company', () => {
      const initialState = {
        ...defaultState,
        collection: [{ id: '1', name: 'Example', slug: 'example', created: 123456789, lastModified: 123456789 }],
      };

      const expectedState = {
        ...initialState,
        selected: '1',
      };

      const nextState = reducer(initialState, { type: COMPANY_SELECTED, payload: '1' });

      expect(nextState).toStrictEqual(expectedState);
    });
  });

  describe('Logout action', () => {
    it('should return initial state', () => {
      const initialState = {
        ...defaultState,
        collection: [{ id: '1', name: 'Example', slug: 'example', created: 123456789, lastModified: 123456789 }],
        selected: '1',
      };

      const nextState = reducer(initialState, { type: LOGOUT_SUCCEED });

      expect(nextState).toStrictEqual(defaultState);
    });
  });
});
