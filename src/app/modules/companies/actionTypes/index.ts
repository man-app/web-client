import { CompaniesActionType } from '../types';

export const COMPANIES_REQUESTED: CompaniesActionType = 'COMPANIES_REQUESTED';
export const COMPANIES_UPDATED: CompaniesActionType = 'COMPANIES_UPDATED';
export const COMPANIES_NOT_UPDATED: CompaniesActionType = 'COMPANIES_NOT_UPDATED';

export const COMPANY_REQUESTED: CompaniesActionType = 'COMPANY_REQUESTED';
export const COMPANY_UPDATED: CompaniesActionType = 'COMPANY_UPDATED';
export const COMPANY_NOT_UPDATED: CompaniesActionType = 'COMPANY_NOT_UPDATED';

export const COMPANY_CREATE_FORM_SUBMITTED: CompaniesActionType = 'COMPANY_CREATE_FORM_SUBMITTED';
export const COMPANY_CREATE_FORM_SUCCEED: CompaniesActionType = 'COMPANY_CREATE_FORM_SUCCEED';
export const COMPANY_CREATE_FORM_FAILED: CompaniesActionType = 'COMPANY_CREATE_FORM_FAILED';

export const COMPANY_SELECTED: CompaniesActionType = 'COMPANY_SELECTED';

export const COMPANY_UPDATE_FORM_SUBMITTED: CompaniesActionType = 'COMPANY_UPDATE_FORM_SUBMITTED';
export const COMPANY_UPDATE_FORM_SUCCEED: CompaniesActionType = 'COMPANY_UPDATE_FORM_SUCCEED';
export const COMPANY_UPDATE_FORM_FAILED: CompaniesActionType = 'COMPANY_UPDATE_FORM_FAILED';

export const COMPANY_DELETE_SUBMITTED: CompaniesActionType = 'COMPANY_DELETE_SUBMITTED';
export const COMPANY_DELETE_SUCCEED: CompaniesActionType = 'COMPANY_DELETE_SUCCEED';
export const COMPANY_DELETE_FAILED: CompaniesActionType = 'COMPANY_DELETE_FAILED';
