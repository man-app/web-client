import manappApi from '../../../../common/apis/manapp';

import {
  COMPANIES_REQUESTED,
  COMPANIES_UPDATED,
  COMPANIES_NOT_UPDATED,
  COMPANY_REQUESTED,
  COMPANY_UPDATED,
  COMPANY_NOT_UPDATED,
  COMPANY_CREATE_FORM_SUBMITTED,
  COMPANY_CREATE_FORM_SUCCEED,
  COMPANY_CREATE_FORM_FAILED,
  COMPANY_SELECTED,
  COMPANY_UPDATE_FORM_SUBMITTED,
  COMPANY_UPDATE_FORM_SUCCEED,
  COMPANY_UPDATE_FORM_FAILED,
} from '../actionTypes';

import { createUsersFromServer } from '../../auth/models';
import { createCompaniesFromServer } from '../models';
import { CompaniesActionCreator } from '../types';

export const getCompanies: CompaniesActionCreator = () => dispatch => {
  dispatch({
    type: COMPANIES_REQUESTED,
  });

  return manappApi.companies
    .getCompanies()
    .then(({ companies }) => {
      const payload = {
        companies: createCompaniesFromServer(companies),
      };

      dispatch({
        type: COMPANIES_UPDATED,
        payload,
      });

      return payload;
    })
    .catch(err => {
      dispatch({
        type: COMPANIES_NOT_UPDATED,
        payload: err,
      });

      return err;
    });
};

export const getCompany: CompaniesActionCreator = payload => dispatch => {
  dispatch({
    type: COMPANY_REQUESTED,
  });

  return manappApi.companies
    .getCompany(payload)
    .then(({ companies }) => {
      const payload = {
        companies: createCompaniesFromServer(companies),
      };

      dispatch({
        type: COMPANY_UPDATED,
        payload,
      });

      return payload;
    })
    .catch(err => {
      dispatch({
        type: COMPANY_NOT_UPDATED,
        payload: err,
      });

      return err;
    });
};

export const createCompany: CompaniesActionCreator = payload => dispatch => {
  const { form, ...params } = payload;
  dispatch({
    type: COMPANY_CREATE_FORM_SUBMITTED,
    payload: {
      form,
    },
  });

  return manappApi.companies
    .create(params)
    .then(({ companies, notifications, users }) => {
      const payload = {
        form,
        companies: createCompaniesFromServer(companies),
        notifications,
        users: createUsersFromServer(users),
      };

      dispatch({
        type: COMPANY_CREATE_FORM_SUCCEED,
        payload,
      });

      return payload;
    })
    .catch(err => {
      dispatch({
        type: COMPANY_CREATE_FORM_FAILED,
        payload: {
          form,
          ...err,
        },
      });

      return err;
    });
};

export const selectCompany: CompaniesActionCreator = payload => dispatch => {
  dispatch({
    type: COMPANY_SELECTED,
    payload,
  });
};

export const updateCompany: CompaniesActionCreator = payload => dispatch => {
  const { form, company } = payload;
  dispatch({
    type: COMPANY_UPDATE_FORM_SUBMITTED,
    payload: {
      form,
    },
  });

  return manappApi.companies
    .update({ company })
    .then(({ companies, notifications }) => {
      const payload = {
        form,
        companies: createCompaniesFromServer(companies),
        notifications,
      };

      dispatch({
        type: COMPANY_UPDATE_FORM_SUCCEED,
        payload,
      });

      return payload;
    })
    .catch(err => {
      dispatch({
        type: COMPANY_UPDATE_FORM_FAILED,
        payload: {
          form,
          ...err,
        },
      });

      return err;
    });
};
