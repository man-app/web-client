import { Dispatch } from 'redux';

interface Module {
  idRole: number;
}

interface Role {
  id: number;
  modules: Module[];
}

export interface CompanyPlan {
  name: string;
  users: number;
  price: number;
  expires: number;
}

export interface Company {
  [key: string]: any;

  id: string;
  slug: string;
  name: string;
  picture?: string;
  publicProfile?: boolean;
  users?: number;
  contactPhone?: string;
  contactEmail?: string;
  website?: string;
  linkedin?: string;
  facebook?: string;
  instagram?: string;
  twitter?: string;
  youtube?: string;
  joined?: number;
  modules?: string[];
  roles?: Role[];
  created: number;
  lastModified: number;
  plan?: CompanyPlan;
}

export type CompaniesActionType =
  | 'COMPANIES_REQUESTED'
  | 'COMPANIES_UPDATED'
  | 'COMPANIES_NOT_UPDATED'
  | 'COMPANY_REQUESTED'
  | 'COMPANY_UPDATED'
  | 'COMPANY_NOT_UPDATED'
  | 'COMPANY_CREATE_FORM_SUBMITTED'
  | 'COMPANY_CREATE_FORM_SUCCEED'
  | 'COMPANY_CREATE_FORM_FAILED'
  | 'COMPANY_SELECTED'
  | 'COMPANY_UPDATE_FORM_SUBMITTED'
  | 'COMPANY_UPDATE_FORM_SUCCEED'
  | 'COMPANY_UPDATE_FORM_FAILED'
  | 'COMPANY_DELETE_SUBMITTED'
  | 'COMPANY_DELETE_SUCCEED'
  | 'COMPANY_DELETE_FAILED';

export interface CompaniesAction {
  type: CompaniesActionType;
  payload?: any;
}

export type CompaniesActionCreator = (payload?: any) => (dispatch: Dispatch<CompaniesAction>) => any;

export interface CompaniesState {
  collection: Company[];
  isLoading: boolean;
  selected: string;
}
