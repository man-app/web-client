import { UPDATE_FIELDS } from '../actionTypes';

import { CompanySettingsFormActionCreator } from '../types';

export const updateCompanySettingsFormFields: CompanySettingsFormActionCreator = payload => ({
  type: UPDATE_FIELDS,
  payload,
});
