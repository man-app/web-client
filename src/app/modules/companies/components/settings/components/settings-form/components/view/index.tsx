import React from 'react';
import { currency } from '../../../../../../../../../common/utils/numbers';
import { Permissions } from '../../../../../../../../utils/permissions';
import { capitalize } from '../../../../../../../../utils/strings';
import { formatDate } from '../../../../../../../../utils/ui';

import SmartForm, { updateCollection } from '../../../../../../../../components/forms';
import { Section, Description } from './styles';

import { FieldProps } from '../../../../../../../../components/forms/types';
import { ButtonProps } from '../../../../../../../../components/touchable/types';
import { CompanyPlan } from '../../../../../../types';
import { CompanySettingsTabIds } from '../../../../types';

interface Props {
  activeTab: CompanySettingsTabIds;
  buttons: ButtonProps[];
  form: string;
  handleSubmit: (fields: FieldProps[]) => void;
  isLoading: boolean;
  modulesFields: FieldProps[];
  optionsFields: FieldProps[];
  paymentFields: FieldProps[];
  permissions: Permissions['listing'];
  plan: CompanyPlan;
  profileFields: FieldProps[];
}

const View: React.FC<Props> = ({
  activeTab,
  buttons,
  form,
  handleSubmit,
  isLoading,
  modulesFields,
  optionsFields,
  paymentFields,
  permissions,
  plan,
  profileFields,
}: Props) => {
  const canPurchase = !!(permissions && permissions.value);

  return (
    <SmartForm
      buttons={buttons}
      fields={[...profileFields, ...modulesFields, ...optionsFields, ...paymentFields]}
      form={form}
      handleSubmit={handleSubmit}
      permissions={permissions}
      isLoading={isLoading}
    >
      {({ Buttons, controlledButtons, controlledFields, Field }) => (
        <>
          {/* Company profile */}
          <Section isActive={activeTab === 'profile'}>
            {updateCollection(profileFields, controlledFields).map(field => (
              <Field key={field.id} options={field} />
            ))}
          </Section>

          {/* Enabled modules */}
          <Section isActive={activeTab === 'modules'}>
            <Description>
              The modules you enable/disable here are going to be enabled/disabled to all your users, while they are
              working in your company.
            </Description>
            <Description>
              If you disable a module, your data{' '}
              <b>
                won
                {"'"}t be deleted
              </b>
              , and will be available when you enable the module again.
            </Description>

            {updateCollection(modulesFields, controlledFields).map(field => (
              <Field key={field.id} options={field} />
            ))}
          </Section>

          {/* Company options */}
          <Section isActive={activeTab === 'options'}>
            {updateCollection(optionsFields, controlledFields).map(field => (
              <Field key={field.id} options={field} />
            ))}
          </Section>

          {/* Payment options */}
          {plan && (
            <Section isActive={activeTab === 'payments'}>
              <h3>Your subscription information:</h3>
              <ul>
                <li>
                  <b>Package</b>: {plan.name}
                </li>
                {canPurchase && (
                  <li>
                    <b>Cost/Month</b>: {currency(plan.price)}/month
                  </li>
                )}
                <li>
                  <b>Available users</b>: {plan.users}
                </li>
                <li>
                  <b>Expiration date</b>:{' '}
                  {plan.expires === Infinity
                    ? 'Never'
                    : `${capitalize(formatDate(plan.expires))} ${formatDate(plan.expires, true)}`}
                </li>
              </ul>

              {canPurchase && <Buttons options={updateCollection(paymentFields, controlledFields)} align="left" />}
            </Section>
          )}

          <Buttons options={controlledButtons} />
        </>
      )}
    </SmartForm>
  );
};

export default View;
