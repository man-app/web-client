import setupEnv from '../../../../../../../../../common/utils/setup-test-env';

import { updateCompanySettingsFormFields } from '../../actions';
import reducer from '..';

import { getState } from '../../models';

beforeEach(() => {
  setupEnv();
});

const company = {
  id: '1',
  name: 'Company',
  slug: 'company',
  created: 123456789,
  lastModified: 123456789,
};

const defaultState = getState({ company, form: 'test', enabledModules: [] });

describe('CompanySettingsForm component reducer', () => {
  describe('updateCompanySettingsFormFields', () => {
    it('should set an error on "test" field', () => {
      const initialState = {
        ...defaultState,
      };

      const expectedState = {
        ...initialState,
        profileFields: initialState.profileFields.map((f, idx) =>
          idx === 0 ? { ...f, error: 'This field has an error' } : f
        ),
      };

      const nextState = reducer(
        initialState,
        updateCompanySettingsFormFields({
          profileFields: initialState.profileFields.map((f, idx) =>
            idx === 0 ? { ...f, error: 'This field has an error' } : f
          ),
          modulesFields: initialState.modulesFields,
        })
      );
      expect(nextState).toStrictEqual(expectedState);
    });
  });
});
