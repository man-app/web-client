import React from 'react';
import { shallow } from 'enzyme';

import CompanySettingsForm from '..';

import { FieldProps } from '../../../../../../../../../components/forms/types';
import { ButtonProps } from '../../../../../../../../../components/touchable/types';

describe('Company Settings Notifications form', () => {
  it('should render a valid CompanySettingsForm', () => {
    const buttons = [] as ButtonProps[];
    const form = 'test';
    const handleSubmit = () => {};
    const isLoading = false;
    const modulesFields = [] as FieldProps[];
    const optionsFields = [] as FieldProps[];
    const paymentFields = [] as FieldProps[];
    const permissions = { label: '', value: true };
    const profileFields = [] as FieldProps[];

    const component = shallow(
      <CompanySettingsForm
        activeTab="profile"
        buttons={buttons}
        form={form}
        handleSubmit={handleSubmit}
        isLoading={isLoading}
        modulesFields={modulesFields}
        optionsFields={optionsFields}
        paymentFields={paymentFields}
        permissions={permissions}
        plan={{ expires: 123456789, name: 'test', price: 0, users: 0 }}
        profileFields={profileFields}
      />
    );
    expect(component).toMatchSnapshot();
  });
});
