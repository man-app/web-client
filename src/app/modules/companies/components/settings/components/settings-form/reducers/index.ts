import { UPDATE_FIELDS } from '../actionTypes';

import { CompanySettingsFormState, CompanySettingsFormAction } from '../types';

const reducer = (state: CompanySettingsFormState, action: CompanySettingsFormAction) => {
  switch (action.type) {
    case UPDATE_FIELDS:
      return {
        ...state,
        profileFields: action.payload.profileFields,
        modulesFields: action.payload.modulesFields,
        optionsFields: action.payload.optionsFields || state.optionsFields,
        paymentFields: action.payload.paymentFields || state.paymentFields,
      };

    default:
      return state;
  }
};

export default reducer;
