import { CompanySettingsFormActionType } from '../types';

export const UPDATE_FIELDS: CompanySettingsFormActionType = 'UPDATE_FIELDS';
