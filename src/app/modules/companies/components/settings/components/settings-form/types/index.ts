import { FieldProps } from '../../../../../../../components/forms/types';

export type CompanySettingsFormActionType = 'UPDATE_FIELDS';

export interface CompanySettingsFormAction {
  type: CompanySettingsFormActionType;
  payload: {
    profileFields: FieldProps[];
    modulesFields: FieldProps[];
    optionsFields?: FieldProps[];
    paymentFields?: FieldProps[];
  };
}

export type CompanySettingsFormActionCreator = (payload: {
  profileFields: FieldProps[];
  modulesFields: FieldProps[];
  optionsFields?: FieldProps[];
  paymentFields?: FieldProps[];
}) => CompanySettingsFormAction;

export interface CompanySettingsFormState {
  profileFields: FieldProps[];
  modulesFields: FieldProps[];
  optionsFields: FieldProps[];
  paymentFields: FieldProps[];
}
