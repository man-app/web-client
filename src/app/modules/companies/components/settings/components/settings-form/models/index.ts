import { PICTURE, TEXT, EMAIL, SWITCH, BUTTON } from '../../../../../../../components/forms/models';
import { FieldProps } from '../../../../../../../components/forms/types';
import { ButtonProps } from '../../../../../../../components/touchable/types';
import { Module } from '../../../../../../modules/types';
import { Company } from '../../../../../types';
import { SUBSCRIPTIONS } from '../../../../../../../../common/constants/appRoutes';

interface ProfileFieldDependencies {
  company: Company;
  form: string;
}

export const generateProfileFields = ({ company, form }: ProfileFieldDependencies) =>
  [
    {
      form,
      icon: 'upload',
      id: 'picture',
      label: 'Upload new logo',
      model: company.picture,
      resource: {
        type: 'company',
        id: company.id,
      },
      type: PICTURE,
      updateId: company.slug,
    },
    {
      id: 'name',
      isRequired: true,
      label: 'Public name',
      model: company.name,
      type: TEXT,
      updateId: company.slug,
    },
    {
      id: 'contactEmail',
      label: 'Contact email',
      model: company.contactEmail,
      type: EMAIL,
      updateId: company.slug,
    },
    {
      id: 'contactPhone',
      label: 'Contact Phone',
      model: company.contactPhone,
      type: TEXT,
      updateId: company.slug,
    },
    {
      id: 'website',
      label: 'Official website',
      model: company.website,
      type: TEXT,
      updateId: company.slug,
    },
    {
      icon: 'linkedin',
      id: 'linkedin',
      model: company.linkedin,
      type: TEXT,
      updateId: company.slug,
    },
    {
      icon: 'facebook',
      id: 'facebook',
      model: company.facebook,
      type: TEXT,
      updateId: company.slug,
    },
    {
      icon: 'twitter',
      id: 'twitter',
      model: company.twitter,
      type: TEXT,
      updateId: company.slug,
    },
    {
      icon: 'instagram',
      id: 'instagram',
      model: company.instagram,
      type: TEXT,
      updateId: company.slug,
    },
    {
      icon: 'youtube',
      id: 'youtube',
      model: company.youtube,
      type: TEXT,
      updateId: company.slug,
    },
    {
      id: 'publicProfile',
      isChecked: company.publicProfile,
      label:
        "Public profile (if activated, anybody will be able to see this company's profile. If disabled, only employees will be able to see it).",
      type: SWITCH,
      updateId: company.slug,
    },
  ] as FieldProps[];

interface ModulesFieldDependencies {
  company: Company;
  enabledModules: Module[];
}

export const generateModulesFields = ({ company, enabledModules }: ModulesFieldDependencies) => {
  const modulesFields: FieldProps[] = [];

  enabledModules.forEach(enabledModule => {
    modulesFields.push({
      id: String(enabledModule.id),
      isChecked: !!(company && company.modules && company.modules.find(mod => mod === enabledModule.id)),
      isAlwaysDisabled: !enabledModule.isDeactivatable,
      label: `${enabledModule.name} (${enabledModule.description})`,
      type: SWITCH,
    });
  });

  return modulesFields;
};

type StateDependencies = ProfileFieldDependencies & ModulesFieldDependencies;

// TODO: Cancel company
const dissolveCompany: ButtonProps = {
  buttonType: 'danger',
  icon: 'chain-broken',
  id: 'dissolveCompany',
  label: 'Dissolve company',
  type: BUTTON,
};

export const getState = ({ company, form, enabledModules }: StateDependencies) => ({
  profileFields: generateProfileFields({ company, form }),
  modulesFields: generateModulesFields({ company, enabledModules }),
  optionsFields: [dissolveCompany] as FieldProps[],
  paymentFields: [
    {
      icon: 'shopping-cart',
      id: 'payment',
      isAlwaysEnabled: true,
      label: `${company.plan ? 'Renew' : 'Purchase a'} plan`,
      to: SUBSCRIPTIONS,
      type: BUTTON,
    } as ButtonProps,
  ] as FieldProps[],
});
