import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import useReactRouter from 'use-react-router';
import { mapServerErrors, isValidForm } from '../../../../../../utils/error-handler';
import { getPermissions, moduleIds } from '../../../../../../utils/permissions';
import { updateCollection } from '../../../../../../components/forms';

import { updateCompany } from '../../../../actions';
import { updateCompanySettingsFormFields } from './actions';
import { getFormErrors, getFormPicture } from '../../../../../../components/forms/selectors';
import { getLoggedUser } from '../../../../../auth/selectors';
import { getModulesInstalled } from '../../../../../modules/selectors';
import { getSelectedCompany, isLoadingCompanies } from '../../../../selectors';
import reducer from './reducers';

import View from './components/view';

import { SAVE_LABEL, SAVE_ICON, GOBACK_LABEL, GOBACK_ICON } from '../../../../../../../common/constants/buttons';
import { ButtonProps } from '../../../../../../components/touchable/types';
import { BUTTON, SUBMIT, CHECKBOX, SWITCH } from '../../../../../../components/forms/models';
import { FieldProps } from '../../../../../../components/forms/types';
import { Company } from '../../../../types';
import { CompanySettingsTabIds } from '../../types';
import { getState, generateProfileFields, generateModulesFields } from './models';
import { getFreeSubscriptionPlan } from '../../../../../subscriptions/selectors';

const form = 'companySettings';

interface Props {
  activeTab: CompanySettingsTabIds;
}

const CompanySettingsForm: React.FC<Props> = ({ activeTab }: Props) => {
  const { history } = useReactRouter();
  const dispatch = useDispatch();

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const freePlan = useSelector(getFreeSubscriptionPlan);
  const user = useSelector(getLoggedUser);
  const errors = useSelector(getFormErrors)(form);
  const formPicture = useSelector(getFormPicture)(form);
  const isLoading = useSelector(isLoadingCompanies);
  const company = useSelector(getSelectedCompany) as Company;
  const enabledModules = useSelector(getModulesInstalled);
  const thisModule = enabledModules.find(m => m.id === moduleIds.companySettings);
  const permissions = thisModule ? getPermissions(thisModule, user, company) : {};

  const [{ profileFields, modulesFields, optionsFields, paymentFields }, dispatchState] = React.useReducer(
    reducer,
    getState({ company, enabledModules, form })
  );

  React.useEffect(() => {
    dispatchState(
      updateCompanySettingsFormFields({
        profileFields: generateProfileFields({ company, form }),
        modulesFields: generateModulesFields({ company, enabledModules }),
      })
    );
  }, [company]);

  React.useEffect(() => {
    dispatchState(
      updateCompanySettingsFormFields({
        profileFields: mapServerErrors(profileFields, errors),
        modulesFields: mapServerErrors(modulesFields, errors),
        optionsFields: mapServerErrors(optionsFields, errors),
        paymentFields: mapServerErrors(paymentFields, errors),
      })
    );
  }, [errors]);

  const handleGoBack = () => {
    history.goBack();
  };

  const handleSubmit = (fields: FieldProps[]) => {
    const controlledProfileFields = updateCollection(profileFields, fields);
    const controlledModulesFields = updateCollection(modulesFields, fields);

    const controlledFields = [
      ...controlledProfileFields,
      ...controlledModulesFields,
      ...updateCollection(optionsFields, fields),
      ...updateCollection(paymentFields, fields),
    ];

    if (isValidForm(controlledFields)) {
      if (!company) {
        return;
      }

      const companyData: Company = {
        id: company.id,
        name: '',
        slug: company.slug,
        created: 123456789,
        lastModified: 123456789,
      };

      // Data related to company settings
      if (permissions.editing && permissions.editing.value) {
        // Update data in the component
        // Data related to the company
        if (controlledProfileFields) {
          controlledProfileFields.forEach(f => {
            if (f.type === CHECKBOX || f.type === SWITCH) {
              companyData[f.id] = f.isChecked;
            } else {
              companyData[f.id] = f.model;
            }
          });

          companyData.picture = (formPicture && formPicture.url) || company.picture;
        }

        // Data related to enabled modules
        if (controlledModulesFields) {
          companyData.modules = controlledModulesFields
            .filter(mod => mod && mod.id && mod.isChecked)
            .map(mod => String(mod.id));
        }

        // Save company data
        dispatch(updateCompany({ form, company: { ...companyData } }));
      }
    }
  };

  const buttons: ButtonProps[] = [
    {
      buttonType: 'primary',
      icon: SAVE_ICON,
      id: 'save',
      label: `${SAVE_LABEL} company`,
      type: SUBMIT,
    },
    {
      icon: GOBACK_ICON,
      id: 'goBack',
      isAlwaysEnabled: true,
      label: GOBACK_LABEL,
      onClick: handleGoBack,
      type: BUTTON,
    },
  ];

  return (
    <View
      activeTab={activeTab}
      buttons={buttons}
      form={form}
      handleSubmit={handleSubmit}
      isLoading={isLoading}
      modulesFields={modulesFields}
      optionsFields={optionsFields}
      paymentFields={paymentFields}
      permissions={activeTab === 'options' ? permissions.disabling : permissions.editing}
      // @ts-ignore TODO: Add default plan
      plan={company.plan}
      profileFields={profileFields}
    />
  );
};

export default CompanySettingsForm;
