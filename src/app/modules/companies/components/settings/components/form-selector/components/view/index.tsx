import React from 'react';

import Tabs from '../../../../../../../../components/tabs';
import NotificationsSettingsForm from '../../../notifications-form';
import CompanySettingsForm from '../../../settings-form';

import { TabProps } from '../../../../../../../../components/tabs/types';
import { CompanySettingsTabIds } from '../../../../types';

interface Props {
  activeTab: CompanySettingsTabIds;
  tabs: TabProps[];
}

const View: React.FC<Props> = ({ activeTab, tabs }: Props) => (
  <>
    <Tabs activeTab={activeTab} options={tabs} />

    {!!tabs.length && (
      <>
        {activeTab === 'notifications' ? <NotificationsSettingsForm /> : <CompanySettingsForm activeTab={activeTab} />}
      </>
    )}

    {!tabs.length && <p>This company has no modules enabled</p>}
  </>
);

export default View;
