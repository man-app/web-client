import React from 'react';
import { useSelector } from 'react-redux';
import useReactRouter from 'use-react-router';
import { getPermissions, moduleIds } from '../../../../../../utils/permissions';

import { getLoggedUser } from '../../../../../auth/selectors';
import { getModulesEnabled } from '../../../../../modules/selectors';
import { getSelectedCompany } from '../../../../selectors';

import View from './components/view';

import { COMPANY_SETTINGS } from '../../../../../../../common/constants/appRoutes';
import { Company } from '../../../../types';
import { CompanySettingsTabIds } from '../../types';
import { payments } from '../../../../../../../common/constants/features';

const FormSelector: React.FC = () => {
  const [activeTab, setActiveTab] = React.useState('profile' as CompanySettingsTabIds);

  const {
    match: { params },
  } = useReactRouter();

  const user = useSelector(getLoggedUser);
  const selectedCompany = useSelector(getSelectedCompany) as Company;
  const enabledModules = selectedCompany.modules ? useSelector(getModulesEnabled)(selectedCompany.modules) : [];
  const thisModule = enabledModules.find(m => m.id === moduleIds.companySettings);
  const permissions = thisModule ? getPermissions(thisModule, user, selectedCompany) : {};

  // Set initial tab based on URL
  const selectDefaultTab = () => {
    // User can see company settings
    if (permissions && permissions.listing && permissions.listing.value) {
      // No 'tab' param
      if (!params || !params.tab) {
        // Default tab is profile
        setActiveTab('profile');
      } else if (params.tab && params.tab !== activeTab) {
        // 'tab' parameter is present
        setActiveTab(params.tab);
      }
    } else {
      // No permissions to see company settings
      setActiveTab('notifications');
    }
  };

  React.useEffect(() => {
    selectDefaultTab();
  }, []);

  React.useEffect(() => {
    if (params.tab) {
      setActiveTab(params.tab);
    }
  }, [params.tab]);

  if (!selectedCompany) {
    return null;
  }

  const canSeeSettings = permissions.listing && permissions.listing.value;
  const canEditSettings = permissions.editing && permissions.editing.value;
  const canDisableSettings = permissions.disabling && permissions.disabling.value;

  const tabs = [];

  // Add 'Company profile' tab
  if (canSeeSettings) {
    tabs.push({
      id: 'profile',
      label: 'Company profile',
      to: COMPANY_SETTINGS.replace(':slug', selectedCompany.slug).replace(':tab?', 'profile'),
      onClick: undefined,
    });
  }

  // Add 'Enabled modules' tab
  if (canSeeSettings) {
    tabs.push({
      id: 'modules',
      label: 'Modules',
      to: COMPANY_SETTINGS.replace(':slug', selectedCompany.slug).replace(':tab?', 'modules'),
      onClick: undefined,
    });
  }

  // Add 'Notifications' tab
  if (enabledModules && enabledModules.length) {
    tabs.push({
      id: 'notifications',
      label: 'Notifications',
      to: COMPANY_SETTINGS.replace(':slug', selectedCompany.slug).replace(':tab?', 'notifications'),
      onClick: undefined,
    });
  }

  // Add 'Company options' tab
  if (canDisableSettings) {
    tabs.push({
      id: 'options',
      label: 'Options',
      to: COMPANY_SETTINGS.replace(':slug', selectedCompany.slug).replace(':tab?', 'options'),
      onClick: undefined,
    });
  }

  // Add 'Payment' tab
  if (payments && canEditSettings) {
    tabs.push({
      id: 'payments',
      label: 'Payments',
      to: COMPANY_SETTINGS.replace(':slug', selectedCompany.slug).replace(':tab?', 'payments'),
      onClick: undefined,
    });
  }

  return <View activeTab={activeTab} tabs={tabs} />;
};

export default FormSelector;
