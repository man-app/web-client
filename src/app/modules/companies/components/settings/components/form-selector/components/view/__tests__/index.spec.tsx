import React from 'react';
import { shallow } from 'enzyme';

import CompanySettings from '..';

describe('Company Settings view', () => {
  const tabs = [
    {
      id: 'profile',
    },
    {
      id: 'notifications',
    },
  ];

  it('should render a valid Company settings screen, with CompanySettingsForm', () => {
    const activeTab = 'profile';

    const component = shallow(<CompanySettings activeTab={activeTab} tabs={tabs} />);
    expect(component).toMatchSnapshot();
  });

  it('should render a valid Company settings screen, with NotificationsSettingsForm', () => {
    const activeTab = 'notifications';

    const component = shallow(<CompanySettings activeTab={activeTab} tabs={tabs} />);
    expect(component).toMatchSnapshot();
  });
});
