import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import useReactRouter from 'use-react-router';
import { isValidForm, mapServerErrors } from '../../../../../../utils/error-handler';

import { userModuleNotificationsUpdate } from '../../../../../account/actions';
import { getFormErrors } from '../../../../../../components/forms/selectors';
import { getLoggedUser } from '../../../../../auth/selectors';
import { getModulesEnabled } from '../../../../../modules/selectors';
import { getSelectedCompany, isLoadingCompanies } from '../../../../selectors';

import View from './components/view';

import { SAVE_ICON, SAVE_LABEL, GOBACK_ICON, GOBACK_LABEL } from '../../../../../../../common/constants/buttons';
import { ButtonProps } from '../../../../../../components/touchable/types';
import { SWITCH, BUTTON, SUBMIT } from '../../../../../../components/forms/models';
import { FieldProps } from '../../../../../../components/forms/types';
import { NotificationBrowserOptions } from '../../../../../auth/types';
import { Company } from '../../../../types';
import { getPermissions } from '../../../../../../utils/permissions';

const form = 'userNotifications';

const NotificationsSettingsForm: React.FC = () => {
  const { history } = useReactRouter();
  const dispatch = useDispatch();

  const user = useSelector(getLoggedUser);
  const company = useSelector(getSelectedCompany) as Company;
  const enabledModules = company.modules ? useSelector(getModulesEnabled)(company.modules) : [];
  const errors = useSelector(getFormErrors)(form);
  const isLoading = useSelector(isLoadingCompanies);

  const generateFields = () => {
    /* Given a list of channels to notify, generate propper ifields */
    const createTableSwitch = ({
      id,
      label,
      verb,
      action,
    }: {
      id: string;
      label: string;
      verb: string;
      action: NotificationBrowserOptions;
    }) => ({
      label,
      verb,
      fields: [
        {
          channel: 'browser',
          id: `${id}-browser`,
          isChecked: action.browser,
          type: SWITCH,
        },
        {
          channel: 'email',
          id: `${id}-email`,
          isChecked: action.email,
          type: SWITCH,
        },
        {
          channel: 'push',
          id: `${id}-push`,
          isChecked: action.push,
          type: SWITCH,
        },
      ],
    });

    if (!user || !company) {
      return [];
    }

    const moduleFields: any[] = [];

    enabledModules
      .filter(mod => {
        const { listing } = getPermissions(mod, user, company);
        return listing && listing.value;
      })
      .forEach(mod => {
        const userModuleNotification =
          user.notifications && user.notifications.find(n => n.idCompany === company.id && n.idModule === mod.id);

        const fields = [];
        const defaultNotificationOptions = {
          browser: false,
          email: false,
          push: false,
        };

        // Enable actions switchers
        if (mod.creating)
          fields.push(
            createTableSwitch({
              id: `${mod.id}-creating`,
              label: 'Create',
              verb: 'creating',
              action: userModuleNotification ? userModuleNotification.creating : defaultNotificationOptions,
            })
          );
        if (mod.editing)
          fields.push(
            createTableSwitch({
              id: `${mod.id}-editing`,
              label: 'Edit',
              verb: 'editing',
              action: userModuleNotification ? userModuleNotification.editing : defaultNotificationOptions,
            })
          );
        if (mod.approving)
          fields.push(
            createTableSwitch({
              id: `${mod.id}-approving`,
              label: 'Approve/Reject',
              verb: 'approving',
              action: userModuleNotification ? userModuleNotification.approving : defaultNotificationOptions,
            })
          );
        if (mod.deleting)
          fields.push(
            createTableSwitch({
              id: `${mod.id}-deleting`,
              label: 'Delete',
              verb: 'deleting',
              action: userModuleNotification ? userModuleNotification.deleting : defaultNotificationOptions,
            })
          );
        if (mod.disabling)
          fields.push(
            createTableSwitch({
              id: `${mod.id}-disabling`,
              label: 'Disable',
              verb: 'disabling',
              action: userModuleNotification ? userModuleNotification.disabling : defaultNotificationOptions,
            })
          );

        moduleFields.push({ idModule: mod.id, name: mod.name, fields });
      });

    return moduleFields;
  };

  const [fields, setFields] = React.useState(generateFields());

  React.useEffect(() => {
    setFields(mapServerErrors(fields, errors));
  }, [errors]);

  const handleGoBack = () => {
    history.goBack();
  };

  const handleSubmit = (newFields: FieldProps[]) => {
    if (user && company && isValidForm(fields)) {
      const companyData = {
        id: company.id,
        slug: company.slug,
      };

      // Data related to personal notifications
      const notificationFields: any[] = [];

      if (fields) {
        fields.forEach((field, fieldIndex) => {
          const mod: { [key: string]: any } = {
            idModule: field.idModule,
            idCompany: companyData.id,
          };

          field.fields.map((action: any, actionIndex: number) => {
            const act: { [key: string]: any } = {};

            action.fields.forEach((f: any, fIndex: number) => {
              // @ts-ignore
              act[f.channel] = newFields[fieldIndex].fields[actionIndex].fields[fIndex].isChecked;
            });

            mod[action.verb] = act;
          });

          notificationFields.push(mod);
        });
      }

      // Save user notifications data
      const data = {
        form: 'userNotifications',
        idCompany: companyData.id,
        params: {
          notifications: notificationFields,
        },
      };

      dispatch(userModuleNotificationsUpdate(data));
    }
  };

  const buttons: ButtonProps[] = [
    {
      buttonType: 'primary',
      icon: SAVE_ICON,
      id: 'save',
      label: `${SAVE_LABEL} company`,
      type: SUBMIT,
    },
    {
      icon: GOBACK_ICON,
      id: 'goBack',
      isAlwaysEnabled: true,
      label: GOBACK_LABEL,
      onClick: handleGoBack,
      type: BUTTON,
    },
  ];

  return <View buttons={buttons} fields={fields} form={form} handleSubmit={handleSubmit} isLoading={isLoading} />;
};

export default NotificationsSettingsForm;
