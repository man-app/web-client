import React from 'react';
import { shallow } from 'enzyme';

import NotificationsSettingsForm from '..';

import { FieldProps } from '../../../../../../../../../components/forms/types';
import { ButtonProps } from '../../../../../../../../../components/touchable/types';

describe('Company Settings Notifications form', () => {
  it('should render a valid NotificationsSettingsForm', () => {
    const buttons = [] as ButtonProps[];
    const fields = [] as FieldProps[];
    const form = 'test';
    const handleSubmit = () => {};
    const isLoading = false;

    const component = shallow(
      <NotificationsSettingsForm
        buttons={buttons}
        fields={fields}
        form={form}
        handleSubmit={handleSubmit}
        isLoading={isLoading}
      />
    );
    expect(component).toMatchSnapshot();
  });
});
