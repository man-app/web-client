import styled from 'styled-components';

import { Card } from '../../../../../../../../components/card';

import {
  DESKTOP_L,
  MOBILE_LANDSCAPE_OR_TABLET,
} from '../../../../../../../../../common/constants/styles/media-queries';

interface SectionProps {
  isActive?: boolean;
}

export const Section = styled(Card)`
  display: ${({ isActive }: SectionProps) => (isActive ? 'inline-block' : 'none')};
  margin: 5px auto;
  text-align: left;
  vertical-align: top;
  width: calc(100% - 10px);

  ${MOBILE_LANDSCAPE_OR_TABLET} {
    width: 75%;
  }

  ${DESKTOP_L} {
    width: 100%;
  }
`;
Section.displayName = 'Section';

export const Description = styled.p`
  color: #999;
  font-size: 12px;
  line-height: 1em;
  font-style: italic;
  margin-bottom: 10px;
  text-align: center;
`;
Description.displayName = 'Description';

export const TableLabel = styled.div``;
TableLabel.displayName = 'TableLabel';

export const TableIcon = styled.div``;
TableIcon.displayName = 'TableIcon';
