import React from 'react';
import { pushNotifications } from '../../../../../../../../../common/constants/features';

import SmartForm from '../../../../../../../../components/forms';
import Space from '../../../../../../../../components/space';

import { FieldProps } from '../../../../../../../../components/forms/types';
import { Table, THead, Tr, TBody, Th, Td } from '../../../../../../../../components/table';
import { ButtonProps } from '../../../../../../../../components/touchable/types';
import { Section, Description } from './styles';

interface Props {
  buttons: ButtonProps[];
  fields: FieldProps[];
  form: string;
  handleSubmit: (fields: FieldProps[]) => void;
  isLoading: boolean;
}

const View: React.FC<Props> = ({ buttons, fields, form, handleSubmit, isLoading }: Props) => (
  <SmartForm buttons={buttons} fields={fields} form={form} handleSubmit={handleSubmit} isLoading={isLoading}>
    {({ Buttons, controlledButtons, controlledFields, Field }) => (
      <>
        {/* User notifications (always active since this is the only section on this component) */}
        <Section isActive={true}>
          <Description>
            <i className="fa fa-chrome" /> means {"'"}
            Browser notification
            {"'"} (only when online)
          </Description>
          <Description>
            <i className="fa fa-envelope" /> means {"'"}
            Notification by email
            {"'"} (anytime)
          </Description>
          {pushNotifications && (
            <Description>
              <i className="fa fa-comments" /> means {"'"}
              Push notification
              {"'"} (anytime)
            </Description>
          )}

          <Table>
            <THead>
              <Tr>
                <Th />
                <Th align="center">
                  <i className="fa fa-chrome" />
                </Th>
                <Th align="center">
                  <i className="fa fa-envelope" />
                </Th>
                {pushNotifications && (
                  <Th align="center">
                    <i className="fa fa-comments" />
                  </Th>
                )}
              </Tr>
            </THead>
            {controlledFields.map(field => (
              // @ts-ignore
              <TBody key={field.idModule}>
                <Tr>
                  <Td>{field.name}</Td>
                </Tr>

                {typeof field.fields !== 'undefined' &&
                  field.fields.map((action: any) => (
                    // @ts-ignore
                    <Tr key={`${field.idModule}-${action.verb}`}>
                      <Td verticalAlign="inherit">
                        <Space />
                        <Space />
                        {action.label}
                      </Td>

                      {typeof action.fields !== 'undefined' &&
                        action.fields.map((channel: any) =>
                          !pushNotifications && channel.channel === 'push' ? null : (
                            <Td align="center" key={`${channel.channel}-${channel.id}`}>
                              <Field
                                options={{
                                  ...channel,
                                  isDisabled: (field && field.isDisabled) || (action && action.isDisabled),
                                }}
                              />
                            </Td>
                          )
                        )}
                    </Tr>
                  ))}
              </TBody>
            ))}
          </Table>
        </Section>

        <Buttons options={controlledButtons} />
      </>
    )}
  </SmartForm>
);

export default View;
