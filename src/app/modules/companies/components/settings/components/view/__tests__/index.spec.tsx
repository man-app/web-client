import React from 'react';
import { shallow } from 'enzyme';

import CompanySettings from '..';

describe('Company Settings view', () => {
  it('should render a valid Company settings screen', () => {
    const component = shallow(<CompanySettings />);
    expect(component).toMatchSnapshot();
  });
});
