import React from 'react';

import { Page } from '../../../../../../components/page';
import FormSelector from '../form-selector';

import { MAX_WIDTH_MD } from '../../../../../../../common/constants/styles/sizes';

const View: React.FC = () => (
  <Page isCentered maxWidth={MAX_WIDTH_MD}>
    <FormSelector />
  </Page>
);

export default View;
