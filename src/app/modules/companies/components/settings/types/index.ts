export type CompanySettingsTabIds = 'profile' | 'modules' | 'notifications' | 'options' | 'payments';
