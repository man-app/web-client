import React from 'react';
import { useSelector } from 'react-redux';
import { Redirect } from 'react-router';
import useReactRouter from 'use-react-router';
import { isEmployee } from '../../../../utils/permissions';

import { getLoggedUser } from '../../../auth/selectors';
import { getSelectedCompany } from '../../selectors';

import View from './components/view';

import { COMPANY } from '../../../../../common/constants/appRoutes';

const CompanySettings: React.FC = () => {
  const company = useSelector(getSelectedCompany);

  const {
    match: { params },
  } = useReactRouter();
  const user = useSelector(getLoggedUser);

  // When no company data is present, stop execution
  // TODO: Show a loading spinner?
  if (!company || company.slug !== params.slug) {
    return null;
  }

  // Not employing company, redirect to company profile
  const isEmployingCompany = user && company ? isEmployee(user, company.id) : false;
  if (!isEmployingCompany) {
    return <Redirect to={COMPANY.replace(':slug', params.slug)} />;
  }

  // Return company settings
  return <View />;
};

export default CompanySettings;
