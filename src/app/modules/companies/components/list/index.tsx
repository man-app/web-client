import React from 'react';
import { useSelector, useDispatch } from 'react-redux';

import { getLoggedUser } from '../../../auth/selectors';
import { getEmployingCompanies, isLoadingCompanies } from '../../selectors';

import View from './components/view';
import { getCompanies } from '../../actions';

const CompaniesList: React.FC = () => {
  const user = useSelector(getLoggedUser);
  const companies = useSelector(getEmployingCompanies)(user);
  const isLoading = useSelector(isLoadingCompanies);

  const dispatch = useDispatch();

  React.useEffect(() => {
    dispatch(getCompanies());
  }, []);

  return <View user={user} companies={companies} isLoading={isLoading} />;
};

export default CompaniesList;
