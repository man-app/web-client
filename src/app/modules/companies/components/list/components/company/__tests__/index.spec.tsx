import React from 'react';
import { shallow } from 'enzyme';

import Company from '..';

describe('Company', () => {
  it('should render a valid company card', () => {
    const company = {
      id: '5',
      name: 'Company',
      picture: './picture.jpg',
      slug: 'company',
      created: 123456789,
      lastModified: 123456789,
    };
    const component = shallow(<Company company={company} />);
    expect(component).toMatchSnapshot();
  });

  it('should render a valid company card without picture', () => {
    const company = {
      id: '5',
      name: 'Company',
      slug: 'company',
      created: 123456789,
      lastModified: 123456789,
    };
    const component = shallow(<Company company={company} />);
    expect(component).toMatchSnapshot();
  });
});
