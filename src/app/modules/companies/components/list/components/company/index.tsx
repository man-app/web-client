import React from 'react';

import Avatar from '../../../../../../components/avatar';
import { Buttons } from '../../../../../../components/touchable';
import { Card, PictureWrapper, Info, Title, Properties, Property, LinksWrapper } from './styles';

import { COMPANY_ACTIVITIES, COMPANY, COMPANY_SETTINGS } from '../../../../../../../common/constants/appRoutes';
import { BUTTON } from '../../../../../../components/forms/models';
import { Company as CompanyType } from '../../../../types';

interface CompanyProps {
  company: CompanyType;
}

const Company: React.FC<CompanyProps> = ({ company }: CompanyProps) => (
  <Card key={company.id}>
    <PictureWrapper>
      <Avatar
        profile={{
          picture: company.picture || '',
          name: company.name,
        }}
      />
    </PictureWrapper>

    <Info>
      <Title>{company.name}</Title>

      <Properties>
        <Property>
          <span>Users</span>: {company.users}
        </Property>
        <Property>
          <span>Joined</span>: {company.joined}
        </Property>
      </Properties>

      <LinksWrapper>
        <Buttons
          align="center"
          options={[
            {
              buttonType: 'primary',
              id: `company-${company.id.toString()}-dashboard`,
              isTransparent: true,
              label: 'Dashboard',
              to: COMPANY_ACTIVITIES.replace(':slug', company.slug),
              type: BUTTON,
            },
            {
              buttonType: 'primary',
              id: `company-${company.id.toString()}-profile`,
              isTransparent: true,
              label: 'Profile',
              to: COMPANY.replace(':slug', company.slug),
              type: BUTTON,
            },
            {
              buttonType: 'primary',
              id: `company-${company.id.toString()}-settings`,
              isTransparent: true,
              label: 'Settings',
              to: COMPANY_SETTINGS.replace(':slug', company.slug).replace('/:tab?', ''),
              type: BUTTON,
            },
          ]}
        />
      </LinksWrapper>
    </Info>
  </Card>
);

export default Company;
