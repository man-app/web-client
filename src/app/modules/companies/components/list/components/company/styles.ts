import styled from 'styled-components';

import { Card as DefaultCard } from '../../../../../../components/card';
import { LANDSCAPE, TABLET, DESKTOP, DESKTOP_L } from '../../../../../../../common/constants/styles/media-queries';
import getColor from '../../../../../../../common/constants/styles/theme';
import { ThemedComponent } from '../../../../../../types/styled-components';

export const Card = styled(DefaultCard)`
  ${LANDSCAPE} {
    max-width: 75%;
  }

  ${TABLET} {
    max-width: 75%;
  }

  ${DESKTOP} {
    max-width: calc(50% - 24px);
  }

  ${DESKTOP_L} {
    max-width: calc(33% - 20px);
  }
`;
Card.displayName = 'Card';

const pictureMargin = 20;
const pictureWidth = 75;

interface PicProps extends ThemedComponent {
  isEmpty?: boolean;
}
export const PictureWrapper = styled.div`
  background-color: ${({ isEmpty, theme }: PicProps) =>
    isEmpty ? getColor('AVATAR_BACKGROUND_EMPTY', theme.active) : 'transparent'};
  border: 0px dashed transparent;
  border-radius: 50%;
  border-width: ${({ isEmpty }: PicProps) => (isEmpty ? '3px' : '0px')};
  color: ${({ isEmpty, theme }: PicProps) => (isEmpty ? getColor('AVATAR_ICONS_COLOR', theme.active) : 'transparent')};
  display: inline-block;
  font-size: 48px;
  height: ${pictureWidth}px;
  margin-right: ${pictureMargin}px;
  overflow: hidden;
  text-align: center;
  transition: background-color 0.2s, color 0.2s;
  vertical-align: top;
  width: ${pictureWidth}px;

  img {
    width: 100%;
  }
`;
PictureWrapper.displayName = 'PictureWrapper';

export const Info = styled.div`
  display: inline-block;
  font-size: 16px;
  vertical-align: top;
  width: calc(100% - ${pictureWidth}px - ${pictureMargin}px);
`;
Info.displayName = 'Info';

export const Title = styled.h4`
  font-size: 20px;
`;
Title.displayName = 'Title';

export const Properties = styled.ul`
  margin-bottom: 10px;
  width: 100%;
`;
Properties.displayName = 'Properties';

export const Property = styled.li`
  color: ${({ theme }: ThemedComponent) => getColor('GLOBAL_COLOR_3', theme.active)};
  transition: color 0.2s;
  width: 100%;

  span {
    color: ${({ theme }: ThemedComponent) => getColor('GLOBAL_COLOR', theme.active)};
  }
`;
Property.displayName = 'Property';

export const LinksWrapper = styled.div`
  display: inline-block;
  text-align: center;
  width: 100%;

  & > a,
  & > button,
  & > span {
    display: inline-block;
    font-size: 14px;
    margin: 5px;
  }
`;
LinksWrapper.displayName = 'LinksWrapper';
