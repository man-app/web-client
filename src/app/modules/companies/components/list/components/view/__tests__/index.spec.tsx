import React from 'react';
import { shallow } from 'enzyme';

import Companies from '..';

import { Company } from '../../../../../types';

describe('Companies List view', () => {
  const companies = [
    {
      id: '1',
      name: 'Company',
      slug: 'company',
      created: 123456789,
      lastModified: 123456789,
    },
  ] as Company[];

  const user = {
    id: '1',
    fullName: 'User test',
    email: 'test@user.com',
    created: 123456789,
    lastModified: 123456789,
    isVerified: false,
  };

  it('should render a valid Companies list, with a Create button', () => {
    const component = shallow(<Companies companies={companies} user={{ ...user, isVerified: true }} />);
    expect(component).toMatchSnapshot();
  });

  it('should render a valid Companies list, with a link to validate user account', () => {
    const component = shallow(<Companies companies={companies} user={user} />);
    expect(component).toMatchSnapshot();
  });

  it('should render an empty Companies list, with a Create button', () => {
    const companies = [] as Company[];

    const component = shallow(<Companies companies={companies} user={{ ...user, isVerified: true }} />);
    expect(component).toMatchSnapshot();
  });

  it('should render an empty Companies list, with a link to validate user account', () => {
    const companies = [] as Company[];

    const component = shallow(<Companies companies={companies} user={user} />);
    expect(component).toMatchSnapshot();
  });
});
