import React from 'react';

import { Avatar as AvatarSkeleton, Bar, Buttons, Skeleton, BarWrapper } from '../../../../../../components/skeletons';

import { Card, PictureWrapper, Info, Title, Properties, Property, LinksWrapper } from '../company/styles';

const CompaniesSkeleton: React.FC = () => (
  <Skeleton>
    {['', ''].map((card, index) => (
      <Card key={`company-skeleton-${index}`}>
        <PictureWrapper>
          <AvatarSkeleton />
        </PictureWrapper>

        <Info>
          <Title>
            <BarWrapper>
              <Bar width="50%" />
            </BarWrapper>
          </Title>

          <Properties>
            <Property>
              <BarWrapper>
                <Bar width="25%" />
                <Bar width="25%" />
              </BarWrapper>
            </Property>
          </Properties>

          <LinksWrapper>
            <Buttons count={2} />
          </LinksWrapper>
        </Info>
      </Card>
    ))}
  </Skeleton>
);

export default CompaniesSkeleton;
