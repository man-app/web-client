import React from 'react';

import { Buttons } from '../../../../../../components/touchable';
import Company from '../company';
import { Card, PictureWrapper, Info, Title, Properties, Property, LinksWrapper } from '../company/styles';
import { Wrapper } from './styles';

import { APP_NAME } from '../../../../../../../common/constants/branding';
import { CREATE_LABEL, CREATE_ICON } from '../../../../../../../common/constants/buttons';
import { ACCOUNT, COMPANY_NEW } from '../../../../../../../common/constants/appRoutes';
import { BUTTON } from '../../../../../../components/forms/models';
import { UserLogged } from '../../../../../auth/types';
import { Company as CompanyType } from '../../../../types';
import { ButtonProps } from '../../../../../../components/touchable/types';
import Avatar from '../../../../../../components/avatar';
import CompaniesSkeleton from './skeleton';

interface Props {
  companies: CompanyType[];
  isLoading?: boolean;
  user?: UserLogged;
}

const View: React.FC<Props> = ({ companies, isLoading, user }: Props) => {
  const newCompanyButton: ButtonProps = {
    buttonType: 'primary',
    id: 'new-company-link',
    isTransparent: true,
    label: `${CREATE_LABEL}`,
    to: COMPANY_NEW,
    type: BUTTON,
  };

  const goToAccountLink: ButtonProps = {
    buttonType: 'primary',
    id: 'resend-link-link',
    isTransparent: true,
    to: ACCOUNT.replace('/:tab?/:id?', ''),
    label: 'Resend verification link',
    type: BUTTON,
  };

  return (
    <Wrapper isCentered maxWidth={1440}>
      {isLoading && !companies.length && <CompaniesSkeleton />}
      {(!isLoading || !!companies.length) && (
        <>
          {companies.map(company => (
            <Company company={company} key={company.id} />
          ))}

          <Card>
            <PictureWrapper isEmpty>
              <Avatar
                profile={{
                  picture:
                    user && user.isVerified ? <i className={`fa fa-${CREATE_ICON}`} /> : <i className="fa fa-check" />,
                }}
              />
            </PictureWrapper>

            <Info>
              <Title>{user && user.isVerified ? 'New company' : 'Verify your account'}</Title>

              <Properties>
                <Property>
                  {user && user.isVerified
                    ? `Start using ${APP_NAME} right now!`
                    : `Verify your account to start using ${APP_NAME}`}
                </Property>
                <Property>{user && user.isVerified ? 'Create your company' : 'Check your email inbox'}</Property>
              </Properties>

              <LinksWrapper>
                <Buttons align="center" options={user && user.isVerified ? [newCompanyButton] : [goToAccountLink]} />
              </LinksWrapper>
            </Info>
          </Card>
        </>
      )}
    </Wrapper>
  );
};

export default View;
