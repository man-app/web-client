import styled from 'styled-components';
import { Page } from '../../../../../../components/page';
import { DESKTOP } from '../../../../../../../common/constants/styles/media-queries';

export const Wrapper = styled(Page)`
  ${DESKTOP} {
    text-align: left;
  }
`;
Wrapper.displayName = 'Wrapper';
