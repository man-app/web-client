import React from 'react';

import { Page } from '../../../../../../components/page';
import Profile from '../../../../../../components/profile';

import { MAX_WIDTH } from '../../../../../../../common/constants/styles/sizes';
import { UserLogged } from '../../../../../auth/types';
import { Company } from '../../../../types';

interface Props {
  company: Company;
  user?: UserLogged;
}

const View: React.FC<Props> = ({ company, user }: Props) => (
  <Page maxWidth={MAX_WIDTH}>
    <Profile loggedUser={user} profile={company} />
  </Page>
);

export default View;
