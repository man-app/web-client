import React from 'react';
import { shallow } from 'enzyme';

import Company from '..';

describe('Company details', () => {
  it('should render a valid screen', () => {
    const company = {
      id: '5',
      name: 'Company',
      picture: './picture.jpg',
      slug: 'company',
      created: 123456789,
      lastModified: 123456789,
    };

    const user = {
      id: '1',
      fullName: 'User test',
      email: 'test@user.com',
      created: 123456789,
      lastModified: 123456789,
    };

    const component = shallow(<Company company={company} user={user} />);
    expect(component).toMatchSnapshot();
  });
});
