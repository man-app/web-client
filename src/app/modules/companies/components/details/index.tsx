import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import useReactRouter from 'use-react-router';

import { getLoggedUser } from '../../../auth/selectors';
import { getSelectedCompany } from '../../selectors';

import View from './components/view';

import { getCompany } from '../../actions';

const Company: React.FC = () => {
  const company = useSelector(getSelectedCompany);

  const {
    match: { params },
  } = useReactRouter();
  const dispatch = useDispatch();
  const user = useSelector(getLoggedUser);

  React.useEffect(() => {
    dispatch(getCompany(params.slug));
  }, []);

  // When no company data is present, stop execution
  // TODO: Show a loading spinner?
  if (!company) {
    return null;
  }

  return <View company={company} user={user} />;
};

export default Company;
