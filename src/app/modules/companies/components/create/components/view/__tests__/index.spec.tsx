import React from 'react';
import { shallow } from 'enzyme';

import View from '../../view';

import { FieldProps } from '../../../../../../../components/forms/types';
import { ButtonProps } from '../../../../../../../components/touchable/types';

describe('New Company form', () => {
  it('should render a valid form', () => {
    const buttons = [] as ButtonProps[];
    const fields = [] as FieldProps[];
    const form = 'newCompany';
    const handleSubmit = () => {};
    const isLoading = false;

    const component = shallow(
      <View buttons={buttons} fields={fields} form={form} handleSubmit={handleSubmit} isLoading={isLoading} />
    );
    expect(component).toMatchSnapshot();
  });
});
