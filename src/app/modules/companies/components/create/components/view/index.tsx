import React from 'react';

import { Card } from '../../../../../../components/card';
import SmartForm from '../../../../../../components/forms';
import { Page } from '../../../../../../components/page';

import { MAX_WIDTH_SM } from '../../../../../../../common/constants/styles/sizes';
import { FieldProps } from '../../../../../../components/forms/types';
import { ButtonProps } from '../../../../../../components/touchable/types';

interface Props {
  buttons: ButtonProps[];
  fields: FieldProps[];
  form: string;
  handleSubmit: (fields: FieldProps[]) => void;
  isLoading: boolean;
}

const View: React.FC<Props> = ({ buttons, fields, form, handleSubmit, isLoading }: Props) => (
  <Page maxWidth={MAX_WIDTH_SM}>
    <Card>
      <SmartForm buttons={buttons} fields={fields} form={form} handleSubmit={handleSubmit} isLoading={isLoading}>
        {({ Buttons, controlledButtons, controlledFields, Field }) => (
          <>
            {controlledFields.map(field => (
              <Field key={field.id} options={field} />
            ))}

            <Buttons options={controlledButtons} />
          </>
        )}
      </SmartForm>
    </Card>
  </Page>
);

export default View;
