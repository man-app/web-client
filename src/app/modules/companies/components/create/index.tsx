import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import useReactRouter from 'use-react-router';
import { isValidForm, mapServerErrors } from '../../../../utils/error-handler';

import { getFormErrors } from '../../../../components/forms/selectors';
import { createCompany } from '../../actions';
import { isLoadingCompanies } from '../../selectors';

import View from './components/view';

import { HOME } from '../../../../../common/constants/appRoutes';
import { CREATE_LABEL, CREATE_ICON, GOBACK_LABEL, GOBACK_ICON } from '../../../../../common/constants/buttons';
import { BUTTON, SUBMIT, TEXT } from '../../../../components/forms/models';
import { FieldProps } from '../../../../components/forms/types';
import { ButtonProps } from '../../../../components/touchable/types';

const form = 'newCompany';

const CreateCompany: React.FC = () => {
  const [fields, setFields] = React.useState([
    {
      id: 'name',
      isRequired: true,
      label: 'Company name',
      type: TEXT,
    },
  ] as FieldProps[]);

  const { history } = useReactRouter();
  const dispatch = useDispatch();

  const errors = useSelector(getFormErrors)(form);
  const isLoading = useSelector(isLoadingCompanies);

  React.useEffect(() => {
    setFields(mapServerErrors(fields, errors));
  }, [errors]);

  const handleGoBack = () => {
    history.goBack();
  };

  const handleSubmit = (fields: FieldProps[]) => {
    if (isValidForm(fields)) {
      const name = fields.find(f => f.id === 'name');

      const data = {
        form,
        name: name ? name.model : '',
      };

      // @ts-ignore
      dispatch(createCompany(data)).then(({ errors }: { errors: any }) => {
        if (!errors || !errors.length) {
          history.push({
            pathname: HOME,
            state: { from: location.pathname },
          });
        }
      });
    }
  };

  const buttons: ButtonProps[] = [
    {
      buttonType: 'primary',
      icon: CREATE_ICON,
      id: 'create',
      label: CREATE_LABEL,
      type: SUBMIT,
    },
    {
      icon: GOBACK_ICON,
      id: 'goBack',
      isAlwaysEnabled: true,
      label: GOBACK_LABEL,
      onClick: handleGoBack,
      type: BUTTON,
    },
  ];

  return <View buttons={buttons} fields={fields} form={form} handleSubmit={handleSubmit} isLoading={isLoading} />;
};

export default CreateCompany;
