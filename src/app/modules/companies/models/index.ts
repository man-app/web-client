import { CompanyFromServer } from '../../../../common/apis/manapp';

import { CompaniesState, Company } from '../types';
import { error } from '../../../../common/utils/logger';

export const getState = (): CompaniesState => ({
  collection: [],
  isLoading: false,
  selected: '-1',
});

export const createCompanyFromServer = ({
  id,
  slug,
  name,
  picture,
  publicProfile,
  users,
  contactPhone,
  contactEmail,
  website,
  linkedin,
  facebook,
  instagram,
  twitter,
  youtube,
  joined,
  modules,
  roles,
  created,
  lastModified,
}: CompanyFromServer): Company | undefined => {
  if (
    typeof id === 'undefined' ||
    typeof slug === 'undefined' ||
    typeof name === 'undefined' ||
    typeof created === 'undefined'
  ) {
    error('Companies: Invalid model from server. Some of the following properties is missing', {
      id,
      slug,
      name,
      created,
    });

    return undefined;
  }

  let newModules = [] as string[];
  if (modules) {
    if (!Array.isArray(modules)) {
      error('Companies: Invalid property <modules> from server', modules);
    } else {
      newModules = modules.filter(mod => typeof mod === 'string').map(mod => String(mod));
    }
  }

  let newRoles = [] as any[];
  if (roles) {
    if (!Array.isArray(roles)) {
      error('Companies: Invalid property <modules> from server', roles);
    } else {
      newRoles = roles;
    }
  }

  return {
    id: String(id),
    slug: String(slug),
    name: String(name),
    picture: picture ? String(picture) : undefined,
    publicProfile: publicProfile ? !!publicProfile : undefined,
    users: users ? Number(users) : undefined,
    contactPhone: contactPhone ? String(contactPhone) : undefined,
    contactEmail: contactEmail ? String(contactEmail) : undefined,
    website: website ? String(website) : undefined,
    linkedin: linkedin ? String(linkedin) : undefined,
    facebook: facebook ? String(facebook) : undefined,
    instagram: instagram ? String(instagram) : undefined,
    twitter: twitter ? String(twitter) : undefined,
    youtube: youtube ? String(youtube) : undefined,
    joined: joined ? Number(joined) : undefined,
    modules: newModules,
    roles: newRoles,
    created: Number(created),
    lastModified: Number(lastModified || created),
  };
};

export const createCompaniesFromServer = (companies: CompanyFromServer[]): Company[] =>
  // @ts-ignore
  companies.map(createCompanyFromServer).filter(company => !!company);
