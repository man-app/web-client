import { sortBy } from '../../../utils/arrays';
import { isEmployee } from '../../../utils/permissions';

import { RootState } from '../../../types';
import { UserLogged } from '../../auth/types';

export const isLoadingCompanies = ({ companies }: RootState) => companies.isLoading;

export const getCompanies = ({ companies }: RootState) => companies.collection.map(c => c).sort(sortBy('id', 'asc'));

export const getEmployingCompanies = ({ companies }: RootState) => (user?: UserLogged) =>
  typeof user === 'undefined'
    ? companies.collection.filter(() => false)
    : companies.collection.filter(c => user && isEmployee(user, c.id)).sort(sortBy('id', 'asc'));

export const getCompany = ({ companies }: RootState) => (slug: string) =>
  companies.collection.find(c => c.slug === slug);

export const getCompanyById = ({ companies }: RootState) => (id: string) => companies.collection.find(c => c.id === id);

export const getSelectedCompany = ({ companies }: RootState) =>
  companies.collection.find(c => c.id === companies.selected);

export const getSelectedCompanyId = ({ companies }: RootState) =>
  companies.selected !== '-1' ? companies.selected : undefined;
