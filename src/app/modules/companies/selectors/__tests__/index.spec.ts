import setupEnv from '../../../../../common/utils/setup-test-env';

import {
  isLoadingCompanies,
  getCompanies,
  getEmployingCompanies,
  getCompany,
  getCompanyById,
  getSelectedCompany,
  getSelectedCompanyId,
} from '..';

import { Company } from '../../types';
import rootReducer from '../../../../reducers';

beforeEach(() => {
  setupEnv();
});

describe('Companies selectors', () => {
  const initialState = rootReducer(undefined, { type: 'INIT_ACTION' });
  const populatedState = {
    ...initialState,
    companies: {
      ...initialState.companies,
      collection: [
        { id: '2', name: 'Example 2', slug: 'example-2', created: 123456789, lastModified: 123456789 },
        { id: '1', name: 'Example', slug: 'example', created: 123456789, lastModified: 123456789 },
      ],
      selected: '2',
    },
  };

  describe('isLoadingCompanies', () => {
    it('should return false', () => {
      const expectedOutput = false;
      const output = isLoadingCompanies(populatedState);

      expect(output).toStrictEqual(expectedOutput);
    });
  });

  describe('getCompanies', () => {
    it('should return the list of companies', () => {
      const expectedOutput = [
        { id: '1', name: 'Example', slug: 'example', created: 123456789, lastModified: 123456789 },
        { id: '2', name: 'Example 2', slug: 'example-2', created: 123456789, lastModified: 123456789 },
      ];
      const output = getCompanies(populatedState);

      expect(output).toStrictEqual(expectedOutput);
    });
  });

  describe('getEmployingCompanies', () => {
    it('should return an empty array when no user is logged', () => {
      const user = undefined;
      const state = {
        ...populatedState,
        companies: {
          ...populatedState.companies,
          collection: [
            ...populatedState.companies.collection,
            { id: '3', name: 'Example 3', slug: 'example-3', created: 123456789, lastModified: 123456789 },
          ],
        },
      };
      const expectedOutput = [] as Company[];
      const output = getEmployingCompanies(state)(user);

      expect(output).toStrictEqual(expectedOutput);
    });

    it('should return the list of employing companies', () => {
      const user = {
        id: '1',
        fullName: 'Mike',
        email: 'test@user.com',
        jobs: [
          { idCompany: '1', permissions: {} },
          { idCompany: '2', permissions: {} },
        ],
        created: 123456789,
        lastModified: 123456789,
      };

      const state = {
        ...populatedState,
        companies: {
          ...populatedState.companies,
          collection: [
            ...populatedState.companies.collection,
            { id: '3', name: 'Example 3', slug: 'example-3', created: 123456789, lastModified: 123456789 },
          ],
        },
      };
      const expectedOutput = [
        { id: '1', name: 'Example', slug: 'example', created: 123456789, lastModified: 123456789 },
        { id: '2', name: 'Example 2', slug: 'example-2', created: 123456789, lastModified: 123456789 },
      ];
      const output = getEmployingCompanies(state)(user);

      expect(output).toStrictEqual(expectedOutput);
    });
  });

  describe('getCompany', () => {
    it('should return company details based on a slug', () => {
      const expectedOutput = {
        id: '2',
        name: 'Example 2',
        slug: 'example-2',
        created: 123456789,
        lastModified: 123456789,
      };
      const output = getCompany(populatedState)('example-2');

      expect(output).toStrictEqual(expectedOutput);
    });
  });

  describe('getCompanyById', () => {
    it('should return company details based on an id', () => {
      const expectedOutput = {
        id: '2',
        name: 'Example 2',
        slug: 'example-2',
        created: 123456789,
        lastModified: 123456789,
      };
      const output = getCompanyById(populatedState)('2');

      expect(output).toStrictEqual(expectedOutput);
    });
  });

  describe('getSelectedCompany', () => {
    it('should return selected company', () => {
      const expectedOutput = {
        id: '2',
        name: 'Example 2',
        slug: 'example-2',
        created: 123456789,
        lastModified: 123456789,
      };
      const output = getSelectedCompany(populatedState);

      expect(output).toStrictEqual(expectedOutput);
    });

    it('should return undefined when no company is selected', () => {
      const output = getSelectedCompany({
        ...populatedState,
        companies: { ...populatedState.companies, selected: '-1' },
      });

      expect(output).toBe(undefined);
    });
  });

  describe('getSelectedCompanyId', () => {
    it('should return selected company id', () => {
      const expectedOutput = '2';
      const output = getSelectedCompanyId(populatedState);

      expect(output).toStrictEqual(expectedOutput);
    });
  });
});
