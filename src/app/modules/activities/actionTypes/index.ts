import { ActivitiesActionType } from '../types';

export const ACTIVITIES_REQUESTED: ActivitiesActionType = 'ACTIVITIES_REQUESTED';
export const ACTIVITIES_UPDATED: ActivitiesActionType = 'ACTIVITIES_UPDATED';
export const ACTIVITIES_NOT_UPDATED: ActivitiesActionType = 'ACTIVITIES_NOT_UPDATED';
export const ACTIVITIES_PAGINATE: ActivitiesActionType = 'ACTIVITIES_PAGINATE';
export const ACTIVITIES_RESET_PAGINATION: ActivitiesActionType = 'ACTIVITIES_RESET_PAGINATION';
