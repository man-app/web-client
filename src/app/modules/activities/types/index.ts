import { Dispatch } from 'redux';
import { TabProps } from '../../../components/tabs/types';

export type ActivitiesActionType =
  | 'ACTIVITIES_REQUESTED'
  | 'ACTIVITIES_UPDATED'
  | 'ACTIVITIES_NOT_UPDATED'
  | 'ACTIVITIES_PAGINATE'
  | 'ACTIVITIES_RESET_PAGINATION';

export interface ActivitiesAction {
  type: ActivitiesActionType;
  payload?: any;
}

export type ActivitiesActionCreator = (payload?: any) => (dispatch: Dispatch<ActivitiesAction>) => any;

export interface Activity {
  id: string; // Log ID
  action: string; // Action performed
  created: number; // Date
  type: string; // What was modified

  idCompany: string; // Where action happened
  companyName: string;

  idElement: string; // Its ID
  elementName: string; // Its name
  idModule: string;

  idUser: string; // ID of the user that performed the action
  userName: string; // Its name
  userPicture?: string; // Its picture
}

export interface ActivitiesState {
  collection: Activity[];
  filters: any;
  pagination: {
    shownPages: number;
  };
  silentUpdate: boolean;
  isLoading: boolean;
}

export type ActivitiesTabIds = 'corporate' | 'social';

// @ts-ignore
export interface ActivitiesTabs extends TabProps {
  id: ActivitiesTabIds;
  onClick: (tab: ActivitiesTabIds) => void;
}
