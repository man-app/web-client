import { ActivityFromServer } from '../../../../common/apis/manapp';

import { ActivitiesState, Activity } from '../types';
import { error } from '../../../../common/utils/logger';
import { moduleElementType, moduleIds } from '../../../utils/permissions';

export const filterActivitiesByCompany = ({ collection, companyId }: { collection: Activity[]; companyId: string }) =>
  collection.filter(log => log.idCompany === companyId);

export const getState = (): ActivitiesState => ({
  collection: [],
  filters: {},
  pagination: {
    shownPages: 1,
  },
  silentUpdate: false,
  isLoading: false,
});

export const createActivityFromServer = ({
  id,
  action,
  created = new Date().getTime().toString(),
  idCompany,
  companyName,
  idModule,
  idElement,
  elementName,
  idUser,
  userName,
  userPicture = '',
}: ActivityFromServer): Activity | undefined => {
  if (
    typeof id === 'undefined' ||
    typeof action === 'undefined' ||
    typeof idCompany === 'undefined' ||
    typeof idModule === 'undefined' ||
    typeof idElement === 'undefined' ||
    typeof idUser === 'undefined' ||
    typeof userName === 'undefined'
  ) {
    error('Activities: Invalid model from server. Some of the following properties is missing', {
      id,
      action,
      idCompany,
      idModule,
      idElement,
      user: idUser,
      userName,
    });

    return undefined;
  }

  const moduleName = Object.keys(moduleIds).find(key => moduleIds[key] === idModule);

  if (!moduleName) {
    error(`Activities: Activity generated for an unsupported module: <${idModule}>`);
    return undefined;
  }

  return {
    id: String(id),
    action: String(action),
    created: Number(created),
    idCompany: String(idCompany),
    companyName: String(companyName),
    type: String(moduleElementType[moduleName]),
    idElement: String(idElement),
    elementName: String(elementName),
    idModule: String(idModule),
    idUser: String(idUser),
    userName: String(userName),
    userPicture: userPicture ? String(userPicture) : undefined,
  };
};

export const createActivitiesFromServer = (activities: ActivityFromServer[]): Activity[] =>
  // @ts-ignore
  activities.map(createActivityFromServer).filter(activity => !!activity);
