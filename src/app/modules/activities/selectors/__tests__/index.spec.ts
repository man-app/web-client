import rootReducer from '../../../../reducers';
import * as selectors from '..';

import { Activity } from '../../types';

describe('Activities selectors', () => {
  const rootState = rootReducer(undefined, { type: 'INIT' });
  const activity = {
    id: '1',
    action: 'create',
    created: new Date().getTime(),
    idCompany: '1',
    companyName: 'Company',
    type: 'role',
    idElement: '1',
    elementName: 'Admin',
    idModule: '2',
    idUser: '1',
    userName: 'User',
    userPicture: './user-picture.jpg',
  };

  describe('isSilentUpdate', () => {
    it('should return false', () => {
      const result = selectors.isSilentUpdate(rootState);
      const expectedResult = false;

      expect(result).toBe(expectedResult);
    });
  });

  describe('getActivities', () => {
    it('should return an empty array', () => {
      const result = selectors.getActivities(rootState);
      const expectedResult = [] as Activity[];

      expect(result).toStrictEqual(expectedResult);
    });
  });

  describe('getActivitiesByCompany', () => {
    it('should return an empty array', () => {
      const result = selectors.getActivitiesByCompany(rootState)('1');
      const expectedResult = [] as Activity[];

      expect(result).toStrictEqual(expectedResult);
    });

    it('should return an array with one activity', () => {
      const result = selectors.getActivitiesByCompany({
        ...rootState,
        activities: {
          ...rootState.activities,
          collection: [activity],
        },
      })('1');
      const expectedResult = [activity];

      expect(result).toStrictEqual(expectedResult);
    });

    it('should return an empty array', () => {
      const result = selectors.getActivitiesByCompany({
        ...rootState,
        activities: {
          ...rootState.activities,
          collection: [activity],
        },
      })('2');
      const expectedResult = [] as Activity[];

      expect(result).toStrictEqual(expectedResult);
    });
  });

  describe('canPaginateActivities', () => {
    it('should return false', () => {
      const result = selectors.canPaginateActivities(rootState)('1');
      const expectedResult = false;

      expect(result).toStrictEqual(expectedResult);
    });
  });
});
