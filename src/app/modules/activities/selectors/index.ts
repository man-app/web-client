import { sortCollection, filterCollection } from '../../../utils/collections';

import { paginationSize } from '../../../../common/constants/features';
import { RootState } from '../../../types';
import { filterActivitiesByCompany } from '../models';

export const isSilentUpdate = ({ activities }: RootState) => activities.silentUpdate;

export const isLoadingActivities = ({ activities }: RootState) => activities.isLoading;

export const getActivities = ({ activities }: RootState) => {
  const { collection, filters } = activities;

  const sortedCollection = sortCollection(collection, 'created');
  const filteredActivities = filterCollection(sortedCollection, filters);

  return filteredActivities;
};

export const getActivitiesByCompany = ({ activities }: RootState) => (companyId: string) => {
  const { collection, filters } = activities;

  const sortedCollection = sortCollection(collection, 'created');
  const companyActivities = filterActivitiesByCompany({ collection: sortedCollection, companyId });
  const filteredCompanyActivities = filterCollection(companyActivities, filters);

  return filteredCompanyActivities.slice(0, paginationSize * activities.pagination.shownPages);
};

export const canPaginateActivities = ({ activities }: RootState) => (companyId: string) => {
  const { collection, filters } = activities;

  const sortedCollection = sortCollection(collection, 'created');
  const companyActivities = filterActivitiesByCompany({ collection: sortedCollection, companyId });
  const filteredCompanyActivities = filterCollection(companyActivities, filters);

  const paginatedActivities = filteredCompanyActivities.slice(0, paginationSize * activities.pagination.shownPages);

  return filteredCompanyActivities > paginatedActivities;
};
