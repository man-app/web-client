import manappApi from '../../../../common/apis/manapp';

import {
  ACTIVITIES_REQUESTED,
  ACTIVITIES_UPDATED,
  ACTIVITIES_NOT_UPDATED,
  ACTIVITIES_PAGINATE,
  ACTIVITIES_RESET_PAGINATION,
} from '../actionTypes';

import { ActivitiesActionCreator } from '../types';
import { createActivitiesFromServer } from '../models';

export const getActivities: ActivitiesActionCreator = payload => dispatch => {
  const { laterThan, silentUpdate = false } = payload;
  dispatch({
    type: ACTIVITIES_REQUESTED,
  });

  return manappApi.activities
    .getActivities(laterThan)
    .then(({ activities }) => {
      const payload = {
        silentUpdate,
        activities: createActivitiesFromServer(activities),
      };

      dispatch({
        type: ACTIVITIES_UPDATED,
        payload,
      });

      return payload;
    })
    .catch(err => {
      dispatch({
        type: ACTIVITIES_NOT_UPDATED,
        payload: err,
      });

      return err;
    });
};

export const paginateActivities: ActivitiesActionCreator = () => dispatch => {
  dispatch({
    type: ACTIVITIES_PAGINATE,
  });
};

export const resetActivitiesPagination: ActivitiesActionCreator = () => dispatch => {
  dispatch({
    type: ACTIVITIES_RESET_PAGINATION,
  });
};
