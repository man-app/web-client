import React from 'react';
import { shallow } from 'enzyme';

import Activities from '..';
import { Activity } from '../../../types';

describe('Activities', () => {
  const activeTab = 'corporate';

  const applyFilter = () => {};

  const company = {
    id: '1',
    name: 'Company',
    slug: 'company',
    created: 123456789,
    lastModified: 123456789,
  };

  const isFilterOpen = true;
  const isPaginationEnabled = true;
  const paginateActivities = () => {};
  const socialActivities = [] as Activity[];
  const toggleFilters = () => {};
  const toggleTabs = () => {};

  it('should render a valid Activity list', () => {
    const activities = [
      {
        id: '1',
        action: 'created',
        created: 1568128777030,
        idCompany: '1',
        companyName: 'Company',
        type: 'role',
        idElement: '1',
        elementName: 'Admin',
        idModule: '2',
        idUser: '2',
        userName: 'User',
        userPicture: './user-picture.jpg',
      },
    ];

    const component = shallow(
      <Activities
        activeTab={activeTab}
        activities={activities}
        applyFilter={applyFilter}
        company={company}
        isFilterOpen={isFilterOpen}
        isPaginationEnabled={isPaginationEnabled}
        paginateActivities={paginateActivities}
        socialActivities={socialActivities}
        toggleFilters={toggleFilters}
        toggleTabs={toggleTabs}
      />
    );
    expect(component).toMatchSnapshot();
  });

  it('should render an empty Activity list', () => {
    const activities = [] as Activity[];

    const component = shallow(
      <Activities
        activeTab={activeTab}
        activities={activities}
        applyFilter={applyFilter}
        company={company}
        isFilterOpen={isFilterOpen}
        isPaginationEnabled={isPaginationEnabled}
        paginateActivities={paginateActivities}
        socialActivities={socialActivities}
        toggleFilters={toggleFilters}
        toggleTabs={toggleTabs}
      />
    );
    expect(component).toMatchSnapshot();
  });
});
