import styled from 'styled-components';

import { DESKTOP } from '../../../../../common/constants/styles/media-queries';

export const PageHeading = styled.div`
  margin-bottom: 10px;
  margin-top: 10px;
  text-align: center;

  ${DESKTOP} {
    text-align: right;
  }
`;
PageHeading.displayName = 'PageHeading';

export const TabsWrapper = styled.div`
  display: inline-block;

  & > div {
    display: inline-block;
    margin: 0 auto;
    vertical-align: top;
    width: auto;

    ${DESKTOP} {
      display: none;
    }
  }
`;
TabsWrapper.displayName = 'TabsWrapper';

export const ToggleFiltersWrapper = styled.div`
  display: inline-block;
  float: right;

  ${DESKTOP} {
    float: none;
  }

  & > div {
    display: inline-block;
    margin: 0 auto;
    margin-right: 5px;
    width: auto;

    & > a,
    & > button,
    & > span {
      margin: 0 auto;
      max-width: none;
    }
  }
`;
ToggleFiltersWrapper.displayName = 'ToggleFiltersWrapper';

export const FiltersWrapper = styled.div``;
FiltersWrapper.displayName = 'FiltersWrapper';

interface SectionProps {
  isActive?: boolean;
  isAside?: boolean;
}
export const Section = styled.div`
  display: ${({ isActive }: SectionProps) => (isActive ? 'inline-block' : 'none')};
  vertical-align: top;
  width: 100%;

  ${DESKTOP} {
    display: inline-block;
    margin-left: ${({ isAside }: SectionProps) => (isAside ? 'initial' : '5px')};
    margin-right: ${({ isAside }: SectionProps) => (isAside ? '5px' : 'initial')};
    width: ${({ isAside }: SectionProps) => `calc(${isAside ? 40 : 60}% - 5px)`};
  }
`;
Section.displayName = 'Section';

export const LoadMoreWrapper = styled.div``;
LoadMoreWrapper.displayName = 'LoadMoreWrapper';
