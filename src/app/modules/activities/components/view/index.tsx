import React from 'react';
import { getResourceUrl } from '../../../../../common/utils/resources';
import { moduleIds } from '../../../../utils/permissions';

import Filters from '../../../../components/filters';
import { Page } from '../../../../components/page';
import Tabs from '../../../../components/tabs';
import { Buttons } from '../../../../components/touchable';
import ActivityCard from '../activity';
import { PageHeading, TabsWrapper, ToggleFiltersWrapper, FiltersWrapper, Section } from './styles';

import { MAX_WIDTH } from '../../../../../common/constants/styles/sizes';
import { FieldProps } from '../../../../components/forms/types';
import { TabProps } from '../../../../components/tabs/types';
import { Company } from '../../../companies/types';
import { Activity, ActivitiesTabs, ActivitiesTabIds } from '../../types';
import ActivitiesSkeleton from './skeleton';

interface Props {
  activeTab: ActivitiesTabIds;
  activities: Activity[];
  applyFilter: (event: React.ChangeEvent<HTMLInputElement>) => void;
  company: Company;
  isFilterOpen: boolean;
  isLoading?: boolean;
  isPaginationEnabled: boolean;
  paginateActivities: () => void;
  socialActivities: Activity[];
  toggleFilters: () => void;
  toggleTabs: (tab: ActivitiesTabIds) => void;
}

const View: React.FC<Props> = ({
  activeTab,
  activities,
  applyFilter,
  company,
  isFilterOpen,
  isLoading = false,
  isPaginationEnabled = false,
  paginateActivities,
  socialActivities,
  toggleFilters,
  toggleTabs,
}: Props) => {
  const filters: FieldProps[] = [
    {
      type: 'dropdown',
      id: 'type',
      label: 'By module',
      model: '',
      options: [
        {
          id: '1',
          type: 'option',
          label: 'All modules',
          value: '',
        },
        {
          id: '2',
          type: 'option',
          label: 'Company settings',
          value: 'company',
        },
        {
          id: '3',
          type: 'option',
          label: 'Roles',
          value: 'role',
        },
      ],
      onChange: applyFilter,
    },
    {
      type: 'text',
      id: 'userName',
      label: 'By author',
      onChange: applyFilter,
    },
    {
      type: 'date',
      id: 'laterThan',
      label: 'Later than',
      onChange: applyFilter,
    },
    {
      type: 'date',
      id: 'beforeThan',
      label: 'Before than',
      onChange: applyFilter,
    },
  ];

  const tabs: ActivitiesTabs[] = [
    {
      id: 'corporate',
      label: 'Corporate',
      onClick: () => toggleTabs('corporate'),
    },
    {
      id: 'social',
      label: 'Social',
      onClick: () => toggleTabs('social'),
    },
  ];

  return (
    <Page maxWidth={MAX_WIDTH}>
      <PageHeading>
        <TabsWrapper>
          <Tabs activeTab={activeTab} options={tabs as TabProps[]} />
        </TabsWrapper>

        <ToggleFiltersWrapper>
          <Buttons options={[{ type: 'button', id: 'toggle-filters', icon: 'filter', onClick: toggleFilters }]} />
        </ToggleFiltersWrapper>
      </PageHeading>

      <FiltersWrapper>
        <Filters options={filters} isOpen={isFilterOpen} />
      </FiltersWrapper>

      <Section isActive={activeTab === 'corporate'}>
        {!isLoading && !activities.length && <ActivityCard message="This timeline is empty" />}

        {isLoading && !activities.length && <ActivitiesSkeleton />}

        {(!isLoading || !!activities.length) &&
          activities.map(activity => (
            <ActivityCard
              key={activity.id}
              activity={activity}
              elementLink={{
                id: 'element-link',
                to: getResourceUrl({ type: activity.idModule, id: activity.idElement, company }),
                label: activity.type === 'company' ? 'settings' : activity.elementName,
              }}
              userLink={{
                id: 'user-link',
                to: getResourceUrl({ type: moduleIds.employees, id: activity.idUser, company }),
                label: activity.userName,
              }}
            />
          ))}

        {isPaginationEnabled && (
          <Buttons
            align="center"
            options={[
              {
                type: 'button',
                id: 'load-more',
                label: 'Load more',
                onClick: paginateActivities,
              },
            ]}
          />
        )}
      </Section>

      <Section isActive={activeTab === 'social'} isAside>
        {(!socialActivities || !socialActivities.length) && <ActivityCard message="This timeline is empty" />}
      </Section>
    </Page>
  );
};

export default View;
