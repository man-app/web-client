import React from 'react';
import styled from 'styled-components';

import { Avatar, Bar, Skeleton, BarWrapper } from '../../../../components/skeletons';
import { Activity as ActivityCard, Info } from '../activity/styles';

const InfoWrapper = styled(Info)`
  width: 100%;
`;

const ActivitiesSkeleton: React.FC = () => (
  <Skeleton>
    {['', '', ''].map((activity, index) => (
      <ActivityCard key={index}>
        <Avatar />
        <InfoWrapper>
          <BarWrapper>
            <Bar />
            <Bar width="25%" />
          </BarWrapper>
        </InfoWrapper>
      </ActivityCard>
    ))}
  </Skeleton>
);

export default ActivitiesSkeleton;
