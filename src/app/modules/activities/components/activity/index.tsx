import React from 'react';
import { formatDate } from '../../../../utils/ui';

import Avatar from '../../../../components/avatar';
import { Link } from '../../../../components/touchable';
import { Activity as ActivityCard, Note, Info } from './styles';

import { Activity as ActivityTypo } from '../../types';
import { LinkProps } from '../../../../components/touchable/types';

interface ActivityProps {
  activity: ActivityTypo;
  elementLink: LinkProps;
  userLink: LinkProps;
}

interface MessageProps {
  message: string;
}

type Props = ActivityProps | MessageProps;

const Activity = (props: Props) => {
  if ('message' in props) {
    return (
      <ActivityCard>
        <Note isCentered isMuted>
          {props.message}
        </Note>
      </ActivityCard>
    );
  }

  const { activity, elementLink, userLink } = props;

  return (
    <ActivityCard key={activity.id}>
      <Avatar profile={{ picture: activity.userPicture, fullName: activity.userName }} />
      <Info>
        <p>
          <Link options={userLink} /> {activity.action} the {activity.type} <Link options={elementLink} />
        </p>
        <Note>{formatDate(activity.created)}</Note>
      </Info>
    </ActivityCard>
  );
};

export default Activity;
