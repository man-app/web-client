import React from 'react';
import { shallow } from 'enzyme';

import Activity from '..';

describe('Activity', () => {
  it('should render a valid Activity', () => {
    const activity = {
      id: '1',
      action: 'created',
      created: new Date().getTime(),
      idCompany: '1',
      companyName: 'Company',
      type: 'role',
      idElement: '1',
      elementName: 'Admin',
      idModule: '2',
      idUser: '2',
      userName: 'User',
      userPicture: './user-picture.jpg',
    };
    const elementLink = {
      id: 'element-link',
    };
    const userLink = {
      id: 'user-link',
    };

    const component = shallow(<Activity activity={activity} elementLink={elementLink} userLink={userLink} />);
    expect(component).toMatchSnapshot();
  });

  it('should render a valid message', () => {
    const component = shallow(<Activity message="Test" />);
    expect(component).toMatchSnapshot();
  });
});
