import styled from 'styled-components';

import { Card } from '../../../../components/card';

import getColor from '../../../../../common/constants/styles/theme';
import { ThemedComponent } from '../../../../types/styled-components';

export const Activity = styled(Card)``;
Activity.displayName = 'Activity';

export const AvatarWrapper = styled.div``;
AvatarWrapper.displayName = 'AvatarWrapper';

export const Info = styled.div`
  display: inline-block;
  margin-left: 10px;
  max-width: calc(100% - 75px - 16px);
  vertical-align: top;
`;
Info.displayName = 'Info';

interface NoteProps extends ThemedComponent {
  isCentered?: boolean;
  isMuted?: boolean;
}
export const Note = styled.div`
  color: ${({ isMuted, theme }: NoteProps) => (isMuted ? getColor('DISABLED_COLOR', theme.active) : 'inherit')};
  text-align: ${({ isCentered }: NoteProps) => (isCentered ? 'center' : 'inherit')};
  transition: color 0.2s;
`;
Note.displayName = 'Note';
