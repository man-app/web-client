import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import useReactRouter from 'use-react-router';

import { applyFilter } from '../../actions';
import { getSelectedCompany } from '../companies/selectors';
import { paginateActivities, resetActivitiesPagination, getActivities } from './actions';

import View from './components/view';

import { Activity, ActivitiesTabIds } from './types';
import { getActivitiesByCompany, canPaginateActivities, isLoadingActivities } from './selectors';
import { getCompany } from '../companies/actions';
import { Company } from '../companies/types';

const Activities: React.FC = () => {
  const company = useSelector(getSelectedCompany) as Company;
  const dispatch = useDispatch();
  const isLoading = useSelector(isLoadingActivities);

  const activities = useSelector(getActivitiesByCompany)(company.id);
  const isPaginationEnabled = useSelector(canPaginateActivities)(company.id);
  const socialActivities: Activity[] = [];

  const [activeTab, setActiveTab] = React.useState('corporate' as ActivitiesTabIds);
  const [isFilterOpen, setIsFilterOpen] = React.useState(false);

  const handleGetActivities = () => {
    dispatch(resetActivitiesPagination());
    dispatch(getActivities({ silentUpdate: true }));
  };

  const {
    match: { params },
  } = useReactRouter();

  React.useEffect(() => {
    if (!company) {
      dispatch(getCompany(params.slug));
    }
  }, []);

  React.useEffect(handleGetActivities, [company]);

  const toggleTabs = (tab: ActivitiesTabIds) => {
    if (activeTab !== tab) {
      setActiveTab(tab);
    }
  };

  const toggleFilters = () => {
    setIsFilterOpen(!isFilterOpen);
  };

  const handlePaginateActivities = () => {
    dispatch(paginateActivities());
  };

  const handleApplyFilter = (event: React.ChangeEvent<HTMLInputElement>) => {
    const {
      target: { name, value },
    } = event;

    const shouldSkipFilter = (name: string, value: string) => name === 'type' && value === 'All modules';

    dispatch(
      applyFilter({
        collection: 'logs',
        filter: {
          name,
          value: shouldSkipFilter(name, value) ? '' : value,
        },
      })
    );
  };

  // When no company data is present, stop execution
  // TODO: Show a loading spinner?
  if (!company) {
    return null;
  }

  return (
    <View
      activeTab={activeTab}
      activities={activities}
      toggleTabs={toggleTabs}
      isFilterOpen={isFilterOpen}
      toggleFilters={toggleFilters}
      applyFilter={handleApplyFilter}
      company={company}
      isLoading={isLoading}
      isPaginationEnabled={isPaginationEnabled}
      paginateActivities={handlePaginateActivities}
      socialActivities={socialActivities}
    />
  );
};

export default Activities;
