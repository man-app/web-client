import { updateCollection, updateStateFilters } from '../../../utils/collections';

import {
  ACTIVITIES_REQUESTED,
  ACTIVITIES_UPDATED,
  ACTIVITIES_NOT_UPDATED,
  ACTIVITIES_PAGINATE,
  ACTIVITIES_RESET_PAGINATION,
} from '../actionTypes';
import { FILTERS_APPLIED } from '../../../actionTypes';
import { LOGOUT_SUCCEED } from '../../auth/actionTypes';

import { FilterCollectionAction } from '../../../types';
import { LogoutAction } from '../../auth/types';
import { ActivitiesState, ActivitiesAction, Activity } from '../types';
import { getState } from '../models';

const collection = 'logs';

const initialState = getState();

const reducer = (
  state = initialState,
  action: ActivitiesAction | FilterCollectionAction | LogoutAction
): ActivitiesState => {
  switch (action.type) {
    case FILTERS_APPLIED:
      return updateStateFilters(collection, state, action.payload);

    case ACTIVITIES_REQUESTED:
      return {
        ...state,
        isLoading: true,
      };

    case ACTIVITIES_UPDATED:
      const activitiesFromServer = action.payload.activities as Activity[];
      return {
        ...state,
        collection: updateCollection(state.collection, activitiesFromServer),
        silentUpdate: !!action.payload.silentUpdate,
        isLoading: false,
      };

    case ACTIVITIES_NOT_UPDATED:
      return {
        ...state,
        isLoading: false,
      };

    case ACTIVITIES_PAGINATE:
      return {
        ...state,
        pagination: {
          ...state.pagination,
          shownPages: state.pagination.shownPages + 1,
        },
      };

    case ACTIVITIES_RESET_PAGINATION:
      return {
        ...state,
        pagination: {
          ...state.pagination,
          shownPages: initialState.pagination.shownPages,
        },
      };

    case LOGOUT_SUCCEED:
      return {
        ...initialState,
      };

    default:
      return state;
  }
};

export default reducer;
