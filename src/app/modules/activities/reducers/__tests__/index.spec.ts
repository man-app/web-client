import reducer from '..';

import { getState } from '../../models';
import {
  ACTIVITIES_REQUESTED,
  ACTIVITIES_UPDATED,
  ACTIVITIES_NOT_UPDATED,
  ACTIVITIES_PAGINATE,
  ACTIVITIES_RESET_PAGINATION,
} from '../../actionTypes';
import { LOGOUT_SUCCEED } from '../../../auth/actionTypes';
import { FILTERS_APPLIED } from '../../../../actionTypes';

describe('Activities reducer', () => {
  const defaultState = getState();

  it('should update filters', () => {
    const expectedState = {
      ...defaultState,
      filters: {
        ...defaultState.filters,
        filterName: 'filterValue',
      },
    };

    const nextState = reducer(defaultState, {
      type: FILTERS_APPLIED,
      payload: {
        collection: 'logs',
        filter: {
          name: 'filterName',
          value: 'filterValue',
        },
      },
    });
    expect(nextState).toStrictEqual(expectedState);
  });

  describe('should update isLoading', () => {
    it('to true', () => {
      const expectedState = {
        ...defaultState,
        isLoading: true,
      };

      const nextState = reducer(defaultState, { type: ACTIVITIES_REQUESTED });
      expect(nextState).toStrictEqual(expectedState);
    });

    it('to false (error)', () => {
      const initialState = {
        ...defaultState,
        isLoading: false,
      };

      const nextState = reducer(initialState, { type: ACTIVITIES_NOT_UPDATED });
      expect(nextState).toStrictEqual(defaultState);
    });

    it('to false (success), and update activities collection', () => {
      const initialState = {
        ...defaultState,
        isLoading: true,
      };

      const expectedState = {
        ...defaultState,
        isLoading: false,
        collection: [
          {
            id: 1,
            action: 'created',
            created: 'A minute ago',
            idCompany: '1',
            companyName: 'Company',
            type: 'role',
            idElement: 1,
            elementName: 'Admin',
            idUser: '2',
            userName: 'User',
            userPicture: './user-picture.jpg',
          },
        ],
      };

      const nextState = reducer(initialState, {
        type: ACTIVITIES_UPDATED,
        payload: {
          activities: [
            {
              id: 1,
              action: 'created',
              created: 'A minute ago',
              idCompany: '1',
              companyName: 'Company',
              type: 'role',
              idElement: 1,
              elementName: 'Admin',
              idUser: '2',
              userName: 'User',
              userPicture: './user-picture.jpg',
            },
          ],
        },
      });
      expect(nextState).toStrictEqual(expectedState);
    });
  });

  describe('should update pagination', () => {
    it('to next page', () => {
      const expectedState = {
        ...defaultState,
        pagination: {
          ...defaultState.pagination,
          shownPages: defaultState.pagination.shownPages + 1,
        },
      };

      const nextState = reducer(defaultState, { type: ACTIVITIES_PAGINATE });
      expect(nextState).toStrictEqual(expectedState);
    });

    it('to default value', () => {
      const initialState = {
        ...defaultState,
        pagination: {
          ...defaultState.pagination,
          shownPages: defaultState.pagination.shownPages + 1,
        },
      };

      const nextState = reducer(initialState, { type: ACTIVITIES_RESET_PAGINATION });
      expect(nextState).toStrictEqual(defaultState);
    });
  });

  it('should return initial state', () => {
    const initialState = {
      ...defaultState,
      collection: [
        {
          id: '1',
          action: 'created',
          created: new Date().getTime(),
          idCompany: '1',
          companyName: 'Company',
          type: 'role',
          idElement: '1',
          elementName: 'Admin',
          idModule: '2',
          idUser: '2',
          userName: 'User',
          userPicture: './user-picture.jpg',
        },
      ],
      pagination: {
        shownPages: 2,
      },
    };

    const nextState = reducer(initialState, { type: LOGOUT_SUCCEED });
    expect(nextState).toStrictEqual(defaultState);
  });
});
