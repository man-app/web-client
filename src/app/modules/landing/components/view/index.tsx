import React from 'react';

import { Card } from '../../../../components/card';
import Spacer from '../../../../components/spacer';
import { Link } from '../../../../components/touchable';
import LoginForm from '../../../auth/components/login/components/form';
import { Wrapper, Section, FormBalancer, InfoBalancer, Secondary, Description } from './styles';

import { REGISTER, RESET_PASSWORD } from '../../../../../common/constants/appRoutes';
import { APP_NAME } from '../../../../../common/constants/branding';
import { CREATE_LABEL } from '../../../../../common/constants/buttons';
import { LinkProps } from '../../../../components/touchable/types';

const registerLink: LinkProps = {
  id: 'register-link',
  to: REGISTER,
  label: `${CREATE_LABEL} account`,
};
const resetPasswordLink: LinkProps = {
  id: 'reset-password-link',
  to: RESET_PASSWORD.replace(':email?', '').replace('/:code?', ''),
  label: 'Reset password',
};

const View: React.FC = () => (
  <Wrapper>
    <Section isLogin>
      <FormBalancer>
        <Card>
          <p>Start now using {APP_NAME} and organize your world</p>
          <LoginForm hideBackBtn={true} />

          <Spacer />

          <Secondary>
            <Link options={registerLink} /> | <Link options={resetPasswordLink} />
          </Secondary>
        </Card>
      </FormBalancer>
    </Section>

    <Section>
      <InfoBalancer>
        <Description>
          <li>
            <i className="fa fa-sign-in" /> Join or create your company
          </li>
          <li>
            <i className="fa fa-sliders" /> Manage your company stuff
          </li>
          <li>
            <i className="fa fa-refresh" /> Get updates about your company
          </li>
        </Description>
      </InfoBalancer>
    </Section>
  </Wrapper>
);

export default View;
