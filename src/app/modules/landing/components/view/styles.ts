import styled from 'styled-components';

import { Page } from '../../../../components/page';

import { TABLET, TABLET_LANDSCAPE, DESKTOP_LANDSCAPE } from '../../../../../common/constants/styles/media-queries';
import { HEADER_SIZE, FOOTER_SIZE_TABLET } from '../../../../../common/constants/styles/sizes';
import getColor from '../../../../../common/constants/styles/theme';
import { ThemedComponent } from '../../../../types/styled-components';

export const Wrapper = styled(Page)`
  background-color: ${({ theme }: ThemedComponent) => getColor('LANDING_BACKGROUND', theme.active)};
  padding: 0px !important;
  text-align: left;

  ${TABLET} {
    min-height: calc(100vh - ${HEADER_SIZE + FOOTER_SIZE_TABLET}px);
  }
`;
Wrapper.displayName = 'Wrapper';

interface SectionProps extends ThemedComponent {
  isLogin?: boolean;
}

export const Section = styled.div`
  background-color: ${({ isLogin, theme }: SectionProps) =>
    isLogin ? getColor('GLOBAL_BACKGROUND', theme.active) : 'transparent'};
  display: inline-block;
  margin: 0 auto;
  min-height: calc(50vh - (${HEADER_SIZE}px / 2));
  padding: 40px;
  position: relative;
  transition: background-color 0.2s;
  vertical-align: top;
  width: 100%;

  ${TABLET_LANDSCAPE} {
    min-height: calc(100vh - ${HEADER_SIZE}px);
    width: calc(50% - 3.1px);
  }

  ${DESKTOP_LANDSCAPE} {
    min-height: calc(100vh - ${HEADER_SIZE + FOOTER_SIZE_TABLET}px);
  }
`;
Section.displayName = 'Section';

const Balancer = styled.div`
  ${TABLET} {
    left: 50%;
    position: absolute;
    text-align: left;
    top: 50%;
    transform: translate(-50%, -50%);
  }
`;
Balancer.displayName = 'Balancer';

export const FormBalancer = styled(Balancer)`
  max-width: 500px;
  margin: 0 auto;
  width: 100%;
`;
FormBalancer.displayName = 'FormBalancer';

export const InfoBalancer = styled(Balancer)`
  left: 50%;
  position: absolute;
  text-align: left;
  top: 50%;
  transform: translate(-50%, -50%);
`;
InfoBalancer.displayName = 'InfoBalancer';

export const Secondary = styled.div`
  color: #999;
  font-style: italic;
  margin-bottom: 10px;
  text-align: center;
`;
Secondary.displayName = 'Secondary';

export const Description = styled.ul`
  color: ${({ theme }: ThemedComponent) => getColor('LANDING_COLOR', theme.active)};
  transition: color 0.2s;

  & > li {
    margin-bottom: 15px;
  }

  & > li > .fa {
    margin-right: 10px;
  }
`;
Description.displayName = 'Description';
