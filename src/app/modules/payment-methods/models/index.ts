import { PaymentMethodFromServer } from '../../../../common/apis/manapp';
import { error } from '../../../../common/utils/logger';

import { PaymentMethod, PaymentMethodState } from '../types';

export const getState = (): PaymentMethodState => ({
  collection: [],
  isLoading: false,
  selected: '',
});

const createPaymentMethodFromServer = ({
  id,
  cardNumber,
  ccv,
  created,
  description,
  isDefault,
  lastModified,
  owner,
  validUntil,
}: PaymentMethodFromServer): PaymentMethod | undefined => {
  if (
    typeof id === 'undefined' ||
    typeof cardNumber === 'undefined' ||
    typeof ccv === 'undefined' ||
    typeof owner === 'undefined' ||
    typeof validUntil === 'undefined' ||
    typeof created === 'undefined'
  ) {
    error('PaymentMethods: Invalid model from server. Some of the following properties is missing', {
      id,
      cardNumber,
      ccv,
      created,
      owner,
      validUntil,
    });

    return undefined;
  }

  return {
    cardNumber: String(cardNumber),
    ccv: String(ccv),
    created: Number(created),
    description: description ? String(description) : undefined,
    id: String(id),
    isDefault: !!isDefault,
    lastModified: Number(lastModified || created),
    owner: String(owner),
    validUntil: String(validUntil),
  };
};

export const createPaymentMethodsFromServer = (paymentMethods: PaymentMethodFromServer[]): PaymentMethod[] =>
  // @ts-ignore
  paymentMethods.map(createPaymentMethodFromServer).filter(paymentMethod => !!paymentMethod);
