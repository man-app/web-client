import { Dispatch } from 'redux';

export interface PaymentMethod {
  id: string;
  cardNumber: string;
  ccv: string;
  description?: string;
  owner: string;
  validUntil: string;
  created: number;
  lastModified: number;
  isDefault: boolean;
}

export type PaymentMethodActionTypes =
  | 'PAYMENT_METHODS_REQUESTED'
  | 'PAYMENT_METHODS_UPDATED'
  | 'PAYMENT_METHODS_NOT_UPDATED'
  | 'PAYMENT_METHOD_FORM_SUBMITTED'
  | 'PAYMENT_METHOD_FORM_SUCCEED'
  | 'PAYMENT_METHOD_FORM_FAILED'
  | 'DELETE_PAYMENT_METHOD_SUBMITTED'
  | 'DELETE_PAYMENT_METHOD_SUCCEED'
  | 'DELETE_PAYMENT_METHOD_FAILED'
  | 'SELECT_PAYMENT_METHOD';

export interface PaymentMethodAction {
  type: PaymentMethodActionTypes;
  payload?: any;
}

export type PaymentMethodActionCreator = (payload?: any) => (dispatch: Dispatch<PaymentMethodAction>) => any;

export interface PaymentMethodState {
  collection: PaymentMethod[];
  isLoading: boolean;
  selected: string;
}
