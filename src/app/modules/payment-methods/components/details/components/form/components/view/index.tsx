import React from 'react';

import SmartForm from '../../../../../../../../components/forms';
import { ButtonProps } from '../../../../../../../../components/touchable/types';
import { FieldProps } from '../../../../../../../../components/forms/types';
import { Card } from '../../../../../../../../components/card';
import PaymentCard from '../../../../../payment-card';

interface Props {
  buttons: ButtonProps[];
  children: React.ReactChild;
  defaultEnabled?: boolean;
  fields: FieldProps[];
  form: string;
  handleSubmit: (values: FieldProps[]) => void;
  isLoading: boolean;
}

const View: React.FC<Props> = ({ buttons, children, defaultEnabled, fields, form, handleSubmit, isLoading }: Props) => (
  <>
    <SmartForm
      buttons={buttons}
      fields={fields}
      form={form}
      permissions={{ label: '', value: true }}
      handleSubmit={handleSubmit}
      isLoading={isLoading}
      defaultEnabled={defaultEnabled}
    >
      {({ Buttons, controlledButtons, controlledFields, Field, values }) => (
        <>
          <PaymentCard
            paymentMethod={{
              id: '',
              description: values.description || '',
              cardNumber: values.cardNumber || '',
              owner: values.owner || '',
              validUntil: values.validUntil || '',
              ccv: values.ccv || '',
              isDefault: values.isDefault || false,
              created: 0,
              lastModified: 0,
            }}
          />

          <Card width="50%">
            {controlledFields.map(field => (
              <Field key={field.id} options={field} />
            ))}
          </Card>

          <Buttons options={controlledButtons} />
        </>
      )}
    </SmartForm>
    {children}
  </>
);

export default View;
