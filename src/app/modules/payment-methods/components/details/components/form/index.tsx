import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import useReactRouter from 'use-react-router';
import { parseCardNumber } from '../../../../../../../common/utils/numbers';
import { isValidForm, mapServerErrors } from '../../../../../../utils/error-handler';

import { getFormErrors } from '../../../../../../components/forms/selectors';
import { createPaymentMethod, updatePaymentMethod, deletePaymentMethod } from '../../../../actions';
import { getSelectedPaymentMethod, isLoadingPaymentMethods } from '../../../../selectors';

import DeleteModal from '../../../../../../components/modal/components/delete';
import View from './components/view';

import { PAYMENT_METHODS } from '../../../../../../../common/constants/appRoutes';
import {
  SAVE_ICON,
  SAVE_LABEL,
  GOBACK_ICON,
  GOBACK_LABEL,
  DELETE_ICON,
  DELETE_LABEL,
} from '../../../../../../../common/constants/buttons';
import { SUBMIT, BUTTON, TEXT, SWITCH } from '../../../../../../components/forms/models';
import { FieldProps } from '../../../../../../components/forms/types';
import { ButtonProps } from '../../../../../../components/touchable/types';

const form = 'paymentMethod';

const PaymentMethodForm: React.FC = () => {
  const [fields, setFields] = React.useState([] as FieldProps[]);
  const [isConfirmOpen, setIsConfirmOpen] = React.useState(false);

  const {
    history,
    location,
    match: { params },
  } = useReactRouter();
  const dispatch = useDispatch();
  const isNewPaymentMethod = params.id === 'new';
  const isLoading = useSelector(isLoadingPaymentMethods);
  const errors = useSelector(getFormErrors)(form);
  const paymentMethod = useSelector(getSelectedPaymentMethod);

  const generateFields = (): FieldProps[] => [
    {
      id: 'description',
      label: 'Description',
      model: !isNewPaymentMethod ? (paymentMethod && paymentMethod.description) || '' : undefined,
      updateId: params.id,
      type: TEXT,
    },
    {
      id: 'cardNumber',
      isRequired: true,
      label: 'Card number',
      maxLength: 16,
      minLength: 16,
      model: !isNewPaymentMethod ? (paymentMethod && paymentMethod.cardNumber) || '' : undefined,
      updateId: params.id,
      type: TEXT,
    },
    {
      id: 'owner',
      isRequired: true,
      label: 'Name',
      model: !isNewPaymentMethod ? (paymentMethod && paymentMethod.owner) || '' : undefined,
      updateId: params.id,
      type: TEXT,
    },
    {
      id: 'validUntil',
      isRequired: true,
      label: 'Expiration date',
      model: !isNewPaymentMethod ? (paymentMethod && paymentMethod.validUntil) || '' : undefined,
      updateId: params.id,
      type: TEXT,
      maxWidth: '50%',
    },
    {
      id: 'ccv',
      isRequired: true,
      label: 'Security code',
      maxLength: 3,
      minLength: 3,
      model: !isNewPaymentMethod ? (paymentMethod && paymentMethod.ccv) || '' : undefined,
      updateId: params.id,
      type: TEXT,
      maxWidth: '50%',
    },
    {
      id: 'isDefault',
      label: 'Mark this method as default payment method',
      isChecked: !isNewPaymentMethod ? paymentMethod && paymentMethod.isDefault : undefined,
      updateId: params.id,
      type: SWITCH,
    },
  ];

  React.useEffect(() => {
    setFields(mapServerErrors(fields, errors));
  }, [errors]);

  React.useEffect(() => {
    if (isNewPaymentMethod || (paymentMethod && paymentMethod.id === params.id)) {
      setFields(generateFields());
    } else {
      setFields([]);
    }
  }, [paymentMethod]);

  const goBack = () => {
    history.goBack();
  };

  const handleSubmit = (controlledFields: FieldProps[]) => {
    if (isValidForm(controlledFields)) {
      const description = controlledFields.find(f => f.id === 'description');
      const cardNumber = controlledFields.find(f => f.id === 'cardNumber');
      const owner = controlledFields.find(f => f.id === 'owner');
      const validUntil = controlledFields.find(f => f.id === 'validUntil');
      const ccv = controlledFields.find(f => f.id === 'ccv');
      const isDefault = controlledFields.find(f => f.id === 'isDefault');

      const params = {
        id: paymentMethod ? paymentMethod.id : undefined,
        description: description && description.model,
        cardNumber: cardNumber && cardNumber.model,
        owner: owner && owner.model,
        validUntil: validUntil && validUntil.model,
        ccv: ccv && ccv.model,
        isDefault: isDefault && isDefault.isChecked,
      };

      const action = !!paymentMethod ? updatePaymentMethod : createPaymentMethod;

      // @ts-ignore
      dispatch(action({ form, params })).then(({ errors }) => {
        if (!errors || !errors.length) {
          history.push({ pathname: location.state ? location.state.from : PAYMENT_METHODS });
        }
      });
    }
  };

  const handleDelete = () => {
    setIsConfirmOpen(true);
  };

  const handleDeleteCancel = () => {
    setIsConfirmOpen(false);
  };

  const handleDeleteConfirm = () => {
    if (paymentMethod) {
      // @ts-ignore
      dispatch(deletePaymentMethod(paymentMethod.id)).then(({ errors }) => {
        if (!errors || !errors.length) {
          history.push({ pathname: PAYMENT_METHODS });
        }
      });
    }
  };

  // When no paymentMethod data is present, stop execution
  // TODO: Show a loading spinner?
  if (fields.length === 0) {
    return null;
  }

  const buttons: ButtonProps[] = [
    {
      buttonType: 'primary',
      icon: SAVE_ICON,
      id: 'save',
      label: SAVE_LABEL,
      type: SUBMIT,
    },
    {
      icon: GOBACK_ICON,
      id: 'goback',
      isAlwaysEnabled: true,
      label: GOBACK_LABEL,
      onClick: goBack,
      type: BUTTON,
    },
  ];

  if (!isNewPaymentMethod) {
    const goBackButton = buttons.pop();

    buttons.push({
      icon: DELETE_ICON,
      id: 'delete',
      label: DELETE_LABEL,
      onClick: handleDelete,
      type: BUTTON,
    });

    if (goBackButton) {
      buttons.push(goBackButton);
    }
  }

  return (
    <View
      buttons={buttons}
      defaultEnabled={isNewPaymentMethod}
      fields={fields}
      form={form}
      handleSubmit={handleSubmit}
      isLoading={isLoading}
    >
      <DeleteModal
        handleClose={handleDeleteCancel}
        handleSubmit={handleDeleteConfirm}
        isLoading={isLoading}
        isOpen={isConfirmOpen}
      >
        <>
          <p>Are you sure that you want to delete this payment method?</p>
          {!!paymentMethod && (
            <>
              <br />
              <p>Card number: {parseCardNumber(paymentMethod.cardNumber)}</p>
              {!!paymentMethod.description && <p>Description: {paymentMethod.description}</p>}
            </>
          )}
        </>
      </DeleteModal>
    </View>
  );
};

export default PaymentMethodForm;
