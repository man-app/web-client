import React from 'react';
import { shallow } from 'enzyme';

import View from '..';

describe('PaymentMethod Form', () => {
  it('Should render a valid screen', () => {
    const component = shallow(
      <View buttons={[]} fields={[]} form={'test'} handleSubmit={() => {}} isLoading={false}>
        <p>Test children</p>
      </View>
    );

    expect(component).toMatchSnapshot();
  });
});
