import React from 'react';

import PaymentMethodForm from '../form';

const View: React.FC = () => <PaymentMethodForm />;

export default View;
