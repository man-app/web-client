import React from 'react';
import { shallow } from 'enzyme';

import View from '..';

describe('PaymentMethod details', () => {
  it('Should render a valid screen', () => {
    const component = shallow(<View />);

    expect(component).toMatchSnapshot();
  });
});
