import React from 'react';
import { useDispatch } from 'react-redux';
import useReactRouter from 'use-react-router';

import { selectPaymentMethod, getPaymentMethods } from '../../actions';

import View from './components/view';

const PaymentMethod: React.FC = () => {
  const dispatch = useDispatch();
  const {
    match: { params },
  } = useReactRouter();

  const isNewPaymentMethod = params.id === 'new';

  React.useEffect(() => {
    if (isNewPaymentMethod) {
      dispatch(selectPaymentMethod(-1));
    } else if (params.id) {
      dispatch(getPaymentMethods());
      dispatch(selectPaymentMethod(params.id));
    }
  }, []);

  return <View />;
};

export default PaymentMethod;
