import React from 'react';
import { StyledComponent } from 'styled-components';
import { obfuscateCardNumber, parseCardNumber } from '../../../../../common/utils/numbers';

import {
  Wrapper,
  LinkShape,
  CardShape,
  Header,
  Nick,
  Logo,
  Body,
  CardNumber,
  Label,
  ValidUntil,
  CardOwner,
} from './styles';

import { PAYMENT_METHOD } from '../../../../../common/constants/appRoutes';
import { PaymentMethod } from '../../types';

const manappLogo = require('../../../../../images/logo.png').default;

interface Props {
  fullSize?: boolean;
  link?: boolean;
  obfuscateNumber?: boolean;
  paymentMethod: PaymentMethod;
}

const PaymentCard: React.FC<Props> = ({ fullSize, link, obfuscateNumber, paymentMethod }: Props) => {
  const Shape: StyledComponent<any, any> = link ? LinkShape : CardShape;

  return (
    <Wrapper isDefault={paymentMethod.isDefault} width={fullSize ? '100%' : '50%'}>
      <Shape to={PAYMENT_METHOD.replace(':id', paymentMethod.id.toString())}>
        <Header>
          <Nick>{paymentMethod.description}</Nick>
          <Logo src={manappLogo} />
        </Header>
        <Body>
          <CardNumber>
            {obfuscateNumber
              ? obfuscateCardNumber(paymentMethod.cardNumber)
              : parseCardNumber(paymentMethod.cardNumber)}
          </CardNumber>
          <p>
            <Label>Valid until:</Label>
            <ValidUntil>{paymentMethod.validUntil}</ValidUntil>
          </p>
          <CardOwner>{paymentMethod.owner}</CardOwner>
        </Body>
      </Shape>
    </Wrapper>
  );
};

export default PaymentCard;
