import React from 'react';
import { shallow } from 'enzyme';

import PaymentCard from '..';
import { PaymentMethod } from '../../../types';

const emptyPaymentMethod: PaymentMethod = {
  cardNumber: '',
  ccv: '',
  id: '1',
  isDefault: false,
  owner: '',
  validUntil: '',
  created: 123456789,
  lastModified: 123456789,
};

const populatedPaymentMethod = {
  ...emptyPaymentMethod,
  cardNumber: '1234567891234567',
  ccv: '123',
  owner: 'Test user',
  validUntil: '01/19',
};

describe('PaymentCard', () => {
  it('should return a card', () => {
    const component = shallow(<PaymentCard paymentMethod={populatedPaymentMethod} />);

    expect(component).toMatchSnapshot();
  });

  it('should return a default card', () => {
    const component = shallow(<PaymentCard paymentMethod={{ ...populatedPaymentMethod, isDefault: true }} />);

    expect(component).toMatchSnapshot();
  });

  it('should return a fullsize card', () => {
    const component = shallow(<PaymentCard fullSize paymentMethod={populatedPaymentMethod} />);

    expect(component).toMatchSnapshot();
  });

  it('should return a linked card', () => {
    const component = shallow(<PaymentCard link paymentMethod={populatedPaymentMethod} />);

    expect(component).toMatchSnapshot();
  });

  it('should return an empty card', () => {
    const component = shallow(<PaymentCard paymentMethod={emptyPaymentMethod} />);

    expect(component).toMatchSnapshot();
  });

  it('should return an obfuscated card', () => {
    const component = shallow(<PaymentCard obfuscateNumber paymentMethod={populatedPaymentMethod} />);

    expect(component).toMatchSnapshot();
  });
});
