import styled, { css } from 'styled-components';
import { Link } from 'react-router-dom';

import { Card } from '../../../../components/card';

import getColor from '../../../../../common/constants/styles/theme';
import { TABLET, BIG_WIDTH_AT_50_PERCENT } from '../../../../../common/constants/styles/media-queries';
import { ThemedComponent } from '../../../../types/styled-components';

export interface WrapperProps extends ThemedComponent {
  isDefault?: boolean;
}

export const Wrapper = styled(Card)`
  background-color: ${({ isDefault, theme }: WrapperProps) =>
    getColor(isDefault ? 'PAYMENT_BACKGROUND_DEFAULT' : 'PAYMENT_BACKGROUND', theme.active)};
  border-radius: 20px;
  color: ${({ isDefault, theme }: WrapperProps) =>
    getColor(isDefault ? 'PAYMENT_COLOR_DEFAULT' : 'PAYMENT_COLOR', theme.active)};
  padding: 0 !important;
  transition: background-color 0.2s, color 0.2s;
`;
Wrapper.displayName = 'Wrapper';

const Shape = css`
  color: inherit;
  display: block;
  padding-top: 63%;
  position: relative;
  text-decoration: none;

  ${TABLET} {
    padding-top: 56%;
  }
`;

export const LinkShape = styled(Link)`
  ${Shape}
`;
LinkShape.displayName = 'LinkShape';

export const CardShape = styled.div`
  ${Shape}
`;
CardShape.displayName = 'CardShape';

export const Header = styled.span`
  display: flex;
  flex-direction: row;
  left: 10px;
  position: absolute;
  right: 10px;
  top: 10px;

  ${BIG_WIDTH_AT_50_PERCENT} {
    left: 20px;
    right: 20px;
    top: 20px;
  }
`;
Header.displayName = 'Header';

export const Nick = styled.h5`
  display: inline-flex;
  flex: 1;
  font-size: 24px;
  font-weight: normal;
  vertical-align: top;
`;
Nick.displayName = 'Nick';

export const Logo = styled.img`
  display: inline-flex;
  height: 48px;
  vertical-align: top;
  width: 48px;
`;
Logo.displayName = 'Logo';

export const Body = styled.span`
  display: block;
  bottom: 10px;
  left: 10px;
  position: absolute;
  right: 10px;

  ${BIG_WIDTH_AT_50_PERCENT} {
    bottom: 20px;
    left: 20px;
    right: 20px;
  }
`;
Body.displayName = 'Body';

export const CardNumber = styled.span`
  display: block;
  font-size: 20px;
  letter-spacing: 2px;
  margin-bottom: 15px;
  text-transform: uppercase;
`;
CardNumber.displayName = 'CardNumber';

export const Label = styled.span`
  display: inline-block;
  font-size: 12px;
  margin-left: 10px;
  vertical-align: top;
`;
Label.displayName = 'Label';

export const ValidUntil = styled.span`
  display: inline-block;
  font-size: 18px;
  margin-left: 5px;
`;
ValidUntil.displayName = 'ValidUntil';

export const CardOwner = styled.span`
  display: block;
  font-size: 20px;
  margin-top: 5px;
  text-transform: uppercase;
`;
CardOwner.displayName = 'CardOwner';
