import React from 'react';
import { useSelector, useDispatch } from 'react-redux';

import { getLoggedUser } from '../../../auth/selectors';
import { getPaymentMethods as getPaymentMethodsAction } from '../../actions';
import { getPaymentMethods, isLoadingPaymentMethods } from '../../selectors';

import View from './components/view';

export const PaymentMethodList: React.FC = () => {
  const dispatch = useDispatch();
  const user = useSelector(getLoggedUser);
  const paymentMethods = useSelector(getPaymentMethods);
  const isLoading = useSelector(isLoadingPaymentMethods);

  React.useEffect(() => {
    if (user) {
      dispatch(getPaymentMethodsAction(user.id));
    }
  }, [user]);

  return <View isLoading={isLoading} paymentMethods={paymentMethods} />;
};

export default PaymentMethodList;
