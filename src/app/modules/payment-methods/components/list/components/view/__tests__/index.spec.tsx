import React from 'react';
import { shallow } from 'enzyme';

import View from '..';

describe('PaymentMethods List view', () => {
  it('should render a valid Payment method list', () => {
    const paymentMethods = [
      {
        id: '1',
        cardNumber: '1234567891234567',
        ccv: '123',
        description: 'Test card',
        isDefault: true,
        owner: 'Test user',
        validUntil: '01/19',
        created: 123456789,
        lastModified: 123456789,
      },
      {
        id: '2',
        cardNumber: '9876543219876543',
        ccv: '321',
        isDefault: false,
        owner: 'Test user',
        validUntil: '01/19',
        created: 123456789,
        lastModified: 123456789,
      },
    ];

    const component = shallow(<View paymentMethods={paymentMethods} />);
    expect(component).toMatchSnapshot();
  });

  it('should render an empty Payment method list', () => {
    const component = shallow(<View paymentMethods={[]} />);
    expect(component).toMatchSnapshot();
  });
});
