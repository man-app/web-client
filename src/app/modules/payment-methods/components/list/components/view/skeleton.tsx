import React from 'react';
import styled from 'styled-components';

import { Skeleton, BarWrapper, Bar, Avatar } from '../../../../../../components/skeletons';
import { Wrapper, CardShape, Header, Nick, CardNumber, Body, CardOwner } from '../../../payment-card/styles';
import getColor from '../../../../../../../common/constants/styles/theme';
import { useThemeContext } from '../../../../../../contexts/theme';
import { ThemedComponent } from '../../../../../../types/styled-components';

const StyledWrapper = styled(Wrapper)`
  background: ${({ theme }: ThemedComponent) => getColor('SKELETON_BACKGROUND', theme.active)};
`;
StyledWrapper.displayName = 'StyledWrapper';

const StyledBar = styled(Bar)`
  :after {
    background: ${({ theme }: ThemedComponent) => getColor('CARD_BACKGROUND', theme.active)};
  }
`;
StyledBar.displayName = 'StyledBar';

const AvatarWrapper = styled.div`
  display: inline-block;

  span {
    background: ${({ theme }: ThemedComponent) => getColor('CARD_BACKGROUND', theme.active)};
  }
`;
AvatarWrapper.displayName = 'AvatarWrapper';

const CardsSkeleton: React.FC = () => {
  const { active } = useThemeContext();
  return (
    <Skeleton background={getColor('CARD_BACKGROUND', active)}>
      {['', ''].map((card, index) => (
        <StyledWrapper key={`card-skeleton-${index}`} width="50%">
          <CardShape>
            <Header>
              <Nick>
                <BarWrapper>
                  <StyledBar width="50%" />
                </BarWrapper>
              </Nick>

              <AvatarWrapper>
                <Avatar size="sm" />
              </AvatarWrapper>
            </Header>
            <Body>
              <CardNumber>
                <BarWrapper>
                  <StyledBar width="60%" />
                </BarWrapper>
              </CardNumber>
              <CardOwner>
                <BarWrapper>
                  <StyledBar width="40%" />
                  <StyledBar width="90%" />
                </BarWrapper>
              </CardOwner>
            </Body>
          </CardShape>
        </StyledWrapper>
      ))}
    </Skeleton>
  );
};

export default CardsSkeleton;
