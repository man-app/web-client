import React from 'react';

import { CardSection } from '../../../../../../components/card';
import { PageH3 } from '../../../../../../components/page';
import PaymentCard from '../../../payment-card';
import { LinkShape } from '../../../payment-card/styles';
import CardsSkeleton from './skeleton';
import { Card, NewPaymentMethod, Content } from './styles';

import { PAYMENT_METHOD } from '../../../../../../../common/constants/appRoutes';
import { PaymentMethod } from '../../../../types';

interface Props {
  isLoading?: boolean;
  paymentMethods: PaymentMethod[];
}

const View: React.FC<Props> = ({ isLoading, paymentMethods }: Props) => (
  <Card>
    <PageH3>Your payment methods</PageH3>

    <CardSection>
      {isLoading && !paymentMethods.length && <CardsSkeleton />}

      {(!isLoading || !!paymentMethods.length) && (
        <>
          {paymentMethods.map(pm => (
            <PaymentCard key={pm.id} link obfuscateNumber paymentMethod={pm} />
          ))}

          <NewPaymentMethod width="50%">
            <LinkShape to={PAYMENT_METHOD.replace(':id', 'new')}>
              <Content>
                <i className="fa fa-plus"></i>
                <span>Add payment method</span>
              </Content>
            </LinkShape>
          </NewPaymentMethod>
        </>
      )}
    </CardSection>
  </Card>
);

export default View;
