import styled from 'styled-components';

import { Card as DefaultCard } from '../../../../../../components/card';

import getColor from '../../../../../../../common/constants/styles/theme';
import { Wrapper } from '../../../payment-card/styles';
import { TABLET } from '../../../../../../../common/constants/styles/media-queries';
import { ThemedComponent } from '../../../../../../types/styled-components';

export const Card = styled(DefaultCard)`
  margin: 5px 0;
  width: 100%;

  ${TABLET} {
    margin: 10px 0;
    width: 100%;
  }
`;

export const NewPaymentMethod = styled(Wrapper)`
  background-color: ${({ theme }: ThemedComponent) => getColor('PAYMENT_BACKGROUND_NEW', theme.active)};
  border: 2px dashed ${({ theme }: ThemedComponent) => getColor('PAYMENT_BORDER_NEW', theme.active)};
  color: ${({ theme }: ThemedComponent) => getColor('PAYMENT_COLOR_NEW', theme.active)};
  transition: background-color 0.2s, border 0.2s, color 0.2s;

  &:hover {
    border-color: ${({ theme }: ThemedComponent) => getColor('PAYMENT_BORDER_NEW_HOVER', theme.active)};
    color: ${({ theme }: ThemedComponent) => getColor('PAYMENT_COLOR_NEW_HOVER', theme.active)};
  }
`;
NewPaymentMethod.displayName = 'NewPaymentMethod';

export const Content = styled.span`
  bottom: 0;
  display: flex;
  flex-direction: column;
  font-size: 24px;
  justify-content: center;
  left: 0;
  position: absolute;
  right: 0;
  top: 0;

  & > i {
    font-size: 32px;
  }

  & > * {
    margin: 10px auto;
  }
`;
Content.displayName = 'Content';
