import { LOGOUT_SUCCEED } from '../../auth/actionTypes';
import { LogoutAction } from '../../auth/types';
import { getState } from '../models';
import { PaymentMethodAction, PaymentMethod } from '../types';
import {
  PAYMENT_METHODS_REQUESTED,
  PAYMENT_METHODS_NOT_UPDATED,
  PAYMENT_METHODS_UPDATED,
  PAYMENT_METHOD_FORM_SUCCEED,
  SELECT_PAYMENT_METHOD,
  PAYMENT_METHOD_FORM_SUBMITTED,
  PAYMENT_METHOD_FORM_FAILED,
  DELETE_PAYMENT_METHOD_SUCCEED,
  DELETE_PAYMENT_METHOD_SUBMITTED,
  DELETE_PAYMENT_METHOD_FAILED,
} from '../actionTypes';
import { updateCollection } from '../../../utils/collections';

const initialState = getState();

const reducer = (state = initialState, action: PaymentMethodAction | LogoutAction) => {
  let paymentMethodFromServer: PaymentMethod;

  switch (action.type) {
    case PAYMENT_METHODS_REQUESTED:
    case PAYMENT_METHOD_FORM_SUBMITTED:
    case DELETE_PAYMENT_METHOD_SUBMITTED:
      return {
        ...state,
        isLoading: true,
      };

    case PAYMENT_METHODS_NOT_UPDATED:
    case PAYMENT_METHOD_FORM_FAILED:
    case DELETE_PAYMENT_METHOD_FAILED:
      return {
        ...state,
        isLoading: false,
      };

    case PAYMENT_METHODS_UPDATED:
    case PAYMENT_METHOD_FORM_SUCCEED:
      const { paymentMethods: paymentMethodsFromServer } = action.payload;
      return {
        ...state,
        collection: updateCollection(state.collection, paymentMethodsFromServer as PaymentMethod[]),
        isLoading: false,
      };

    case DELETE_PAYMENT_METHOD_SUCCEED:
      paymentMethodFromServer = action.payload.paymentMethod;
      return {
        ...state,
        collection: state.collection.filter(pm => pm.id !== paymentMethodFromServer.id),
      };

    case SELECT_PAYMENT_METHOD:
      paymentMethodFromServer = action.payload;
      return {
        ...state,
        selected: paymentMethodFromServer.id,
      };

    case LOGOUT_SUCCEED:
      return {
        ...initialState,
      };

    default:
      return state;
  }
};

export default reducer;
