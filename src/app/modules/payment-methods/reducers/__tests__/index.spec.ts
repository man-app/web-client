import setupEnv from '../../../../../common/utils/setup-test-env';

import reducer from '..';

import {
  PAYMENT_METHODS_REQUESTED,
  PAYMENT_METHODS_UPDATED,
  PAYMENT_METHODS_NOT_UPDATED,
  SELECT_PAYMENT_METHOD,
  DELETE_PAYMENT_METHOD_SUCCEED,
} from '../../actionTypes';
import { LOGOUT_SUCCEED } from '../../../auth/actionTypes';

import { getState } from '../../models';
import { PaymentMethod } from '../../types';

const paymentMethod1: PaymentMethod = {
  id: '1',
  cardNumber: '1234567891234567',
  owner: 'Test user',
  ccv: '123',
  isDefault: false,
  validUntil: '01/19',
  description: 'Sample card',
  created: 123456789,
  lastModified: 123456789,
};

const paymentMethod2: PaymentMethod = {
  id: '2',
  cardNumber: '9876543219876543',
  owner: 'Test user',
  ccv: '123',
  isDefault: true,
  validUntil: '01/19',
  created: 123456789,
  lastModified: 123456789,
};

beforeEach(() => {
  setupEnv();
});

describe('PaymentMethods reducer', () => {
  const defaultState = getState();

  describe('PaymentMethod actions', () => {
    describe('Fetch payment methods', () => {
      it('should mark reducer as isLoading', () => {
        const expectedState = {
          ...defaultState,
          isLoading: true,
        };

        const nextState = reducer(defaultState, { type: PAYMENT_METHODS_REQUESTED });

        expect(nextState).toStrictEqual(expectedState);
      });

      it('should store payment methods', () => {
        const initialState = {
          ...defaultState,
          isLoading: true,
        };

        const expectedState = {
          ...initialState,
          collection: [paymentMethod1],
          isLoading: false,
        };

        const nextState = reducer(initialState, {
          type: PAYMENT_METHODS_UPDATED,
          payload: { paymentMethods: [paymentMethod1] },
        });

        expect(nextState).toStrictEqual(expectedState);
      });

      it('should update payment methods', () => {
        const initialState = {
          ...defaultState,
          collection: [paymentMethod1],
          isLoading: true,
        };

        const expectedState = {
          ...defaultState,
          collection: [paymentMethod1, paymentMethod2],
        };

        const nextState = reducer(initialState, {
          type: PAYMENT_METHODS_UPDATED,
          payload: { paymentMethods: [paymentMethod2] },
        });

        expect(nextState).toStrictEqual(expectedState);
      });

      it('should mark reducer as not fetching', () => {
        const initialState = {
          ...defaultState,
          isLoading: true,
        };

        const nextState = reducer(initialState, { type: PAYMENT_METHODS_NOT_UPDATED });

        expect(nextState).toStrictEqual(defaultState);
      });

      it('should remove the second payment method', () => {
        const initialState = {
          ...defaultState,
          collection: [paymentMethod1, paymentMethod2],
        };

        const expectedState = {
          ...defaultState,
          collection: [paymentMethod1],
        };

        const nextState = reducer(initialState, {
          type: DELETE_PAYMENT_METHOD_SUCCEED,
          payload: { paymentMethod: { id: '2' } },
        });

        expect(nextState).toStrictEqual(expectedState);
      });
    });

    describe('select payment method', () => {
      it('should return the selected payment method', () => {
        const initialState = {
          ...defaultState,
          collection: [paymentMethod1],
        };

        const expectedState = {
          ...initialState,
          selected: '1',
        };

        const nextState = reducer(initialState, { type: SELECT_PAYMENT_METHOD, payload: { id: '1' } });

        expect(nextState).toStrictEqual(expectedState);
      });
    });
  });

  describe('Logout action', () => {
    it('should return initial state', () => {
      const initialState = {
        ...defaultState,
        collection: [paymentMethod1],
      };

      const expectedState = {
        ...defaultState,
      };

      const nextState = reducer(initialState, { type: LOGOUT_SUCCEED });

      expect(nextState).toStrictEqual(expectedState);
    });
  });
});
