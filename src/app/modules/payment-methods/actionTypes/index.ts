import { PaymentMethodActionTypes } from '../types';

export const PAYMENT_METHODS_REQUESTED: PaymentMethodActionTypes = 'PAYMENT_METHODS_REQUESTED';
export const PAYMENT_METHODS_UPDATED: PaymentMethodActionTypes = 'PAYMENT_METHODS_UPDATED';
export const PAYMENT_METHODS_NOT_UPDATED: PaymentMethodActionTypes = 'PAYMENT_METHODS_NOT_UPDATED';

export const PAYMENT_METHOD_FORM_SUBMITTED: PaymentMethodActionTypes = 'PAYMENT_METHOD_FORM_SUBMITTED';
export const PAYMENT_METHOD_FORM_SUCCEED: PaymentMethodActionTypes = 'PAYMENT_METHOD_FORM_SUCCEED';
export const PAYMENT_METHOD_FORM_FAILED: PaymentMethodActionTypes = 'PAYMENT_METHOD_FORM_FAILED';

export const DELETE_PAYMENT_METHOD_SUBMITTED: PaymentMethodActionTypes = 'DELETE_PAYMENT_METHOD_SUBMITTED';
export const DELETE_PAYMENT_METHOD_SUCCEED: PaymentMethodActionTypes = 'DELETE_PAYMENT_METHOD_SUCCEED';
export const DELETE_PAYMENT_METHOD_FAILED: PaymentMethodActionTypes = 'DELETE_PAYMENT_METHOD_FAILED';

export const SELECT_PAYMENT_METHOD: PaymentMethodActionTypes = 'SELECT_PAYMENT_METHOD';
