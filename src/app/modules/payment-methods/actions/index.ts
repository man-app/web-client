import manappApi, { PaymentMethodFromServer } from '../../../../common/apis/manapp';
import { isProduction } from '../../../../common/utils/platforms';

import {
  PAYMENT_METHODS_REQUESTED,
  PAYMENT_METHODS_UPDATED,
  PAYMENT_METHODS_NOT_UPDATED,
  SELECT_PAYMENT_METHOD,
  PAYMENT_METHOD_FORM_SUBMITTED,
  PAYMENT_METHOD_FORM_SUCCEED,
  PAYMENT_METHOD_FORM_FAILED,
  DELETE_PAYMENT_METHOD_SUBMITTED,
  DELETE_PAYMENT_METHOD_SUCCEED,
  DELETE_PAYMENT_METHOD_FAILED,
} from '../actionTypes';

import { createPaymentMethodsFromServer } from '../models';
import { PaymentMethodActionCreator, PaymentMethod } from '../types';

export const getPaymentMethods: PaymentMethodActionCreator = () => dispatch => {
  dispatch({
    type: PAYMENT_METHODS_REQUESTED,
  });

  return manappApi.paymentMethods
    .requestPaymentMethods()
    .then(({ paymentMethods = [] }) => {
      const payload = {
        paymentMethods: createPaymentMethodsFromServer(paymentMethods),
      };

      dispatch({
        type: PAYMENT_METHODS_UPDATED,
        payload,
      });

      return payload;
    })
    .catch(err => {
      dispatch({
        type: PAYMENT_METHODS_NOT_UPDATED,
        payload: {
          err,
        },
      });

      return {
        err,
      };
    });
};

interface CreatePaymentMethodParams {
  form: string;
  params: PaymentMethod;
}

export const createPaymentMethod: PaymentMethodActionCreator = ({
  form,
  params,
}: CreatePaymentMethodParams) => dispatch => {
  dispatch({
    type: PAYMENT_METHOD_FORM_SUBMITTED,
    payload: {
      form,
    },
  });

  const response = !isProduction() ? [{ ...params, id: new Date().getTime() } as PaymentMethodFromServer] : [];

  return manappApi.paymentMethods
    .createPaymentMethod(params)
    .then(({ notifications = [], paymentMethods = response }) => {
      const payload = {
        form,
        notifications,
        paymentMethods: createPaymentMethodsFromServer(paymentMethods),
      };

      dispatch({
        type: PAYMENT_METHOD_FORM_SUCCEED,
        payload,
      });

      return payload;
    })
    .catch(err => {
      dispatch({
        type: PAYMENT_METHOD_FORM_FAILED,
        payload: {
          form,
          ...err,
        },
      });
    });
};

interface UpdatePaymentMethodParams {
  form: string;
  params: PaymentMethod;
}

export const updatePaymentMethod: PaymentMethodActionCreator = ({
  form,
  params,
}: UpdatePaymentMethodParams) => dispatch => {
  dispatch({
    type: PAYMENT_METHOD_FORM_SUBMITTED,
    payload: {
      form,
    },
  });

  return manappApi.paymentMethods
    .updatePaymentMethod(params)
    .then(({ notifications, paymentMethods }) => {
      const payload = {
        form,
        notifications,
        paymentMethods: createPaymentMethodsFromServer(paymentMethods),
      };

      dispatch({
        type: PAYMENT_METHOD_FORM_SUCCEED,
        payload,
      });

      return payload;
    })
    .catch(err => {
      dispatch({
        type: PAYMENT_METHOD_FORM_FAILED,
        payload: {
          form,
          ...err,
        },
      });
    });
};

export const deletePaymentMethod: PaymentMethodActionCreator = (id: string) => dispatch => {
  dispatch({
    type: DELETE_PAYMENT_METHOD_SUBMITTED,
  });

  return manappApi.paymentMethods
    .deletePaymentMethod(id)
    .then(({ notifications }) => {
      const payload = {
        notifications,
        paymentMethod: { id },
      };

      dispatch({
        type: DELETE_PAYMENT_METHOD_SUCCEED,
        payload,
      });

      return payload;
    })
    .catch(err => {
      dispatch({
        type: DELETE_PAYMENT_METHOD_FAILED,
        payload: {
          ...err,
        },
      });

      return {
        ...err,
      };
    });
};

export const selectPaymentMethod: PaymentMethodActionCreator = (id: string) => dispatch => {
  dispatch({
    type: SELECT_PAYMENT_METHOD,
    payload: {
      id,
    },
  });
};
