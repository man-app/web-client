import { RootState } from '../../../types';
import { sortBy } from '../../../utils/arrays';

export const getPaymentMethods = ({ paymentMethods }: RootState) =>
  paymentMethods.collection.map(p => p).sort(sortBy('id', 'asc'));

export const isLoadingPaymentMethods = ({ paymentMethods }: RootState) => paymentMethods.isLoading;

export const getSelectedPaymentMethod = ({ paymentMethods }: RootState) =>
  paymentMethods.collection.find(pm => pm.id === paymentMethods.selected);
