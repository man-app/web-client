import rootReducer from '../../../../reducers';

import { getPaymentMethods, getSelectedPaymentMethod, isLoadingPaymentMethods } from '..';
import { PaymentMethod } from '../../types';

const paymentMethod1: PaymentMethod = {
  id: '1',
  cardNumber: '1234567891234567',
  owner: 'Test user',
  ccv: '123',
  isDefault: false,
  validUntil: '01/19',
  description: 'Sample card',
  created: 123456789,
  lastModified: 123456789,
};

const paymentMethod2: PaymentMethod = {
  id: '2',
  cardNumber: '9876543219876543',
  owner: 'Test user',
  ccv: '123',
  isDefault: true,
  validUntil: '01/19',
  created: 123456789,
  lastModified: 123456789,
};

describe('PaymentMethods selectors', () => {
  const rootState = rootReducer(undefined, { type: 'INIT' });
  const populatedState = {
    ...rootState,
    paymentMethods: {
      ...rootState.paymentMethods,
      collection: [paymentMethod1, paymentMethod2],
    },
  };

  describe('getPaymentMethods', () => {
    it('should return collection', () => {
      const output = getPaymentMethods(populatedState);
      const expectedOutput = [paymentMethod1, paymentMethod2];

      expect(output).toStrictEqual(expectedOutput);
    });
  });

  describe('getSelectedPaymentMethod', () => {
    it('should return undefined', () => {
      const output = getSelectedPaymentMethod(populatedState);

      expect(output).toBeUndefined();
    });

    it('should return a payment method', () => {
      const output = getSelectedPaymentMethod({
        ...populatedState,
        paymentMethods: { ...populatedState.paymentMethods, selected: '2' },
      });
      const expectedOutput = paymentMethod2;

      expect(output).toStrictEqual(expectedOutput);
    });
  });

  describe('isLoadingPaymentMethods', () => {
    it('should return false', () => {
      const output = isLoadingPaymentMethods(populatedState);
      expect(output).toBeFalsy();
    });
  });
});
