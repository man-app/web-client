import React from 'react';
import { useSelector } from 'react-redux';
import { Switch } from 'react-router';

import { isLogged } from '../auth/selectors';

import RouteAuth from '../../components/routes/auth';
import PaymentMethodList from './components/list';
import PaymentMethod from './components/details';

import { PAYMENT_METHODS, PAYMENT_METHOD } from '../../../common/constants/appRoutes';
import { payments } from '../../../common/constants/features';

const PaymentMethods: React.FC = () => {
  const isLoggedUser = useSelector(isLogged) && payments;

  return (
    <Switch>
      <RouteAuth path={PAYMENT_METHOD} isLoggedUser={isLoggedUser} component={PaymentMethod} />
      <RouteAuth path={PAYMENT_METHODS} isLoggedUser={isLoggedUser} component={PaymentMethodList} />
    </Switch>
  );
};

export default PaymentMethods;
