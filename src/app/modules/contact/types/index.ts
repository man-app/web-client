import { Dispatch } from 'redux';

export type ContactActionType = 'CONTACT_FORM_SUBMITTED' | 'CONTACT_FORM_SUCCEED' | 'CONTACT_FORM_FAILED';

export interface ContactAction {
  type: ContactActionType;
  payload?: any;
}

export type ContactActionCreator = (payload?: any) => (dispatch: Dispatch<ContactAction>) => any;

export interface ContactState {
  isLoading: boolean;
}

export interface Contact {
  fullName: string;
  email: string;
  subject: string;
  message: string;
}
