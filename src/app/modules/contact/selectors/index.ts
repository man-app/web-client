import { RootState } from '../../../types';

export const isLoadingContact = ({ contact }: RootState) => contact.isLoading;
