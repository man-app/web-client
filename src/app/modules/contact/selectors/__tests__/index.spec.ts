import { isLoadingContact } from '..';
import rootReducer from '../../../../reducers';

describe('Selectors', () => {
  describe('isLoadingContact', () => {
    it('should return false', () => {
      const initialState = rootReducer(undefined, { type: 'INIT' });

      const output = isLoadingContact(initialState);

      expect(output).toBe(false);
    });
  });
});
