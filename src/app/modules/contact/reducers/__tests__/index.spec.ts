import reducer from '..';

import { CONTACT_FORM_SUBMITTED, CONTACT_FORM_SUCCEED } from '../../actionTypes';
import { LOGOUT_SUCCEED } from '../../../auth/actionTypes';

describe('Contact reducer', () => {
  describe('Form submission', () => {
    it('should mark reducer as isLoading', () => {
      const initialState = {
        isLoading: false,
      };

      const expectedState = {
        isLoading: true,
      };

      const nextState = reducer(initialState, { type: CONTACT_FORM_SUBMITTED });

      expect(nextState).toStrictEqual(expectedState);
    });

    it('should mark reducer as no fetching', () => {
      const initialState = {
        isLoading: true,
      };

      const expectedState = {
        isLoading: false,
      };

      const nextState = reducer(initialState, { type: CONTACT_FORM_SUCCEED });

      expect(nextState).toStrictEqual(expectedState);
    });
  });

  describe('Logout action', () => {
    it('should return initial state', () => {
      const initialState = {
        isLoading: false,
      };

      const nextState = reducer(initialState, { type: LOGOUT_SUCCEED });

      expect(nextState).toStrictEqual(initialState);
    });
  });
});
