import { CONTACT_FORM_SUBMITTED, CONTACT_FORM_SUCCEED, CONTACT_FORM_FAILED } from '../actionTypes';

import { LOGOUT_SUCCEED } from '../../auth/actionTypes';

import { LogoutAction } from '../../auth/types';
import { getState } from '../models';
import { ContactAction } from '../types';

const initialState = getState();

const reducer = (state = initialState, action: ContactAction | LogoutAction) => {
  switch (action.type) {
    case CONTACT_FORM_SUBMITTED:
      return {
        ...state,
        isLoading: true,
      };

    case CONTACT_FORM_SUCCEED:
    case CONTACT_FORM_FAILED:
      return {
        ...state,
        isLoading: false,
      };

    case LOGOUT_SUCCEED:
      return {
        ...initialState,
      };

    default:
      return state;
  }
};

export default reducer;
