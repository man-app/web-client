import React from 'react';

import { Page } from '../../../../components/page';
import ContactForm from '../form';

import { MAX_WIDTH_SM } from '../../../../../common/constants/styles/sizes';
import { Card } from '../../../../components/card';

const View: React.FC = () => (
  <Page maxWidth={MAX_WIDTH_SM}>
    <Card>
      <ContactForm />
    </Card>
  </Page>
);

export default View;
