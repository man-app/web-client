import React from 'react';
import { shallow } from 'enzyme';

import ContactForm from '..';

import { FieldProps } from '../../../../../../../components/forms/types';
import { ButtonProps } from '../../../../../../../components/touchable/types';

describe('Contact Form', () => {
  it('should render a valid form', () => {
    const buttons = [] as ButtonProps[];
    const fields = [] as FieldProps[];
    const form = 'test';
    const handleSubmit = () => {};
    const isLoading = false;

    const component = shallow(
      <ContactForm buttons={buttons} fields={fields} form={form} handleSubmit={handleSubmit} isLoading={isLoading} />
    );
    expect(component).toMatchSnapshot();
  });
});
