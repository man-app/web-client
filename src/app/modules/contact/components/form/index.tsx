import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Redirect } from 'react-router-dom';
import useReactRouter from 'use-react-router';
import { mapServerErrors, isValidForm } from '../../../../utils/error-handler';

import { sendContact } from '../../actions';
import { getFormErrors } from '../../../../components/forms/selectors';
import { getLoggedUser } from '../../../auth/selectors';
import { isLoadingContact } from '../../selectors';

import View from './components/view';

import { HOME } from '../../../../../common/constants/appRoutes';
import { SEND_ICON, SEND_LABEL, GOBACK_ICON, GOBACK_LABEL } from '../../../../../common/constants/buttons';
import { BUTTON, SUBMIT, TEXT, TEXTAREA } from '../../../../components/forms/models';
import { FieldProps } from '../../../../components/forms/types';
import { ButtonProps } from '../../../../components/touchable/types';

const form = 'contact';

const ContactForm = () => {
  const [redirectTo, setRedirectTo] = React.useState('' as any);

  const { history } = useReactRouter();
  const dispatch = useDispatch();

  const errors = useSelector(getFormErrors)(form);
  const user = useSelector(getLoggedUser);
  const isLoading = useSelector(isLoadingContact);

  const [fields, setFields] = React.useState([
    {
      type: TEXT,
      id: 'fullName',
      label: 'Your name',
      model: user ? user.fullName : '',
      isRequired: true,
    },
    {
      type: TEXT,
      id: 'email',
      label: 'Your email',
      model: user ? user.email : '',
      isRequired: true,
    },
    {
      type: TEXT,
      id: 'subject',
      label: 'Subject',
      isRequired: true,
    },
    {
      type: TEXTAREA,
      id: 'message',
      label: 'Your message',
      isRequired: true,
    },
  ] as FieldProps[]);

  React.useEffect(() => {
    if (errors && errors.length) {
      setFields(mapServerErrors(fields, errors));
    }
  }, [errors]);

  if (redirectTo) {
    return <Redirect to={redirectTo} />;
  }

  const handleGoBack = () => {
    history.goBack();
  };

  const handleSubmit = (fields: FieldProps[]) => {
    if (isValidForm(fields)) {
      const email = fields.find(f => f.id === 'email');
      const fullName = fields.find(f => f.id === 'fullName');
      const subject = fields.find(f => f.id === 'subject');
      const message = fields.find(f => f.id === 'message');

      const data = {
        form,
        fullName: fullName ? fullName.model : '',
        email: email ? email.model : '',
        subject: subject ? subject.model : '',
        message: message ? message.model : '',
      };

      // @ts-ignore
      dispatch(sendContact(data)).then(({ errors }: { errors: any[] }) => {
        if (!errors || !errors.length) {
          setRedirectTo({
            pathname: HOME,
            state: { from: window.location.pathname },
          });
        }
      });
    }
  };

  const buttons: ButtonProps[] = [
    {
      buttonType: 'primary',
      icon: SEND_ICON,
      id: 'send',
      label: SEND_LABEL,
      type: SUBMIT,
    },
    {
      icon: GOBACK_ICON,
      id: 'goback',
      isAlwaysEnabled: true,
      label: GOBACK_LABEL,
      onClick: handleGoBack,
      type: BUTTON,
    },
  ];

  return <View buttons={buttons} fields={fields} form={form} handleSubmit={handleSubmit} isLoading={isLoading} />;
};

export default ContactForm;
