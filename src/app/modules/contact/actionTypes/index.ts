import { ContactActionType } from '../types';

export const CONTACT_FORM_SUBMITTED: ContactActionType = 'CONTACT_FORM_SUBMITTED';
export const CONTACT_FORM_SUCCEED: ContactActionType = 'CONTACT_FORM_SUCCEED';
export const CONTACT_FORM_FAILED: ContactActionType = 'CONTACT_FORM_FAILED';
