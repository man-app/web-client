import manappApi from '../../../../common/apis/manapp';

import { CONTACT_FORM_SUBMITTED, CONTACT_FORM_SUCCEED, CONTACT_FORM_FAILED } from '../actionTypes';
import { ContactActionCreator } from '../types';

export const sendContact: ContactActionCreator = payload => dispatch => {
  const { form, ...params } = payload;
  dispatch({
    type: CONTACT_FORM_SUBMITTED,
    payload: {
      form,
    },
  });

  return manappApi.contact
    .send(params)
    .then(({ notifications }) => {
      const payload = {
        form,
        notifications,
      };

      dispatch({
        type: CONTACT_FORM_SUCCEED,
        payload,
      });

      return payload;
    })
    .catch(err => {
      dispatch({
        type: CONTACT_FORM_FAILED,
        payload: {
          form,
          ...err,
        },
      });

      return err;
    });
};
