import { RootState } from '../../../types';

export const getNotifications = ({ notifier }: RootState) => [...notifier.collection, ...notifier.errors];
