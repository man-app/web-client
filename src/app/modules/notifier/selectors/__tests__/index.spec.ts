import rootReducer from '../../../../reducers';
import { NotificationParams } from '../../../../../definitions/notification';

import * as selectors from '..';

const notification1: NotificationParams = {
  title: 'Test 1',
  timestamp: 12345,
  options: {
    badge: '',
    body: 'Notification test',
    tag: '1',
    actions: [],
    icon: undefined,
    image: undefined,
    renotify: true,
    vibrate: [300, 100, 300],
    data: {
      isActive: true,
      reason: undefined,
      type: undefined,
    },
  },
};

const notification2: NotificationParams = {
  title: 'Test 2',
  timestamp: new Date().getTime(),
  options: {
    badge: './test-badge.png',
    body: 'Notification test',
    renotify: false,
    tag: '2',
    data: {},
  },
};

const notificationError: NotificationParams = {
  title: 'Error',
  timestamp: new Date().getTime(),
  options: {
    body: 'Notification error',
    renotify: true,
    tag: '3',
    data: {
      isActive: true,
      reason: undefined,
      type: undefined,
    },
    actions: [],
    badge: '',
    icon: undefined,
    image: undefined,
    vibrate: [300, 100, 300],
  },
};

const initialState = rootReducer(undefined, { type: 'INIT' });
const populatedState = {
  ...initialState,
  notifier: {
    ...initialState.notifier,
    collection: [notification1, notification2],
    errors: [notificationError],
  },
};

describe('Notifier selectors', () => {
  describe('getNotifications', () => {
    it('should return all notifications and errors', () => {
      const notifications = selectors.getNotifications(populatedState);

      expect(notifications).toStrictEqual([...populatedState.notifier.collection, ...populatedState.notifier.errors]);
    });
  });
});
