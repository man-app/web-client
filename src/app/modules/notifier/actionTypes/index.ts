import { NotifierActionType } from '../types';

export const NOTIFIER_NOTIFICATION_DISPLAYED: NotifierActionType = 'NOTIFIER_NOTIFICATION_DISPLAYED';
export const NOTIFIER_NOTIFICATION_REMOVED: NotifierActionType = 'NOTIFIER_NOTIFICATION_REMOVED';
