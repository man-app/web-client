import React from 'react';
import { ReactNotification } from '../../../../../definitions/notification';

import { Buttons } from '../../../../components/touchable';
import { notConnectionError } from '../../utils';
import { Wrapper, Notification, Text, ButtonsWrapper } from './styles';

interface Props {
  children?: JSX.Element;
  notifications: ReactNotification[];
}

const View = ({ children, notifications }: Props) => (
  <Wrapper>
    {notifications.filter(notConnectionError).map(notification => (
      <Notification
        key={notification.options.tag}
        id={`notification-${notification.options.tag}`}
        isActive={notification.options.data.isActive}
        type={notification.options.data.type}
      >
        <>
          <Text>{notification.options.body}</Text>

          {notification.options.actions && (
            <ButtonsWrapper>
              <Buttons options={notification.options.actions.map(b => ({ ...b, isTransparent: true }))} />
            </ButtonsWrapper>
          )}
        </>
      </Notification>
    ))}
    {children}
  </Wrapper>
);

export default View;
