import styled from 'styled-components';

import Message from '../../../../components/message';
import { TABLET } from '../../../../../common/constants/styles/media-queries';
import getLayerPosition from '../../../../../common/constants/styles/z-indexes';
import { MessageButtonsWrapper } from '../../../../components/message/styles';

export const Wrapper = styled.div`
  bottom: 0;
  display: block;
  position: fixed;
  right: 0;
  width: 100%;
  z-index: ${getLayerPosition('NOTIFICATION')};

  ${TABLET} {
    bottom: 20px;
    max-width: 400px;
    right: 20px;
  }
`;
Wrapper.displayName = 'Wrapper';

interface NotificationProps {
  id: string;
  isActive?: boolean;
}

export const Notification = styled(Message)`
  border-width: ${({ isActive }: NotificationProps) => (isActive ? '1px' : '0px')};
  display: inline-block;
  margin: ${({ isActive }: NotificationProps) => (isActive ? '2.5px 5px' : '0px')};
  overflow: hidden;
  padding: ${({ isActive }: NotificationProps) => (isActive ? '10px' : '0px')};
  position: relative;
  right: ${({ isActive }: NotificationProps) => (isActive ? '0px' : '-100px')};
  transition: right 0.3s 0s, border 0.3s 0.3s, margint 0.3s 0.3s, padding 0.3s 0.3s;
`;
Notification.displayName = 'Notification';

export const Text = styled.p``;
Text.displayName = 'Text';

export const ButtonsWrapper = styled(MessageButtonsWrapper)`
  & > div {
    margin: 0px;
    width: 100%;
  }
`;
ButtonsWrapper.displayName = 'ButtonsWrapper';
