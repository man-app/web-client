import React from 'react';
import { shallow } from 'enzyme';

import View from '..';
import { ReactNotification } from '../../../../../../definitions/notification';
import { BUTTON } from '../../../../../components/forms/models';

const notification1: ReactNotification = {
  title: 'Test 1',
  timestamp: new Date().getTime(),
  options: {
    badge: './test-badge.png',
    body: 'Notification test',
    renotify: false,
    tag: '1',
    data: {},
  },
};

const notification2: ReactNotification = {
  title: 'Test 2',
  timestamp: new Date().getTime(),
  options: {
    badge: './test-badge.png',
    body: 'Notification test',
    renotify: false,
    tag: '2',
    data: {},
  },
};

const notificationError: ReactNotification = {
  title: 'Error',
  timestamp: new Date().getTime(),
  options: {
    badge: './test-badge.png',
    body: 'Notification error',
    renotify: false,
    tag: '3',
    data: {
      type: 'error',
    },
  },
};

const notificationWarning: ReactNotification = {
  title: 'Warning',
  timestamp: new Date().getTime(),
  options: {
    badge: './test-badge.png',
    body: 'Notification warning',
    renotify: false,
    tag: '4',
    data: {
      type: 'warning',
    },
  },
};

const notificationSuccess: ReactNotification = {
  title: 'Success',
  timestamp: new Date().getTime(),
  options: {
    badge: './test-badge.png',
    body: 'Notification success',
    renotify: false,
    tag: '5',
    data: {
      type: 'success',
    },
  },
};

const notificationWithButtons: ReactNotification = {
  title: 'Buttons',
  timestamp: new Date().getTime(),
  options: {
    badge: './test-badge.png',
    body: 'Notification with buttons',
    renotify: false,
    tag: '6',
    data: {},
    actions: [
      {
        id: 'dismiss',
        label: 'Dismiss',
        type: BUTTON,
      },
    ],
  },
};

describe('Notifier', () => {
  it('should render 1 notification', () => {
    const component = shallow(<View notifications={[notification1]} />);

    expect(component).toMatchSnapshot();
  });

  it('should render 2 notifications', () => {
    const component = shallow(<View notifications={[notification1, notification2]} />);

    expect(component).toMatchSnapshot();
  });

  it('should render a notification with buttons', () => {
    const component = shallow(<View notifications={[notificationWithButtons]} />);

    expect(component).toMatchSnapshot();
  });

  it('should render a success notification', () => {
    const component = shallow(<View notifications={[notificationSuccess]} />);

    expect(component).toMatchSnapshot();
  });

  it('should render a warning notification', () => {
    const component = shallow(<View notifications={[notificationWarning]} />);

    expect(component).toMatchSnapshot();
  });

  it('should render an error notification', () => {
    const component = shallow(<View notifications={[notificationError]} />);

    expect(component).toMatchSnapshot();
  });
});
