import { v1 as uuid } from 'uuid';
import { NotificationParams, ReactNotification } from '../../../../definitions/notification';
import { TagDependencies, RawNotificationParams, NotifierState } from '../types';
import { ButtonProps } from '../../../components/touchable/types';
import { getResourceUrl } from '../../../../common/utils/resources';
import { NAVIGATE_ACTION } from '../../../../common/constants/notifications';

export const createNotificationTag = ({ type, action, company, element }: TagDependencies) =>
  `${type}_${action}_${company.id}_${element}`;

export const createNotificationParams = ({
  title = 'New Activity',
  timestamp = new Date().getTime(),
  tag,
  body = '',
  icon,
  image,
  badge = '',
  vibrate = [300, 100, 300],
  actions = [],
  renotify = true,
  type,
  reason,
  isActive = true,
  data,
}: RawNotificationParams): NotificationParams => ({
  title,
  timestamp,
  options: {
    body,
    icon,
    image,
    badge,
    vibrate,
    actions,
    tag: tag || uuid(),
    renotify,
    data: {
      type,
      reason,
      isActive,
      ...data,
    },
  },
});

export const getState = (): NotifierState => ({
  collection: [],
  errors: [],
  isLoading: false,
});

export const transformNativeActionsIntoReactActions = (
  params: NotificationParams,
  history: any,
  removeNotification: (tags: string[]) => void
): ReactNotification => {
  if (!params.options.actions) {
    const reactNotification: ReactNotification = {
      ...params,
      options: {
        ...params.options,
        data: {
          ...params.options.data,
        },
        actions: [],
      },
    };

    return reactNotification;
  }

  const { actions } = params.options;
  const reactNotification: ReactNotification = {
    ...params,
    options: {
      ...params.options,
      data: {
        ...params.options.data,
      },
      actions: actions.map(
        (action): ButtonProps => ({
          type: 'button',
          id: `${params.options.tag}-${action.action}-button`,
          label: action.label,
          icon: action.icon,
          className: 'Message-button',
          onClick: () => {
            removeNotification([params.options.tag]);

            switch (action.action) {
              case NAVIGATE_ACTION.action:
                if (params.options.data.isExternal) {
                  return window.open(params.options.data.url);
                }

                const newUrl = getResourceUrl({
                  id: params.options.data.idElement || params.options.data.element,
                  type: params.options.data.idModule || params.options.data.type,
                  company: params.options.data.company,
                });

                return history.push(params.options.data.url || newUrl);

              default:
                return;
            }
          },
        })
      ),
    },
  };

  return reactNotification;
};
