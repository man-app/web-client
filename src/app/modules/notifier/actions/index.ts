import { DISMISS_ACTION, NAVIGATE_ACTION } from '../../../../common/constants/notifications';
import { NOTIFIER_NOTIFICATION_DISPLAYED, NOTIFIER_NOTIFICATION_REMOVED } from '../actionTypes';
import { NotifierActionCreator } from '../types';

export const createNotification: NotifierActionCreator = payload => dispatch => {
  dispatch({
    type: NOTIFIER_NOTIFICATION_DISPLAYED,
    payload: {
      notifications: [
        {
          channel: 'web',
          title: payload.title,
          ...payload.options,
          actions:
            payload.options.actions && payload.options.actions.length
              ? [...payload.options.actions, DISMISS_ACTION]
              : [NAVIGATE_ACTION, DISMISS_ACTION],
        },
      ],
    },
  });
};

export const removeNotification: NotifierActionCreator = payload => dispatch => {
  dispatch({
    type: NOTIFIER_NOTIFICATION_REMOVED,
    payload,
  });
};
