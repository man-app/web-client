import { updateCollection } from '../../../utils/collections';

import { LOGOUT_SUCCEED } from '../../auth/actionTypes';
import { NOTIFIER_NOTIFICATION_REMOVED } from '../actionTypes';

import { LogoutAction } from '../../auth/types';
import { createNotificationParams, getState } from '../models';
import { NotifierState, RawNotificationParams, NotifierAction } from '../types';
import { isNotFormError, isConnectionError, isSuccessNotification, shouldPersistOnLogout } from '../utils';

const initialState = getState();

const reducer = (state = initialState, action: NotifierAction | LogoutAction): NotifierState => {
  if (action.type === LOGOUT_SUCCEED) {
    return {
      ...initialState,
      collection: [...state.collection.filter(shouldPersistOnLogout)],
    };
  }

  const { type, payload } = action as NotifierAction;

  if (type === NOTIFIER_NOTIFICATION_REMOVED) {
    return {
      ...state,
      collection: state.collection.filter(({ options }) => options.tag !== payload.tag),
      errors: state.errors.filter(({ options }) => options.tag !== payload.tag),
    };
  }

  const notificationsFromServer: RawNotificationParams[] = (payload && payload.notifications) || [];
  const errorsFromServer: RawNotificationParams[] = (payload && payload.errors) || [];

  const parsedNotifications = notificationsFromServer.filter(isNotFormError).map(createNotificationParams);
  const parsedErrors = errorsFromServer.filter(isNotFormError).map(createNotificationParams);

  return {
    ...state,
    collection: updateCollection(state.collection, parsedNotifications),
    errors: updateCollection(state.errors, parsedErrors).filter(error =>
      isSuccessNotification(type) ? !isConnectionError(error) : true
    ),
  };
};

export default reducer;
