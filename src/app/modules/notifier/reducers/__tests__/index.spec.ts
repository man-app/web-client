import { NotificationParams } from '../../../../../definitions/notification';
import { getState } from '../../models';

import reducer from '..';
import { LOGOUT_SUCCEED } from '../../../auth/actionTypes';
import { NOTIFIER_NOTIFICATION_REMOVED, NOTIFIER_NOTIFICATION_DISPLAYED } from '../../actionTypes';

const initialState = getState();

const notification1: NotificationParams = {
  title: 'Test 1',
  timestamp: 12345,
  options: {
    badge: '',
    body: 'Notification test',
    tag: '1',
    actions: [],
    icon: undefined,
    image: undefined,
    renotify: true,
    vibrate: [300, 100, 300],
    data: {
      isActive: true,
      reason: undefined,
      type: undefined,
    },
  },
};

const notification2: NotificationParams = {
  title: 'Test 2',
  timestamp: new Date().getTime(),
  options: {
    badge: './test-badge.png',
    body: 'Notification test',
    renotify: false,
    tag: '2',
    data: {},
  },
};

const notificationError: NotificationParams = {
  title: 'Error',
  timestamp: new Date().getTime(),
  options: {
    body: 'Notification error',
    renotify: true,
    tag: '3',
    data: {
      isActive: true,
      reason: undefined,
      type: undefined,
    },
    actions: [],
    badge: '',
    icon: undefined,
    image: undefined,
    vibrate: [300, 100, 300],
  },
};

const populatedState = {
  ...initialState,
  collection: [notification1, notification2],
  errors: [notificationError],
};

describe('Notifier reducer', () => {
  describe('Logout action', () => {
    it('should return initial state', () => {
      const nextState = reducer(populatedState, { type: LOGOUT_SUCCEED });

      expect(nextState).toStrictEqual(initialState);
    });
  });

  describe('Remove notification', () => {
    it('should remove first error', () => {
      const nextState = reducer(populatedState, { type: NOTIFIER_NOTIFICATION_REMOVED, payload: { tag: '3' } });

      expect(nextState).toStrictEqual({
        ...populatedState,
        errors: [],
      });
    });

    it('should remove first notification', () => {
      const nextState = reducer(populatedState, { type: NOTIFIER_NOTIFICATION_REMOVED, payload: { tag: '1' } });

      expect(nextState).toStrictEqual({
        ...populatedState,
        collection: [notification2],
      });
    });

    it('should remove second notification', () => {
      const nextState = reducer(populatedState, { type: NOTIFIER_NOTIFICATION_REMOVED, payload: { tag: '2' } });

      expect(nextState).toStrictEqual({
        ...populatedState,
        collection: [notification1],
      });
    });
  });

  describe('store notifications', () => {
    it('should store a notification', () => {
      const expectedState = { ...initialState, collection: [notification1] };

      const nextState = reducer(initialState, {
        type: NOTIFIER_NOTIFICATION_DISPLAYED,
        payload: {
          notifications: [
            {
              title: notification1.title,
              body: notification1.options.body,
              channel: 'web',
              timestamp: notification1.timestamp,
              tag: notification1.options.tag,
            },
          ],
        },
      });

      expect(nextState).toStrictEqual(expectedState);
    });
  });

  describe('store errors', () => {
    it('should store an error', () => {
      const expectedState = { ...initialState, errors: [notificationError] };

      const nextState = reducer(initialState, {
        type: NOTIFIER_NOTIFICATION_DISPLAYED,
        payload: {
          errors: [
            {
              title: notificationError.title,
              body: notificationError.options.body,
              channel: 'web',
              timestamp: notificationError.timestamp,
              tag: notificationError.options.tag,
              type: notificationError.options.data.type,
            },
          ],
        },
      });

      expect(nextState).toStrictEqual(expectedState);
    });
  });
});
