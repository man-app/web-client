import { Dispatch } from 'redux';
import { NotificationAction, NotificationParams } from '../../../../definitions/notification';

export interface TagDependencies {
  [index: string]: any;

  action: string;
  company: {
    id?: string;
    name?: string;
    slug?: string;
    picture?: string;
  };
  element: string;
  type: string;
}

export interface RawNotificationParams {
  title: string;
  timestamp?: number;
  tag: string;

  // Optional
  body?: string;
  icon?: string;
  image?: string;
  badge?: string;
  vibrate?: number[];
  actions?: NotificationAction[];
  renotify?: boolean;

  // Data
  data?: any;
  type?: string;
  reason?: string;
  channel?: string;
  isActive?: boolean;
}

export interface ManappNotification {
  channel: 'form' | 'web' | 'native';
}

export interface NotifierState {
  collection: NotificationParams[];
  errors: NotificationParams[];
  isLoading: boolean;
}

export type NotifierActionType = 'NOTIFIER_NOTIFICATION_DISPLAYED' | 'NOTIFIER_NOTIFICATION_REMOVED';

export interface NotifierAction {
  type: NotifierActionType;
  payload?: any;
}

export type NotifierActionCreator = (payload?: any) => (dispatch: Dispatch<NotifierAction>) => any;
