import React from 'react';
import { connect } from 'react-redux';
import { Redirect, RouteComponentProps, withRouter } from 'react-router-dom';
import { log } from '../../../common/utils/logger';
import { sortBy } from '../../utils/arrays';
import { canDisplayNotifications, showNotification } from '../../utils/notifications';
import { getPushSubscription } from '../../utils/push';
import { shouldBeNotified } from '../../utils/ui';

import { getActivities as getActivitiesAction } from '../activities/actions';
import { getActivities as getActivitiesSelector, isSilentUpdate } from '../activities/selectors';
import { getLoggedUser, getUserNotificationPreferences } from '../auth/selectors';
import { getEmployingCompanies } from '../companies/selectors';
import { createNotification, removeNotification } from './actions';
import { createNotificationTag, createNotificationParams, transformNativeActionsIntoReactActions } from './models';
import { getNotifications } from './selectors';

import NotifierView from './components/view';

import { LOGOUT } from '../../../common/constants/appRoutes';
import { pollingFrequency, notificationCloseTimeout } from '../../../common/constants/features';
import { Activity } from '../activities/types';
import { Company } from '../companies/types';
import { NotificationParams, ReactNotification } from '../../../definitions/notification';
import { UserLogged, NotificationPreference } from '../auth/types';
import { RootState } from '../../types';

const notificationSound = require('../../../assets/sounds/notification.mp3');

interface OwnState {
  goTo?: {
    pathname: string;
    state: any;
  };
}

type RouteProps = RouteComponentProps<{
  history: any;
}>;

interface StateProps {
  loggedUser: UserLogged | undefined;
  logs: Activity[];
  notificationPreferences: NotificationPreference[];
  shouldNotify: boolean;
  companies: Company[];
  notifications: NotificationParams[];
}

interface DispatchProps {
  createNotification: Function;
  removeNotification: Function;
  getActivities: Function;
}

type Props = RouteProps & StateProps & DispatchProps;

class NotifierWrapper extends React.Component<Props, OwnState> {
  displayName = 'NotifierWrapper';

  // @ts-ignore
  polling = undefined;
  sound: React.RefObject<HTMLAudioElement>;

  constructor(props: Props) {
    super(props);

    this.state = {};
    this.sound = React.createRef();
  }

  componentDidMount() {
    const { loggedUser } = this.props;

    if (loggedUser && loggedUser.id) {
      this.startPolling();
    }
  }

  UNSAFE_componentWillReceiveProps(nextProps: Props) {
    const { notifications } = nextProps;

    const authErrors = notifications.filter(n => n.options.data.reason === 'auth');
    if (authErrors.length > 0) {
      this.setState({
        goTo: {
          pathname: LOGOUT,
          state: {},
        },
      });
    }
  }

  componentDidUpdate(prevProps: Props) {
    const {
      loggedUser,
      notifications,
      createNotification,
      notificationPreferences,
      logs,
      shouldNotify,
      companies,
    } = this.props;

    if (!prevProps.loggedUser && loggedUser) {
      this.startPolling();
    }

    if (notifications.length) {
      this.cleanOldNotifications();
    }

    // New notifications
    if (logs.length > prevProps.logs.length && shouldNotify) {
      logs.forEach(newLog => {
        const oldLog = prevProps.logs.find(log => log.id === newLog.id);

        if (!oldLog) {
          const resource = {
            user: newLog.idUser,
            type: newLog.type,
            action: newLog.action,
            company: newLog.idCompany,
            me: loggedUser && loggedUser.id,
          };

          const shouldNotify = shouldBeNotified(resource, notificationPreferences);

          if (shouldNotify) {
            const modifiedCompany = companies.find(c => c.id === newLog.idCompany);
            const company = {
              id: modifiedCompany ? modifiedCompany.id : '0',
              name: modifiedCompany ? modifiedCompany.name : '',
              slug: modifiedCompany ? modifiedCompany.slug : '',
              picture: modifiedCompany ? modifiedCompany.picture : '',
            };

            const element = newLog.type === 'company' ? 'settings' : newLog.elementName;

            const notificationParams = createNotificationParams({
              title: `New activity in ${company.name}`,
              body: `${newLog.userName} ${newLog.action} the ${newLog.type} ${element}`,
              icon: company.picture,
              tag: createNotificationTag({
                type: newLog.type,
                action: newLog.action,
                company,
                element: newLog.elementName,
              }),
              data: { ...newLog, company },
            });

            if (canDisplayNotifications()) {
              showNotification(notificationParams);
            } else {
              createNotification(notificationParams);
              if (this.sound.current) {
                this.sound.current.currentTime = 0;

                const soundPromise = this.sound.current.play();
                if (soundPromise !== undefined) {
                  soundPromise.catch(() => {});
                }
              }
            }
          }
        }
      });
    }
  }

  cleanOldNotifications = () => {
    if (notificationCloseTimeout) {
      const { notifications } = this.props;
      const removableNotifications = notifications.filter(n => !n.options.data.isPermanent);

      // If there are notifications displayed
      if (removableNotifications.length) {
        // Get current time
        const now = new Date().getTime();
        const notifsToClean = removableNotifications
          .filter(n => n.timestamp && now - n.timestamp >= notificationCloseTimeout)
          .map(n => n.options.tag);

        this.removeNotification(notifsToClean);

        // Each 1s, cleanOldNotifs again
        setTimeout(() => {
          this.cleanOldNotifications();
        }, 1000);
      }
    }
  };

  removeNotification = (tags: string[]) => {
    tags.forEach(tag => {
      this.props.removeNotification({ tag });
    });
  };

  startPolling = () => {
    const { getActivities } = this;
    clearInterval(this.polling);

    if (pollingFrequency) {
      getActivities(true);

      // @ts-ignore
      this.polling = setInterval(() => {
        getActivities();
      }, pollingFrequency);
    }
  };

  getActivities = (silentUpdate?: boolean) => {
    const { loggedUser, getActivities, logs } = this.props;

    // If there is an active Push Subscription, stop long polling
    // because updates will come via Push
    if (getPushSubscription()) {
      log('There is an active PushSubscription, stopping long polling');
      // @ts-ignore
      clearInterval(this.polling);
      return;
    }

    if (loggedUser && loggedUser.id) {
      getActivities({
        silentUpdate,
        laterThan: logs && logs.length ? logs[0].created : undefined,
      });
    }
  };

  transformNativeActionsIntoReactActions = (params: NotificationParams): ReactNotification => {
    const { history } = this.props;

    return transformNativeActionsIntoReactActions(params, history, this.removeNotification);
  };

  render() {
    const { notifications } = this.props;
    const { goTo } = this.state;

    if (goTo) {
      return <Redirect to={goTo} />;
    }

    return (
      <NotifierView notifications={notifications.map(this.transformNativeActionsIntoReactActions)}>
        <audio ref={this.sound} src={notificationSound} style={{ display: 'none' }}></audio>
      </NotifierView>
    );
  }
}

const mapStateToProps = (state: RootState): StateProps => {
  const loggedUser = getLoggedUser(state);
  const logs = getActivitiesSelector(state);
  const notificationPreferences = getUserNotificationPreferences(state);
  const shouldNotify = !isSilentUpdate(state);
  const companies = getEmployingCompanies(state)(loggedUser);
  const notifications = getNotifications(state).sort(sortBy('timestamp'));

  return {
    loggedUser,
    logs,
    notificationPreferences,
    shouldNotify,
    companies,
    notifications,
  };
};

const mapDispatchToProps: DispatchProps = {
  createNotification,
  removeNotification,
  getActivities: getActivitiesAction,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(NotifierWrapper));
