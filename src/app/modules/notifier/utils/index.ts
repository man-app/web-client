import { RawNotificationParams } from '../types';
import { NotificationParams, ReactNotification } from '../../../../definitions/notification';

export const isSuccessNotification = (actionType: string) =>
  /SUCCEED/.test(actionType) || (/UPDATED/.test(actionType) && /NOT_UPDATED/.test(actionType) === false);

export const shouldPersistOnLogout = (notification: NotificationParams) =>
  notification.options.data.shouldPersistOnLogout;

export const isNotFormError = (notification: RawNotificationParams) => notification.channel !== 'form';

export const isConnectionError = ({ options }: NotificationParams) =>
  !options.data || !options.data.reason || options.data.reason === 'connection';

export const notConnectionError = (notification: ReactNotification) =>
  notification.options.data.reason !== 'connection';
