import { FeedbackState } from '../types';

export const getState = (): FeedbackState => ({
  isLoading: false,
});
