import { isLoadingFeedback } from '..';
import rootReducer from '../../../../reducers';

describe('Feedback Selectors', () => {
  describe('isLoadingFeedback', () => {
    it('should return false', () => {
      const initialState = rootReducer(undefined, { type: 'INIT' });

      const output = isLoadingFeedback(initialState);

      expect(output).toBe(false);
    });
  });
});
