import { RootState } from '../../../types';

export const isLoadingFeedback = ({ feedback }: RootState) => feedback.isLoading;
