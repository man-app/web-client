import manappApi from '../../../../common/apis/manapp';

import { FEEDBACK_FORM_SUBMITTED, FEEDBACK_FORM_SUCCEED, FEEDBACK_FORM_FAILED } from '../actionTypes';

import { FeedbackActionCreator } from '../types';

export const sendFeedback: FeedbackActionCreator = payload => dispatch => {
  const { form, ...params } = payload;
  dispatch({
    type: FEEDBACK_FORM_SUBMITTED,
    payload: {
      form,
    },
  });

  return manappApi.feedback
    .send(params)
    .then(({ notifications }) => {
      const payload = {
        form,
        notifications,
      };

      dispatch({
        type: FEEDBACK_FORM_SUCCEED,
        payload,
      });

      return payload;
    })
    .catch(err => {
      dispatch({
        type: FEEDBACK_FORM_FAILED,
        payload: {
          form,
          ...err,
        },
      });

      return err;
    });
};
