import { FeedbackActionType } from '../types';

export const FEEDBACK_FORM_SUBMITTED: FeedbackActionType = 'FEEDBACK_FORM_SUBMITTED';
export const FEEDBACK_FORM_SUCCEED: FeedbackActionType = 'FEEDBACK_FORM_SUCCEED';
export const FEEDBACK_FORM_FAILED: FeedbackActionType = 'FEEDBACK_FORM_FAILED';
