import { FEEDBACK_FORM_SUBMITTED, FEEDBACK_FORM_SUCCEED, FEEDBACK_FORM_FAILED } from '../actionTypes';
import { LOGOUT_SUCCEED } from '../../auth/actionTypes';

import { FeedbackAction } from '../types';
import { LogoutAction } from '../../auth/types';
import { getState } from '../models';

const initialState = getState();

const reducer = (state = initialState, action: FeedbackAction | LogoutAction) => {
  switch (action.type) {
    case FEEDBACK_FORM_SUBMITTED:
      return {
        ...state,
        isLoading: true,
      };

    case FEEDBACK_FORM_SUCCEED:
    case FEEDBACK_FORM_FAILED:
      return {
        ...state,
        isLoading: false,
      };

    case LOGOUT_SUCCEED:
      return {
        ...initialState,
      };

    default:
      return state;
  }
};

export default reducer;
