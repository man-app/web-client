import reducer from '..';

import { FEEDBACK_FORM_SUBMITTED, FEEDBACK_FORM_SUCCEED } from '../../actionTypes';
import { LOGOUT_SUCCEED } from '../../../auth/actionTypes';

describe('Feedback reducer', () => {
  describe('Form submission', () => {
    it('should mark reducer as isLoading', () => {
      const initialState = {
        isLoading: false,
      };

      const expectedState = {
        isLoading: true,
      };

      const nextState = reducer(initialState, { type: FEEDBACK_FORM_SUBMITTED });

      expect(nextState).toStrictEqual(expectedState);
    });

    it('should mark reducer as no fetching', () => {
      const initialState = {
        isLoading: true,
      };

      const expectedState = {
        isLoading: false,
      };

      const nextState = reducer(initialState, { type: FEEDBACK_FORM_SUCCEED });

      expect(nextState).toStrictEqual(expectedState);
    });
  });

  describe('Logout action', () => {
    it('should return initial state', () => {
      const initialState = {
        isLoading: false,
      };

      const nextState = reducer(initialState, { type: LOGOUT_SUCCEED });

      expect(nextState).toStrictEqual(initialState);
    });
  });
});
