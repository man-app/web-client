import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Redirect } from 'react-router-dom';
import useReactRouter from 'use-react-router';
import { mapServerErrors, isValidForm } from '../../../../utils/error-handler';

import { getFormErrors } from '../../../../components/forms/selectors';
import { sendFeedback } from '../../actions';
import { isLoadingFeedback } from '../../selectors';

import View from './components/view';

import { HOME } from '../../../../../common/constants/appRoutes';
import { SEND_ICON, SEND_LABEL, GOBACK_ICON, GOBACK_LABEL } from '../../../../../common/constants/buttons';
import { BUTTON, SUBMIT, DROPDOWN, TEXT, TEXTAREA } from '../../../../components/forms/models';
import { FieldProps } from '../../../../components/forms/types';
import { ButtonProps } from '../../../../components/touchable/types';

const form = 'feedback';

const FeedbackForm = () => {
  const [redirectTo, setRedirectTo] = React.useState('' as any);

  const [fields, setFields] = React.useState([
    {
      id: 'type',
      isRequired: true,
      label: 'Report type',
      model: 'feature',
      options: [
        {
          id: '1',
          label: 'New feature',
          value: 'feature',
          type: 'option',
        },
        {
          id: '2',
          label: 'Improve a feature',
          value: 'improve',
          type: 'option',
        },
        {
          id: '3',
          label: 'Bug',
          value: 'bug',
          type: 'option',
        },
      ],
      type: DROPDOWN,
    },
    {
      id: 'title',
      isRequired: true,
      label: 'Title',
      type: TEXT,
    },
    {
      id: 'description',
      isRequired: true,
      label: 'Description',
      type: TEXTAREA,
    },
  ] as FieldProps[]);

  const { history } = useReactRouter();
  const dispatch = useDispatch();

  const errors = useSelector(getFormErrors)(form);
  const isLoading = useSelector(isLoadingFeedback);

  React.useEffect(() => {
    if (errors && errors.length) {
      setFields(mapServerErrors(fields, errors));
    }
  }, [errors]);

  if (redirectTo) {
    return <Redirect to={redirectTo} />;
  }

  const handleGoBack = () => {
    history.goBack();
  };

  const handleSubmit = (fields: FieldProps[]) => {
    if (isValidForm(fields)) {
      const type = fields.find(f => f.id === 'type');
      const title = fields.find(f => f.id === 'title');
      const description = fields.find(f => f.id === 'description');

      const data = {
        form,
        type: type ? type.model : '',
        title: title ? title.model : '',
        description: description ? description.model : '',
      };

      // @ts-ignore
      dispatch(sendFeedback(data)).then(({ errors }: { errors: any[] }) => {
        if (!errors || !errors.length) {
          setRedirectTo({
            pathname: HOME,
            state: { from: window.location.pathname },
          });
        }
      });
    }
  };

  const buttons: ButtonProps[] = [
    {
      buttonType: 'primary',
      icon: SEND_ICON,
      id: 'send',
      label: SEND_LABEL,
      type: SUBMIT,
    },
    {
      icon: GOBACK_ICON,
      id: 'goback',
      isAlwaysEnabled: true,
      label: GOBACK_LABEL,
      onClick: handleGoBack,
      type: BUTTON,
    },
  ];

  return <View buttons={buttons} fields={fields} form={form} handleSubmit={handleSubmit} isLoading={isLoading} />;
};

export default FeedbackForm;
