import React from 'react';

import { Card } from '../../../../../../components/card';
import SmartForm from '../../../../../../components/forms';
import { Link } from '../../../../../../components/touchable';

import { MAILING_POLICY } from '../../../../../../../common/constants/appRoutes';
import { FieldProps } from '../../../../../../components/forms/types';
import { LinkProps, ButtonProps } from '../../../../../../components/touchable/types';

const mailingPolicyLink: LinkProps = {
  id: 'mailing-link',
  label: 'Mailing Policy',
  to: MAILING_POLICY,
};

interface Props {
  buttons: ButtonProps[];
  fields: FieldProps[];
  form: string;
  handleSubmit: (fields: FieldProps[]) => void;
  isLoading: boolean;
}

const View: React.FC<Props> = ({ buttons, fields, form, handleSubmit, isLoading }: Props) => (
  <SmartForm buttons={buttons} fields={fields} form={form} handleSubmit={handleSubmit} isLoading={isLoading}>
    {({ Buttons, controlledButtons, controlledFields, Field }) => (
      <>
        {controlledFields.map(field => (
          <Field key={field.id} options={field} />
        ))}

        <Card>
          <p>
            By submitting this form, you are accepting our <Link options={mailingPolicyLink} />.
          </p>
        </Card>

        <Buttons options={controlledButtons} />
      </>
    )}
  </SmartForm>
);

export default View;
