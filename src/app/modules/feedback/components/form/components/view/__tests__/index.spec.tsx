import React from 'react';
import { shallow } from 'enzyme';

import Feedback from '..';

import { FieldProps } from '../../../../../../../components/forms/types';
import { ButtonProps } from '../../../../../../../components/touchable/types';

describe('Feedback Form', () => {
  it('should render a valid form', () => {
    const buttons = [] as ButtonProps[];
    const fields = [] as FieldProps[];
    const form = 'test';
    const handleSubmit = () => {};
    const isLoading = false;

    const component = shallow(
      <Feedback buttons={buttons} fields={fields} form={form} handleSubmit={handleSubmit} isLoading={isLoading} />
    );
    expect(component).toMatchSnapshot();
  });
});
