import React from 'react';

import { Page } from '../../../../components/page';
import { Link } from '../../../../components/touchable';
import FeedbackForm from '../form';

import { APP_REPOSITORY } from '../../../../../common/constants/branding';
import { MAX_WIDTH_SM } from '../../../../../common/constants/styles/sizes';
import { LinkProps } from '../../../../components/touchable/types';
import { Card } from '../../../../components/card';

const gitlabLink: LinkProps = {
  id: 'gitlab-link',
  to: `${APP_REPOSITORY}/issues`,
  icon: 'gitlab',
  iconLast: true,
  label: 'Gitlab repo',
  isExternal: true,
};

const View = () => (
  <Page maxWidth={MAX_WIDTH_SM}>
    <Card>
      <p>
        If you are a developer, feel free to open an issue in our <Link options={gitlabLink} />.
      </p>
      <p>If you don{"'"}t, you can simply use this form.</p>
    </Card>
    <Card>
      <FeedbackForm />
    </Card>
  </Page>
);

export default View;
