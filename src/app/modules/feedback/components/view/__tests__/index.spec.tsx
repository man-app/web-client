import React from 'react';
import { shallow } from 'enzyme';

import View from '..';

describe('Feedback', () => {
  it('should render a valid screen', () => {
    const component = shallow(<View />);
    expect(component).toMatchSnapshot();
  });
});
