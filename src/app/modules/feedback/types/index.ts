import { Dispatch } from 'redux';

export type FeedbackActionType = 'FEEDBACK_FORM_SUBMITTED' | 'FEEDBACK_FORM_SUCCEED' | 'FEEDBACK_FORM_FAILED';

export interface FeedbackAction {
  type: FeedbackActionType;
  payload?: any;
}

export type FeedbackActionCreator = (payload?: any) => (dispatch: Dispatch<FeedbackAction>) => any;

export interface FeedbackState {
  isLoading: boolean;
}

export interface Feedback {
  type: string;
  title: string;
  description: string;
}
