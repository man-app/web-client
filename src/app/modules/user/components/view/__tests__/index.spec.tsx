import React from 'react';
import { shallow } from 'enzyme';

import View from '..';
import { UserLogged, UserProfile } from '../../../../auth/types';

const loggedUser: UserLogged = {
  id: '1',
  fullName: 'Test user',
  email: 'test@user.com',
  created: 123456789,
  lastModified: 123456789,
};

const user: UserProfile = {
  id: '1',
  fullName: 'Test user',
  email: 'test@user.com',
  address: 'Test address',
  city: 'Test city',
  contactEmail: 'Test email',
  contactPhone: 'Test phone',
  country: 'Test country',
  document: 'Test document',
  facebook: 'Test facebook',
  instagram: 'Test instagram',
  lastModified: 123456789,
  linkedin: 'Test linkedin',
  picture: 'Test picture',
  created: 123456785,
  state: 'Test state',
  twitter: 'Test twitter',
  youtube: 'Test youtube',
};

describe('User', () => {
  it('should render a valid page', () => {
    const component = shallow(<View loggedUser={loggedUser} user={user} />);
    expect(component).toMatchSnapshot();
  });

  it('should render an empty page', () => {
    const component = shallow(<View loggedUser={loggedUser} />);
    expect(component).toMatchSnapshot();
  });
});
