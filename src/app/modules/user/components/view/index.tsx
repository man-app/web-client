import React from 'react';

import { Page } from '../../../../components/page';
import Profile from '../../../../components/profile';

import { MAX_WIDTH } from '../../../../../common/constants/styles/sizes';
import { UserProfile, UserLogged } from '../../../auth/types';

interface Props {
  loggedUser?: UserLogged;
  user?: UserProfile;
}

const View: React.FC<Props> = ({ loggedUser, user }: Props) => {
  // When no company data is present, stop execution
  // TODO: Show a loading spinner?
  if (!user) {
    return null;
  }

  return (
    <Page maxWidth={MAX_WIDTH}>
      <Profile loggedUser={loggedUser} profile={user} />
    </Page>
  );
};

export default View;
