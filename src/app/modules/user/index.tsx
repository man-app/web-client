import React from 'react';
import { useSelector } from 'react-redux';

import { getLoggedUser, getUserProfile } from '../auth/selectors';

import View from './components/view';

const User: React.FC = () => {
  const loggedUser = useSelector(getLoggedUser);
  const user = useSelector(getUserProfile);

  return <View loggedUser={loggedUser} user={user} />;
};

export default User;
