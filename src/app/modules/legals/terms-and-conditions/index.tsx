import React from 'react';

import { Card } from '../../../components/card';
import { Page, PageH2, PageH4 } from '../../../components/page';
import Spacer from '../../../components/spacer';
import { Link } from '../../../components/touchable';

import { FEEDBACK } from '../../../../common/constants/appRoutes';
import { APP_DEVELOPER, APP_DEVELOPER_WEBSITE } from '../../../../common/constants/branding';
import { MAX_WIDTH } from '../../../../common/constants/styles/sizes';
import { LinkProps } from '../../../components/touchable/types';

const feedbackLink: LinkProps = {
  id: 'feedback-tac-link',
  to: FEEDBACK,
  label: 'report it',
};

const developerLink: LinkProps = {
  id: 'developer-tac',
  to: APP_DEVELOPER_WEBSITE,
  label: APP_DEVELOPER,
  isExternal: true,
};

const TermsAndConditions = () => (
  <Page maxWidth={MAX_WIDTH}>
    <Card>
      <PageH2>Manapp{"'"}s Terms and conditions</PageH2>

      <Card>
        <PageH4>User{"'"}s identity</PageH4>

        <p>
          When Manapp{"'"}s tools, you should have only one account. Having several accounts will worsend the user
          experience.
        </p>
        <Spacer size="sm" />

        <p>Any personal information you provide has to be truth and authentic.</p>
      </Card>

      <Card>
        <PageH4>User{"'"}s behavior</PageH4>

        <p>When using Manapp{"'"}s tools you should respect any other user.</p>
        <Spacer size="sm" />

        <p>
          You won{"'"}t use any information obtained through this application for commercial purposes (selling
          information to others, steal information from others, sending commercial emails, sending commercial sms, and
          so on).
        </p>
        <Spacer size="sm" />

        <p>You won{"'"}t take advantage of any possible bug in the software.</p>
        <Spacer size="sm" />

        <p>
          If you detect a possible bug in the software, you should <Link options={feedbackLink} /> as soon as possible.
        </p>
        <Spacer size="sm" />

        <p>
          You won{"'"}t use Manapp{"'"}s tools for committing a crime or carry out illegal actions.
        </p>
      </Card>

      <Card>
        <PageH4>Users rejections</PageH4>

        <p>
          Manapp will be able to block the access to its applications to any user who does not follow the present Terms
          and Conditions.
        </p>
      </Card>
      <Spacer />

      <Card>
        <PageH4>Company creation</PageH4>

        <p>
          When a user is creating a new company, user states he/she has permissions from the company to use its logo,
          logotype, commercial name
        </p>
        <Spacer size="sm" />

        <p>When a user is creating a new company, user states he/she has permissions for representing the company.</p>
      </Card>

      <Card>
        <PageH4>Company usage</PageH4>

        <p>
          When a user is given a particular role, the company states that the person is authorized to do all the tasks
          that the role allows to do.
        </p>
      </Card>
      <Spacer />

      <Card>
        <PageH4>Data ownership</PageH4>

        <p>
          All data stored in our databases owns to <Link options={developerLink} /> &copy;.
        </p>
        <Spacer size="sm" />

        <p>
          Any user that generates any kind of data is responsible of generating it, and he/she states this data is
          truth.
        </p>
        <Spacer size="sm" />

        <p>We reserve the right to use any stored data for developing/testing purposes.</p>
      </Card>

      <Card>
        <PageH4>Canceling a user account</PageH4>

        <p>
          When you decide to cancel your user account, we will mark it as disabled, and it will enter in Preventive
          state.
        </p>
        <Spacer size="sm" />

        <p>
          An email will be sent to each company administrator of the companies you are currently joined, so they can
          know why you aren{"'"}t appearing anymore in the application.
        </p>
      </Card>

      <Card>
        <PageH4>Deleting a company</PageH4>

        <p>When you decide to delete a company, we will mark it as disabled, and it will enter in Preventive state.</p>
        <Spacer size="sm" />

        <p>
          An email will be sent to all company members, so they can know why that company is not appearing anymore in
          their menus.
        </p>
      </Card>

      <Card>
        <PageH4>Preventive state</PageH4>

        <p>
          All your data will be saved during three months, just in case you want to recover it. After this period, data
          will be completely removed and there won{"'"}t be any way of recovering it.
        </p>
        <Spacer size="sm" />

        <p>It will be gone. Forever.</p>
        <Spacer size="sm" />

        <p>
          Manapp reserves the right of keeping any relevant information if any user/company has not followed the present
          Terms and Conditions.
        </p>
      </Card>
    </Card>
  </Page>
);

export default TermsAndConditions;
