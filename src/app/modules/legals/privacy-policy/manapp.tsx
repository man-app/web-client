import React from 'react';

import { Card } from '../../../components/card';
import List from '../../../components/list';
import { PageH4 } from '../../../components/page';
import Spacer from '../../../components/spacer';
import { Link } from '../../../components/touchable';

import { LinkProps } from '../../../components/touchable/types';

const gaLink: LinkProps = {
  id: 'analytics-link',
  to: 'https://policies.google.com/privacy',
  isExternal: true,
  label: "Google's Privacy Policy",
};

const Manapp = () => (
  <>
    <p>Manapp needs to store some data related to you, in order to work properly.</p>
    <Spacer size="sm" />

    <p>The data we{"'"}re storing is not going to be shared with anyone other than us.</p>

    <Card>
      <PageH4>Registration</PageH4>
      <p>When you register in Manapp, you provide us the following information:</p>
      <List>
        {({ Item }) => (
          <>
            <Item>Your name</Item>
            <Item>Your email</Item>
          </>
        )}
      </List>
    </Card>

    <Card>
      <PageH4>Optional information</PageH4>
      <p>If you decide to complete your profile, probably you will provide us with the following information:</p>
      <List>
        {({ Item }) => (
          <>
            <Item>Your identity document</Item>
            <Item>Your contact email</Item>
            <Item>Your contact phone</Item>
            <Item>Your address, city, state and country</Item>
          </>
        )}
      </List>
    </Card>

    <Card>
      <PageH4>Usage</PageH4>
      <p>
        We{"'"}re also registering some of your habits in our website, in order to detect what features work best, and
        which ones need to be improved.
      </p>
      <Spacer size="sm" />

      <p>
        To achieve this, we{"'"}re using GA (Google Analytics), and when you login, we send your Manapp{"'"}s user ID to
        GA, so we can relate Manapp{"'"}s users to GA users. This allows us to track:
      </p>
      <List>
        {({ Item }) => (
          <>
            <Item>Visits you make to Manapp</Item>
            <Item>Time you spend in Manapp, and spent time on each page/section/module</Item>
            <Item>Your language and location</Item>
            <Item>Your OS and browser</Item>
          </>
        )}
      </List>
      <p>
        If you want to know more info about what GA tracks, please go to <Link options={gaLink} />
      </p>
    </Card>
  </>
);

export default Manapp;
