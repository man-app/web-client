import React from 'react';
import { capitalize } from '../../../utils/strings';

import { Card } from '../../../components/card';
import { Page, PageH3, PageH2 } from '../../../components/page';
import Space from '../../../components/space';
import Spacer from '../../../components/spacer';
import Cookies from './cookies';
import Mailing from './mailing';
import Manapp from './manapp';
import SocialNetwork from './social-network';

import { SUPPORTED_PLATFORMS } from '../../../../common/constants/social-networks';
import { MAX_WIDTH } from '../../../../common/constants/styles/sizes';

const PrivacyPolicy = () => (
  <Page maxWidth={MAX_WIDTH}>
    <Card>
      <PageH2>Manapp{"'"}s Privacy Policy</PageH2>
      <Manapp />
      <Spacer />

      <PageH2>Cookies</PageH2>
      <Cookies isEmbed />
      <Spacer />

      <PageH2>Mailing</PageH2>
      <Mailing isEmbed />
      <Spacer />

      <PageH2>Social networks</PageH2>
      <p>
        In Manapp you can connect your social networks to your Manapp account, and get some extra features, such as
        1-click-login or 1-click-register.
      </p>
      <Spacer size="sm" />

      <p>If you want to know our policies related to your social network, these they are:</p>
      {SUPPORTED_PLATFORMS.map(platform => {
        const platformName = capitalize(platform);

        return (
          <Card key={`card-${platform}`}>
            <PageH3>
              <i className={`fa fa-${platform} is-${platform}`} />
              <Space />
              {platformName}
            </PageH3>

            <SocialNetwork platform={platformName} />
          </Card>
        );
      })}
    </Card>
  </Page>
);

export default PrivacyPolicy;
