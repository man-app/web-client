import React from 'react';

import List from '../../../components/list';
import Spacer from '../../../components/spacer';

interface Props {
  platform: string;
}

const SocialNetwork = ({ platform }: Props) => (
  <>
    <p>We need to store some data from your {platform} account in order to connect it to your Manapp account.</p>
    <Spacer size="sm" />

    <p>If you connect your {platform} account, the only information that will be stored is:</p>
    <List>
      {({ Item }) => (
        <>
          <Item>Your {platform} user ID</Item>
          <Item>Your {platform} main email</Item>
          <Item>Your {platform} profile URL</Item>
        </>
      )}
    </List>

    <p>
      If you didn
      {"'"}t have a Manapp account and you login with your {platform} account, we will also store:
    </p>

    <List>
      {({ Item }) => (
        <>
          <Item>Your {platform} full name</Item>
          <Item>Your {platform} avatar</Item>
        </>
      )}
    </List>
  </>
);

export default SocialNetwork;
