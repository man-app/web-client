import React from 'react';

import { Card } from '../../../components/card';
import List from '../../../components/list';
import { Page, PageH2 } from '../../../components/page';

import { INSTALLATION_ID } from '../../../../common/constants/cookies';
import { MAX_WIDTH } from '../../../../common/constants/styles/sizes';

const cookiesList = [INSTALLATION_ID];

interface Props {
  isEmbed?: boolean;
}

const Cookies = ({ isEmbed }: Props) => {
  const renderInfo = () => (
    <>
      <p>
        In order to improve your user experience, Manapp is going to install a short list of cookies in your browser:
      </p>
      <List>{({ Item }) => cookiesList.map(cookie => <Item key={cookie}>{cookie}</Item>)}</List>
    </>
  );

  if (isEmbed) {
    return <Card>{renderInfo()}</Card>;
  }

  return (
    <Page maxWidth={MAX_WIDTH}>
      <Card>
        <PageH2>Cookies Policy</PageH2>

        {renderInfo()}
      </Card>
    </Page>
  );
};

export default Cookies;
