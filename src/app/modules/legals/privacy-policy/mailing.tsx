import React from 'react';

import { Card } from '../../../components/card';
import List from '../../../components/list';
import { Page, PageH2 } from '../../../components/page';
import Spacer from '../../../components/spacer';

import { MAX_WIDTH } from '../../../../common/constants/styles/sizes';

interface Props {
  isEmbed?: boolean;
}

const Mailing = ({ isEmbed }: Props) => {
  const renderInfo = () => (
    <>
      <p>Manapps{"'"} webapp provides you the ability to send emails directly to our teams.</p>
      <p>
        If you do it, our team members will be able to access your name and email, in order to identify you and give you
        a solution or an answer.
      </p>
      <Spacer size="sm" />

      <p>
        This information will be used exclusively to answer your messages. We will not use this information for any
        other purposes, and we will not store it in any other place than our mail clients.
      </p>
      <Spacer size="sm" />

      <p>In some cases, we will need to access to the personal data we{"'"}ve stored about you, such as:</p>
      <List>
        {({ Item }) => (
          <>
            <Item>Your personal data</Item>
            <Item>The companies you have joined</Item>
            <Item>
              Any data related to companies, such as budgets, employees, clients, or any other module the company is
              using
            </Item>
          </>
        )}
      </List>
    </>
  );

  if (isEmbed) {
    return <Card>{renderInfo()}</Card>;
  }

  return (
    <Page maxWidth={MAX_WIDTH}>
      <Card>
        <PageH2>Mailing Policy</PageH2>

        {renderInfo()}
      </Card>
    </Page>
  );
};

export default Mailing;
