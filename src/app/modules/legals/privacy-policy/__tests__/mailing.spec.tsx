import React from 'react';
import { shallow } from 'enzyme';

import View from '../mailing';

describe('Cookies', () => {
  it('should render a valid embed section', () => {
    const component = shallow(<View isEmbed />);

    expect(component).toMatchSnapshot();
  });

  it('should render a valid screen', () => {
    const component = shallow(<View />);

    expect(component).toMatchSnapshot();
  });
});
