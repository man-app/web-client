import React from 'react';
import { shallow } from 'enzyme';

import View from '../social-network';

describe('Cookies', () => {
  it('should render a valid embed section', () => {
    const component = shallow(<View platform="test" />);

    expect(component).toMatchSnapshot();
  });
});
