import React from 'react';

import { Wrapper, Line, ErrorMessage, LineSpacer, ErrorSpacer } from './styles';

interface Props {
  fixedSidebar: boolean;
  handleRetry?: () => void;
  isLoading?: boolean;
  isOffline?: boolean;
}

const View: React.FC<Props> = ({ fixedSidebar, handleRetry, isLoading = false, isOffline = false }: Props) => (
  <>
    <Wrapper fixedSidebar={fixedSidebar} isLoading={isLoading}>
      <Line effect="increase" isLoading={isLoading} />
      <Line effect="decrease" isLoading={isLoading} />
      <ErrorMessage onClick={handleRetry} isOffline={isOffline}>
        Offline mode (tap to retry)
      </ErrorMessage>
    </Wrapper>

    <LineSpacer isLoading={isLoading} />
    <ErrorSpacer isOffline={isOffline} />
  </>
);

export default View;
