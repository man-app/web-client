import { CheckActionType } from '../types';

export const CHECK_REQUESTED: CheckActionType = 'CHECK_REQUESTED';
export const CHECK_SUCCEED: CheckActionType = 'CHECK_SUCCEED';
export const CHECK_FAILED: CheckActionType = 'CHECK_FAILED';
