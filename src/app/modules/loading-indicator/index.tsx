import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { shouldShowCompanyModules } from '../../utils/permissions';

import { isLoadingAnything, isOffline as isOfflineSelector } from '../../selectors';
import { getLoggedUser } from '../auth/selectors';
import { getSelectedCompany } from '../companies/selectors';

import View from './components/view';
import { check } from './actions';

const LoadingIndicator: React.FC = () => {
  const dispatch = useDispatch();
  const isLoading = useSelector(isLoadingAnything);
  const isOffline = useSelector(isOfflineSelector);

  const user = useSelector(getLoggedUser);
  const company = useSelector(getSelectedCompany);
  const fixedSidebar = shouldShowCompanyModules(user, company);

  const handleRetry = () => {
    dispatch(check());
  };

  return <View fixedSidebar={fixedSidebar} handleRetry={handleRetry} isLoading={isLoading} isOffline={isOffline} />;
};

export default LoadingIndicator;
