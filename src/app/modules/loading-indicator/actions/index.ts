import manappApi from '../../../../common/apis/manapp';

import { CHECK_REQUESTED, CHECK_SUCCEED, CHECK_FAILED } from '../actionTypes';

import { CheckActionCreator } from '../types';

export const check: CheckActionCreator = () => dispatch => {
  dispatch({
    type: CHECK_REQUESTED,
  });

  return manappApi
    .check()
    .then(() => {
      dispatch({
        type: CHECK_SUCCEED,
      });
    })
    .catch(err => {
      dispatch({
        type: CHECK_FAILED,
        payload: {
          err,
        },
      });
    });
};
