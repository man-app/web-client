import {
  INSTAGRAM_LOGIN_SUBMITTED,
  INSTAGRAM_LOGOUT_SUBMITTED,
  INSTAGRAM_LOGIN_FAILED,
  INSTAGRAM_LOGOUT_FAILED,
  INSTAGRAM_LOGIN_SUCCEED,
  INSTAGRAM_LOGOUT_SUCCEED,
} from '../actionTypes';
import { LOGOUT_SUCCEED } from '../../auth/actionTypes';

import { LogoutAction } from '../../auth/types';
import { getSocialNetworkState } from '../models';
import { SocialNetworkState, InstagramAction } from '../types';

const initialState = getSocialNetworkState();

const reducer = (state = initialState, action: InstagramAction | LogoutAction): SocialNetworkState => {
  switch (action.type) {
    case INSTAGRAM_LOGIN_SUBMITTED:
    case INSTAGRAM_LOGOUT_SUBMITTED:
      return {
        ...state,
        error: undefined,
        isLoading: true,
      };

    case INSTAGRAM_LOGIN_FAILED:
    case INSTAGRAM_LOGOUT_FAILED:
      return {
        ...state,
        error: action.payload.error,
        isLoading: false,
      };

    case INSTAGRAM_LOGIN_SUCCEED:
      return {
        ...state,
        ...action.payload,
        error: undefined,
        isLoading: false,
      };

    case INSTAGRAM_LOGOUT_SUCCEED:
      return {
        ...state,
        status: 'disconnected',
        auth: {},
        error: undefined,
        isLoading: false,
      };

    case LOGOUT_SUCCEED:
      return {
        ...initialState,
      };

    default:
      return state;
  }
};

export default reducer;
