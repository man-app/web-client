import {
  LINKEDIN_LOGIN_SUBMITTED,
  LINKEDIN_LOGOUT_SUBMITTED,
  LINKEDIN_LOGIN_FAILED,
  LINKEDIN_LOGOUT_FAILED,
  LINKEDIN_LOGIN_SUCCEED,
  LINKEDIN_LOGOUT_SUCCEED,
} from '../actionTypes';
import { LOGOUT_SUCCEED } from '../../auth/actionTypes';

import { LogoutAction } from '../../auth/types';
import { getSocialNetworkState } from '../models';
import { SocialNetworkState, LinkedinAction } from '../types';

const initialState = getSocialNetworkState();

const reducer = (state = initialState, action: LinkedinAction | LogoutAction): SocialNetworkState => {
  switch (action.type) {
    case LINKEDIN_LOGIN_SUBMITTED:
    case LINKEDIN_LOGOUT_SUBMITTED:
      return {
        ...state,
        error: undefined,
        isLoading: true,
      };

    case LINKEDIN_LOGIN_FAILED:
    case LINKEDIN_LOGOUT_FAILED:
      return {
        ...state,
        error: action.payload.error,
        isLoading: false,
      };

    case LINKEDIN_LOGIN_SUCCEED:
      return {
        ...state,
        ...action.payload,
        error: undefined,
        isLoading: false,
      };

    case LINKEDIN_LOGOUT_SUCCEED:
      return {
        ...state,
        status: 'disconnected',
        auth: {},
        error: undefined,
        isLoading: false,
      };

    case LOGOUT_SUCCEED:
      return {
        ...initialState,
      };

    default:
      return state;
  }
};

export default reducer;
