import { LogoutAction } from '../../auth/types';
import { getSocialNetworkState } from '../models';
import { SocialNetworkState, GoogleAction } from '../types';

const initialState = getSocialNetworkState();

const reducer = (state = initialState, action: GoogleAction | LogoutAction): SocialNetworkState => {
  switch (action.type) {
    default:
      return state;
  }
};

export default reducer;
