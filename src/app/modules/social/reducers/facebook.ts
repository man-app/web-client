import {
  FACEBOOK_LOGIN_SUCCEED,
  FACEBOOK_LOGIN_FAILED,
  FACEBOOK_LOGIN_SUBMITTED,
  FACEBOOK_LOGOUT_SUBMITTED,
  FACEBOOK_LOGOUT_SUCCEED,
  FACEBOOK_LOGOUT_FAILED,
} from '../actionTypes';
import { LOGOUT_SUCCEED } from '../../auth/actionTypes';

import { LogoutAction } from '../../auth/types';
import { getSocialNetworkState } from '../models';
import { SocialNetworkState, FacebookAction } from '../types';

const initialState = getSocialNetworkState();

const reducer = (state = initialState, action: FacebookAction | LogoutAction): SocialNetworkState => {
  switch (action.type) {
    case FACEBOOK_LOGIN_SUBMITTED:
    case FACEBOOK_LOGOUT_SUBMITTED:
      return {
        ...state,
        error: undefined,
        isLoading: true,
      };

    case FACEBOOK_LOGIN_FAILED:
    case FACEBOOK_LOGOUT_FAILED:
      return {
        ...state,
        error: action.payload.error,
        isLoading: false,
      };

    case FACEBOOK_LOGIN_SUCCEED:
      return {
        ...state,
        ...action.payload,
        error: undefined,
        isLoading: false,
      };

    case FACEBOOK_LOGOUT_SUCCEED:
      return {
        ...state,
        status: 'disconnected',
        auth: {},
        error: undefined,
        isLoading: false,
      };

    case LOGOUT_SUCCEED:
      return {
        ...initialState,
      };

    default:
      return state;
  }
};

export default reducer;
