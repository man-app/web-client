import { LogoutAction } from '../../auth/types';
import { getSocialNetworkState } from '../models';
import { SocialNetworkState, TwitterAction } from '../types';

const initialState = getSocialNetworkState();

const reducer = (state = initialState, action: TwitterAction | LogoutAction): SocialNetworkState => {
  switch (action.type) {
    default:
      return state;
  }
};

export default reducer;
