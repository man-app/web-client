import { combineReducers, Reducer } from 'redux';

import facebook from './facebook';
import google from './google';
import instagram from './instagram';
import linkedin from './linkedin';
import twitter from './twitter';

import { SocialState } from '../types';

// @ts-ignore
const reducer: Reducer<SocialState> = combineReducers({
  facebook,
  google,
  instagram,
  linkedin,
  twitter,
});

export default reducer;
