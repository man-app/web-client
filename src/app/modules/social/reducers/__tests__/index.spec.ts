import reducer from '..';

import { getSocialNetworkState } from '../../models';
import { SocialState } from '../../types';

const initialState = reducer(undefined, { type: 'INIT' });

describe('Social reducer', () => {
  it('should retur a valid object', () => {
    const expectedOutput: SocialState = {
      facebook: getSocialNetworkState(),
      google: getSocialNetworkState(),
      instagram: getSocialNetworkState(),
      linkedin: getSocialNetworkState(),
      twitter: getSocialNetworkState(),
    };
    expect(initialState).toStrictEqual(expectedOutput);
  });
});
