import reducer from '../facebook';

import { getSocialNetworkState } from '../../models';
import {
  FACEBOOK_LOGIN_SUBMITTED,
  FACEBOOK_LOGIN_FAILED,
  FACEBOOK_LOGIN_SUCCEED,
  FACEBOOK_LOGOUT_SUCCEED,
} from '../../actionTypes';
import { LOGOUT_SUCCEED } from '../../../auth/actionTypes';

const initialState = getSocialNetworkState();

describe('Social reducer', () => {
  describe('Facebook', () => {
    describe('Facebook actions', () => {
      it('should mark isLoading as true', () => {
        const nextState = reducer(initialState, { type: FACEBOOK_LOGIN_SUBMITTED });
        const expectedState = {
          ...initialState,
          isLoading: true,
        };

        expect(nextState).toStrictEqual(expectedState);
      });

      it('should save errors, and set isLoading as false', () => {
        const nextState = reducer(
          { ...initialState, isLoading: true },
          { type: FACEBOOK_LOGIN_FAILED, payload: { error: 'test-error' } }
        );
        const expectedState = {
          ...initialState,
          error: 'test-error',
          isLoading: false,
        };

        expect(nextState).toStrictEqual(expectedState);
      });

      it('should save user data, and mark isLoading as true', () => {
        const nextState = reducer(
          { ...initialState, isLoading: true },
          { type: FACEBOOK_LOGIN_SUCCEED, payload: { auth: { token: 'test-token' } } }
        );
        const expectedState = {
          ...initialState,
          auth: {
            ...initialState.auth,
            token: 'test-token',
          },
          isLoading: false,
        };

        expect(nextState).toStrictEqual(expectedState);
      });

      it('should remove user data, and mark isLoading as true', () => {
        const nextState = reducer(
          { ...initialState, auth: { ...initialState.auth, token: 'test-token' } },
          { type: FACEBOOK_LOGOUT_SUCCEED }
        );
        const expectedState = { ...initialState, status: 'disconnected' };

        expect(nextState).toStrictEqual(expectedState);
      });
    });

    describe('Logout action', () => {
      it('should return initial state', () => {
        const nextState = reducer(
          { ...initialState, auth: { ...initialState.auth, token: 'test-token' } },
          { type: LOGOUT_SUCCEED }
        );

        expect(nextState).toStrictEqual(initialState);
      });
    });
  });
});
