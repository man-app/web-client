import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { capitalize } from '../../utils/strings';

import { getLoggedUser } from '../auth/selectors';
import {
  facebookLogin,
  facebookLogout,
  instagramLogin,
  instagramLogout,
  linkedinLogin,
  linkedinLogout,
  setPlatformState,
  showPlatformOnProfile,
} from './actions';
import { getSocialNetworkData } from './selectors';

import SocialNetworkInfo from '../legals/privacy-policy/social-network';
import View from './components/view';

import { SUPPORTED_PLATFORMS, LINKEDIN, FACEBOOK, INSTAGRAM } from '../../../common/constants/social-networks';
import { createNetworkDataFromReducer } from './models';

interface Props {
  platform: string;
}

const SocialConnector: React.FC<Props> = ({ platform }: Props) => {
  const [isLoading, setIsFetching] = React.useState(false);
  const [infoExpanded, setInfoExpanded] = React.useState(false);

  const dispatch = useDispatch();
  const user = useSelector(getLoggedUser);
  const networkRawData = useSelector(getSocialNetworkData)(platform);
  const networkData = {
    ...createNetworkDataFromReducer(networkRawData),
    isEnabled: !!SUPPORTED_PLATFORMS.find(p => p === platform),
  };

  const [showProfile, setShowProfile] = React.useState(!!(user && user[platform]));

  React.useEffect(() => {
    const { isConnected } = networkData;
    if (isConnected) {
      dispatch(setPlatformState({ platform, userData: networkData.user, isConnected }));
    }
  }, [networkData]);

  const connect = async () => {
    setIsFetching(true);

    switch (platform) {
      case FACEBOOK:
        await dispatch(facebookLogin());
        break;

      case INSTAGRAM:
        await dispatch(instagramLogin());
        break;

      case LINKEDIN:
        await dispatch(linkedinLogin());
        break;

      default:
    }

    setIsFetching(false);
  };

  const disconnect = async () => {
    setIsFetching(true);

    switch (platform) {
      case LINKEDIN:
        await dispatch(linkedinLogout());
        break;

      case FACEBOOK:
        await dispatch(facebookLogout());
        break;

      case INSTAGRAM:
        await dispatch(instagramLogout());
        break;

      default:
    }

    setIsFetching(false);
  };

  const toggleShowProfile = () => {
    const isShowingProfile = !!showProfile;
    const enabled = !isShowingProfile;

    setIsFetching(true);
    setShowProfile(enabled);

    dispatch(showPlatformOnProfile({ platform, enabled }))
      // @ts-ignore
      .then(() => {
        setIsFetching(false);
      })
      .catch(() => {
        setIsFetching(false);
        setShowProfile(isShowingProfile);
      });
  };

  const toggleInfo = () => {
    setInfoExpanded(!infoExpanded);
  };

  const renderInfo = () => {
    const platformName = capitalize(platform);

    switch (platform) {
      case FACEBOOK:
      case INSTAGRAM:
      case LINKEDIN:
        return <SocialNetworkInfo platform={platformName} />;

      default:
        return false;
    }
  };

  return (
    <View
      isLoading={isLoading}
      platform={platform}
      networkData={networkData}
      infoExpanded={infoExpanded}
      showProfile={showProfile}
      connect={() => connect()}
      disconnect={() => disconnect()}
      toggleInfo={() => toggleInfo()}
      toggleShowProfile={() => toggleShowProfile()}
      renderInfo={() => renderInfo()}
    />
  );
};

export default SocialConnector;
