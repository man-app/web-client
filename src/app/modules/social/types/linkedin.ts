import { Dispatch } from 'redux';

export type LinkedinActionType =
  | 'LINKEDIN_LOGIN_SUBMITTED'
  | 'LINKEDIN_LOGIN_SUCCEED'
  | 'LINKEDIN_LOGIN_FAILED'
  | 'LINKEDIN_LOGOUT_SUBMITTED'
  | 'LINKEDIN_LOGOUT_SUCCEED'
  | 'LINKEDIN_LOGOUT_FAILED';

export interface LinkedinAction {
  type: LinkedinActionType;
  payload?: any;
}

export type LinkedinActionCreator = (payload?: any) => (dispatch: Dispatch<LinkedinAction>) => Promise<any>;
