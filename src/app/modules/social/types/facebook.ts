import { Dispatch } from 'redux';

export type FacebookActionType =
  | 'FACEBOOK_LOGIN_SUBMITTED'
  | 'FACEBOOK_LOGIN_SUCCEED'
  | 'FACEBOOK_LOGIN_FAILED'
  | 'FACEBOOK_LOGOUT_SUBMITTED'
  | 'FACEBOOK_LOGOUT_SUCCEED'
  | 'FACEBOOK_LOGOUT_FAILED';

export interface FacebookAction {
  type: FacebookActionType;
  payload?: any;
}

export type FacebookActionCreator = (payload?: any) => (dispatch: Dispatch<FacebookAction>) => Promise<any>;
