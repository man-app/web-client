import { Dispatch } from 'redux';

export type GoogleActionType =
  | 'GOOGLE_LOGIN_SUBMITTED'
  | 'GOOGLE_LOGIN_SUCCEED'
  | 'GOOGLE_LOGIN_FAILED'
  | 'GOOGLE_LOGOUT_SUBMITTED'
  | 'GOOGLE_LOGOUT_SUCCEED'
  | 'GOOGLE_LOGOUT_FAILED';

export interface GoogleAction {
  type: GoogleActionType;
  payload?: any;
}

export type GoogleActionCreator = (payload?: any) => (dispatch: Dispatch<GoogleAction>) => Promise<any>;
