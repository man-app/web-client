import { FacebookActionType, FacebookAction, FacebookActionCreator } from './facebook';
import { GoogleActionType, GoogleAction, GoogleActionCreator } from './google';
import { InstagramActionType, InstagramAction, InstagramActionCreator } from './instagram';
import { LinkedinActionType, LinkedinAction, LinkedinActionCreator } from './linkedin';
import { TwitterActionType, TwitterAction, TwitterActionCreator } from './twitter';
import { Dispatch } from 'redux';

export * from './facebook';
export * from './google';
export * from './instagram';
export * from './linkedin';
export * from './twitter';

export interface SocialUser {
  id?: string;
  fullName?: string;
  email?: string;
  profile?: string;
  picture?: string;
}

export interface SocialNetworkData {
  isEnabled: boolean;
  isConnected: boolean;
  user?: SocialUser;
  error?: string;
  isLoading: boolean;
}

export interface SocialNetworkState {
  status: string;
  auth: {
    token?: string;
  };
  user: SocialUser;
  error?: string;
  isLoading: boolean;
}

export interface SocialState {
  [key: string]: SocialNetworkState;

  facebook: SocialNetworkState;
  google: SocialNetworkState;
  instagram: SocialNetworkState;
  linkedin: SocialNetworkState;
  twitter: SocialNetworkState;
}

export type GenericSocialActionType = 'CONNECT_SOCIAL_NETWORK' | 'PUBLISH_SOCIAL_PROFILE';

export type SocialActionType =
  | GenericSocialActionType
  | FacebookActionType
  | GoogleActionType
  | InstagramActionType
  | LinkedinActionType
  | TwitterActionType;

export interface GenericSocialAction {
  type: 'CONNECT_SOCIAL_NETWORK' | 'PUBLISH_SOCIAL_PROFILE';
  payload?: any;
}

export type SocialAction =
  | GenericSocialAction
  | FacebookAction
  | GoogleAction
  | InstagramAction
  | LinkedinAction
  | TwitterAction;

export type genericSocialActionCreator = (payload?: any) => (dispatch: Dispatch<GenericSocialAction>) => Promise<any>;

export type SocialActionCreator =
  | genericSocialActionCreator
  | FacebookActionCreator
  | GoogleActionCreator
  | InstagramActionCreator
  | LinkedinActionCreator
  | TwitterActionCreator;
