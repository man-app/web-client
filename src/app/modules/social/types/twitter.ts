import { Dispatch } from 'redux';

export type TwitterActionType =
  | 'TWITTER_LOGIN_SUBMITTED'
  | 'TWITTER_LOGIN_SUCCEED'
  | 'TWITTER_LOGIN_FAILED'
  | 'TWITTER_LOGOUT_SUBMITTED'
  | 'TWITTER_LOGOUT_SUCCEED'
  | 'TWITTER_LOGOUT_FAILED';

export interface TwitterAction {
  type: TwitterActionType;
  payload?: any;
}

export type TwitterActionCreator = (payload?: any) => (dispatch: Dispatch<TwitterAction>) => Promise<any>;
