import { Dispatch } from 'redux';

export type InstagramActionType =
  | 'INSTAGRAM_LOGIN_SUBMITTED'
  | 'INSTAGRAM_LOGIN_SUCCEED'
  | 'INSTAGRAM_LOGIN_FAILED'
  | 'INSTAGRAM_LOGOUT_SUBMITTED'
  | 'INSTAGRAM_LOGOUT_SUCCEED'
  | 'INSTAGRAM_LOGOUT_FAILED';

export interface InstagramAction {
  type: InstagramActionType;
  payload?: any;
}

export type InstagramActionCreator = (payload?: any) => (dispatch: Dispatch<InstagramAction>) => Promise<any>;
