import React from 'react';

import Avatar from '../../../../components/avatar';
import { Buttons } from '../../../../components/touchable';
import SmartForm from '../../../../components/forms';
import { Wrapper, Label, UserName } from './styles';

import { SocialNetworkData } from '../../types';
import { BUTTON, SWITCH } from '../../../../components/forms/models';
import { ButtonProps, ButtonType } from '../../../../components/touchable/types';
import Message from '../../../../components/message';
import { MessageText } from '../../../../components/message/styles';

interface Props {
  isLoading: boolean;
  platform: string;
  networkData: SocialNetworkData;
  infoExpanded: boolean;
  showProfile: boolean;
  connect: () => void;
  disconnect: () => void;
  toggleInfo: () => void;
  toggleShowProfile: () => void;
  renderInfo: () => void;
}

const View: React.FC<Props> = ({
  isLoading,
  platform,
  networkData,
  infoExpanded,
  showProfile,
  connect,
  disconnect,
  toggleInfo,
  toggleShowProfile,
  renderInfo,
}: Props) => {
  const buttons: ButtonProps[] = [
    {
      buttonType: networkData.isConnected ? 'danger' : (platform as ButtonType),
      icon: platform,
      id: networkData.isConnected ? `${platform}-disconnect` : `${platform}-connect`,
      isAlwaysDisabled: !networkData.isEnabled,
      label: networkData.isConnected ? 'Disconnect' : 'Connect',
      onClick: networkData.isConnected ? disconnect : connect,
      type: BUTTON,
    },
    {
      icon: 'info',
      id: `${platform}-info`,
      isAlwaysDisabled: !networkData.isEnabled,
      onClick: toggleInfo,
      type: BUTTON,
    },
  ];

  const showProfileField =
    networkData.isEnabled && networkData.isConnected
      ? {
          id: `${platform}-profile`,
          isChecked: showProfile,
          isDisabled: isLoading,
          label: 'Show on my profile',
          onChange: toggleShowProfile,
          type: SWITCH,
        }
      : undefined;

  return (
    <Wrapper>
      {networkData.user && networkData.isConnected && (
        <div>
          <Label>Connected as:</Label>

          <Avatar
            profile={{
              picture: networkData.user.picture || '',
              fullName: networkData.user.fullName,
            }}
            size="sm"
          />

          <UserName>{networkData.user.fullName}</UserName>
        </div>
      )}

      {networkData.error && (
        <Message type="error">
          <MessageText>{networkData.error}</MessageText>
        </Message>
      )}

      {showProfileField && (
        <SmartForm
          buttons={[]}
          fields={[showProfileField]}
          form={`account-${platform}`}
          handleSubmit={() => {}}
          isLoading={isLoading}
        >
          {({ controlledFields, Field }) => (
            <>
              {controlledFields.map(field => (
                <Field key={field.id} options={field} />
              ))}
            </>
          )}
        </SmartForm>
      )}

      <Buttons align="left" options={buttons} />

      {infoExpanded && renderInfo()}
    </Wrapper>
  );
};

export default View;
