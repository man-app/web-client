import React from 'react';
import { shallow } from 'enzyme';

import View from '..';

import { getSocialNetworkState, createNetworkDataFromReducer } from '../../../models';

describe('SocialConnector', () => {
  const networkData = { ...createNetworkDataFromReducer(getSocialNetworkState()) };

  it('should render a valid connected component', () => {
    const component = shallow(
      <View
        connect={() => {}}
        disconnect={() => {}}
        infoExpanded={false}
        isLoading={false}
        networkData={{ ...networkData, isConnected: true }}
        platform={'test'}
        renderInfo={() => {}}
        showProfile={false}
        toggleInfo={() => {}}
        toggleShowProfile={() => {}}
      />
    );

    expect(component).toMatchSnapshot();
  });

  it('should render a valid disconnected component', () => {
    const component = shallow(
      <View
        connect={() => {}}
        disconnect={() => {}}
        infoExpanded={false}
        isLoading={false}
        networkData={networkData}
        platform={'test'}
        renderInfo={() => {}}
        showProfile={false}
        toggleInfo={() => {}}
        toggleShowProfile={() => {}}
      />
    );

    expect(component).toMatchSnapshot();
  });

  it('should render a valid disabled component', () => {
    const component = shallow(
      <View
        connect={() => {}}
        disconnect={() => {}}
        infoExpanded={false}
        isLoading={false}
        networkData={{ ...networkData, isEnabled: false }}
        platform={'test'}
        renderInfo={() => {}}
        showProfile={false}
        toggleInfo={() => {}}
        toggleShowProfile={() => {}}
      />
    );

    expect(component).toMatchSnapshot();
  });
});
