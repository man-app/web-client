import styled from 'styled-components';

export const Wrapper = styled.div`
  p,
  li {
    font-size: 13px;
    line-height: 1em;
  }

  p {
    margin-bottom: 10px;
  }

  li {
    margin-bottom: 5px;
  }
`;
Wrapper.displayName = 'Wrapper';

export const Label = styled.div``;
Label.displayName = 'Label';

export const UserName = styled.div``;
UserName.displayName = 'UserName';
