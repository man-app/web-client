import { SocialUser } from '../types';

export const createNetworkUserFromLinkedin = (linkedinUser: any): SocialUser => ({
  id: linkedinUser.id,
  fullName: `${linkedinUser.firstName} ${linkedinUser.lastName}`,
  email: linkedinUser.emailAddress,
  profile: linkedinUser.publicProfileUrl,
  picture: linkedinUser.pictureUrl,
});
