import { INSTAGRAM_PROFILE_PATTERN } from '../../../../common/constants/social-networks';
import { SocialUser } from '../types';

export const createNetworkUserFromInstagram = (instagramUser: any): SocialUser => ({
  id: instagramUser.user.id,
  fullName: instagramUser.user.full_name,
  email: instagramUser.user.email,
  profile: INSTAGRAM_PROFILE_PATTERN.replace(':username', instagramUser.user.username),
  picture: instagramUser.user.profile_picture,
});
