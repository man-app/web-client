import { SocialUser } from '../types';

export const createNetworkUserFromGoogle = (googleUser: any): SocialUser => ({
  id: googleUser.id,
  fullName: googleUser.name,
  email: googleUser.email,
  profile: googleUser.link,
  picture: googleUser.picture.data.url,
});
