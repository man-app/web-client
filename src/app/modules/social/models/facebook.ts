import { SocialUser } from '../types';

export const createNetworkUserFromFacebook = (facebookUser: any): SocialUser => ({
  id: facebookUser.id,
  fullName: facebookUser.name,
  email: facebookUser.email,
  profile: facebookUser.link,
  picture: facebookUser.picture.data.url,
});
