import { SocialNetworkState, SocialNetworkData, SocialState } from '../types';

export * from './facebook';
export * from './google';
export * from './instagram';
export * from './linkedin';
export * from './twitter';

export const createNetworkDataFromReducer = (rawData: SocialNetworkState): SocialNetworkData => ({
  isEnabled: false,
  isConnected: rawData && rawData.status === 'connected',
  user: rawData && rawData.user,
  error: rawData && rawData.error,
  isLoading: rawData && rawData.isLoading,
});

export const getSocialNetworkState = (): SocialNetworkState => ({
  status: 'unknown',
  auth: {},
  user: {},
  error: undefined,
  isLoading: false,
});

export const getState = (): SocialState => ({
  facebook: getSocialNetworkState(),
  google: getSocialNetworkState(),
  instagram: getSocialNetworkState(),
  linkedin: getSocialNetworkState(),
  twitter: getSocialNetworkState(),
});
