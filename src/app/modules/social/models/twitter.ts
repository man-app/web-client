import { SocialUser } from '../types';

export const createNetworkUserFromTwitter = (twitter: any): SocialUser => ({
  id: twitter.id,
  fullName: `${twitter.firstName} ${twitter.lastName}`,
  email: twitter.emailAddress,
  profile: twitter.publicProfileUrl,
  picture: twitter.pictureUrl,
});
