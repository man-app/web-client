import { GenericSocialActionType } from '../types';

export * from './facebook';
export * from './google';
export * from './instagram';
export * from './linkedin';
export * from './twitter';

export const CONNECT_SOCIAL_NETWORK: GenericSocialActionType = 'CONNECT_SOCIAL_NETWORK';
export const PUBLISH_SOCIAL_PROFILE: GenericSocialActionType = 'PUBLISH_SOCIAL_PROFILE';
