import { LinkedinActionType } from '../types/linkedin';

export const LINKEDIN_LOGIN_SUBMITTED: LinkedinActionType = 'LINKEDIN_LOGIN_SUBMITTED';
export const LINKEDIN_LOGIN_SUCCEED: LinkedinActionType = 'LINKEDIN_LOGIN_SUCCEED';
export const LINKEDIN_LOGIN_FAILED: LinkedinActionType = 'LINKEDIN_LOGIN_FAILED';
export const LINKEDIN_LOGOUT_SUBMITTED: LinkedinActionType = 'LINKEDIN_LOGOUT_SUBMITTED';
export const LINKEDIN_LOGOUT_SUCCEED: LinkedinActionType = 'LINKEDIN_LOGOUT_SUCCEED';
export const LINKEDIN_LOGOUT_FAILED: LinkedinActionType = 'LINKEDIN_LOGOUT_FAILED';
