import { GoogleActionType } from '../types/google';

export const GOOGLE_LOGIN_SUBMITTED: GoogleActionType = 'GOOGLE_LOGIN_SUBMITTED';
export const GOOGLE_LOGIN_SUCCEED: GoogleActionType = 'GOOGLE_LOGIN_SUCCEED';
export const GOOGLE_LOGIN_FAILED: GoogleActionType = 'GOOGLE_LOGIN_FAILED';
export const GOOGLE_LOGOUT_SUBMITTED: GoogleActionType = 'GOOGLE_LOGOUT_SUBMITTED';
export const GOOGLE_LOGOUT_SUCCEED: GoogleActionType = 'GOOGLE_LOGOUT_SUCCEED';
export const GOOGLE_LOGOUT_FAILED: GoogleActionType = 'GOOGLE_LOGOUT_FAILED';
