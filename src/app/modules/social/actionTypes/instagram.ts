import { InstagramActionType } from '../types/instagram';

export const INSTAGRAM_LOGIN_SUBMITTED: InstagramActionType = 'INSTAGRAM_LOGIN_SUBMITTED';
export const INSTAGRAM_LOGIN_SUCCEED: InstagramActionType = 'INSTAGRAM_LOGIN_SUCCEED';
export const INSTAGRAM_LOGIN_FAILED: InstagramActionType = 'INSTAGRAM_LOGIN_FAILED';
export const INSTAGRAM_LOGOUT_SUBMITTED: InstagramActionType = 'INSTAGRAM_LOGOUT_SUBMITTED';
export const INSTAGRAM_LOGOUT_SUCCEED: InstagramActionType = 'INSTAGRAM_LOGOUT_SUCCEED';
export const INSTAGRAM_LOGOUT_FAILED: InstagramActionType = 'INSTAGRAM_LOGOUT_FAILED';
