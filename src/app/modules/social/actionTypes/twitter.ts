import { TwitterActionType } from '../types/twitter';

export const TWITTER_LOGIN_SUBMITTED: TwitterActionType = 'TWITTER_LOGIN_SUBMITTED';
export const TWITTER_LOGIN_SUCCEED: TwitterActionType = 'TWITTER_LOGIN_SUCCEED';
export const TWITTER_LOGIN_FAILED: TwitterActionType = 'TWITTER_LOGIN_FAILED';
export const TWITTER_LOGOUT_SUBMITTED: TwitterActionType = 'TWITTER_LOGOUT_SUBMITTED';
export const TWITTER_LOGOUT_SUCCEED: TwitterActionType = 'TWITTER_LOGOUT_SUCCEED';
export const TWITTER_LOGOUT_FAILED: TwitterActionType = 'TWITTER_LOGOUT_FAILED';
