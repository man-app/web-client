import { FacebookActionType } from '../types/facebook';

export const FACEBOOK_LOGIN_SUBMITTED: FacebookActionType = 'FACEBOOK_LOGIN_SUBMITTED';
export const FACEBOOK_LOGIN_SUCCEED: FacebookActionType = 'FACEBOOK_LOGIN_SUCCEED';
export const FACEBOOK_LOGIN_FAILED: FacebookActionType = 'FACEBOOK_LOGIN_FAILED';
export const FACEBOOK_LOGOUT_SUBMITTED: FacebookActionType = 'FACEBOOK_LOGOUT_SUBMITTED';
export const FACEBOOK_LOGOUT_SUCCEED: FacebookActionType = 'FACEBOOK_LOGOUT_SUCCEED';
export const FACEBOOK_LOGOUT_FAILED: FacebookActionType = 'FACEBOOK_LOGOUT_FAILED';
