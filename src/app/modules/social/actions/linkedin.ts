import linkedinApi from '../../../../common/apis/social/linkedin';

import {
  LINKEDIN_LOGIN_SUCCEED,
  LINKEDIN_LOGIN_FAILED,
  LINKEDIN_LOGIN_SUBMITTED,
  LINKEDIN_LOGOUT_SUBMITTED,
  LINKEDIN_LOGOUT_SUCCEED,
  LINKEDIN_LOGOUT_FAILED,
} from '../actionTypes';
import { createNetworkUserFromLinkedin } from '../models';
import { LinkedinActionCreator } from '../types';

const linkedinGetUser: LinkedinActionCreator = () => dispatch =>
  linkedinApi
    .getUser()
    .then(linkedinUser => {
      const payload = {
        status: 'connected',
        auth: {},
        user: createNetworkUserFromLinkedin(linkedinUser),
      };

      // Stores user's facebook data on state
      dispatch({
        type: LINKEDIN_LOGIN_SUCCEED,
        payload,
      });

      return payload;
    })
    .catch(error => {
      // Display an error
      dispatch({
        type: LINKEDIN_LOGIN_FAILED,
        payload: {
          status: 'connected',
          error,
        },
      });

      return error;
    });

export const linkedinGetResidualLogin: LinkedinActionCreator = () => dispatch => {
  dispatch({
    type: LINKEDIN_LOGIN_SUBMITTED,
  });

  return linkedinApi
    .getStatus()
    .then(loginResponse => {
      // User is connected
      if (loginResponse) {
        // @ts-ignore
        return dispatch(linkedinGetUser());
      }

      // User is not connected
      dispatch({
        type: LINKEDIN_LOGIN_FAILED,
      });

      return undefined;
    })
    .catch(error => {
      dispatch({
        type: LINKEDIN_LOGIN_FAILED,
        payload: error,
      });

      return undefined;
    });
};

export const linkedinLogin: LinkedinActionCreator = () => dispatch => {
  dispatch({
    type: LINKEDIN_LOGIN_SUBMITTED,
  });

  // Makes user to login into Manapp's linkedin app
  return linkedinApi.login().then((loginResponse: any) => {
    // User is connected
    if (!loginResponse || !loginResponse.error) {
      // @ts-ignore
      return dispatch(linkedinGetUser());
    }

    // User not connected, display an error
    dispatch({
      type: LINKEDIN_LOGIN_FAILED,
      payload: loginResponse,
    });

    return undefined;
  });
};

export const linkedinLogout: LinkedinActionCreator = () => dispatch => {
  dispatch({
    type: LINKEDIN_LOGOUT_SUBMITTED,
  });

  return linkedinApi
    .logout()
    .then(logoutResponse => {
      dispatch({
        type: LINKEDIN_LOGOUT_SUCCEED,
        payload: logoutResponse,
      });

      return logoutResponse;
    })
    .catch(error => {
      dispatch({
        type: LINKEDIN_LOGOUT_FAILED,
        payload: error,
      });

      return undefined;
    });
};
