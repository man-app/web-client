import manappApi from '../../../../common/apis/manapp';

import { PUBLISH_SOCIAL_PROFILE } from '../actionTypes';
import { genericSocialActionCreator } from '../types';

export * from './facebook';
export * from './google';
export * from './instagram';
export * from './linkedin';
export * from './twitter';

export const setPlatformState: genericSocialActionCreator = ({ platform, userData, isConnected }) => () =>
  manappApi.social.setPlatformState({ platform, ...userData, isConnected });

export const showPlatformOnProfile: genericSocialActionCreator = ({ platform, enabled }) => dispatch =>
  manappApi.social
    .showPlatformOnProfile({ platform, enabled })
    .then(() => {
      const payload = {
        platform,
        enabled,
      };

      dispatch({
        type: PUBLISH_SOCIAL_PROFILE,
        payload,
      });

      return payload;
    })
    .catch(error => {
      dispatch({
        type: PUBLISH_SOCIAL_PROFILE,
        payload: {
          platform,
          enabled: !enabled,
        },
      });

      return error;
    });
