import instagramApi from '../../../../common/apis/social/instagram';

import {
  INSTAGRAM_LOGIN_SUBMITTED,
  INSTAGRAM_LOGIN_SUCCEED,
  INSTAGRAM_LOGIN_FAILED,
  INSTAGRAM_LOGOUT_SUCCEED,
  INSTAGRAM_LOGOUT_SUBMITTED,
  INSTAGRAM_LOGOUT_FAILED,
} from '../actionTypes';
import { createNetworkUserFromInstagram } from '../models';
import { InstagramActionCreator } from '../types';

const instagramGetUser: InstagramActionCreator = storedUser => dispatch => {
  const getPayloadFromResponse = (res: any) => {
    return {
      status: 'connected',
      auth: {
        token: res.access_token,
      },
      user: createNetworkUserFromInstagram(res),
    };
  };

  if (storedUser) {
    const payload = getPayloadFromResponse(storedUser);

    dispatch({
      type: INSTAGRAM_LOGIN_SUCCEED,
      payload,
    });

    return Promise.resolve();
  }

  return instagramApi
    .getUser()
    .then(instagramUser => {
      const payload = getPayloadFromResponse(instagramUser);

      // When user is connected
      dispatch({
        type: INSTAGRAM_LOGIN_SUCCEED,
        payload,
      });

      return payload;
    })
    .catch(error => {
      dispatch({
        type: INSTAGRAM_LOGIN_FAILED,
        payload: {
          error,
        },
      });
    });
};

// TODO: Instagram residual login breaks when user logs out from manapp, or init session in other device
export const instagramGetResidualLogin: InstagramActionCreator = () => dispatch => {
  dispatch({
    type: INSTAGRAM_LOGIN_SUBMITTED,
  });

  return (
    instagramApi
      .getStatus()
      // @ts-ignore
      .then(loginResponse => dispatch(instagramGetUser(loginResponse)))
      .catch(error => {
        dispatch({
          type: INSTAGRAM_LOGIN_FAILED,
          payload: {
            error: error,
          },
        });

        return undefined;
      })
  );
};

export const instagramLogin: InstagramActionCreator = () => dispatch => {
  dispatch({
    type: INSTAGRAM_LOGIN_SUBMITTED,
  });

  // Makes user to login into Manapp's instagram app
  return instagramApi
    .login()
    .then(loginResponse => {
      // @ts-ignore
      return dispatch(instagramGetUser(loginResponse));
    })
    .catch(error => {
      dispatch({
        type: INSTAGRAM_LOGIN_FAILED,
        payload: {
          error,
        },
      });
    });
};

export const instagramLogout: InstagramActionCreator = () => dispatch => {
  dispatch({
    type: INSTAGRAM_LOGOUT_SUBMITTED,
  });

  // Disconnect instagram user from instagram app
  return instagramApi
    .logout()
    .then(() => {
      dispatch({
        type: INSTAGRAM_LOGOUT_SUCCEED,
      });

      return undefined;
    })
    .catch(error => {
      dispatch({
        type: INSTAGRAM_LOGOUT_FAILED,
        payload: error,
      });

      return undefined;
    });
};
