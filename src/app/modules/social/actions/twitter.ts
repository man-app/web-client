import twitterApi from '../../../../common/apis/social/twitter';
import { info } from '../../../../common/utils/logger';

import { TWITTER_LOGIN_SUBMITTED } from '../actionTypes';
import { TwitterActionCreator } from '../types';

export const twitterLogin: TwitterActionCreator = () => dispatch => {
  dispatch({
    type: TWITTER_LOGIN_SUBMITTED,
  });

  return twitterApi.requestToken().then(response => {
    info('Twitter token', response);
  });
};
