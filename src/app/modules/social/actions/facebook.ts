import facebookApi from '../../../../common/apis/social/facebook';

import {
  FACEBOOK_LOGIN_SUBMITTED,
  FACEBOOK_LOGIN_SUCCEED,
  FACEBOOK_LOGIN_FAILED,
  FACEBOOK_LOGOUT_SUCCEED,
  FACEBOOK_LOGOUT_SUBMITTED,
  FACEBOOK_LOGOUT_FAILED,
} from '../actionTypes';
import { createNetworkUserFromFacebook } from '../models';
import { FacebookActionCreator } from '../types';

const FACEBOOK_PERMISSIONS = ['public_profile', 'email', 'user_link'].join(',');

const facebookGetUser: FacebookActionCreator = authResponse => dispatch =>
  facebookApi
    .getUser()
    .then(facebookUser => {
      const payload = {
        status: 'connected',
        auth: {
          token: authResponse.accessToken,
        },
        user: createNetworkUserFromFacebook(facebookUser),
      };

      // Stores user's facebook data on state
      dispatch({
        type: FACEBOOK_LOGIN_SUCCEED,
        payload,
      });

      return payload;
    })
    .catch(error => {
      // Display an error
      dispatch({
        type: FACEBOOK_LOGIN_FAILED,
        payload: {
          status: 'connected',
          error,
        },
      });

      return error;
    });

export const facebookGetResidualLogin: FacebookActionCreator = () => dispatch => {
  dispatch({
    type: FACEBOOK_LOGIN_SUBMITTED,
  });

  return facebookApi.getStatus().then(loginResponse => {
    // When user is connected
    if (loginResponse.status === 'connected') {
      // @ts-ignore
      return dispatch(facebookGetUser(loginResponse.authResponse));
    }

    // User not connected, display an error
    dispatch({
      type: FACEBOOK_LOGIN_FAILED,
      payload: {
        error: loginResponse.status === 'not_authorized' ? loginResponse.status : undefined,
      },
    });

    return undefined;
  });
};

export const facebookLogin: FacebookActionCreator = () => dispatch => {
  dispatch({
    type: FACEBOOK_LOGIN_SUBMITTED,
  });

  // Makes user to login into Manapp's facebook app
  return facebookApi.login({ scope: FACEBOOK_PERMISSIONS }).then((loginResponse: any) => {
    // When user is connected
    if (loginResponse.status === 'connected') {
      // @ts-ignore
      return dispatch(facebookGetUser(loginResponse.authResponse));
    }

    // User not connected, display an error
    dispatch({
      type: FACEBOOK_LOGIN_FAILED,
      payload: {
        error: loginResponse.status === 'not_authorized' ? loginResponse.status : undefined,
      },
    });

    return undefined;
  });
};

export const facebookLogout: FacebookActionCreator = () => dispatch => {
  dispatch({
    type: FACEBOOK_LOGOUT_SUBMITTED,
  });

  // Disconnect facebook user from facebook app
  return facebookApi
    .logout()
    .then(logoutResponse => {
      const payload = logoutResponse;

      dispatch({
        type: FACEBOOK_LOGOUT_SUCCEED,
        payload,
      });

      return payload;
    })
    .catch(error => {
      dispatch({
        type: FACEBOOK_LOGOUT_FAILED,
        payload: error,
      });

      return undefined;
    });
};
