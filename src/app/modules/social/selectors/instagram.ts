import { RootState } from '../../../types';

export const getInstagramStatus = ({ social: { instagram } }: RootState) => instagram.status;

export const getInstagramUser = ({ social: { instagram } }: RootState) => instagram.user;

export const getInstagramProfile = ({ social: { instagram } }: RootState) =>
  instagram.user && instagram.user.profile ? instagram.user.profile : '';

export const isInstagramFetching = ({ social: { instagram } }: RootState) => instagram.isLoading;
