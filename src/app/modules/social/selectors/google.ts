import { RootState } from '../../../types';

export const getGoogleStatus = ({ social: { google } }: RootState) => google.status;

export const getGoogleUser = ({ social: { google } }: RootState) => google.user;

export const getGoogleProfile = ({ social: { google } }: RootState) =>
  google.user && google.user.profile ? google.user.profile : '';

export const isGoogleFetching = ({ social: { google } }: RootState) => google.isLoading;
