import rootReducer from '../../../../reducers';
import * as selectors from '..';

import { FACEBOOK, GOOGLE, INSTAGRAM, LINKEDIN, TWITTER } from '../../../../../common/constants/social-networks';
import { SocialNetworkState } from '../../types';

const initialState = rootReducer(undefined, { type: 'INIT' });
const populatedState = {
  ...initialState,
  social: {
    ...initialState.social,
    facebook: {
      ...initialState.social.facebook,
      user: {
        ...initialState.social.facebook.user,
        profile: 'facebook-profile',
      },
    },
    google: {
      ...initialState.social.google,
      user: {
        ...initialState.social.google.user,
        profile: 'google-profile',
      },
    },
    instagram: {
      ...initialState.social.instagram,
      user: {
        ...initialState.social.instagram.user,
        profile: 'instagram-profile',
      },
    },
    linkedin: {
      ...initialState.social.linkedin,
      user: {
        ...initialState.social.linkedin.user,
        profile: 'linkedin-profile',
      },
    },
    twitter: {
      ...initialState.social.twitter,
      user: {
        ...initialState.social.twitter.user,
        profile: 'twitter-profile',
      },
    },
  },
};

describe('Social selectors', () => {
  describe('getSocialNetworkData', () => {
    it('should return facebook data', () => {
      const expectedOutput: SocialNetworkState = {
        auth: {},
        error: undefined,
        isLoading: false,
        status: 'unknown',
        user: {},
      };
      const output = selectors.getSocialNetworkData(initialState)(FACEBOOK);

      expect(output).toStrictEqual(expectedOutput);
    });

    it('should return google data', () => {
      const expectedOutput: SocialNetworkState = {
        auth: {},
        error: undefined,
        isLoading: false,
        status: 'unknown',
        user: {},
      };
      const output = selectors.getSocialNetworkData(initialState)(GOOGLE);

      expect(output).toStrictEqual(expectedOutput);
    });

    it('should return instagram data', () => {
      const expectedOutput: SocialNetworkState = {
        auth: {},
        error: undefined,
        isLoading: false,
        status: 'unknown',
        user: {},
      };
      const output = selectors.getSocialNetworkData(initialState)(INSTAGRAM);

      expect(output).toStrictEqual(expectedOutput);
    });

    it('should return linkedin data', () => {
      const expectedOutput: SocialNetworkState = {
        auth: {},
        error: undefined,
        isLoading: false,
        status: 'unknown',
        user: {},
      };
      const output = selectors.getSocialNetworkData(initialState)(LINKEDIN);

      expect(output).toStrictEqual(expectedOutput);
    });

    it('should return twitter data', () => {
      const expectedOutput: SocialNetworkState = {
        auth: {},
        error: undefined,
        isLoading: false,
        status: 'unknown',
        user: {},
      };
      const output = selectors.getSocialNetworkData(initialState)(TWITTER);

      expect(output).toStrictEqual(expectedOutput);
    });
  });

  describe('isLoadingSocial', () => {
    it('should return false', () => {
      const output = selectors.isLoadingSocial(initialState);
      expect(output).toBeFalsy();
    });

    it('should return true', () => {
      const populatedState = {
        ...initialState,
        social: {
          ...initialState.social,
          facebook: {
            ...initialState.social.facebook,
            isLoading: true,
          },
        },
      };

      const output = selectors.isLoadingSocial(populatedState);
      expect(output).toBeTruthy();
    });
  });

  describe('getNetworkStatus', () => {
    [
      selectors.getFacebookStatus,
      selectors.getGoogleStatus,
      selectors.getInstagramStatus,
      selectors.getLinkedinStatus,
      selectors.getTwitterStatus,
    ].forEach(selector => {
      const networkName = selector.name.replace('get', '').replace('Status', '');
      it(`Should return ${networkName} status`, () => {
        const expectedOutput = 'unknown';
        const output = selector(initialState);

        expect(output).toStrictEqual(expectedOutput);
      });
    });
  });

  describe('getNetworkUser', () => {
    [
      selectors.getFacebookUser,
      selectors.getGoogleUser,
      selectors.getInstagramUser,
      selectors.getLinkedinUser,
      selectors.getTwitterUser,
    ].forEach(selector => {
      const networkName = selector.name.replace('get', '').replace('User', '');
      it(`Should return ${networkName} user`, () => {
        const expectedOutput = {};
        const output = selector(initialState);

        expect(output).toStrictEqual(expectedOutput);
      });
    });
  });

  describe('getNetworkProfile', () => {
    [
      selectors.getFacebookProfile,
      selectors.getGoogleProfile,
      selectors.getInstagramProfile,
      selectors.getLinkedinProfile,
      selectors.getTwitterProfile,
    ].forEach(selector => {
      const networkName = selector.name.replace('get', '').replace('Profile', '');

      it(`Should return an empty url`, () => {
        const expectedOutput = '';
        const output = selector(initialState);

        expect(output).toStrictEqual(expectedOutput);
      });

      it(`Should return ${networkName} profile url`, () => {
        const expectedOutput = `${networkName.toLowerCase()}-profile`;
        const output = selector(populatedState);

        expect(output).toStrictEqual(expectedOutput);
      });
    });
  });

  describe('isNetworkFetching', () => {
    [
      selectors.isFacebookFetching,
      selectors.isGoogleFetching,
      selectors.isInstagramFetching,
      selectors.isLinkedinFetching,
      selectors.isTwitterFetching,
    ].forEach(selector => {
      const networkName = selector.name.replace('is', '').replace('Fetching', '');
      it(`Should return ${networkName} fetching state`, () => {
        const output = selector(initialState);
        expect(output).toBeFalsy();
      });
    });
  });
});
