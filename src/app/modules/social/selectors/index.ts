import { RootState } from '../../../types';
import { isFacebookFetching } from './facebook';
import { isGoogleFetching } from './google';
import { isInstagramFetching } from './instagram';
import { isLinkedinFetching } from './linkedin';
import { isTwitterFetching } from './twitter';

export * from './facebook';
export * from './google';
export * from './instagram';
export * from './linkedin';
export * from './twitter';

export const getSocialNetworkData = ({ social }: RootState) => (platform: string) => social[platform];

export const isLoadingSocial = (rootState: RootState) =>
  [
    isFacebookFetching(rootState),
    isGoogleFetching(rootState),
    isInstagramFetching(rootState),
    isLinkedinFetching(rootState),
    isTwitterFetching(rootState),
  ].filter(isLoading => !!isLoading).length > 0;
