import { RootState } from '../../../types';

export const getFacebookStatus = ({ social: { facebook } }: RootState) => facebook.status;

export const getFacebookUser = ({ social: { facebook } }: RootState) => facebook.user;

export const getFacebookProfile = ({ social: { facebook } }: RootState) =>
  facebook.user && facebook.user.profile ? facebook.user.profile : '';

export const isFacebookFetching = ({ social: { facebook } }: RootState) => facebook.isLoading;
