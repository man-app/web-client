import { RootState } from '../../../types';

export const getLinkedinStatus = ({ social: { linkedin } }: RootState) => linkedin.status;

export const getLinkedinUser = ({ social: { linkedin } }: RootState) => linkedin.user;

export const getLinkedinProfile = ({ social: { linkedin } }: RootState) =>
  linkedin.user && linkedin.user.profile ? linkedin.user.profile : '';

export const isLinkedinFetching = ({ social: { linkedin } }: RootState) => linkedin.isLoading;
