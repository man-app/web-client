import { RootState } from '../../../types';

export const getTwitterStatus = ({ social: { twitter } }: RootState) => twitter.status;

export const getTwitterUser = ({ social: { twitter } }: RootState) => twitter.user;

export const getTwitterProfile = ({ social: { twitter } }: RootState) =>
  twitter.user && twitter.user.profile ? twitter.user.profile : '';

export const isTwitterFetching = ({ social: { twitter } }: RootState) => twitter.isLoading;
