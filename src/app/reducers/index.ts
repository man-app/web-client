import { combineReducers, Reducer } from 'redux';
import { RootState } from '../types';

// Import reducers
import account from '../modules/account/reducers';
import activities from '../modules/activities/reducers';
import auth from '../modules/auth/reducers';
import companies from '../modules/companies/reducers';
import contact from '../modules/contact/reducers';
import feedback from '../modules/feedback/reducers';
import form from '../components/forms/reducers';
import modules from '../modules/modules/reducers';
import notifier from '../modules/notifier/reducers';
import paymentMethods from '../modules/payment-methods/reducers';
import roles from '../modules/roles/reducers';
import social from '../modules/social/reducers';
import subscriptions from '../modules/subscriptions/reducers';

const rootReducer: Reducer<RootState> = combineReducers({
  account,
  activities,
  auth,
  companies,
  contact,
  feedback,
  form,
  modules,
  notifier,
  paymentMethods,
  roles,
  social,
  subscriptions,
});

export default rootReducer;
