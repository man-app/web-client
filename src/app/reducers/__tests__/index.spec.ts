import reducer from '..';

import { getState as formsGetState } from '../../components/forms/models';
import { getState as accountGetState } from '../../modules/account/models';
import { getState as activitiesGetState } from '../../modules/activities/models';
import { getState as authGetState } from '../../modules/auth/models';
import { getState as companiesGetState } from '../../modules/companies/models';
import { getState as contactGetState } from '../../modules/contact/models';
import { getState as feedbackGetState } from '../../modules/feedback/models';
import { getState as modulesGetState } from '../../modules/modules/models';
import { getState as notifierGetState } from '../../modules/notifier/models';
import { getState as paymentMethodsGetState } from '../../modules/payment-methods/models';
import { getState as rolesGetState } from '../../modules/roles/models';
import { getState as socialGetState } from '../../modules/social/models';
import { getState as subscriptionsGetState } from '../../modules/subscriptions/models';

import { RootState } from '../../types';

const rootState = reducer(undefined, { type: 'INIT' });

describe('Root reducer', () => {
  it('should return a valid object', () => {
    const expectedState: RootState = {
      account: accountGetState(),
      activities: activitiesGetState(),
      auth: authGetState(),
      companies: companiesGetState(),
      contact: contactGetState(),
      feedback: feedbackGetState(),
      form: formsGetState(),
      modules: modulesGetState(),
      notifier: notifierGetState(),
      paymentMethods: paymentMethodsGetState(),
      roles: rolesGetState(),
      social: socialGetState(),
      subscriptions: subscriptionsGetState(),
    };

    expect(rootState).toStrictEqual(expectedState);
  });
});
