import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import useReactRouter from 'use-react-router';
import analyticsApi from '../common/apis/reports/analytics';
import { setLastSession, setStore, clearStore } from '../common/utils/idb';
import { log } from '../common/utils/logger';
import { getInstallationData } from './utils/installation';
import { requestNotificationsPermission } from './utils/notifications';
import { subscribeToPush } from './utils/push';
import registerServiceWorker from './utils/service-worker';

import { restoreLastRoute, persistStore as shouldPersistStore } from '../common/constants/features';

import { getModulesInstalled } from './modules/modules/actions';
import { getResidualLogin, logout } from './modules/auth/actions';
import { createNotification } from './modules/notifier/actions';
import { getSubscriptionPlans } from './modules/subscriptions/actions';
// import {
//   facebookGetResidualLogin,
//   instagramGetResidualLogin,
//   linkedinGetResidualLogin,
// } from './modules/social/actions';
import { selectCompany } from './modules/companies/actions';
import { getLoggedUser } from './modules/auth/selectors';
import { getCompanies, getSelectedCompany } from './modules/companies/selectors';
import { getAppStore } from './selectors';

import View from './view';

import { PAGE_LOAD, APP_LOAD, FINISHED } from '../common/constants/analytics';
import { APP_CHANGELOG, APP_NAME } from '../common/constants/branding';
import { REVIEW_CHANGELOG_ACTION } from '../common/constants/notifications';
import { createNotificationParams } from './modules/notifier/models';
import ThemeWrapper from './contexts/theme';

const packageJson = require('../../package.json');
const appVersion = packageJson.version;

const getCompanySlug = (url: string): string | void => {
  let match = url.match(new RegExp('/companies/(.*)/(.*)/(.*)'));
  if (match !== null) {
    return match[1];
  }

  match = url.match(new RegExp('/companies/(.*)/(.*)'));
  if (match !== null) {
    return match[1];
  }

  match = url.match(new RegExp('/companies/(.*)'));
  if (match !== null) {
    return match[1];
  }

  return undefined;
};

interface Props {
  lastRoute: string;
  isNewRelease: boolean;
}

const AppWrapper: React.FC<Props> = ({ isNewRelease, lastRoute }: Props) => {
  const { history, location } = useReactRouter();
  const dispatch = useDispatch();

  const store = useSelector(getAppStore);
  const loggedUser = useSelector(getLoggedUser);
  const companies = useSelector(getCompanies);
  const selectedCompany = useSelector(getSelectedCompany);

  const persistStore = () => {
    if (shouldPersistStore) {
      const ignoredStates = ['account', 'contact', 'feedback', 'form', 'notifier'];

      const persistedStore: { [index: string]: any } = {};

      Object.keys(store).forEach(key => {
        if (ignoredStates.indexOf(key) < 0) {
          persistedStore[key] = {
            // @ts-ignore
            ...store[key],
            filters: undefined,
            isLoading: undefined,
          };
        }
      });

      setStore(JSON.parse(JSON.stringify(persistedStore)));
    }
  };

  React.useEffect(() => {
    // Init the service worker
    registerServiceWorker(history);

    // Get installed modules
    dispatch(getModulesInstalled());

    // Get residual login
    const data = getInstallationData();

    // Get available payment plans
    dispatch(getSubscriptionPlans());

    // @ts-ignore
    dispatch(getResidualLogin(data.installationId)).then(({ users }) => {
      if (!users) {
        log('Invalid or no user logged', {
          persistedUser: loggedUser,
          loggedUser: undefined,
        });

        log('Clearing persisted store');
        clearStore();

        const installationData = getInstallationData();
        dispatch(logout(installationData.installationId));
      } else {
        requestNotificationsPermission(subscribeToPush);
        // dispatch(linkedinGetResidualLogin());
        // dispatch(facebookGetResidualLogin());
        // dispatch(instagramGetResidualLogin());

        // Show new release notification
        if (isNewRelease) {
          dispatch(
            createNotification(
              createNotificationParams({
                title: "What's new?",
                body: `A new version of ${APP_NAME} has been launched! Check out what new features we've implemented.`,
                tag: 'new_release',
                actions: [REVIEW_CHANGELOG_ACTION],
                data: {
                  isPermanent: true,
                  isExternal: true,
                  url: APP_CHANGELOG,
                  shouldPersistOnLogout: true,
                },
              })
            )
          );
        }
      }
    });

    analyticsApi.logTiming({
      category: PAGE_LOAD,
      label: APP_LOAD,
      variable: FINISHED,
      value: new Date().getTime() - analyticsApi.getInitialTime(),
    });
  }, []);

  React.useEffect(() => {
    if (restoreLastRoute) {
      history.push({
        pathname: lastRoute,
        state: {
          from: location,
          isInitial: true,
        },
      });
    }
  }, [lastRoute]);

  React.useEffect(() => {
    if (loggedUser) {
      const params = {
        slug: getCompanySlug(location.pathname),
      };

      if (
        !selectedCompany ||
        (selectedCompany && params.slug && params.slug !== 'new' && params.slug !== selectedCompany.slug)
      ) {
        const urlCompany = companies.find(company => company.slug === params.slug);

        if (urlCompany) {
          dispatch(selectCompany(urlCompany.id));
        } else if (companies.length) {
          dispatch(selectCompany(companies[0].id));
        }
      }
    }
  }, [location, companies, selectedCompany, loggedUser]);

  React.useEffect(() => {
    analyticsApi.logPageView(location.pathname);

    setLastSession({
      version: appVersion,
      route: location.pathname,
    });
  }, [location]);

  React.useEffect(() => {
    if (loggedUser) {
      persistStore();
    }
  }, [loggedUser, store]);

  return (
    <ThemeWrapper>
      <View company={selectedCompany} user={loggedUser} />
    </ThemeWrapper>
  );
};

export default AppWrapper;
