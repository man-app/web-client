import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { shouldShowCompanyModules } from './utils/permissions';

import RouteAuth from './components/routes/auth';
import RouteGuest from './components/routes/guest';
import About from './modules/about';
import Account from './modules/account';
import Activities from './modules/activities';
import Login from './modules/auth/components/login';
import Logout from './modules/auth/components/logout';
import Register from './modules/auth/components/register';
import ResetEmail from './modules/auth/components/reset-email';
import ResetPassword from './modules/auth/components/reset-password';
import ValidateEmail from './modules/auth/components/validate-email';
import ShoppingCart from './modules/subscriptions';
import Companies from './modules/companies';
import Contact from './modules/contact';
import Feedback from './modules/feedback';
import Landing from './modules/landing';
import PrivacyPolicy from './modules/legals/privacy-policy/index';
import Cookies from './modules/legals/privacy-policy/cookies';
import Mailing from './modules/legals/privacy-policy/mailing';
import TermsAndConditions from './modules/legals/terms-and-conditions/index';
import LoadingIndicator from './modules/loading-indicator';
import NotifierWrapper from './modules/notifier';
import Roles from './modules/roles';
import Subscriptions from './modules/subscriptions';
import User from './modules/user';
import Footer from './shell/footer';
import Header from './shell/header';
import { GlobalStyle, Wrapper, Content } from './styles';

import * as routes from '../common/constants/appRoutes';
import { UserLogged } from './modules/auth/types';
import { Company } from './modules/companies/types';
import { payments } from '../common/constants/features';

interface Props {
  company?: Company;
  user?: UserLogged;
}

const View: React.FC<Props> = ({ company, user }: Props) => (
  <Wrapper id="app">
    <GlobalStyle />

    <Header />
    <div id="sidebar-root"></div>

    <Content fixedSidebar={shouldShowCompanyModules(user, company)}>
      <LoadingIndicator />

      <Switch>
        <RouteAuth path={routes.USER_PROFILE} component={User} isLoggedUser={!!user} />
        <RouteAuth path={routes.FEEDBACK} component={Feedback} isLoggedUser={!!user} />
        <RouteAuth path={routes.RESET_EMAIL} component={ResetEmail} isLoggedUser={!!user} />
        <RouteAuth path={routes.ACCOUNT} component={Account} isLoggedUser={!!user} />

        <Route path={routes.COMPANY_ROLES} render={() => <Roles isLoggedUser={!!user} />} />
        <RouteAuth path={routes.COMPANY_ACTIVITIES} component={Activities} isLoggedUser={!!user} />
        <Route path={routes.COMPANIES} render={() => <Companies isLoggedUser={!!user} />} />

        <Route path={routes.TERMS_AND_CONDITIONS} component={TermsAndConditions} />
        <Route path={routes.MAILING_POLICY} component={Mailing} />
        <Route path={routes.COOKIES_POLICY} component={Cookies} />
        <Route path={routes.PRIVACY_POLICY} component={PrivacyPolicy} />
        <Route path={routes.CONTACT} component={Contact} />
        <Route path={routes.SUBSCRIPTIONS} component={Subscriptions} />
        <Route path={routes.ABOUT} component={About} />

        {payments && <Route path={routes.ACTIVATE_SUBSCRIPTION} component={ShoppingCart} />}

        <Route path={routes.LOGOUT} component={Logout} />
        <Route path={routes.RESET_PASSWORD} component={ResetPassword} />
        <RouteGuest path={routes.LOGIN} component={Login} isLoggedUser={!!user} />
        <Route path={routes.VALIDATE_EMAIL} component={ValidateEmail} />
        <RouteGuest path={routes.REGISTER} component={Register} isLoggedUser={!!user} />
        <RouteGuest path={routes.HOME} component={Landing} isLoggedUser={!!user} />
      </Switch>
    </Content>

    <Footer />

    <NotifierWrapper />

    <div id="modal-root"></div>
  </Wrapper>
);

export default View;
