import { log } from '../../common/utils/logger';
import { getWorkerConfig } from '../../common/utils/idb';
import { createNotificationParams, createNotificationTag } from '../../app/modules/notifier/models';
import { NotificationParams } from '../../definitions/notification';
import { ActivityPush } from '../../definitions/push';

/**
 * TS TODO: Need to tell TS that self is GlobalServiceWorkerScope instead of Window
 */
export const showNotification = (notificationParams: NotificationParams): Promise<void> => {
  log('Displaying a notification', notificationParams);
  // @ts-ignore
  return self.registration.showNotification(notificationParams.title, notificationParams.options);
};

export const createNotificationFromActivity = async (pushData: ActivityPush): Promise<NotificationParams> => {
  const workerConfig = await getWorkerConfig();

  const notificationParams = createNotificationParams({
    badge: workerConfig.defaultBadgeUrl,
    body: pushData.body,
    data: {
      company: pushData.data.company,
      element: pushData.data.idElement,
      type: pushData.data.idModule,
    },
    icon: pushData.icon || workerConfig.defaultIconUrl,
    image: pushData.image,
    tag: createNotificationTag({
      action: pushData.data.verb,
      company: pushData.data.company,
      element: pushData.data.idElement,
      type: pushData.data.idModule,
    }),
    title: pushData.title,
  });

  log('NotificationParams have been created', notificationParams);

  return notificationParams;
};
