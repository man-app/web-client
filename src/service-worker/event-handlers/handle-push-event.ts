import { log } from '../../common/utils/logger';

import { getStore } from '../../common/utils/idb';
import { showNotification, createNotificationFromActivity } from '../utils/notification';
import { PushData } from '../../definitions/push';
import { getUserNotificationPreferences } from '../../app/modules/auth/selectors';

const shouldNotifyAboutThis = async (pushData: PushData) => {
  const rootState = await getStore();
  const notifications = getUserNotificationPreferences(rootState);

  const notificationPreferences = notifications
    // Filter notification preferences by company
    .filter(
      preference =>
        pushData.data &&
        pushData.data.company &&
        pushData.data.company.id &&
        pushData.data.idModule &&
        preference.idCompany === pushData.data.company.id &&
        preference.idModule === pushData.data.idModule
    );

  if (!notificationPreferences || !pushData.data.verb) {
    log('User has no preferences for this module, no notifications');
    return false;
  }

  const preferences = {
    browser: notificationPreferences[0][pushData.data.verb].browser as boolean,
    push: notificationPreferences[0][pushData.data.verb].push as boolean,
  };

  log('These are user preferences for this company and module', preferences);

  // @ts-ignore
  const openWindows = await self.clients.matchAll();
  log('These are currently open windows', openWindows);

  return preferences.push || (openWindows.length > 0 && preferences.browser);
};

const handler = async (event: any) => {
  if (event.data) {
    const pushData = JSON.parse(event.data.text()) as PushData;
    log('Got a push event', pushData);

    const shouldNotify = await shouldNotifyAboutThis(pushData);
    if (!shouldNotify) {
      return Promise.resolve();
    }

    switch (pushData.data.type) {
      case 'info':
      case 'success':
      case 'warning':
      case 'error':
        log('Need to create a notification');
        return showNotification(await createNotificationFromActivity(pushData));

      default:
        return Promise.resolve();
    }
  }

  return Promise.resolve();
};

/**
 * TS TODO: Find RequestEvent or ExtendableEvent definition
 * TS TODO: Need to tell TS that self is GlobalServiceWorkerScope instead of Window
 */
const handlePushEvent = (event: any) => {
  event.waitUntil(handler(event));
};

export default handlePushEvent;
