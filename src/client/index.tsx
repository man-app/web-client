import 'core-js';
import 'pwacompat';
import React from 'react';
import ReactDom from 'react-dom';

import ReactApp from '..';

ReactDom.render(<ReactApp />, document.querySelector('#app-wrapper'));
