import { COMPANY_SETTINGS, COMPANY_ROLE, ACTIVATE_SUBSCRIPTION } from '../constants/appRoutes';
import { moduleIds } from '../../app/utils/permissions';

interface Resource {
  type: string;
  id: string;
  company: {
    slug: string;
  };
}

/**
 * Create a URL based on a resource
 */
export const getResourceUrl = ({ type, id, company }: Resource) => {
  switch (type) {
    case moduleIds.companySettings:
      return COMPANY_SETTINGS.replace(':slug', company.slug).replace(':tab?', '');

    case moduleIds.roles:
      return COMPANY_ROLE.replace(':slug', company.slug).replace(':id', id);

    case moduleIds.shoppingCart:
      return ACTIVATE_SUBSCRIPTION;

    case moduleIds.employees:
      return '';

    default:
      return '';
  }
};
