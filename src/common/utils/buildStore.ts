import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { isDev, isPre } from './platforms';

import rootReducer from '../../app/reducers';
import { RootState } from '../../app/types';

const initialState = rootReducer(undefined, { type: 'INIT' });

const buildStore = async (persistedState: RootState = initialState) => {
  // @ts-ignore
  const composeEnhancers = isDev() || isPre() ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose : compose;
  const store = createStore(rootReducer, persistedState, composeEnhancers(applyMiddleware(thunk)));

  return store;
};

export default buildStore;
