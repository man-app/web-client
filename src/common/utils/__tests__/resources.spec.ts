import setupEnv from '../setup-test-env';

import { getResourceUrl } from '../resources';

beforeEach(() => {
  setupEnv();
});

describe('Resources utils', () => {
  describe('getResourceUrl', () => {
    it('should return a company settings url', () => {
      const input = { type: '1', id: '1', company: { slug: 'my-company' } };
      const output = getResourceUrl(input);

      expect(output).toBe(`/companies/${input.company.slug}/settings/`);
    });

    it('should return a company role url', () => {
      const input = { type: '2', id: '1', company: { slug: 'my-company' } };
      const output = getResourceUrl(input);

      expect(output).toBe(`/companies/${input.company.slug}/roles/${input.id}`);
    });
  });
});
