export interface LinkedinInitParameters {
  appId: string;
  onLoad?: string;
  authorize?: boolean;
  locale?: string;
}

export interface LinkedinUserData {
  id: string;
  emailAddress: string;
  firstName: string;
  lastName: string;
  publicProfileUrl: string;
  pictureUrl: string;
}
