export interface InstagramInitParams {
  appId: string;
}

export interface BackendInstagramResponse {
  access_token: string;
  user: {
    id: string;
    full_name: string;
    bio: string;
    username: string;
    profile_picture: string;
    website: string;
    is_business: boolean;
  };
}
