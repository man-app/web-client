export interface FacebookInitParameters {
  appId: string;
  apiVersion: string;
}

export interface FacebookLoginParameters {
  scope: string;
}

export interface FacebookStatusResponse {
  status: string;
  error?: string;
  authResponse?: {
    accessToken: string;
    expiresIn: number;
    reauthorize_required_in: number;
    signedRequest: string;
    userID: string;
  };
}

export interface FacebookUserResponse {
  error?: any;
  email?: string;
  id?: string;
  link?: string;
  name?: string;
  picture?: {
    data: {
      height: number;
      is_silhouette: boolean;
      url: string;
      width: number;
    };
  };
}
