import {
  FacebookInitParameters,
  FacebookLoginParameters,
  FacebookStatusResponse,
  FacebookUserResponse,
} from './facebook.types';

const fields = ['id', 'name', 'email', 'picture', 'link'].join(',');

/**
 * Collection of methods to interact with Facebook
 */
const facebookApi = {
  /**
   * Initialize Facebook API
   */
  init: ({ appId, apiVersion: version }: FacebookInitParameters) =>
    new Promise(resolve => {
      // @ts-ignore
      window.fbAsyncInit = function() {
        // @ts-ignore
        FB.init({
          appId,
          cookie: true,
          xfbml: true,
          version,
        });

        // @ts-ignore
        FB.AppEvents.logPageView();
        resolve();
      };

      (function(d, s, id) {
        const fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {
          return;
        }
        const js = d.createElement(s);
        js.id = id;
        // @ts-ignore
        js.src = 'https://connect.facebook.net/en_US/sdk.js';
        fjs && fjs.parentNode && fjs.parentNode.insertBefore(js, fjs);
      })(document, 'script', 'facebook-jssdk');
    }),

  /**
   * Check if user is logged into Facebook application,
   * if it is, get user data from Facebook
   */
  getStatus: (): Promise<FacebookStatusResponse> =>
    new Promise(resolve => {
      // @ts-ignore
      FB.getLoginStatus((fbResponse: FacebookStatusResponse) => {
        resolve(fbResponse);
      });
    }),

  /**
   * Login Facebook user into Facebook application, so app can
   * get user data from Facebook
   */
  login: ({ scope }: FacebookLoginParameters) =>
    new Promise(async resolve => {
      const fbResponse: FacebookStatusResponse = await facebookApi.getStatus();

      switch (fbResponse.status) {
        case 'connected':
          // case 'not_authorized':
          resolve(fbResponse);
          break;

        default:
          // @ts-ignore
          FB.login(
            (loginResponse: FacebookStatusResponse) => {
              resolve(loginResponse);
            },
            { scope }
          );
      }
    }),

  /**
   * Logout the Facebook user from Facebook application,
   * so app cannot get user data from Facebook
   */
  logout: () =>
    new Promise((resolve, reject) => {
      // @ts-ignore
      FB.logout((fbResponse: FacebookStatusResponse) => {
        if (!fbResponse || fbResponse.error) {
          reject(fbResponse.error);
        }

        resolve(fbResponse);
      });
    }),

  /**
   * Get user data from Facebook
   */
  getUser: () =>
    new Promise((resolve, reject) => {
      // @ts-ignore
      FB.api(`/me?fields=${fields}`, (fbResponse: FacebookUserResponse) => {
        if (!fbResponse || fbResponse.error) {
          reject(fbResponse.error);
        }

        resolve(fbResponse);
      });
    }),
};

export default facebookApi;
