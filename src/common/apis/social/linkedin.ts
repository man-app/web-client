import { LinkedinInitParameters, LinkedinUserData } from './linkedin.types';

if (!process.env.BROWSER) {
  // @ts-ignore
  global.window = {};
}

const fields = ['id', 'first-name', 'last-name', 'email-address', 'picture-url', 'public-profile-url'];
const libraryUrl = '//platform.linkedin.com/in.js';

let hasBeenInit = false;

// @ts-ignore
window.onLinkedinLoad = () => {
  hasBeenInit = true;
};

/**
 * Collection of methods to interact with LinkedIn
 */
const linkedinApi = {
  /**
   * Initialize LinkedIn API
   */
  init: ({ appId, onLoad = 'onLinkedinLoad', authorize = true, locale = 'en_US' }: LinkedinInitParameters) =>
    new Promise(resolve => {
      const text = `api_key: ${appId}
      onLoad: ${onLoad}
      authorize: ${authorize}
      lang: ${locale}`;

      const script = document.createElement('script');
      script.type = 'text/javascript';
      script.src = libraryUrl;
      script.text = text;

      document.head.appendChild(script);

      const checkLoaded = () => {
        if (hasBeenInit) {
          resolve();
        } else {
          setTimeout(checkLoaded, 250);
        }
      };
      checkLoaded();
    }),

  /**
   * Check if user is logged into LinkedIn application,
   * if it is, get user data from LinkedIn
   */
  getStatus: (): Promise<boolean> =>
    new Promise(async resolve => {
      // @ts-ignore
      const isConnected = !!IN && (await IN.User.isAuthorized());
      resolve(isConnected);
    }),

  /**
   * Login LinkedIn user into LinkedIn application, so app can
   * get user data from LinkedIn
   */
  login: () =>
    new Promise(async resolve => {
      // @ts-ignore
      !!IN && (await IN.User.authorize());
      resolve();
    }),

  /**
   * Logout the LinkedIn user from LinkedIn application,
   * so app cannot get user data from LinkedIn
   */
  logout: () =>
    new Promise(async resolve => {
      // @ts-ignore
      !!IN && (await IN.User.logout());
      resolve();
    }),

  /**
   * Get user data from Linkedin
   */
  getUser: (): Promise<LinkedinUserData> =>
    new Promise((resolve, reject) => {
      // @ts-ignore
      if (!!IN) {
        // @ts-ignore
        IN.API.Profile('me')
          .fields(...fields)
          .result((response: any) => {
            const userData = response.values[0];
            delete userData['_key'];

            resolve(userData);
          })
          .error((error: any) => {
            reject(error);
          });
      } else {
        resolve();
      }
    }),
};

export default linkedinApi;
