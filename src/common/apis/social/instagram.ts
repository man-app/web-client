import manappApi from '../manapp';
import http from '../../../app/utils/http';

import { BASE_URL } from '../../constants/apiRoutes';
import { INSTAGRAM } from '../../constants/social-networks';
import { InstagramInitParams, BackendInstagramResponse } from './instagram.types';

if (!process.env.BROWSER) {
  // @ts-ignore
  global.location = {};
}

const instagramBaseUrl = 'https://api.instagram.com/oauth/';
const instagramLoginUrl = `${instagramBaseUrl}authorize/?client_id=:appId:&redirect_uri=:redirectionUrl:&response_type=code`;
const instagramRedirectUrl = `${location.protocol}${BASE_URL}`.replace('?endpoint=', '');
const instagramLogoutUrl = 'https://www.instagram.com/accounts/logout/';

let appId: string;

/**
 * Collection of methods to interact with Instagram
 */
const instagramApi = {
  /**
   * Initialize Instagram API
   */
  init: ({ appId: applicationId }: InstagramInitParams) =>
    new Promise(resolve => {
      appId = applicationId;
      resolve();
    }),

  /**
   * Check if user is logged into Instagram application,
   * if it is, get user data from Instagram
   */
  getStatus: (): Promise<BackendInstagramResponse> =>
    new Promise((resolve, reject) => {
      return manappApi.social
        .getPlatformData(INSTAGRAM)
        .then((response: any) => {
          // @ts-ignore
          const data: BackendInstagramResponse = JSON.parse(response);
          resolve(data);
        })
        .catch((error: any) => {
          reject(error);
        });
    }),

  /**
   * Login Instagram user into Instagram application, so app can
   * get user data from Instagram
   */
  login: () =>
    new Promise(resolve => {
      const url = instagramLoginUrl.replace(':appId:', appId).replace(':redirectionUrl:', instagramRedirectUrl);
      const authWindow = window.open(url);

      const checkWindow = () => {
        if (authWindow && authWindow.closed) {
          return resolve();
        }

        setTimeout(checkWindow, 100);
      };
      checkWindow();
    }),

  /**
   * Logout the Instagram user from Instagram application,
   * so app cannot get user data from Instagram
   */
  logout: () =>
    new Promise((resolve, reject) => {
      http({
        url: instagramLogoutUrl,
        withCredentials: false,
      })
        .then(() => {
          manappApi.social
            .removePlatformData(INSTAGRAM)
            .then(() => {
              resolve();
            })
            .catch(error => {
              reject(error);
            });
        })
        .catch(error => {
          reject(error);
        });
    }),

  /**
   * Get user data from Instagram
   */
  getUser: (): Promise<BackendInstagramResponse> =>
    new Promise((resolve, reject) => {
      return manappApi.social
        .getPlatformData(INSTAGRAM)
        .then((response: any) => {
          // @ts-ignore
          const data: BackendInstagramResponse = JSON.parse(response);
          resolve(data);
        })
        .catch((error: any) => {
          reject(error);
        });
    }),
};

export default instagramApi;
