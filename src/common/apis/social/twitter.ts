import http from '../../../app/utils/http';

const TWITTER_API = 'https://api.twitter.com';

/**
 * Collection of methods to interact with Twitter
 */
const twitterApi = {
  /**
   * Request application token, so we can operate
   */
  requestToken: () =>
    http({
      method: 'POST',
      url: `${TWITTER_API}/oauth/request_token`,
      payload: {
        // eslint-disable-next-line @typescript-eslint/camelcase
        oauth_callback: 'https://www.man-app.com',
      },
    }),

  /**
   * Authenticate oauthToken
   */
  authenticate: (oauthToken: string) =>
    new Promise(resolve => {
      // const twitterWindow =
      window.open(`${TWITTER_API}/oauth/authenticate?oauth_token=${oauthToken}`);

      // TODO: Need to detect window close event. Tip: Instagram-sdk has resolved this
      resolve();
    }),

  /**
   * Login the user into Twitter's app
   */
  login: (oauthToken: string, oauthVerifier: string) =>
    http({
      url: `${TWITTER_API}/?oauthToken=${oauthToken}&oauth_verifier=${oauthVerifier}`,
    }),
};

export default twitterApi;
