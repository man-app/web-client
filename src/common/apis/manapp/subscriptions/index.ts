import http from '../../../../app/utils/http';

import { SUBSCRIPTIONS } from '../../../constants/apiRoutes';

import { GetSubscriptionPlansResponse, ActivateSubscriptionParams, ActivateSubscriptionResponse } from './types';

/**
 * Get pricing plans
 */
export const getSubscriptionPlans = (): Promise<GetSubscriptionPlansResponse> =>
  http({
    url: SUBSCRIPTIONS,
  });

/**
 * Send payment data to backend in order to process the subscription activation
 */
export const activateSubscription = async (params: ActivateSubscriptionParams): Promise<ActivateSubscriptionResponse> =>
  await http({
    url: SUBSCRIPTIONS,
    method: 'post',
    payload: params,
  });
