import { NotificationFromServer } from '../../types';

interface SubscriptionFromServer {
  id: unknown;
  idCompany: unknown;
  idPaymentMethod: unknown;
  idSubscriptionPlan: unknown;
  created: unknown;
  lastModified: unknown;
}

interface SubscriptionPaymentFromServer {
  id: unknown;
  idSubscription: unknown;
  billed: unknown;
  created: unknown;
}

export interface SubscriptionPlanFromServer {
  id: unknown;
  name: unknown;
  icon?: unknown;
  price: unknown;
  users: unknown;
  discount?: unknown;
}

export interface GetSubscriptionPlansResponse {
  notifications?: NotificationFromServer[];
  subscriptionPlans: SubscriptionPlanFromServer[];
}

export interface ActivateSubscriptionParams {
  idCompany: string;
  idPaymentMethod: string;
  idSubscriptionPlan: string;
}

export interface ActivateSubscriptionResponse {
  notifications: NotificationFromServer[];
  payments: SubscriptionPaymentFromServer[];
  subscriptions: SubscriptionFromServer[];
}
