import http from '../../../../app/utils/http';

import { ROLES } from '../../../constants/apiRoutes';
import { GetRolesResponse, CreateRoleParams, CreateRoleResponse, UpdateRoleParams, UpdateRoleResponse } from './types';

/**
 * Get a list of roles for a company
 */
export const getRoles = async (idCompany: string): Promise<GetRolesResponse> =>
  await http({ url: ROLES.replace(':idCompany', idCompany) });

/**
 * Update role data
 */
export const createRole = async (idCompany: string, params: CreateRoleParams): Promise<CreateRoleResponse> =>
  await http({
    url: ROLES.replace(':idCompany', idCompany),
    method: 'post',
    payload: params,
  });

/**
 * Update role data
 */
export const updateRole = async (idCompany: string, params: UpdateRoleParams): Promise<UpdateRoleResponse> =>
  await http({
    url: ROLES.replace(':idCompany', idCompany),
    method: 'put',
    payload: params,
  });
