import { NotificationFromServer } from '../../types';

export interface RoleModuleFromServer {
  idModule: unknown;
  listing?: unknown;
  creating?: unknown;
  editing?: unknown;
  disabling?: unknown;
  approving?: unknown;
  deleting?: unknown;
}

export interface RoleFromServer {
  id: unknown;
  idCompany: unknown;
  name: unknown;
  modules: unknown[];
  description?: unknown;
  created: unknown;
  lastModified: unknown;
  isActive: unknown;
  isDeactivatable?: unknown;
}

export interface RoleModule {
  id: string;
  listing?: boolean;
  creating?: boolean;
  editing?: boolean;
  disabling?: boolean;
  approving?: boolean;
  deleting?: boolean;
}

export interface CreateRole {
  id?: string;
  idCompany: string;
  name: string;
  modules: RoleModule[];
  description?: string;
}

export interface GetRolesResponse {
  roles: RoleFromServer[];
}

export interface CreateRoleParams {
  roles: CreateRole[];
}

export interface CreateRoleResponse {
  roles: RoleFromServer[];
  notifications: NotificationFromServer[];
}

export interface UpdateRoleParams {
  roles: CreateRole[];
}

export interface UpdateRoleResponse {
  roles: RoleFromServer[];
  notifications: NotificationFromServer[];
}

export interface UpdateRoleStatusParams {
  roles: CreateRole[];
}

export interface UpdateRoleStatusResponse {
  roles: RoleFromServer[];
  notifications: NotificationFromServer[];
}
