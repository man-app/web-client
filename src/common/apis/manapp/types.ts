export * from './account/types';
export * from './activities/types';
export * from './auth/types';
export * from './companies/types';
export * from './contact/types';
export * from './feedback/types';
export * from './modules/types';
export * from './payment-methods/types';
export * from './roles/types';
export * from './social/types';
export * from './subscriptions/types';
export * from './uploader/types';

export interface NotificationFromServer {
  title: unknown;
  timestamp?: unknown;
  tag: unknown;
  body?: unknown;
  icon?: unknown;
  image?: unknown;
  badge?: unknown;
  vibrate?: unknown[];
  actions?: unknown[];
  renotify?: unknown;
  data?: any;
  type?: unknown;
  reason?: unknown;
  channel?: unknown;
  isActive?: unknown;
}
