import { Company } from '../../../../../app/modules/companies/types';
import { NotificationFromServer } from '../../types';
import { UserFromServer } from '../../auth/types';

export interface CompanyFromServer {
  id: unknown;
  slug: unknown;
  name: unknown;
  picture?: unknown;
  publicProfile?: unknown;
  users?: unknown;
  contactPhone?: unknown;
  contactEmail?: unknown;
  website?: unknown;
  linkedin?: unknown;
  facebook?: unknown;
  instagram?: unknown;
  twitter?: unknown;
  youtube?: unknown;
  joined?: unknown;
  modules?: unknown[];
  roles?: unknown[];
  created?: unknown;
  lastModified?: unknown;
}

export interface CompanyCreateParams {
  name: string;
}

export interface CompanyCreateResponse {
  companies: CompanyFromServer[];
  notifications: NotificationFromServer[];
  users: UserFromServer[];
}

export interface GetCompaniesResponse {
  companies: CompanyFromServer[];
}

export interface UpdateCompanyParams {
  company: Company;
}

export interface UpdateCompanyResponse {
  companies: CompanyFromServer[];
  notifications: NotificationFromServer[];
}
