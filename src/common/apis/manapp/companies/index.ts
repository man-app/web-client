import http from '../../../../app/utils/http';

import { COMPANIES, COMPANY } from '../../../constants/apiRoutes';
import {
  CompanyCreateParams,
  CompanyCreateResponse,
  GetCompaniesResponse,
  UpdateCompanyParams,
  UpdateCompanyResponse,
} from './types';

/**
 * Creates a new company
 */
export const create = async (params: CompanyCreateParams): Promise<CompanyCreateResponse> =>
  await http({
    url: COMPANIES,
    method: 'post',
    payload: params,
  });

/**
 * Get a list of all employing companies
 */
export const getCompanies = async (): Promise<GetCompaniesResponse> =>
  await http({
    url: COMPANIES,
  });

/**
 * Get company details
 */
export const getCompany = async (companySlug: string): Promise<GetCompaniesResponse> =>
  await http({
    url: COMPANY.replace(':slug', companySlug),
  });

/**
 * Update company data
 */
export const update = async (params: UpdateCompanyParams): Promise<UpdateCompanyResponse> =>
  await http({
    url: COMPANY.replace(':slug', params.company.slug),
    method: 'put',
    payload: params,
  });
