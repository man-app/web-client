import http from '../../../../app/utils/http';

import { UploadParams, UploadResponse } from './types';

/**
 * Upload a file to a custom url
 */
export const upload = async ({
  url,
  formData,
  onDownloadProgress,
  onUploadProgress,
}: UploadParams): Promise<UploadResponse> =>
  await http({
    url,
    method: 'post',
    payload: formData,
    onDownloadProgress,
    onUploadProgress,
  });
