export interface UploadParams {
  url: string;
  formData: FormData;
  onDownloadProgress?: (progressEvent: ProgressEvent) => void;
  onUploadProgress?: (progressEvent: ProgressEvent) => void;
}

export interface UploadResponse {
  picture: string;
  resource: string;
}
