import { SocialUser } from '../../../../../app/modules/social/types';

export interface SocialNetworkDataFromServer {
  idUser: number;
  platform: string;
  idPlatform: string;
  fullName: string;
  email: string;
  profileUrl: string;
  picture: string;
  isConnected: boolean;
}

export type GetPlatformDataResponse = SocialNetworkDataFromServer;

export interface SetPlatformStateParams extends SocialUser {
  platform: string;
  isConnected: boolean;
}

export interface ShowPlatformProfileParams {
  platform: string;
  enabled: boolean;
}
