import http from '../../../../app/utils/http';

import { SOCIAL } from '../../../constants/apiRoutes';
import { SocialNetworkDataFromServer, SetPlatformStateParams, ShowPlatformProfileParams } from './types';

/**
 * Get platform data stored on session (in case a platform needs server-side logic)
 */
export const getPlatformData = async (platform: string): Promise<SocialNetworkDataFromServer> =>
  await http({
    url: `${SOCIAL}/${platform}`,
    method: 'get',
  });

/**
 * Update 'connected' state of a platform
 */
export const setPlatformState = async (params: SetPlatformStateParams): Promise<undefined> =>
  await http({
    url: `${SOCIAL}/${params.platform}`,
    method: 'post',
    payload: params,
  });

/**
 * Update 'enabled' state of a platform
 */
export const showPlatformOnProfile = async (params: ShowPlatformProfileParams): Promise<undefined> =>
  await http({
    url: `${SOCIAL}/${params.platform}`,
    method: 'put',
    payload: params,
  });

/**
 * Removes platform data
 */
export const removePlatformData = async (platform: string): Promise<undefined> =>
  await http({
    url: `${SOCIAL}/${platform}`,
    method: 'delete',
  });
