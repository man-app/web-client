import http from '../../../../app/utils/http';

import { SESSIONS } from '../../../constants/apiRoutes';
import { UpdateInstallationDataParams, UpdateInstallationDataResponse } from './types';

export * from './login';
export * from './logout';
export * from './register';
export * from './reset-password';
export * from './validate-email';

/**
 * Updates installation data
 */
export const updateInstallationData = async (
  params: UpdateInstallationDataParams
): Promise<UpdateInstallationDataResponse> =>
  await http({
    url: SESSIONS,
    method: 'put',
    payload: params,
  });
