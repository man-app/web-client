import http from '../../../../app/utils/http';

import { LOGOUT } from '../../../constants/apiRoutes';

/**
 * Logs out the user from its current session
 */
export const logout = async (installationId?: string): Promise<undefined> =>
  await http({ url: `${LOGOUT}/${installationId}` });
