import http from '../../../../app/utils/http';

import { REGISTER } from '../../../constants/apiRoutes';
import { RegisterParams, RegisterResponse } from './types';

/**
 * Creates a new user for ManApp
 */
export const register = async (params: RegisterParams): Promise<RegisterResponse> =>
  await http({
    url: REGISTER,
    method: 'post',
    payload: params,
  });
