import http from '../../../../app/utils/http';

import { LOGIN } from '../../../constants/apiRoutes';
import { LoginParams, LoginResponse, RenewSessionResponse } from './types';

/**
 * Tries to use an existing session
 */
export const renewSession = async (params: string): Promise<RenewSessionResponse> =>
  await http({
    url: `${LOGIN}/${params}`,
    method: 'get',
  });

/**
 * Login the user in Manapp or in the specified platform
 */
export const login = async (params: LoginParams): Promise<LoginResponse> =>
  await http({
    url: LOGIN,
    method: 'post',
    payload: params,
  });
