import http from '../../../../app/utils/http';

import { VALIDATE_EMAIL } from '../../../constants/apiRoutes';
import {
  RequestValidationLinkParams,
  RequestValidationLinkResponse,
  ValidateEmailParams,
  ValidateEmailResponse,
} from './types';

/**
 * Ask for a new validate-email link
 */
export const requestValidationLink = async (
  params: RequestValidationLinkParams
): Promise<RequestValidationLinkResponse> =>
  await http({
    url: VALIDATE_EMAIL,
    method: 'post',
    payload: params,
  });

/**
 * When user clicks on a validate-email link, this function is
 * executed to mark email as validated
 */
export const validateEmail = async (params: ValidateEmailParams): Promise<ValidateEmailResponse> =>
  await http({
    url: VALIDATE_EMAIL,
    method: 'put',
    payload: params,
  });
