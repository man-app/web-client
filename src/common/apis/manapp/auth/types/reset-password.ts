import { NotificationFromServer } from '../../types';

export interface RequestResetPasswordLinkParams {
  email: string;
}

export interface RequestResetPasswordLinkResponse {
  notifications: NotificationFromServer[];
}

export interface ResetPasswordParams {
  email: string;
  code: string;
  password: string;
}

// TODO: Define this response
export type ResetPasswordResponse = void;
