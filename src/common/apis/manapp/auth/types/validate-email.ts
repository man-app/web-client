import { NotificationFromServer } from '../../types';
import { UserFromServer } from '.';

export interface RequestValidationLinkParams {
  email: string;
}

export interface RequestValidationLinkResponse {
  notifications: NotificationFromServer[];
}

export interface ValidateEmailParams {
  email: string;
  code: string;
}

export interface ValidateEmailResponse {
  notifications: NotificationFromServer[];
  users: UserFromServer[];
}
