import { UserFromServer } from '.';

export interface LoginParams {
  email: string;
  password: string;
  platform?: string;
}

export interface LoginResponse {
  users: UserFromServer[];
}

export interface RenewSessionResponse {
  users: UserFromServer[];
}
