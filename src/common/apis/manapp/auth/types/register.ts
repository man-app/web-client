import { LoginParams } from './login';
import { UserFromServer } from '.';
import { NotificationFromServer } from '../../types';

export interface RegisterParams extends LoginParams {
  fullName: string;
}

export interface RegisterResponse {
  notifications: NotificationFromServer[];
  users: UserFromServer[];
}
