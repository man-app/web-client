import { InstallationData } from '../../../../../app/utils/installation';

export * from './login';
export * from './register';
export * from './reset-password';
export * from './validate-email';

export interface NotificationPreferencesFromServer {
  idUser: unknown;
  idCompany: unknown;
  idModule: unknown;
  listing?: { [key: string]: unknown };
  creating?: { [key: string]: unknown };
  editing?: { [key: string]: unknown };
  approving?: { [key: string]: unknown };
  disabling?: { [key: string]: unknown };
  deleting?: { [key: string]: unknown };
}

export interface UserFromServer {
  id: unknown;
  fullName: unknown;
  email: unknown;
  picture?: unknown;
  document?: unknown;
  contactEmail?: unknown;
  contactPhone?: unknown;
  address?: unknown;
  city?: unknown;
  state?: unknown;
  country?: unknown;
  jobs?: unknown[];
  created?: unknown;
  lastModified?: unknown;
  linkedin?: unknown;
  facebook?: unknown;
  instagram?: unknown;
  twitter?: unknown;
  youtube?: unknown;
  notifications?: NotificationPreferencesFromServer[];
  isVerified?: unknown;
  newsletter?: unknown;
}

export type UpdateInstallationDataParams = InstallationData;

export type UpdateInstallationDataResponse = undefined;
