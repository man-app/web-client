import http from '../../../../app/utils/http';

import { RESET_PASSWORD } from '../../../constants/apiRoutes';
import {
  RequestResetPasswordLinkParams,
  RequestResetPasswordLinkResponse,
  ResetPasswordParams,
  ResetPasswordResponse,
} from './types';

/**
 * Ask for a new reset-password link
 */
export const requestResetPasswordLink = async (
  params: RequestResetPasswordLinkParams
): Promise<RequestResetPasswordLinkResponse> =>
  await http({
    url: RESET_PASSWORD.replace(':email?', params.email),
    method: 'get',
    payload: params,
  });

/**
 * When user clicks on a reset-password link and completes
 * the form, form data is send with this function
 */
export const resetPassword = async (params: ResetPasswordParams): Promise<ResetPasswordResponse> =>
  await http({
    url: RESET_PASSWORD.replace(':email?', ''),
    method: 'put',
    payload: params,
  });
