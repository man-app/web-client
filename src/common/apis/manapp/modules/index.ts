import http from '../../../../app/utils/http';

import { MODULES } from '../../../constants/apiRoutes';
import { GetModulesResponse } from './types';

/**
 * Get a list of all available modules in the platform
 */
export const getInstalledModules = async (): Promise<GetModulesResponse> =>
  await http({
    url: MODULES,
  });
