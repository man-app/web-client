export interface ModuleFromServer {
  id: unknown;
  name: unknown;
  description?: unknown;
  url: unknown;
  actions: {
    listing?: unknown;
    creating?: unknown;
    editing?: unknown;
    disabling?: unknown;
    approving?: unknown;
    deleting?: unknown;
  };
  isDeactivatable?: unknown;
}

export interface GetModulesResponse {
  modules: ModuleFromServer[];
}
