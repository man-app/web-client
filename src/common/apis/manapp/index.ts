import http from '../../../app/utils/http';
import { CHECK } from '../../constants/apiRoutes';

import * as account from './account';
import * as activities from './activities';
import * as auth from './auth';
import * as companies from './companies';
import * as contact from './contact';
import * as feedback from './feedback';
import * as modules from './modules';
import * as paymentMethods from './payment-methods';
import * as subscriptions from './subscriptions';
import * as roles from './roles';
import * as social from './social';
import * as uploader from './uploader';

export * from './types';

/**
 * This API is a collection of methods to communicate with Manapps backend, organized by modules
 */
const manappApi = {
  /**
   * Operations related with user account
   */
  account,

  /**
   * Operations related with activities
   */
  activities,

  /**
   * Operations related with auth
   */
  auth,

  /**
   * Operations related with companies
   */
  companies,

  /**
   * Operations related with contact
   */
  contact,

  /**
   * Operations related with feedback
   */
  feedback,

  /**
   * Operations related with modules
   */
  modules,

  /**
   * Operations related with payment methods
   */
  paymentMethods,

  /**
   * Operations related with roles
   */
  roles,

  /**
   * Operations related with social networks
   */
  social,

  /**
   * Operations related with subscriptions plans
   */
  subscriptions,

  /**
   * Operations related with file management
   */
  uploader,

  /**
   * Simple and empty 'get' operation to check
   * conectivity
   */
  check: (): Promise<void> =>
    http({
      url: CHECK,
    }),
};

export default manappApi;
