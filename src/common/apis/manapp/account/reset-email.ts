import http from '../../../../app/utils/http';

import { RESET_EMAIL } from '../../../constants/apiRoutes';
import { RequestResetEmailLinkResponse, ResetEmailParams, ResetEmailResponse } from './types';

/**
 * Request a change-email link
 */
export const requestResetEmailLink = async (): Promise<RequestResetEmailLinkResponse> =>
  await http({
    url: RESET_EMAIL,
  });

/**
 * Once the user got the change-email link and clicked on it,
 * this function will send updated email to server, so we can update our ddbb
 */
export const resetEmail = async (params: ResetEmailParams): Promise<ResetEmailResponse> =>
  await http({
    url: RESET_EMAIL,
    method: 'put',
    payload: params,
  });
