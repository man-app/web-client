import http from '../../../../app/utils/http';

import { ACCOUNT, SESSIONS } from '../../../constants/apiRoutes';
import {
  UpdateAccountParams,
  UpdateAccountResponse,
  UpdateNotificationsParams,
  UpdateNotificationsResponse,
  GetSessionsResponse,
} from './types';

export * from './reset-email';

/**
 * Update user data
 */
export const update = async (params: UpdateAccountParams): Promise<UpdateAccountResponse> =>
  await http({
    url: ACCOUNT,
    method: 'put',
    payload: params,
  });

/**
 * Update user's notifications settings
 */
export const updateNotifications = async (
  idCompany: number,
  params: UpdateNotificationsParams
): Promise<UpdateNotificationsResponse> =>
  await http({
    url: `${ACCOUNT}/notifications/${idCompany}`,
    method: 'put',
    payload: params,
  });

/**
 * Get user's open sessions
 */
export const getSessions = async (): Promise<GetSessionsResponse> =>
  await http({
    url: SESSIONS,
  });

/**
 * Close a particular session
 */
export const closeSession = async (idSession: string): Promise<void> =>
  await http({
    url: `${SESSIONS}/${idSession}`,
    method: 'delete',
  });

/**
 * Close all user's session
 */
export const closeAllSessions = async (): Promise<void> =>
  await http({
    url: SESSIONS,
    method: 'delete',
  });
