import { NotificationFromServer } from '../../types';

export type RequestResetEmailLinkResponse = undefined;

export interface ResetEmailParams {
  email: string;
  newEmail?: string;
  code: string;
}

export interface ResetEmailResponse {
  notifications: NotificationFromServer[];
}
