import { NotificationPreference, UserLogged } from '../../../../../app/modules/auth/types';
import { UserFromServer } from '../../auth/types';
import { NotificationFromServer } from '../../types';

export * from './reset-email';

export type UpdateNotificationsParams = {
  notifications: NotificationPreference[];
};

export interface UpdateNotificationsResponse {
  notifications: NotificationFromServer[];
  user: UserFromServer;
}

export type UpdateAccountParams = UserLogged;

export interface UpdateAccountResponse {
  notifications: NotificationFromServer[];
  users: UserFromServer[];
}

export interface SessionFromServer {
  id: unknown;
  idUser: unknown;
  installationId: unknown;
  userAgent?: unknown;
  created: unknown;
  lastModified: unknown;
  currentSession: unknown;
}

export interface GetSessionsResponse {
  notifications: NotificationFromServer[];
  sessions: SessionFromServer[];
}
