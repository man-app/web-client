import { NotificationFromServer } from '../../types';

export interface SendFeedbackParams {
  email: string;
  fullName: string;
  message: string;
}

export interface SendFeedbackResponse {
  notifications: NotificationFromServer[];
}
