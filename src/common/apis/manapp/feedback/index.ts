import http from '../../../../app/utils/http';

import { FEEDBACK } from '../../../constants/apiRoutes';
import { SendFeedbackParams, SendFeedbackResponse } from './types';

/**
 * Send some feedback to development team feedback@man-app.com
 */
export const send = async (params: SendFeedbackParams): Promise<SendFeedbackResponse> =>
  await http({
    url: FEEDBACK,
    method: 'post',
    payload: params,
  });
