import { NotificationFromServer } from '../../types';

export interface SendContactParams {
  email: string;
  fullName: string;
  message: string;
  subject: string;
}

export interface SendContactResponse {
  notifications: NotificationFromServer[];
}
