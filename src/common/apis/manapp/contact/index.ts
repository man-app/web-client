import http from '../../../../app/utils/http';

import { CONTACT } from '../../../constants/apiRoutes';
import { SendContactParams, SendContactResponse } from './types';

/**
 * Send a contact email to support team support@man-app.com
 */
export const send = async (params: SendContactParams): Promise<SendContactResponse> =>
  await http({
    url: CONTACT,
    method: 'post',
    payload: params,
  });
