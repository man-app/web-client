export interface ActivityFromServer {
  id: unknown;
  action: unknown;
  created: unknown;
  idModule: unknown;

  idCompany: unknown;
  companyName: unknown;

  idElement: unknown;
  elementName: unknown;

  idUser: unknown;
  userName: unknown;
  userPicture: unknown;
}

export interface GetActivitiesResponse {
  activities: ActivityFromServer[];
}
