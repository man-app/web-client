import http from '../../../../app/utils/http';

import { ACTIVITIES } from '../../../constants/apiRoutes';

import { GetActivitiesResponse } from './types';

/**
 * Get latest activities
 */
export const getActivities = async (laterThan?: number): Promise<GetActivitiesResponse> =>
  await http({
    url: ACTIVITIES.replace(':laterThan?', laterThan ? laterThan.toString() : ''),
  });
