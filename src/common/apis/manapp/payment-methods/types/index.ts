import { NotificationFromServer } from '../../types';

export interface PaymentMethodFromServer {
  id: unknown;
  idUser: unknown;
  cardNumber: unknown;
  ccv: unknown;
  description?: unknown;
  owner: unknown;
  validUntil: unknown;
  isDefault: unknown;
  created: unknown;
  lastModified: unknown;
}

export interface RequestPaymentMethodsResponse {
  notifications: NotificationFromServer[];
  paymentMethods: PaymentMethodFromServer[];
}

export interface CreatePaymentMethodParams {
  description?: string;
  cardNumber: string;
  owner: string;
  validUntil: string;
  ccv: string;
  isDefault?: boolean;
}

export interface CreatePaymentMethodResponse {
  notifications: NotificationFromServer[];
  paymentMethods: PaymentMethodFromServer[];
}

export interface UpdatePaymentMethodParams {
  id: string;
  description?: string;
  cardNumber: string;
  owner: string;
  validUntil: string;
  ccv: string;
  isDefault: boolean;
}

export interface UpdatePaymentMethodResponse {
  notifications: NotificationFromServer[];
  paymentMethods: PaymentMethodFromServer[];
}

export interface DeletePaymentMethodResponse {
  notifications: NotificationFromServer[];
}
