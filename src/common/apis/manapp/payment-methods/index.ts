import http from '../../../../app/utils/http';

import { PAYMENT_METHODS } from '../../../constants/apiRoutes';
import {
  RequestPaymentMethodsResponse,
  CreatePaymentMethodParams,
  CreatePaymentMethodResponse,
  UpdatePaymentMethodParams,
  UpdatePaymentMethodResponse,
  DeletePaymentMethodResponse,
} from './types';

export const requestPaymentMethods = async (): Promise<RequestPaymentMethodsResponse> =>
  await http({
    url: PAYMENT_METHODS.replace(':id?', ''),
  });

export const createPaymentMethod = async (params: CreatePaymentMethodParams): Promise<CreatePaymentMethodResponse> =>
  await http({
    url: PAYMENT_METHODS.replace(':id?', ''),
    method: 'post',
    payload: params,
  });

export const updatePaymentMethod = async ({
  id,
  ...params
}: UpdatePaymentMethodParams): Promise<UpdatePaymentMethodResponse> =>
  await http({
    url: PAYMENT_METHODS.replace(':id?', id),
    method: 'put',
    payload: params,
  });

export const deletePaymentMethod = async (id: string): Promise<DeletePaymentMethodResponse> =>
  await http({
    url: PAYMENT_METHODS.replace(':id?', id),
    method: 'delete',
  });
