// eslint-disable-next-line no-restricted-imports
import { BRAND_COLOR } from './styles/do-not-export/colors';

export const APP_NAME = 'Manapp';
export const APP_DESC = 'Your management application';

export const APP_COLOR = BRAND_COLOR;

export const APP_DOMAIN = 'man-app.com';

export const APP_WEB = `https://www.${APP_DOMAIN}`;
export const APP_EMAIL_INFO = `info@${APP_DOMAIN}`;
export const APP_EMAIL_FEEDBACK = `feedback@${APP_DOMAIN}`;
export const APP_PHONE = '+34 671 18 37 14';
export const APP_REPOSITORY = 'https://gitlab.com/man-app/web-client';
export const APP_CHANGELOG = `${APP_REPOSITORY}/blob/master/CHANGELOG.md#release-v:version`;
export const APP_PATREON = 'https://www.patreon.com/bePatron?c=1587037';

export const APP_DEVELOPER = 'ImperdibleSoft';
export const APP_DEVELOPER_WEBSITE = 'https://www.imperdiblesoft.com';

// Google Analytics
export const ANALYTICS_TAG = 'UA-41472956-24';

// SENTRY
export const SENTRY_URL = 'https://9a6162a704db4ac99f8d1fbb7e5503aa@sentry.io/1235030';
