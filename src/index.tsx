import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';

import AppInit from './app';
import AppWrapper from './app/wrapper';
import ScrollTop from './app/components/routes/scroll-top';

const ReactApp: React.FC = () => (
  <AppInit>
    {({ isNewRelease, lastRoute, store }) => (
      <Provider store={store}>
        <BrowserRouter>
          <ScrollTop>
            <AppWrapper lastRoute={lastRoute} isNewRelease={isNewRelease} />
          </ScrollTop>
        </BrowserRouter>
      </Provider>
    )}
  </AppInit>
);

export default ReactApp;
