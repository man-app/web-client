const path = require('path');
const { app, BrowserWindow } = require('electron');

let win;
const iconPath = path.join(__dirname, '/src/images/logo.png');

function initWindow() {
  const aspectRatio = 16/9;
  const width = 1280;

  win = new BrowserWindow({
    'min-width': width,
    'min-height': width/aspectRatio,
    width: width,
    height: width/aspectRatio,
    icon: iconPath,
  });

  // Open new window
  win.loadURL(`file://${__dirname}/index.html`);

  // Removes menu bar
  win.setMenu(null);

  // Enables dev tools
  // win.webContents.openDevTools();

  // On close, unset win variable
  win.on('closed', () => {
    win = undefined;
  });
}

// If all windows are closed, finish the process (Mac OS Cmd+Q emulator)
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

// When activate application, open a new window
app.on('activate', () => {
  if (!win) {
    createWindow()
  }
});

app.on('ready', initWindow);
